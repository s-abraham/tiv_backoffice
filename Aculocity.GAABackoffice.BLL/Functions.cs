﻿using System;
using System.Globalization;

namespace Aculocity.GAABackOffice.BLL
{
    public class Functions
    {
        #region ' Custom Methods '
        /// <summary>
        /// Checks whether or not a date is a valid date.
        /// </summary>
        /// <param name="sdate"></param>
        /// <returns></returns>
        public static bool IsDate(string sdate)
        {
            bool isDate = true;
            try
            {
                DateTime.Parse(sdate, CultureInfo.InvariantCulture);
            }
            catch
            {
                isDate = false;
            }
            return isDate;
        } 
        #endregion
    }
}