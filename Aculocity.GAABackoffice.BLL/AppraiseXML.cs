﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.Global;

namespace GAABackOffice.BLL
{
    public class AppraiseXML
    {
        public static string GetXML(int aprID, string dealerKey)
        {
            var sb = new StringBuilder();
            try
            {
                var dt = AppraiseDAL.GetAppraiseDateCreatedByAprID(aprID);
                if (dt.Rows.Count > 0)
                {
                    var x = new XmlTextWriter(new EncodedStringWriter(sb, Encoding.UTF8)) { Formatting = Formatting.Indented };

                    x.WriteRaw("<?xml version=\"1.0\"?>");
                    x.WriteWhitespace(@"   ");
                    x.WriteRaw("<?adf version=\"1.0\"?>");
                    x.WriteStartElement("adf");
                    x.WriteStartElement("prospect");

                    x.WriteStartElement("id");
                    x.WriteAttributeString("sequence", "1");
                    x.WriteAttributeString("source", "GetAutoAppraise2");
                    x.WriteValue(aprID);
                    x.WriteEndElement();

                    x.WriteStartElement("requestdate");
                    x.WriteValue(dt.Rows[0][0]);
                    x.WriteEndElement();

                    WriteXMLTradeWanted(aprID, ref x);
                    WriteXMLTradeIn(aprID, dealerKey, ref x);
                    WriteXMLCustomer(aprID, ref x);
                    WriteXMLVendor(ref x);
                    WriteXMLProvider(ref x);

                    x.WriteEndElement();
                    //prospect
                    x.WriteEndElement();
                    //adf
                    x.Flush();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return sb.ToString();
        }

        protected static void WriteXMLTradeWanted(int aprID, ref XmlTextWriter x)
        {
            var tradeWanted = new TradeWantedBLL(aprID);
            var vehicle = new VehicleEntity();

            x.WriteStartElement("vehicle");
            x.WriteAttributeString("interest", "buy");

            switch (tradeWanted.New)
            {
                case "new":
                    vehicle = new NewVehicleBLL(tradeWanted.Style, tradeWanted.Model);
                    break;
                case "used":
                    vehicle = new UsedVehicleBLL(tradeWanted.Style, tradeWanted.Model);
                    break;
                default:
                    x.WriteAttributeString("status", "new");
                    x.WriteStartElement("year");
                    x.WriteValue("2007");
                    x.WriteEndElement();
                    x.WriteStartElement("make");
                    x.WriteValue("N/A");
                    x.WriteEndElement();
                    x.WriteStartElement("model");
                    x.WriteValue("N/A");
                    x.WriteEndElement();
                    x.WriteEndElement();
                    break;
            }

            if (tradeWanted.New == "used" || tradeWanted.New == "new")
            {
                x.WriteAttributeString("status", tradeWanted.New);
                try
                {
                    if (tradeWanted.VIN == string.Empty)
                    {
                        x.WriteStartElement("year");
                        x.WriteValue(vehicle.Year);
                        x.WriteEndElement();
                        x.WriteStartElement("make");
                        x.WriteValue(vehicle.Make);
                        x.WriteEndElement();
                        x.WriteStartElement("model");
                        x.WriteValue(vehicle.Model);
                        x.WriteEndElement();
                        x.WriteStartElement("trim");
                        x.WriteValue(vehicle.Trim);
                        x.WriteEndElement();

                        x.WriteStartElement("colorcombination");
                        x.WriteStartElement("interiorcolor");
                        x.WriteValue("No Preference");
                        x.WriteEndElement();
                        x.WriteStartElement("exteriorcolor");
                        x.WriteValue(tradeWanted.ManufacturerColor);
                        x.WriteEndElement();
                        x.WriteStartElement("preference");
                        x.WriteValue("1");
                        x.WriteEndElement();
                        x.WriteEndElement();
                    }
                    else
                    {
                        x.WriteStartElement("year");
                        x.WriteValue(tradeWanted.ModelYear);
                        x.WriteEndElement();
                        x.WriteStartElement("make");
                        x.WriteValue(tradeWanted.Make);
                        x.WriteEndElement();
                        x.WriteStartElement("model");
                        x.WriteValue(tradeWanted.Model);
                        x.WriteEndElement();
                        x.WriteStartElement("VIN");
                        x.WriteValue(tradeWanted.VIN);
                        x.WriteEndElement();
                        x.WriteStartElement("Miles");
                        x.WriteValue(tradeWanted.Miles);
                        x.WriteEndElement();
                        x.WriteStartElement("Dealer");
                        x.WriteValue(tradeWanted.DealerID);
                        x.WriteEndElement();

                        x.WriteStartElement("interiorcolor");
                        x.WriteValue("No Preference");
                        x.WriteEndElement();
                        x.WriteStartElement("exteriorcolor");
                        x.WriteValue(tradeWanted.ManufacturerColor);
                        x.WriteEndElement();
                        x.WriteStartElement("preference");
                        x.WriteValue("1");
                        x.WriteEndElement();
                        x.WriteEndElement();
                    }
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                }
            }
        }

        protected static void WriteXMLTradeIn(int aprID, string dealerKey, ref XmlTextWriter x)
        {
            x.WriteStartElement("vehicle");
            x.WriteAttributeString("interest", "trade-in");
            x.WriteAttributeString("status", "used");

            var tradeIn = new TradeInBLL(aprID);
            try
            {
                x.WriteStartElement("year");
                x.WriteValue(tradeIn.Year);
                x.WriteEndElement();
                x.WriteStartElement("make");
                x.WriteValue(tradeIn.Manufacturer);
                x.WriteEndElement();
                x.WriteStartElement("model");
                x.WriteValue(tradeIn.Model);
                x.WriteEndElement();
                x.WriteStartElement("trim");
                x.WriteValue(string.Format("{0} {1}",tradeIn.Body, tradeIn.Engine));
                x.WriteEndElement();
                x.WriteStartElement("odometer");
                x.WriteValue(tradeIn.Mileage);
                x.WriteEndElement();
                x.WriteStartElement("condition");
                x.WriteValue(tradeIn.Condition);
                x.WriteEndElement();
                x.WriteStartElement("colorcombination");
                x.WriteStartElement("interiorcolor");
                x.WriteValue("No Preference");
                x.WriteEndElement();
                x.WriteStartElement("exteriorcolor");
                x.WriteValue("Not Sure");
                x.WriteEndElement();
                x.WriteStartElement("MonthlyPayment");
                x.WriteValue(tradeIn.MonthlyPayment);
                x.WriteEndElement();
                x.WriteStartElement("SettlementValue");
                x.WriteValue(tradeIn.SettlementValue);
                x.WriteEndElement();
                x.WriteStartElement("preference");
                x.WriteValue("1");
                x.WriteEndElement();
                x.WriteEndElement();
                
                //colorcombination

                WriteXMLTradeInOptions(aprID, ref x);
                x.WriteStartElement("comments");
                x.WriteCData(string.Format("BaseValueRange {0} TotalValueRange {1} {2}", tradeIn.BaseValueRange, tradeIn.TotalValueRange, tradeIn.Language));

                var imagesID = VehicleDAL.GetImagesID(aprID, dealerKey);
                for (int i = 0; i < imagesID.Length; i++)
                {
                    x.WriteStartElement("TradeInImage");
                    x.WriteValue(string.Format("/TradeImage.aspx?AprID={0}&DealerKey={1}&ImgID={2}&width=500&height=450 ", SettingManager.AppURL, aprID, dealerKey));
                    x.WriteEndElement();
                }
                x.WriteEndElement();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            

            //vehicle
            x.WriteEndElement();
        }

        protected static void WriteXMLTradeInOptions(int aprId, ref XmlTextWriter x)
        {
            try
            {
                var optionalsID = VehicleDAL.GetOptionalsID(aprId);

                for (int i = 0; i < optionalsID.Length; i++)
                {
                    x.WriteStartElement("option");
                    x.WriteStartElement("optionname");
                    x.WriteValue(optionalsID[i]);
                    x.WriteEndElement();
                    //optionname
                    x.WriteEndElement();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            
        }

        protected static void WriteXMLCustomer(int aprID, ref XmlTextWriter x)
        {
            try
            {
                var aprContact = new AprContactBLL(aprID);
                if(aprContact.AprID != 0)
                {
                    x.WriteStartElement("customer");
                    x.WriteStartElement("contact");
                    x.WriteStartElement("name");
                    x.WriteAttributeString("part", "first");
                    x.WriteValue(aprContact.FirstName);
                    x.WriteEndElement();
                    x.WriteStartElement("name");
                    x.WriteAttributeString("part", "last");
                    x.WriteValue(aprContact.LastName);
                    x.WriteEndElement();
                    x.WriteStartElement("email");
                    x.WriteValue(aprContact.Email);
                    x.WriteEndElement();
                    x.WriteStartElement("phone");
                    x.WriteAttributeString("type", "voice");
                    x.WriteValue(aprContact.Phone);
                    x.WriteEndElement();
                    x.WriteStartElement("address");
                    x.WriteStartElement("street");
                    x.WriteAttributeString("line", "1");
                    x.WriteValue(aprContact.Address);
                    x.WriteEndElement();
                    x.WriteStartElement("city");
                    x.WriteEndElement();
                    x.WriteStartElement("regioncode");
                    x.WriteEndElement();
                    x.WriteStartElement("country");
                    x.WriteEndElement();
                    x.WriteStartElement("postalcode");
                    x.WriteValue(aprContact.ZipCode);
                    x.WriteEndElement();
                    x.WriteEndElement();
                    //address
                    x.WriteEndElement();
                    //contact
                    x.WriteStartElement("timeframe");
                    x.WriteStartElement("description");
                    x.WriteValue(aprContact.BuyWithin);
                    x.WriteEndElement();
                    x.WriteEndElement();
                    x.WriteStartElement("comments");
                    x.WriteCData(aprContact.Comments);
                    x.WriteEndElement();
                    //customer
                    x.WriteEndElement();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        protected static void WriteXMLVendor(ref XmlTextWriter x)
        {
            x.WriteStartElement("vendor");
            x.WriteStartElement("id");
            x.WriteAttributeString("sequence", "1");
            x.WriteAttributeString("source", SettingManager.AppName);
            x.WriteEndElement();
            x.WriteStartElement("vendorname");
            x.WriteEndElement();
            x.WriteStartElement("contact");
            x.WriteAttributeString("primarycontact", "1");
            x.WriteStartElement("name");
            x.WriteAttributeString("part", "full");
            x.WriteEndElement();
            x.WriteStartElement("phone");
            x.WriteAttributeString("type", "voice");
            x.WriteAttributeString("time", "day");
            x.WriteStartElement("address");
            x.WriteStartElement("street");
            x.WriteAttributeString("line", "1");
            x.WriteEndElement();
            x.WriteStartElement("city");
            x.WriteEndElement();
            x.WriteStartElement("regioncode");
            x.WriteEndElement();
            x.WriteStartElement("postalcode");
            x.WriteEndElement();
            x.WriteStartElement("country");
            x.WriteEndElement();
            x.WriteEndElement();
            x.WriteEndElement();
            x.WriteEndElement();
            //vendor
            x.WriteEndElement();
        }

        protected static void WriteXMLProvider(ref XmlTextWriter x)
        {
            x.WriteStartElement("provider");
            x.WriteStartElement("name");
            x.WriteAttributeString("part", "full");
            x.WriteValue(SettingManager.AppName);
            x.WriteEndElement();
            x.WriteStartElement("service");
            x.WriteEndElement();
            x.WriteStartElement("url");
            x.WriteEndElement();
            x.WriteStartElement("email");
            x.WriteEndElement();
            x.WriteStartElement("phone");
            x.WriteEndElement();
            x.WriteStartElement("contact");
            x.WriteAttributeString("primarycontact", "1");
            x.WriteStartElement("name");
            x.WriteAttributeString("part", "full");
            x.WriteEndElement();
            x.WriteStartElement("email");
            x.WriteEndElement();
            x.WriteStartElement("phone");
            x.WriteAttributeString("type", "voice");
            x.WriteAttributeString("time", "day");
            x.WriteEndElement();
            x.WriteStartElement("address");
            x.WriteStartElement("street");
            x.WriteAttributeString("line", "1");
            x.WriteEndElement();
            x.WriteStartElement("city");
            x.WriteEndElement();
            x.WriteStartElement("regioncode");
            x.WriteEndElement();
            x.WriteStartElement("postalcode");
            x.WriteEndElement();
            x.WriteStartElement("country");
            x.WriteEndElement();
            x.WriteEndElement();
            //address
            x.WriteEndElement();
            //contact
            //provider
            x.WriteEndElement();
        }
    }
    ///<summary>Implements a TextWriter for writing information to a string. The information is stored in an underlying StringBuilder.</summary>
    public class EncodedStringWriter : StringWriter
    {
        //Private property setter
        private readonly Encoding _encoding = Encoding.UTF8;

        ///<summary>Default constructor for the EncodedStringWriter class.</summary>
        public EncodedStringWriter(StringBuilder sb, Encoding encoding)
            : base(sb)
        {
            _encoding = encoding;
        }
        ///<summary>Gets the Encoding in which the output is written.</summary>
        public override Encoding Encoding
        {
            get { return _encoding; }
        }
    }
}
