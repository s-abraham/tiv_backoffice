﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class UserBLL : UserEntity
    {
        #region Constructors

        public UserBLL()
        {

        }

        #endregion

        #region Properties

        private bool _loadSuccessful;
        public bool LoadSuccessful
        {
            get { return _loadSuccessful; }
            set { _loadSuccessful = value; }
        }

        private Enumerations.AuthUserGroup _group = Enumerations.AuthUserGroup.None;
        public Enumerations.AuthUserGroup AuthGroup
        {
            get { return _group; }
            set { _group = value; }
        }

        private int _groupID = 0;
        public int GroupID
        {
            get { return _groupID; }
            set { _groupID = value; }
        }

        private Enumerations.GroupDealerAdmin _subGroupDealerAdmin = Enumerations.GroupDealerAdmin.None;
        public Enumerations.GroupDealerAdmin SubGroupDealerAdmin
        {
            get { return _subGroupDealerAdmin; }
            set { _subGroupDealerAdmin = value; }
        }

        private int _subGroupDealerAdminID = 0;
        public int SubGroupDealerAdminID
        {
            get { return _subGroupDealerAdminID; }
            set { _subGroupDealerAdminID = value; }
        }

        private string _dealerName = string.Empty;
        public string DealerName
        {
            get { return _dealerName; }
            set { _dealerName = value; }
        }
        private string _ipAddress = string.Empty;
        public string UserIPAddress
        {
            get { return _ipAddress; }
            set { _ipAddress = value; }
        }
        #endregion

        #region ' Custom Methods '

        /// <summary>
        /// Returns a user by user's name
        /// </summary>
        /// <param name="username"></param>
        public void GetUserByUserName(string username)
        {
            DataTable dtUser = null;
            DataTable dtAuthGroup = null;
            DataTable dtSubGroup = null;
            try
            {
                dtUser = UserDAL.GetUserByUserName(username);
                if (dtUser != null && dtUser.Rows.Count > 0)
                {
                    //populate user:
                    this.PopulateFromDataRow(dtUser.Rows[0]);
                    this._loadSuccessful = true;

                    //set groups for user:
                    dtAuthGroup = UserDAL.GetGroupIDByUserID(this.UserID);
                    if (dtAuthGroup != null && dtAuthGroup.Rows.Count > 0)
                    {
                        this._group = (Enumerations.AuthUserGroup)dtAuthGroup.Rows[0][0];
                    }

                    //set group dealer admin type if neccessory
                    if (this._group == Enumerations.AuthUserGroup.GroupDealerAdmin)
                    {
                        dtSubGroup = UserDAL.GetSubGroupDealerTypeIDByUserID(this.UserID);
                        if (dtSubGroup != null && dtSubGroup.Rows.Count > 0)
                        {
                            this._subGroupDealerAdmin = (Enumerations.GroupDealerAdmin)dtSubGroup.Rows[0][0];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this._loadSuccessful = false;
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtUser != null) { dtUser.Dispose(); }
                if (dtAuthGroup != null) { dtAuthGroup.Dispose(); }
                if (dtSubGroup != null) { dtSubGroup.Dispose(); }
            }
        }
        public void GetUserByUserIP(string userIP)
        {
            DataTable dtUser = null;
            try
            {
                dtUser = UserDAL.GetUserBySavedUserIP(userIP);
                if (dtUser != null && dtUser.Rows.Count > 0)
                {
                    //populate user:
                    this.PopulateFromDataRow(dtUser.Rows[0]);
                    this._loadSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                this._loadSuccessful = false;
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtUser != null) { dtUser.Dispose(); }
            }

        }
        #endregion

        #region ' Additional Methods '
        public int InsertLoggedUserIPData()
        {
            int result = 0;
            try
            {
                result = (int)((DataTable)UserDAL.InsertLoggedUserIPData(UserIPAddress, UserID)).Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result;
        }
        public int DeleteLoggedUserIPData()
        {
            int result = 1;
            try
            {
                result = (int)((DataTable)UserDAL.DeleteLoggedUserIPData(UserIPAddress)).Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result;
        }
        #endregion

        #region Instance's Methods
        public int Update()
        {
            int result = 0;
            try
            {
                result = (int)((DataTable)UserDAL.UpdateUser(UserID, UserName, Password, DealerKey, GroupID, SubGroupDealerAdminID, Active)).Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result;
        }
        public int Insert()
        {
            int result = 0;
            try
            {
                result = (int)((DataTable)UserDAL.InsertUser(UserName, Password, DealerKey, GroupID, SubGroupDealerAdminID)).Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result;
        }
        public int Delete()
        {
            int result = 1;
            try
            {
                result = (int)((DataTable)UserDAL.DeleteUser(UserID)).Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result;
        }

        #endregion

        #region ' Compare '
        public virtual int CompareTo(UserBLL user2, Enumerations.UsersComparisonType comparisonType)
        {
            switch (comparisonType)
            {
                case Enumerations.UsersComparisonType.UserName: return UserName.CompareTo(user2.UserName);
                case Enumerations.UsersComparisonType.Password: return Password.CompareTo(user2.Password);
                case Enumerations.UsersComparisonType.Active: return Active.CompareTo(user2.Active);
                default: return DealerName.CompareTo(user2.DealerName);
            }
        }
        #endregion
    }
}