﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class DealerHeaderTextBLL: DealerHeaderTextEntity
    {
        #region ' Constructors '
        public DealerHeaderTextBLL() { }
        #endregion

        #region Instance's Methods
        public void Update()
        {
            try
            {
                DealerHeaderTextDAL.UpdateDealerHeader(DealerKey, PageNo, TextLanguage, HeaderText, Active);       
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region Instance's Methods
        public static bool IsAllowedCompanyDealer(int userID)
        {
            bool result = false;
            var dt = DealerHeaderTextDAL.GetAllowedCompanyDealers();
            foreach(DataRow row in dt.Rows)
            {
                if(Convert.ToInt32(row[0]) == userID) result = true;
            }
            return result;
        }

        #endregion

        public virtual int CompareTo(DealerHeaderTextBLL dealerHeaderText2, Enumerations.DealerHeaderTextComparisonType comparisonType)
        {
            switch (comparisonType)
            {
                case Enumerations.DealerHeaderTextComparisonType.PageNo: return PageNo.CompareTo(dealerHeaderText2.PageNo);
                case Enumerations.DealerHeaderTextComparisonType.TextLanguage: return TextLanguage.CompareTo(dealerHeaderText2.TextLanguage);
                case Enumerations.DealerHeaderTextComparisonType.Active: return Active.CompareTo(dealerHeaderText2.Active);
                default: return PageNo.CompareTo(dealerHeaderText2.PageNo);
            }
        }
    }
}
