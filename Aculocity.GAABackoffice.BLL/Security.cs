﻿using System;
using System.Web;
using System.Web.Security;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class Security
    {
        public static UserBLL PerformLogin(string username, string password)
        {
            try
            {
                var user = new UserBLL();
                user.GetUserByUserName(username);
                if(user.LoadSuccessful && user.Password == password)
                {
                    return user;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, GetLoggedUserID());
            }
            return null;
        }

        public static void PerformLogout()
        {
            try
            {
                FormsAuthentication.SignOut();
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, GetLoggedUserID());}
        }
        public static UserBLL GetSavedLoggedUser(string userIP)
        {
            try
            {
                var user = new UserBLL();
                user.GetUserByUserIP(userIP);
                if (user.LoadSuccessful)
                {
                    return user;
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, GetLoggedUserID());
            }
            return null;
        }
        public static HttpCookie CreateAuthCookie(string username, string customUserData)
        {
            var cookie = new HttpCookie(string.Empty);
            try
            {
                FormsAuthentication.Initialize();

                //create a new ticket used for authentication
                var authTicket = new FormsAuthenticationTicket(1, username, DateTime.Now,
                                                               DateTime.Now.AddMinutes(SettingManager.LoginTimeout),
                                                               false, customUserData);
                string ticketHash = FormsAuthentication.Encrypt(authTicket);
                cookie = new HttpCookie(FormsAuthentication.FormsCookieName, ticketHash);
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, GetLoggedUserID());}
            return cookie;
        }

        private static string RetrieveAuthCookieUserData(string valueToRetrieve)
        {
            try
            {
                var formsID = (FormsIdentity)HttpContext.Current.User.Identity;
                FormsAuthenticationTicket authTicket = formsID.Ticket;
                string[] userData = authTicket.UserData.Split(";".ToCharArray());

                for (int x = 0; x < userData.Length; x++)
                {
                    string userDataValue = userData[x];
                    if (userDataValue.Substring(0, valueToRetrieve.Length) == valueToRetrieve)
                    {
                        return userDataValue.Split(":".ToCharArray())[1];
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, GetLoggedUserID()); }
            return null;
        }

        public static string GetLoggedInUser()
        {
            try
            {
                return HttpContext.Current.User.Identity.Name != string.Empty ? HttpContext.Current.User.Identity.Name : "Guest";
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, GetLoggedUserID());
                return "Guest";
            }
        }

        public static bool UserIsInGroup(Enumerations.AuthUserGroup groupName)
        {
            try
            {
                if (HttpContext.Current.User != null &&
                    HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.User.Identity is FormsIdentity)
                {
                    string groupData = RetrieveAuthCookieUserData("Group");
                    if (groupData != null)
                    {
                        if (groupData == groupName.ToString())
                        {
                            return true;
                        }
                    }
                }
                return false; 
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, GetLoggedUserID());
                return false;
            }
            
        }
        
        public static bool UserIsInSubGroupDealerAdmin(Enumerations.GroupDealerAdmin subGroupName)
        {
            try
            {
                if (HttpContext.Current.User != null &&
                    HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.User.Identity is FormsIdentity)
                {
                    string subGroupData = RetrieveAuthCookieUserData("SubGroup");
                    if (subGroupData != null)
                    {
                        if (subGroupData == subGroupName.ToString())
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, GetLoggedUserID());
                return false;
            }
            return false;
        }
        
        public static int GetLoggedUserID()
        {
            try
            {
                if (HttpContext.Current.User != null &&
                    HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.User.Identity is FormsIdentity)
                {
                    string userData = RetrieveAuthCookieUserData("UserID");
                    if (userData != null) return Convert.ToInt32(userData);
                }
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, GetLoggedUserID());}
            return 0;
        }

        public static Enumerations.AuthUserGroup GetGroupLoggedUser()
        {
            try
            {
                if (HttpContext.Current.User != null &&
                    HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.User.Identity is FormsIdentity)
                {
                    string groupData = RetrieveAuthCookieUserData("Group");
                    if (groupData != null)
                    {
                        return (Enumerations.AuthUserGroup) Enum.Parse(typeof (Enumerations.AuthUserGroup), groupData);
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, GetLoggedUserID()); }
            return Enumerations.AuthUserGroup.None;
        }

        public static Enumerations.GroupDealerAdmin GetSubGroupDealerAdminLoggedUser()
        {
            try
            {
                if (HttpContext.Current.User != null &&
                    HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.User.Identity is FormsIdentity)
                {
                    string groupData = RetrieveAuthCookieUserData("SubGroup");
                    if (groupData != null)
                    {
                        return (Enumerations.GroupDealerAdmin)Enum.Parse(typeof(Enumerations.GroupDealerAdmin), groupData);
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, GetLoggedUserID()); }
            return Enumerations.GroupDealerAdmin.None;
        }

        public static Enumerations.AuthUserGroup GetCurrentUserGroup() {

            Enumerations.AuthUserGroup result = Enumerations.AuthUserGroup.None;

            string groupName = RetrieveAuthCookieUserData("Group");

            if (string.IsNullOrEmpty(groupName))
                return result;

            return (Enumerations.AuthUserGroup)Enum.Parse(typeof(Enumerations.AuthUserGroup), groupName);

        }
        public static int GetCurrentUserDealerGroupID()
        {
            return DealerGroupBLL.GetDealerGroupIDByUserID(GetLoggedUserID());
        }

        public static Enumerations.GroupDealerAdmin GetDealerTypeLoggedUser(Enumerations.AuthUserGroup userGroup)
        {
            try
            {
                if (HttpContext.Current.User != null &&
                    HttpContext.Current.User.Identity.IsAuthenticated &&
                    HttpContext.Current.User.Identity is FormsIdentity)
                {
                    switch (userGroup)
                    {
                        case Enumerations.AuthUserGroup.HomeNetSuperUser:
                            return Enumerations.GroupDealerAdmin.HomeNet;
                        case Enumerations.AuthUserGroup.HomeNetDealerAdmin:
                            return Enumerations.GroupDealerAdmin.HomeNet;
                        case Enumerations.AuthUserGroup.GalvesFullAdmin:
                            return Enumerations.GroupDealerAdmin.Galves;
                        case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                            return Enumerations.GroupDealerAdmin.Galves;
                        case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                            return Enumerations.GroupDealerAdmin.BSB;
                        case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                            return Enumerations.GroupDealerAdmin.BSB;
                        case Enumerations.AuthUserGroup.AllDealerAdmin:
                            return Enumerations.GroupDealerAdmin.Admin;
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, GetLoggedUserID()); }
            return Enumerations.GroupDealerAdmin.None;
        }

    }
}