﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class TradeWantedBLL: TradeWantedEntity
    {
        #region Constructors
        public TradeWantedBLL() { }
        public TradeWantedBLL(int aprID)
        {
            GetTradeWantedByAprID(aprID);
        }
        #endregion

        #region ' Custom Methods '
        public void GetTradeWantedByAprID(int aprID)
        {
            DataTable dtTradeWanted = null;
            try
            {
                dtTradeWanted = TradeDAL.GetTradeWantedByAprID(aprID);
                if (dtTradeWanted != null && dtTradeWanted.Rows.Count > 0)
                {
                    //populate trade:
                    PopulateFromDataRow(dtTradeWanted.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtTradeWanted != null) { dtTradeWanted.Dispose(); }
            }

        }
        #endregion
    }

    public class TradeInBLL : TradeInEntity
    {
        #region Constructors
        public TradeInBLL() { }
        public TradeInBLL(int aprID)
        {
            GetTradeInByAprID(aprID);
        }
        #endregion

        #region ' Custom Methods '
        public void GetTradeInByAprID(int aprID)
        {
            DataTable dt = null;
            try
            {
                dt = TradeDAL.GetTradeInByAprID(aprID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //populate trade:
                    PopulateFromDataRow(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) { dt.Dispose(); }
            }

        }
        #endregion
    }
}