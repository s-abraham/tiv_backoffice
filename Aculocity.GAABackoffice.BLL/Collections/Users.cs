﻿using System;
using System.Collections;
using System.Data;
using Aculocity.GAABackOffice.BLL.Comparers;
using Aculocity.GAABackOffice.Global;
using Aculocity.GAABackOffice.DAL;


namespace Aculocity.GAABackOffice.BLL.Collections
{
    public class Users: CollectionBase
    {
        #region ' Constructors '
        public Users() { }
        #endregion

        #region ' Properties '
        public UserBLL this[int index]
        {
            get { return (UserBLL)List[index]; }
            set { List[index] = value; }
        }
        #endregion

        #region ' Collection Methods '
        public int Add(UserBLL user)
        {
            if (user == null) throw new ArgumentNullException("user");
            return List.Add(user);
        }
        public int IndexOf(UserBLL value)
        {
            return InnerList.IndexOf(value);
        }
        public void Insert(int index, UserBLL value)
        {
            InnerList.Insert(index, value);
        }
        public void Remove(UserBLL value)
        {
            InnerList.Remove(value);
        }
        public bool Contains(UserBLL value)
        {
            return InnerList.Contains(value);
        }
        
        /// <summary>
        /// Sorts the collection using the specified comparer
        /// </summary>
        public virtual void Sort()
        {
            base.InnerList.Sort(new UserComparer());
        }

        /// <summary>
        /// Sorts the collection using the specified comparer
        /// </summary>
        /// <param name="comparer">Comparer to use for sorting</param>
        public virtual void Sort(IComparer comparer)
        {
            base.InnerList.Sort(comparer);
        }
        #endregion

        #region ' Lookup Methods '
        public void GetUsersForSpecialGroup(Enumerations.AuthUserGroup group, string searchBy, string searchValue)
        {
            InnerList.Clear();
            DataTable dtUsers = null;
            try
            {
                dtUsers = UserDAL.GetUsersByAuthUserGroup(group.ToString(), searchBy, searchValue);
                if (dtUsers != null && dtUsers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtUsers.Rows.Count; i++)
                    {
                        var user = new UserBLL();
                        user.PopulateFromDataRow(dtUsers.Rows[i]);
                        //user.DealerName = DealerBLL.GetDealerName(user.DealerKey);
                        user.DealerName = dtUsers.Rows[i]["DealerName"].ToString();
                        user.GroupID = Convert.ToInt32(dtUsers.Rows[i]["GroupID"].ToString());
                        user.SubGroupDealerAdminID = Convert.ToInt32(dtUsers.Rows[i]["DealerGroupID"].ToString());
                        InnerList.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtUsers != null) dtUsers.Dispose();
            }
        }
        /// <summary>
        /// Returns users by dealer key
        /// </summary>
        /// <param name="dealerKey"></param>
        public void GetUsersByDealerKey(string dealerKey)
        {
            InnerList.Clear();
            DataTable dtUsers = null;
            try
            {
                dtUsers = UserDAL.GetUsersByDealerKey(dealerKey);
                if (dtUsers != null && dtUsers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtUsers.Rows.Count; i++)
                    {
                        var user = new UserBLL();
                        user.PopulateFromDataRow(dtUsers.Rows[i]);
                        InnerList.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtUsers != null) dtUsers.Dispose();
            }
        }
        #endregion


    }
}