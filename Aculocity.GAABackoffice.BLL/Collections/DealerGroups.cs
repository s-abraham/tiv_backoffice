﻿using System;
using System.Collections;
using System.Data;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL.Collections
{
    public class DealerGroups: CollectionBase
    {
        #region ' Constructors '
        public DealerGroups() { }
        #endregion

        #region ' Properties '
        public DealerGroupBLL this[int index]
        {
            get { return (DealerGroupBLL)List[index]; }
            set { List[index] = value; }
        }
        #endregion 

        #region ' Collection Methods '
        public int Add(DealerGroupBLL dealerGroup)
        {
            if (dealerGroup == null) throw new ArgumentNullException("dealerGroup");
            return List.Add(dealerGroup);
        }
        public int IndexOf(DealerGroupBLL value)
        {
            return InnerList.IndexOf(value);
        }
        public void Insert(int index, DealerGroupBLL value)
        {
            InnerList.Insert(index, value);
        }
        public void Remove(DealerGroupBLL value)
        {
            InnerList.Remove(value);
        }
        public bool Contains(DealerGroupBLL value)
        {
            return InnerList.Contains(value);
        }

        /// <summary>
        /// Sorts the collection using the specified comparer
        /// </summary>
        /// <param name="comparer">Comparer to use for sorting</param>
        public virtual void Sort(IComparer comparer)
        {
            base.InnerList.Sort(comparer);
        }
        #endregion

        #region ' Lookup Methods '
        /// <summary>
        /// returns all dealer groups
        /// </summary>
        public void GetAllDealerGroups()
        {
            InnerList.Clear();
            DataTable dtDealerGroups = null;

            try
            {
                dtDealerGroups = DealerGroupDAL.GetAllDealerGroups();
                if(dtDealerGroups != null && dtDealerGroups.Rows.Count > 0)
                {
                    for(int i = 0; i<dtDealerGroups.Rows.Count; i++)
                    {
                        var dealerGroup = new DealerGroupBLL();
                        dealerGroup.PupulateFromDataRow(dtDealerGroups.Rows[i]);
                        dealerGroup.DealerTypeName = DealerGroupBLL.GetDealerGrouptypeNameByDealerTypeID(dealerGroup.DealerTypeID.ToString());
                        InnerList.Add(dealerGroup);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealerGroups != null) dtDealerGroups.Dispose();
            }
        }
        public void GetDealerGroupsByDealerTypeID(int dealerTypeID)
        {
            InnerList.Clear();
            DataTable dtDealerGroups = null;

            try
            {
                dtDealerGroups = DealerGroupDAL.GetDealerGroupsByDealerType(dealerTypeID);
                if (dtDealerGroups != null && dtDealerGroups.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDealerGroups.Rows.Count; i++)
                    {
                        var dealerGroup = new DealerGroupBLL();
                        dealerGroup.PupulateFromDataRow(dtDealerGroups.Rows[i]);
                        dealerGroup.DealerTypeName = DealerGroupBLL.GetDealerGrouptypeNameByDealerTypeID(dealerGroup.DealerTypeID.ToString());
                        InnerList.Add(dealerGroup);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealerGroups != null) dtDealerGroups.Dispose();
            }
        }
        public void GetDealerGroupsByDealerAdmin(string authUserGroup)
        {
            InnerList.Clear();
            DataTable dtDealerGroups = null;

            try
            {
                dtDealerGroups = DealerGroupDAL.GetDealerGroupsByDealerAdmin(authUserGroup);
                if (dtDealerGroups != null && dtDealerGroups.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDealerGroups.Rows.Count; i++)
                    {
                        var dealerGroup = new DealerGroupBLL();
                        dealerGroup.PupulateFromDataRow(dtDealerGroups.Rows[i]);
                        dealerGroup.DealerTypeName = DealerGroupBLL.GetDealerGrouptypeNameByDealerTypeID(dealerGroup.DealerTypeID.ToString());
                        InnerList.Add(dealerGroup);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealerGroups != null) dtDealerGroups.Dispose();
            }
        }

        #endregion
    }
}