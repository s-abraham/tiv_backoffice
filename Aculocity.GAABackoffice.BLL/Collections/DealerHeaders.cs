﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL.Collections
{
    public class DealerHeaders : CollectionBase
    {
        #region ' Constructors '
        public DealerHeaders() { }
        #endregion

        #region ' Properties '
        public DealerHeaderTextBLL this[int index]
        {
            get { return (DealerHeaderTextBLL)List[index]; }
            set { List[index] = value; }
        }
        public ArrayList Items
        {
            get { return InnerList; }
        }
        #endregion 

        #region ' Collection Methods '
        public int Add(DealerHeaderTextBLL dealerHeaderText)
        {
            if (dealerHeaderText == null) throw new ArgumentNullException("dealerHeaderText");
            return List.Add(dealerHeaderText);
        }
        public int IndexOf(DealerHeaderTextBLL value)
        {
            return InnerList.IndexOf(value);
        }
        public void Insert(int index, DealerHeaderTextBLL value)
        {
            InnerList.Insert(index, value);
        }
        public void Remove(DealerHeaderTextBLL value)
        {
            InnerList.Remove(value);
        }
        public bool Contains(DealerHeaderTextBLL value)
        {
            return InnerList.Contains(value);
        }
        /// <summary>
        /// Sorts the collection using the specified comparer
        /// </summary>
        /// <param name="comparer">Comparer to use for sorting</param>
        public virtual void Sort(IComparer comparer)
        {
            base.InnerList.Sort(comparer);
        }
        #endregion

        #region ' Lookup Methods '
        /// <summary>
        /// returns all dealer groups
        /// </summary>
        public void GetAllDealerHeaders(string dealerKey)
        {
            InnerList.Clear();
            DataTable dt = null;

            try
            {
                dt = DealerHeaderTextDAL.GetAllDealerHeaders(dealerKey);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var dealerHeader = new DealerHeaderTextBLL();
                        dealerHeader.PopulateFromDataRow(dt.Rows[i]);
                        InnerList.Add(dealerHeader);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) dt.Dispose();
            }
        }
        #endregion
    }
}
