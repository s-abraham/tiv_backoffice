﻿using System;
using System.Collections;
using System.Data;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;
using GAABackOffice.BLL;

namespace Aculocity.GAABackOffice.BLL.Collections
{
    public class Groups: CollectionBase
    {
        #region ' Constructors '
        public Groups() { }
        #endregion

        #region ' Properties '
        public GroupBLL this[int index]
        {
            get { return (GroupBLL)List[index]; }
            set { List[index] = value; }
        }
        #endregion

        #region ' Collection Methods '
        public int Add(GroupBLL group)
        {
            if (group == null) throw new ArgumentNullException("group");
            return List.Add(group);
        }
        public int IndexOf(GroupBLL value)
        {
            return InnerList.IndexOf(value);
        }
        public void Insert(int index, GroupBLL value)
        {
            InnerList.Insert(index, value);
        }
        public void Remove(GroupBLL value)
        {
            InnerList.Remove(value);
        }
        public bool Contains(GroupBLL value)
        {
            return InnerList.Contains(value);
        }

        /// <summary>
        /// Sorts the collection using the specified comparer
        /// </summary>
        /// <param name="comparer">Comparer to use for sorting</param>
        public virtual void Sort(IComparer comparer)
        {
            base.InnerList.Sort(comparer);
        }
        #endregion

        #region ' Lookup Methods '
        public void GetGroupsForSpecialGroup(Enumerations.AuthUserGroup userGroup)
        {
            InnerList.Clear();
            DataTable dtGroups = null;
            try
            {
                dtGroups = GroupDAL.GetGroupsByAuthUserGroup(userGroup.ToString());
                if (dtGroups != null && dtGroups.Rows.Count > 0)
                {
                    for (int i = 0; i < dtGroups.Rows.Count; i++)
                    {
                        var group = new GroupBLL();
                        group.PopulateFromDataRow(dtGroups.Rows[i]);
                        InnerList.Add(group);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtGroups != null) dtGroups.Dispose();
            }
        }
        #endregion

    }
}