﻿using System;
using System.Collections;
using System.Data;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL.Collections
{
    public class Dealers: CollectionBase
    {
        #region ' Constructors '
        public Dealers(){}
        #endregion

        #region ' Properties '
        public DealerBLL this[int index]
        {
            get { return (DealerBLL) List[index]; }
            set { List[index] = value; }
        }
        #endregion 

        #region ' Collection Methods '
        public int Add(DealerBLL dealer)
        {
            if (dealer == null) throw new ArgumentNullException("dealer");
            return List.Add(dealer);
        }
        public int IndexOf(DealerBLL value)
        {
            return InnerList.IndexOf(value);
        }
        public void Insert(int index, DealerBLL value)
        {
            InnerList.Insert(index, value);
        }
        public void Remove(DealerBLL value)
        {
            InnerList.Remove(value);
        }
        public bool Contains(DealerBLL value)
        {
            return InnerList.Contains(value);
        }

        /// <summary>
        /// Sorts the collection using the specified comparer
        /// </summary>
        /// <param name="comparer">Comparer to use for sorting</param>
        public virtual void Sort(IComparer comparer)
        {
            base.InnerList.Sort(comparer);
        }
        #endregion

        #region ' Lookup Methods '
        /// <summary>
        /// returns all non video dealers
        /// </summary>
        public void GetNonVideoDealers()
        {
            InnerList.Clear();
            DataTable dtDealers = null;
            try
            {
                dtDealers = DealerDAL.GetNonVideoDealers((int)Security.GetGroupLoggedUser());
                if(dtDealers != null && dtDealers.Rows.Count > 0)
                {
                    for(int i=0; i<dtDealers.Rows.Count; i++)
                    {
                        var dealer = new DealerBLL();
                        dealer.PopulateFromDataRowAsDealerList(dtDealers.Rows[i]);
                        InnerList.Add(dealer);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealers != null) dtDealers.Dispose();
            }
        }

        /// <summary>
        /// Returns all allowed video dealers
        /// </summary>
        public void GetAllowedVideoDealers()
        {
            InnerList.Clear();
            DataTable dtDealers = null;
            try
            {
                dtDealers = DealerDAL.GetAllowedVideoDealers((int)Security.GetGroupLoggedUser());
                if (dtDealers != null && dtDealers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDealers.Rows.Count; i++)
                    {
                        var dealer = new DealerBLL();
                        dealer.PopulateFromDataRowAsDealerList(dtDealers.Rows[i]);
                        InnerList.Add(dealer);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealers != null) dtDealers.Dispose();
            }
        }
        /// <summary>
        /// Returns Dealers for Admin Account
        /// </summary>
        public void GetDealersForSpecialGroup(Enumerations.AuthUserGroup userGroup)
        {
            InnerList.Clear();
            DataTable dtDealers = null;
            try
            {
                dtDealers = DealerDAL.GetDealersListByAuthUserGroup(userGroup.ToString());
                if (dtDealers != null && dtDealers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDealers.Rows.Count; i++)
                    {
                        var dealer = new DealerBLL();
                        dealer.PopulateFromDataRowAsDealerList(dtDealers.Rows[i]);
                        InnerList.Add(dealer);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealers != null) dtDealers.Dispose();
            }
        }
        /// <summary>
        /// Returns Dealers by DealerType
        /// </summary>
        public void GetDealersByDealerType(int dealerType)
        {
            InnerList.Clear();
            DataTable dtDealers = null;
            try
            {
                dtDealers = DealerDAL.GetDealersListByDealerType(dealerType.ToString());
                if (dtDealers != null && dtDealers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDealers.Rows.Count; i++)
                    {
                        var dealer = new DealerBLL();
                        dealer.PopulateFromDataRowAsDealerList(dtDealers.Rows[i]);
                        InnerList.Add(dealer);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealers != null) dtDealers.Dispose();
            }
        }
        /// <summary>
        /// Returns Dealers by user ID for group dealer admins
        /// </summary>
        public void GetDealersByUserID(int userID)
        {
            InnerList.Clear();
            DataTable dtDealers = null;
            try
            {
                dtDealers = DealerDAL.GetDealersListByUserID(userID.ToString());
                if (dtDealers != null && dtDealers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDealers.Rows.Count; i++)
                    {
                        var dealer = new DealerBLL();
                        dealer.PopulateFromDataRowAsDealerList(dtDealers.Rows[i]);
                        InnerList.Add(dealer);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtDealers != null) dtDealers.Dispose();
            }
        }
        /// <summary>
        /// Returns all selected dealers by dealer group ID
        /// </summary>
        public void GetAllSelectedDealers(int dealerGroupID)
        {
            InnerList.Clear();
            DataTable dt = null;
            try
            {
                dt = DealerDAL.GetAllSelectedDealers(dealerGroupID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var dealer = new DealerBLL();
                        dealer.PopulateFromDataRowAsDealerList(dt.Rows[i]);
                        InnerList.Add(dealer);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) dt.Dispose();
            }
        }

        /// <summary>
        /// Returns all free dealers by dealer group ID
        /// </summary>
        public void GetAllFreeDealers(int dealerGroupID)
        {
            InnerList.Clear();
            DataTable dt = null;
            try
            {
                dt = DealerDAL.GetAllFreeDealers(dealerGroupID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var dealer = new DealerBLL();
                        dealer.PopulateFromDataRowAsDealerList(dt.Rows[i]);
                        InnerList.Add(dealer);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) dt.Dispose();
            }
        }
        #endregion

        #region ' Manage Methods '
        #endregion
    }
}