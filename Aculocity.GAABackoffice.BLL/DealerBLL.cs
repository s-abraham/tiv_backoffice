﻿using System;
using System.Collections.Generic;
using System.Data;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class DealerBLL: DealerEntity 
    {
        #region ' Constructors '

        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Set dealer as allowed video 
        /// </summary>
        /// <param name="dealerKey"></param>
        public static void AllowVideo(string dealerKey)
        {
            try
            {
                DealerDAL.AllowVideo(dealerKey);
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        public static void DeleteDraftDealer(Int32 DealerID)
        {
            try
            {
                DealerDAL.DeleteDraftDealer(DealerID);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public static void AddLeadProvider(String Provider)
        {
            try
            {
                DealerDAL.AddLeadProvider(Provider);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public static Int32 CheckLeadProviderUsage(String Provider)
        {
            Int32 Count = 1;
            try
            {
                Count = DealerDAL.CheckLeadProviderUsage(Provider);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return Count;
        }

        public static void DeleteLeadProvider(String Provider)
        {
            try
            {
                DealerDAL.DeleteLeadProvider(Provider);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        /// <summary>
        /// Set dealer as non video
        /// </summary>
        /// <param name="dealerKey"></param>
        public static void DenyVideo(string dealerKey)
        {
            try
            {
                DealerDAL.DenyVideo(dealerKey);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public static string GetDealerName(string dealerKey)
        {
            DataTable dtDealer;
            try
            {
                dtDealer = DealerDAL.GetDealerNameByPrimaryKey(dealerKey);
                if (dtDealer != null && dtDealer.Rows.Count > 0)
                {
                    return dtDealer.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return "None";
        }

        public int GetDealerGroupByID(int Groupid)
        {
            int GroupType = 0;
            DataTable Group = new DataTable();

            try
            {
                Group = DealerDAL.GetDealerGroupByID(Groupid);
                GroupType = Convert.ToInt32(Group.Rows[0][2].ToString());
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return GroupType;
        }

        public static void AuditDealerChanges(DealerHolder UpdatedDealer, DealerHolder OrigninalDealer)
        {
            String AuditString = "";

            if (!UpdatedDealer.DealerName.Equals(OrigninalDealer.DealerName)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Name</div> updated from " + OrigninalDealer.DealerName + " to " + UpdatedDealer.DealerName + ";"; }
            if (!UpdatedDealer.Salesperson.Equals(OrigninalDealer.Salesperson)) { AuditString = AuditString + "<div style='font-weight: bold'>Sales Person</div> updated From " + OrigninalDealer.Salesperson + "  to " + UpdatedDealer.Salesperson + ";"; }
            if (!UpdatedDealer.OriginalSalesPerson.Equals(OrigninalDealer.OriginalSalesPerson)) { AuditString = AuditString + "<div style='font-weight: bold'>Original Sales Person</div> updated from " + OrigninalDealer.OriginalSalesPerson + " to " + UpdatedDealer.OriginalSalesPerson + ";"; }
            if (!UpdatedDealer.GroupID.Equals(OrigninalDealer.GroupID)) { AuditString = AuditString + "<div style='font-weight: bold'>Group ID</div> updated from " + OrigninalDealer.GroupID + " to " + UpdatedDealer.GroupID + ";"; }
            if (!UpdatedDealer.DealerStreet.Equals(OrigninalDealer.DealerStreet)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Street</div> updated from " + OrigninalDealer.DealerStreet + " to " + UpdatedDealer.DealerStreet + ";"; }
            if (!UpdatedDealer.DealerCity.Equals(OrigninalDealer.DealerCity)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer City</div> updated from " + OrigninalDealer.DealerCity + " to " + UpdatedDealer.DealerCity + ";"; }
            if (!UpdatedDealer.DealerState.Equals(OrigninalDealer.DealerState)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer State</div> updated from " + OrigninalDealer.DealerState + " to " + UpdatedDealer.DealerState + ";"; }
            if (!UpdatedDealer.DealerZip.Equals(OrigninalDealer.DealerZip)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Zip</div> updated from " + OrigninalDealer.DealerZip + " to " + UpdatedDealer.DealerZip + ";"; }
            if (!UpdatedDealer.DealerAutoGroup.Equals(OrigninalDealer.DealerAutoGroup)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer AutoGroup</div> updated from" + OrigninalDealer.DealerAutoGroup + " to " + UpdatedDealer.DealerAutoGroup + ";"; }
            if (!UpdatedDealer.DealerContactEmail.Equals(OrigninalDealer.DealerContactEmail)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Contact Email</div> updated from " + OrigninalDealer.DealerContactEmail + " to " + UpdatedDealer.DealerContactEmail + ";"; }
            if (!UpdatedDealer.DealerContactName.Equals(OrigninalDealer.DealerContactName)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Contact Name</div> updated from" + OrigninalDealer.DealerContactName + " to " + UpdatedDealer.DealerContactName + ";"; }
            if (!UpdatedDealer.DealerContactPhone.Equals(OrigninalDealer.DealerContactPhone)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Contact Phone</div> updated from" + OrigninalDealer.DealerContactPhone + " to " + UpdatedDealer.DealerContactPhone + ";"; }
            if (!UpdatedDealer.AppointmentEmail.Equals(OrigninalDealer.AppointmentEmail)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Appointment Email</div> updated from " + OrigninalDealer.AppointmentEmail + " to " + UpdatedDealer.AppointmentEmail + ";"; }
            if (!UpdatedDealer.DealerContactPhone2.Equals(OrigninalDealer.DealerContactPhone2)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Contact Phone 2</div> updated from" + OrigninalDealer.DealerContactPhone2 + " to " + UpdatedDealer.DealerContactPhone2 + ";"; }
            if (!UpdatedDealer.DealerContactFax.Equals(OrigninalDealer.DealerContactFax)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Contact Fax</div> updated from" + OrigninalDealer.DealerContactFax + " to " + UpdatedDealer.DealerContactFax + ";"; }
            if (!UpdatedDealer.DealerTitle.Equals(OrigninalDealer.DealerTitle)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Contact Title</div> updated from" + OrigninalDealer.DealerTitle + " to " + UpdatedDealer.DealerTitle + ";"; }
            if (!UpdatedDealer.SecondDealerContactName.Equals(OrigninalDealer.SecondDealerContactName)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Second Contact Name</div> updated from " + OrigninalDealer.SecondDealerContactName + " to " + UpdatedDealer.SecondDealerContactName + ";"; }
            if (!UpdatedDealer.SecondDealerContactEmail.Equals(OrigninalDealer.SecondDealerContactEmail)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Second Contact Email</div> updated from" + OrigninalDealer.SecondDealerContactEmail + " to " + UpdatedDealer.SecondDealerContactEmail + ";"; }
            if (!UpdatedDealer.SecondDealerContactPhone.Equals(OrigninalDealer.SecondDealerContactPhone)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Second Contact Phone</div> updated from" + OrigninalDealer.SecondDealerContactPhone + " to " + UpdatedDealer.SecondDealerContactPhone + ";"; }
            if (!UpdatedDealer.SecondDealerContactPhone2.Equals(OrigninalDealer.SecondDealerContactPhone2)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Second Contact Phone 2</div> updated from " + OrigninalDealer.SecondDealerContactPhone2 + " to " + UpdatedDealer.SecondDealerContactPhone2 + ";"; }
            if (!UpdatedDealer.SecondDealerContactFax.Equals(OrigninalDealer.SecondDealerContactFax)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Second Contact Fax</div> updated from " + UpdatedDealer.SecondDealerContactFax + " to " + UpdatedDealer.SecondDealerContactFax + ";"; }
            if (!UpdatedDealer.SecondDealerTitle.Equals(OrigninalDealer.SecondDealerTitle)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer Second Dealer Title</div> updated from " + OrigninalDealer.SecondDealerTitle + " to " + UpdatedDealer.SecondDealerTitle + ";"; }
            if (!UpdatedDealer.DealerWebSite.Equals(OrigninalDealer.DealerWebSite)) { AuditString = AuditString + "<div style='font-weight: bold'>Dealer website</div> updated from " + OrigninalDealer.DealerWebSite + " to " + UpdatedDealer.DealerWebSite + ";"; }
            if (!UpdatedDealer.CreditApp.Equals(OrigninalDealer.CreditApp)) { AuditString = AuditString + "<div style='font-weight: bold'>Credit App URL</div> updated from " + OrigninalDealer.CreditApp + " to " + UpdatedDealer.CreditApp + ";"; }
            if (!UpdatedDealer.CreditEnabled.Equals(OrigninalDealer.CreditEnabled)) { AuditString = AuditString + "<div style='font-weight: bold'>Credit Enabled</div> updated from " + OrigninalDealer.CreditEnabled + " to " + UpdatedDealer.CreditEnabled + ";"; }
            if (!UpdatedDealer.CreditMsg.Equals(OrigninalDealer.CreditMsg)) { AuditString = AuditString + "<div style='font-weight: bold'>Credit Messag</div>e updated from " + OrigninalDealer.CreditMsg + " to " + UpdatedDealer.CreditMsg + ";"; }
            if (!UpdatedDealer.InvUsed.Equals(OrigninalDealer.InvUsed)) { AuditString = AuditString + "<div style='font-weight: bold'>Inv Used UR</div>L updated from " + OrigninalDealer.InvUsed + " to " + UpdatedDealer.InvUsed + ";"; }
            if (!UpdatedDealer.InvUsedEnabled.Equals(OrigninalDealer.InvUsedEnabled)) { AuditString = AuditString + "<div style='font-weight: bold'>Inv Used Enabled</div> updated from " + OrigninalDealer.InvUsedEnabled + " to " + UpdatedDealer.InvUsedEnabled + ";"; }
            if (!UpdatedDealer.InvNew.Equals(OrigninalDealer.InvNew)) { AuditString = AuditString + "<div style='font-weight: bold'>Inv New URL</div> updated from " + OrigninalDealer.InvNew + " to " + UpdatedDealer.InvNew + ";"; }
            if (!UpdatedDealer.InvNewEnabled.Equals(OrigninalDealer.InvNewEnabled)) { AuditString = AuditString + "<div style='font-weight: bold'>Inv New Enabled</div> updated from " + OrigninalDealer.InvNewEnabled + " to " + UpdatedDealer.InvNewEnabled + ";"; }
            //if (!UpdatedDealer.DealerPublicSite.Equals(OrigninalDealer.DealerPublicSite)) { AuditString = AuditString + "Dealer Public site updated from " + OrigninalDealer.DealerPublicSite + " to " + UpdatedDealer.DealerPublicSite + ";"; }
            if (!UpdatedDealer.InstantOptionPricing.Equals(OrigninalDealer.InstantOptionPricing)) { AuditString = AuditString + "<div style='font-weight: bold'>Instant option pricing</div> updated from " + OrigninalDealer.InstantOptionPricing + " to " + UpdatedDealer.InstantOptionPricing + ";"; }
            if (!UpdatedDealer.EmailAppraisal.Equals(OrigninalDealer.EmailAppraisal)) { AuditString = AuditString + "<div style='font-weight: bold'>Email Appraisal</div> updated from " + OrigninalDealer.EmailAppraisal + " to " + UpdatedDealer.EmailAppraisal + ";"; };
            if (!UpdatedDealer.FourStep.Equals(OrigninalDealer.FourStep)) { AuditString = AuditString + "<div style='font-weight: bold'>FourStep</div> updated from " + OrigninalDealer.FourStep + " to " + UpdatedDealer.FourStep + ";"; }
            if (!UpdatedDealer.NewSelection.Equals(OrigninalDealer.NewSelection)) { AuditString = AuditString + "<div style='font-weight: bold'>New vehicle Selectio</div>n updated from " + OrigninalDealer.NewSelection + " to " + UpdatedDealer.NewSelection + ";"; }
            if (!UpdatedDealer.ShowAmountOwed.Equals(OrigninalDealer.ShowAmountOwed)) { AuditString = AuditString + "<div style='font-weight: bold'>Show amount owed</div> updated from " + OrigninalDealer.ShowAmountOwed + " to " + UpdatedDealer.ShowAmountOwed + ";"; }
            if (!UpdatedDealer.ShowIncentives.Equals(OrigninalDealer.ShowIncentives)) { AuditString = AuditString + "<div style='font-weight: bold'>Show incentives</div> updated from " + OrigninalDealer.ShowIncentives + " to " + UpdatedDealer.ShowIncentives + ";"; }
            if (!UpdatedDealer.ShowMonthlyPayment.Equals(OrigninalDealer.ShowMonthlyPayment)) { AuditString = AuditString + "<div style='font-weight: bold'>Show monthly payment</div> updated from " + OrigninalDealer.ShowMonthlyPayment + " to " + UpdatedDealer.ShowMonthlyPayment + ";"; }
            if (!UpdatedDealer.UnsureOption.Equals(OrigninalDealer.UnsureOption)) { AuditString = AuditString + "<div style='font-weight: bold'>Unsure option</div> updated from " + OrigninalDealer.UnsureOption + " to " + UpdatedDealer.UnsureOption + ";"; }
            if (!UpdatedDealer.RetailPricing.Equals(OrigninalDealer.RetailPricing)) { AuditString = AuditString + "<div style='font-weight: bold'>Retail pricing</div> updated from " + OrigninalDealer.RetailPricing + " to " + UpdatedDealer.RetailPricing + ";"; }
            if (!UpdatedDealer.TradeImageUpload.Equals(OrigninalDealer.TradeImageUpload)) { AuditString = AuditString + "<div style='font-weight: bold'>Trade image upload</div> updated from " + OrigninalDealer.TradeImageUpload + " to " + UpdatedDealer.TradeImageUpload + ";"; }
            if (!UpdatedDealer.TradeVideoUpload.Equals(OrigninalDealer.TradeVideoUpload)) { AuditString = AuditString + "<div style='font-weight: bold'>Trade video upload</div> updated from " + OrigninalDealer.TradeVideoUpload + " to " + UpdatedDealer.TradeVideoUpload + ";"; }
            if (!UpdatedDealer.GoogleUrchin.Equals(OrigninalDealer.GoogleUrchin)) { AuditString = AuditString + "<div style='font-weight: bold'>Google Urchin</div> updated from " + OrigninalDealer.GoogleUrchin + " to " + UpdatedDealer.GoogleUrchin + ";"; }
            if (!UpdatedDealer.AppraisalExpiration.Equals(OrigninalDealer.AppraisalExpiration)) { AuditString = AuditString + "<div style='font-weight: bold'>Appraisal Expiration</div> updated from " + OrigninalDealer.AppraisalExpiration + " to " + UpdatedDealer.AppraisalExpiration + ";"; }
            //if (!UpdatedDealer.Comments.Equals(OrigninalDealer.Comments)) { AuditString = AuditString + "Comments updated from " + OrigninalDealer.Comments + " to " + UpdatedDealer.Comments + ";"; }
            if (!UpdatedDealer.GalvesRangeCategory.Equals(OrigninalDealer.GalvesRangeCategory)) { AuditString = AuditString + "<div style='font-weight: bold'>Galves Range</div> updated from " + OrigninalDealer.GalvesRangeCategory + " to " + UpdatedDealer.GalvesRangeCategory + ";"; }
            if (!UpdatedDealer.TruckGalvesRangeCategory.Equals(OrigninalDealer.TruckGalvesRangeCategory)) { AuditString = AuditString + "<div style='font-weight: bold'>galves truck range</div> updated from " + OrigninalDealer.TruckGalvesRangeCategory + " to " + UpdatedDealer.TruckGalvesRangeCategory + ";"; }
            if (!UpdatedDealer.Language.Equals(OrigninalDealer.Language)) { AuditString = AuditString + "<div style='font-weight: bold'>Language</div> updated from " + OrigninalDealer.Language + " to " + UpdatedDealer.Language + ";"; }
            if (!UpdatedDealer.NADABranding.Equals(OrigninalDealer.NADABranding)) { AuditString = AuditString + "<div style='font-weight: bold'>NADA Branding</div> updated from " + OrigninalDealer.NADABranding + " to " + UpdatedDealer.NADABranding + ";"; }
            if (!UpdatedDealer.HideTradeIn.Equals(OrigninalDealer.HideTradeIn)) { AuditString = AuditString + "<div style='font-weight: bold'>Hide TradeIn</div> updated from " + OrigninalDealer.HideTradeIn + " to " + UpdatedDealer.HideTradeIn + ";"; }
            if (!UpdatedDealer.DataProvider.Equals(OrigninalDealer.DataProvider)) { AuditString = AuditString + "<div style='font-weight: bold'>Data Provider</div> updated from " + OrigninalDealer.DataProvider + " to " + UpdatedDealer.DataProvider + ";"; }
            if (!UpdatedDealer.NewModelYears.Equals(OrigninalDealer.NewModelYears)) { AuditString = AuditString + "<div style='font-weight: bold'>New Model Years</div> updated from " + OrigninalDealer.NewModelYears + " to " + UpdatedDealer.NewModelYears + ";"; }
            if (!UpdatedDealer.Theme.Equals(OrigninalDealer.Theme)) { AuditString = AuditString + "<div style='font-weight: bold'>Theme</div> updated from " + OrigninalDealer.Theme + " to " + UpdatedDealer.Theme + ";"; }
            if (!UpdatedDealer.WebsiteCompany.Equals(OrigninalDealer.WebsiteCompany)) { AuditString = AuditString + "<div style='font-weight: bold'>Website Company</div> updated from " + OrigninalDealer.WebsiteCompany + " to " + UpdatedDealer.WebsiteCompany + ";"; }
            if (!UpdatedDealer.WebsiteContactName.Equals(OrigninalDealer.WebsiteContactName)) { AuditString = AuditString + "<div style='font-weight: bold'>Website contact nam</div>e updated from " + OrigninalDealer.WebsiteContactName + " to " + UpdatedDealer.WebsiteContactName + ";"; }
            if (!UpdatedDealer.WebsiteContactPhone.Equals(OrigninalDealer.WebsiteContactPhone)) { AuditString = AuditString + "<div style='font-weight: bold'>Website Contact Phone</div> updated from " + OrigninalDealer.WebsiteContactPhone + " to " + UpdatedDealer.WebsiteContactPhone + ";"; }
            if (!UpdatedDealer.WebsiteContactEmail.Equals(OrigninalDealer.WebsiteContactEmail)) { AuditString = AuditString + "<div style='font-weight: bold'>Website Contact Email</div> updated from " + OrigninalDealer.WebsiteContactEmail + " to " + UpdatedDealer.WebsiteContactEmail + ";"; }
            if (!UpdatedDealer.LeadProvider.Equals(OrigninalDealer.LeadProvider)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadProvider</div> updated from " + OrigninalDealer.LeadProvider + " to " + UpdatedDealer.LeadProvider + ";"; }
            if (!UpdatedDealer.LeadEmail.Equals(OrigninalDealer.LeadEmail)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadEmail 1</div> updated from " + OrigninalDealer.LeadEmail + " to " + UpdatedDealer.LeadEmail + ";"; }
            if (!UpdatedDealer.LeadEmail2.Equals(OrigninalDealer.LeadEmail2)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadEmail 2</div> updated from " + OrigninalDealer.LeadEmail2 + " to " + UpdatedDealer.LeadEmail2 + ";"; }
            if (!UpdatedDealer.LeadEmail3.Equals(OrigninalDealer.LeadEmail3)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadEmail 3</div> updated from " + OrigninalDealer.LeadEmail3 + " to " + UpdatedDealer.LeadEmail3 + ";"; }
            if (!UpdatedDealer.LeadEmail4.Equals(OrigninalDealer.LeadEmail4)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadEmail 4</div> updated from " + OrigninalDealer.LeadEmail4 + " to " + UpdatedDealer.LeadEmail4 + ";"; }
            if (!UpdatedDealer.LeadFormat.Equals(OrigninalDealer.LeadFormat)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadFormat 1</div> updated from " + OrigninalDealer.LeadFormat + " to " + UpdatedDealer.LeadFormat + ";"; }
            if (!UpdatedDealer.LeadFormat2.Equals(OrigninalDealer.LeadFormat2)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadFormat 2</div> updated from " + OrigninalDealer.LeadFormat2 + " to " + UpdatedDealer.LeadFormat2 + ";"; }
            if (!UpdatedDealer.LeadFormat3.Equals(OrigninalDealer.LeadFormat3)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadFormat 3</div> updated from " + OrigninalDealer.LeadFormat3 + " to " + UpdatedDealer.LeadFormat3 + ";"; }
            if (!UpdatedDealer.LeadFormat4.Equals(OrigninalDealer.LeadFormat4)) { AuditString = AuditString + "<div style='font-weight: bold'>LeadFormat 4</div> updated from " + OrigninalDealer.LeadFormat4 + " to " + UpdatedDealer.LeadFormat4 + ";"; }
            if (!UpdatedDealer.LeadContactName.Equals(OrigninalDealer.LeadContactName)) { AuditString = AuditString + "<div style='font-weight: bold'>Lead Contact Name</div> updated from " + OrigninalDealer.LeadContactName + " to " + UpdatedDealer.LeadContactName + ";"; }
            if (!UpdatedDealer.LeadContactEmail.Equals(OrigninalDealer.LeadContactEmail)) { AuditString = AuditString + "<div style='font-weight: bold'>Lead Contact Email</div> updated from " + OrigninalDealer.LeadContactEmail + " to " + UpdatedDealer.LeadContactEmail + ";"; }
            if (!UpdatedDealer.LeadContactPhone.Equals(OrigninalDealer.LeadContactPhone)) { AuditString = AuditString + "<div style='font-weight: bold'>Lead Contact Phone</div> updated from " + OrigninalDealer.LeadContactPhone + " to " + UpdatedDealer.LeadContactPhone + ";"; }
            if (!UpdatedDealer.LeadContactFax.Equals(OrigninalDealer.LeadContactFax)) { AuditString = AuditString + "<div style='font-weight: bold'>Lead Contact Fax</div> updated from " + OrigninalDealer.LeadContactFax + " to " + UpdatedDealer.LeadContactFax + ";"; }
            if (!UpdatedDealer.Billable.Equals(OrigninalDealer.Billable)) { AuditString = AuditString + "<div style='font-weight: bold'>Billable</div> updated from " + OrigninalDealer.Billable + " to " + UpdatedDealer.Billable + ";"; }
            if (!UpdatedDealer.AdminAutoAppraisalCost.Equals(OrigninalDealer.AdminAutoAppraisalCost)) { AuditString = AuditString + "<div style='font-weight: bold'>Admin AutoAppraisalCost</div> updated from " + OrigninalDealer.AdminAutoAppraisalCost + " to " + UpdatedDealer.AdminAutoAppraisalCost + ";"; }
            if (!UpdatedDealer.BillingInfo.Equals(OrigninalDealer.BillingInfo)) { AuditString = AuditString + "<div style='font-weight: bold'>Billing Info</div> updated from " + OrigninalDealer.BillingInfo + " to " + UpdatedDealer.BillingInfo + ";"; }
            if (!UpdatedDealer.BillingLastUpdated.Equals(OrigninalDealer.BillingLastUpdated)) { AuditString = AuditString + "<div style='font-weight: bold'>Billing Last</div> Updated updated from " + OrigninalDealer.BillingLastUpdated + " to " + UpdatedDealer.BillingLastUpdated + ";"; }
            if (!UpdatedDealer.DateCreated.Equals(OrigninalDealer.DateCreated)) { AuditString = AuditString + "<div style='font-weight: bold'>Date Created</div> updated from " + OrigninalDealer.DateCreated + " to " + UpdatedDealer.DateCreated + ";"; }
            if (!UpdatedDealer.SystemBillingDate.Equals(OrigninalDealer.SystemBillingDate)) { AuditString = AuditString + "<div style='font-weight: bold'>System Billing Date</div> updated from " + OrigninalDealer.SystemBillingDate + " to " + UpdatedDealer.SystemBillingDate + ";"; }
            if (!UpdatedDealer.AdminSetupFees.Equals(OrigninalDealer.AdminSetupFees)) { AuditString = AuditString + "<div style='font-weight: bold'>Admin Setup Fees</div> updated from " + OrigninalDealer.AdminSetupFees + " to " + UpdatedDealer.AdminSetupFees + ";"; }
            if (!UpdatedDealer.BillableStartDate.Equals(OrigninalDealer.BillableStartDate)) { AuditString = AuditString + "<div style='font-weight: bold'>Billable StartDate</div> updated from " + OrigninalDealer.BillableStartDate + " to " + UpdatedDealer.BillableStartDate + ";"; }
            if (!UpdatedDealer.BillingExplanation.Equals(OrigninalDealer.BillingExplanation)) { AuditString = AuditString + "<div style='font-weight: bold'>Billing Explanation</div> from " + OrigninalDealer.BillingExplanation + " to " + UpdatedDealer.BillingExplanation + ";"; }
            if (!UpdatedDealer.HyperLead.Equals(OrigninalDealer.HyperLead)) { AuditString = AuditString + "<div style='font-weight: bold'>HyperLead</div> updated from " + OrigninalDealer.HyperLead + " to " + UpdatedDealer.HyperLead + ";"; }
            if (!UpdatedDealer.TaxIncentive.Equals(OrigninalDealer.TaxIncentive)) { AuditString = AuditString + "<div style='font-weight: bold'>Tax Incentive</div> updated from " + OrigninalDealer.TaxIncentive + " to " + UpdatedDealer.TaxIncentive + ";"; }
            if (!UpdatedDealer.Defaultbackground.Equals(OrigninalDealer.Defaultbackground)) { AuditString = AuditString + "<div style='font-weight: bold'>Default background</div> updated from " + OrigninalDealer.Defaultbackground + " to " + UpdatedDealer.Defaultbackground + ";"; }
            if (!UpdatedDealer.WebsiteComments.Equals(OrigninalDealer.WebsiteComments)) { AuditString = AuditString + "<div style='font-weight: bold'>Website Comments</div> updated from " + OrigninalDealer.WebsiteComments + " to " + UpdatedDealer.WebsiteComments + ";"; }

            try
            {
                if (AuditString.Length > 1)
                {
                    DealerDAL.SaveAuditResults(UpdatedDealer.DealerKey, AuditString, Security.GetLoggedInUser().Trim());
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void SaveDealer(DealerHolder DealerTosave)
        {
            try
            {
                DealerDAL.SaveDealer(DealerTosave);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public void SaveDealerInfoTab(DealerHolder DealerTosave)
        {
            try
            {
                DealerDAL.SaveDealerInfoTab(DealerTosave);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public void SaveDataTab(DealerHolder DealerTosave)
        {
            try
            {
                DealerDAL.SaveDataTab(DealerTosave);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        //public void SaveCustomizationTab(DealerHolder dealerToSave)
        //{
        //    try
        //    {
        //        DealerDAL.SaveCustomizationTab(dealerToSave);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
        //    }
        //}

        public void SaveLeadTab(DealerHolder DealerTosave)
        {
            try
            {
                DealerDAL.SaveLeadTab(DealerTosave);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public void SaveBillingTab(DealerHolder DealerTosave)
        {
            try
            {
                DealerDAL.SaveBillingTab(DealerTosave);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public void SaveFacebookTab(DealerHolder DealerTosave)
        {
            try
            {
                DealerDAL.SaveFacebookTab(DealerTosave);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public void SaveWebsiteTab(DealerHolder DealerTosave)
        {
            try
            {
                DealerDAL.SaveWebsiteTab(DealerTosave);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static string[] GetOfferText(string dealerKey)
        {
            DataSet ds;
            var result = new string[2];
            try
            {
                ds = DealerDAL.GetOfferText(dealerKey);
                if (ds != null && ds.Tables.Count > 0)
                {
                    result[0] = ds.Tables[0].Rows[0][0].ToString();
                    result[1] = ds.Tables[1].Rows[0][0].ToString();
                    return result;
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return result;
        }
        
        public static bool GetOfferTextDateRange(string dealerKey, ref string startDate, ref string stopdate)
        {
            DataTable dt;
            try
            {
                dt = DealerDAL.GetOfferTextDate(dealerKey);
                if (dt != null && dt.Rows.Count > 0)
                {
                    startDate = Convert.ToDateTime(dt.Rows[0][1]).ToString("MM/dd/yyyy");
                    stopdate = Convert.ToDateTime(dt.Rows[0][2]).ToString("MM/dd/yyyy");
                    return Convert.ToBoolean(dt.Rows[0][0]);
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return false;
        }

        public static string GetFaceBookHeader(string dealerKey)
        {
            DataSet ds;
            try
            {
                ds = DealerDAL.GetFaceBookHeader(dealerKey);
                if (ds != null && ds.Tables.Count > 0)
                {
                    return ds.Tables[0].Rows[0][0].ToString();
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return string.Empty;
        }

        public static DataTable GetImages(string dealerKey, string lang)
        {
            var dt = new DataTable();
            try
            {
                dt = DealerDAL.GetImages(dealerKey, lang);
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
            return dt;
        }

        public DataTable GetTriMonthlyLeads(string dealerKey)
        {
            var dt = new DataTable();
            try
            {
                dt = DealerDAL.GetTriMonthlyLeads(dealerKey);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return dt;
        }

        public static int GetCountAvailableModelsForSpecifiedYear(string year, string makes)
        {
            DataTable dt;
            int count = 0;
            try
            {
                dt = DealerDAL.GetCountAvailableModelsForSpecifiedYear(year, makes);
                count = (int)dt.Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return count;
        }

        public static byte[] GetImageFile(string dealerKey, int imageID)
        {
            DataTable dt;
            var image = new byte[0];
            try
            {
                dt = DealerDAL.GetImageFile(dealerKey, imageID);
                image = (byte[]) dt.Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return image;
        }

        public static void UpdateOfferText(string dealerKey, string lang, string html)
        {
            try
            {
                DealerDAL.UpdateOfferText(dealerKey, lang, html);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void UpdateFaceBookHeader(string dealerKey, string html)
        {
            try
            {
                DealerDAL.UpdateFaceBookHeader(dealerKey, html);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }


        public static void InsertDealerImage(string dealerKey, string lang, byte[] data)
        {
            try
            {
                DealerDAL.InsertDealerImage(dealerKey, lang, data);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void DeleteDealerImage(string dealerKey, string lang, string imageID)
        {
            try
            {
                DealerDAL.DeleteDealerImage(dealerKey, lang, imageID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void UpdateDealerImageDate(string dealerKey, string lang, int imageID, DateTime sDate, DateTime eDate)
        {
            try
            {
                DealerDAL.UpdateDealerImageDate(dealerKey, lang, imageID, sDate, eDate);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void ActivateDraftDealer(Int32 DealerID)
        {
            try
            {
                DealerDAL.ActivateDraftDealer(DealerID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void SetDealerImageActive(string dealerKey, string lang, string imageID)
        {
            try
            {
                DealerDAL.SetDealerImageActive(dealerKey, lang, imageID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void EnableDealerImageDate(string dealerKey, string lang, int imageID)
        {
            try
            {
                DealerDAL.EnableDealerImageDate(dealerKey, lang, imageID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static void UpdateOfferTextDate(string dealerKey, DateTime startDate, DateTime stopdate, bool enabled)
        {
            try
            {
                DealerDAL.UpdateOfferTextDate(dealerKey, startDate, stopdate, enabled);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public DataTable GetCreditAppTexts()
        {
            DataTable CreditTexts = new DataTable();

            try
            {
                CreditTexts = DealerDAL.GetCreditAppTexts();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return CreditTexts;
        }

        public DataTable GetAllReplacementYears()
        {
            DataTable ReplacementYears = new DataTable();

            try
            {
                ReplacementYears = DealerDAL.GetAllReplacementYears();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return ReplacementYears;
        }

        public DataTable GetEditLog(String DealerKey)
        {
            DataTable EditLog = new DataTable();

            try
            {
                EditLog = DealerDAL.GetEditLog(DealerKey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return EditLog;
        }

        public List<String> GetAvailableExcludedUsedCarMakes(String Dealerkey)
        {
            List<String> FreeMakes = new List<String>();

            try
            {
                FreeMakes = DealerDAL.GetAvailableExcludedUsedCarMakes(Dealerkey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return FreeMakes;
        }

        public List<String> GetExcludedUsedCarMakes(String dealerKey)
        {
            List<String> FreeMakes = new List<String>();

            try
            {
                FreeMakes = DealerDAL.GetExcludedUsedCarMakes(dealerKey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return FreeMakes;
        }

        public List<String> GetHNIFeeds(String dealerKey)
        {
            List<String> list = new List<String>();

            try
            {
                list = DealerDAL.GetHNIFeeds(dealerKey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return list;
        }

        public List<String> GetAvailableHNIFeeds(String dealerKey)
        {
            List<String> list = new List<String>();

            try
            {
                list = DealerDAL.GetAvailableHNIFeeds(dealerKey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return list;
        }

        public List<String> GetDefaultMakes(String dealerKey)
        {
            List<String> list = new List<String>();

            try
            {
                list = DealerDAL.GetDefaultMakes(dealerKey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return list;
        }

        public List<String> GetAvailableDefaultMakes(String dealerKey)
        {
            List<String> list = new List<String>();

            try
            {
                list = DealerDAL.GetAvailableDefaultMakes(dealerKey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return list;
        }

        public List<String> GetFreeMakes(String Dealerkey)
        {
            List<String> FreeMakes = new List<String>();

            try
            {
                FreeMakes = DealerDAL.GetFreeMakes(Dealerkey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return FreeMakes;
        }

        //public List<String> GetFreeHNIDealers(String Dealerkey)
        //{
        //    List<String> FreeHNIDealers = new List<String>();

        //    try
        //    {
        //        FreeHNIDealers = DealerDAL.GetFreeHNIDealers(Dealerkey);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
        //    }

        //    return FreeHNIDealers;
        //}

        public List<String> GetAllProviders()
        {
            List<String> Providers = new List<String>();

            try
            {
                Providers = DealerDAL.GetAllProviders();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return Providers;
        }

        public DataTable GetGalvesCarRanges()
        {
            DataTable CarRanges = new DataTable();

            try
            {
                CarRanges = DealerDAL.GetGalvesCarRanges();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return CarRanges;
        }

        public DataTable GetGalvesTruckRanges()
        {
            DataTable TruckRanges = new DataTable();

            try
            {
                TruckRanges = DealerDAL.GetGalvesTruckRanges();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return TruckRanges;
        }

        public static DataTable GetDraftAllDealers()
        {
            DataTable DraftDealers = new DataTable();

            try
            {
                DraftDealers = DealerDAL.GetDraftAllDealers();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return DraftDealers;
        }

        public static DataTable GetBlacklistedEmails()
        {
            DataTable Emails = new DataTable();

            try
            {
                Emails = DealerDAL.GetBlacklistedEmails();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return Emails;
        }

        public static DataTable GetHNIFeedDealerList()
        {
            try
            {
                return DealerDAL.GetHNIFeedDealerList();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return null;
        }

        public static void AssignPrimaryDealerToHNIFeed(string hniDealerID, string dealerKey)
        {
            try
            {
                DealerDAL.AssignPrimaryDealerToHNIFeed(hniDealerID, dealerKey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        public static DataTable GetDealerList()
        {
            try
            {
                return DealerDAL.GetDealerList();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return null;
        }

        public static Int32 BlackListEmail(string Email)
        {
            Int32 result = 0;

            try
            {
                result = DealerDAL.BlackListEmail(Email);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

            return result;
        }

        public static void UnBlackListEmail(string Email)
        {
            try
            {
                DealerDAL.UnBlackListEmail(Email);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public DataTable LoadDealer(String Dealerkey)
        {
            DataTable Dealer = new DataTable();

            try
            {
                Dealer = DealerDAL.LoadDealer(Dealerkey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return Dealer;
        }

        public DataTable LoadDealerFacebookSettings(String Dealerkey)
        {
            DataTable Settings = new DataTable();

            try
            {
                Settings = DealerDAL.LoadDealerFacebookSettings(Dealerkey);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return Settings;
        }

        public List<int> GetNewMakeYears(String SelectedMake)
        {
            List<int> lstYears = new List<int>();

            try
            {
                DataTable dtYears = DealerDAL.GetNewMakeYears(SelectedMake);
                foreach (DataRow dr in dtYears.Rows)
                {
                    lstYears.Add(Convert.ToInt32(dr["Year"]));
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

            return lstYears;
        }

        public static void UpdateDealerGroup(int dealerGroupID, string dealerKey)
        {
            try
            {
                DealerDAL.UpdateDealerGroup(dealerGroupID, dealerKey);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public static void ActivateDealerByMainAdmin(string dealerKey)
        {
            try
            {
                DealerDAL.ActivateDealerByMainAdmin(dealerKey);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public static void DeactivateDealerByMainAdmin(string dealerKey, string dealerName, string leadEmail1, string leadEmail2, string leadEmail3, string leadEmail4)
        {
            try
            {
                DealerDAL.DeactivateDealerByMainAdmin(dealerKey, dealerName, leadEmail1, leadEmail2, leadEmail3, leadEmail4);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public static void UpdateDeactivatedDealerData(string dealerKey, string dealerName, string leadEmail1, string leadEmail2, string leadEmail3, string leadEmail4)
        {
            try
            {
                DealerDAL.UpdateDeactivatedDealerData(dealerKey, dealerName, leadEmail1, leadEmail2, leadEmail3, leadEmail4);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public static bool IsDeactivated(string dealerKey)
        {
            bool result = false;
            try
            {
                var dt = DealerDAL.IsDeactivated(dealerKey);
                if(dt != null && dt.Rows.Count > 0)
                {
                    result = (Convert.ToInt32(dt.Rows[0][0].ToString()) > 0);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            
            return result;
        }

        public static bool IsDealerExists(string dealerKey)
        {
            bool result = false;
            try
            {
                var dt = DealerDAL.IsDealerExists(dealerKey);
                if (dt != null && dt.Rows.Count > 0)
                {
                    result = (Convert.ToInt32(dt.Rows[0][0].ToString()) > 0);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

            return result;
        }

        public static DataTable GetAllDeactivatedDealers()
        {
            var dt = new DataTable();
            try
            {
                dt = DealerDAL.GetAllDeactivatedDealers();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

            return dt;
        }

        public static DataTable GetMatchesFromDeactivatedDealers(string dealerName, string leadEmail1, string leadEmail2, string leadEmail3, string leadEmail4)
        {
            var dt = new DataTable();
            try
            {
                dt = DealerDAL.GetMatchesFromDeactivatedDealers(dealerName, leadEmail1, leadEmail2, leadEmail3, leadEmail4);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

            return dt;
        }

        #endregion
    }
}