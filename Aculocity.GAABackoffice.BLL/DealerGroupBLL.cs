﻿using System;
using System.Collections.Generic;
using System.Data;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class DealerGroupBLL: DealerGroupEntity
    {
        #region ' Constructors '
        public DealerGroupBLL(){}
        #endregion

        #region ' Properties '

        private Enumerations.GroupDealerAdmin _dealerTypeName;
        public Enumerations.GroupDealerAdmin DealerTypeName
        {
            get { return _dealerTypeName; }
            set { _dealerTypeName = value; }
        }
        #endregion

        #region Instance's Methods
        public int Update()
        {
            int result = 0;
            try
            {
                result = (int)((DataTable)DealerGroupDAL.UpdateDealerGroup(ID, Description, DealerTypeID)).Rows[0][0];
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
            return result;
        }
        public int Insert()
        {
            int result = 0;
            try
            {
                result = (int)((DataTable)DealerGroupDAL.InsertDealerGroup(Description, DealerTypeID)).Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result;
        }
        public int Delete()
        {
            int result = 0;
            try
            {
                result = (int)((DataTable)DealerGroupDAL.DeleteDealerGroup(ID)).Rows[0][0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result;
        }

        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Returns Dealer Group Type's name by Dealer Type ID
        /// </summary>
        /// <param name="dealerTypeID"></param>
        /// <returns></returns>
        public static Enumerations.GroupDealerAdmin GetDealerGrouptypeNameByDealerTypeID(string dealerTypeID)
        {
            return (Enumerations.GroupDealerAdmin)Enum.Parse(typeof(Enumerations.GroupDealerAdmin), dealerTypeID);
        }
        public void GetDealerGroupByID(int groupID)
        {
            DataTable dt = null;
            try
            {
                dt = DealerGroupDAL.GetDealerGroupByID(groupID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //populate vehicle:
                    PupulateFromDataRow(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) { dt.Dispose(); }
            }
        }
        public static int GetDealerGroupIDByUserID(int userID)
        {
            DataTable dt;
            try
            {
                dt = DealerGroupDAL.GetDealerGroupIDByUserID(userID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return Convert.ToInt32(dt.Rows[0][0]);
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return 0;
        }

        #endregion

        #region ' Compare '

        public virtual int CompareTo(DealerGroupBLL dealerGroup2, Enumerations.DealerGroupComparisonType comparisonType)
        {
            switch (comparisonType)
            {
                case Enumerations.DealerGroupComparisonType.Description: return Description.CompareTo(dealerGroup2.Description);
                case Enumerations.DealerGroupComparisonType.DealerTypeName: return DealerTypeName.CompareTo(dealerGroup2.DealerTypeName);
                default: return Description.CompareTo(dealerGroup2.Description);
            }
        }
        #endregion
    }
}