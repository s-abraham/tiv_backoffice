﻿using System;
using System.Collections.Generic;
using System.Data;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class ReportBLL
    {
        #region Constructors
        public ReportBLL() { }
        #endregion

        #region ' Custom Methods '
        public static Dictionary<string, string> GetTotalLeadsAsXML(string dealerKey, string LeadResultType)
        {
            DataSet dsData = null;
            var data = new Dictionary<string, string>();

            try
            {
                dsData = ReportDAL.GetTotalLeadAsXML(dealerKey, LeadResultType);
                if (dsData != null && dsData.Tables.Count > 0)
                {
                    data.Add("TotalLeads", dsData.Tables[0].Rows[0][0].ToString());
                    data.Add("MaxValue", dsData.Tables[1].Rows[0][0].ToString());
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dsData != null)
                {
                    dsData.Dispose();
                }
            }
            return data;
        }

        public static string GetTop10DesiredAsXML(string dealerKey, string LeadResultType)
        {
            DataTable dtData = null;
            string data = string.Empty;

            try
            {
                dtData = ReportDAL.GetNADATop10ModelsAsXML(dealerKey, LeadResultType);
                if (dtData != null && dtData.Rows.Count > 0)
                {
                    data = dtData.Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dtData != null)
                {
                    dtData.Dispose();
                }
            }
            return data;
        }

        public static Dictionary<string, string> GetBillableDealersXML(int year, int month, int dealerTypeID)
        {
            DataSet dsData = null;
            var data = new Dictionary<string, string>();

            try
            {
                dsData = ReportDAL.GetBillableDealersXML(year, month, dealerTypeID);
                if (dsData != null && dsData.Tables.Count > 0)
                {
                    data.Add("CatigoriesDealer", dsData.Tables[0].Rows[0][0].ToString());
                    data.Add("SetDealers", dsData.Tables[1].Rows[0][0].ToString());
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dsData != null)
                {
                    dsData.Dispose();
                }
            }
            return data;
        }

        public static DataSet GetLeadTotalScoreboard(string dealerKey, string range, string status,
            string leadResultType, Enumerations.Layout layout, Enumerations.ScaleType scale)
        {
            DataSet ds = null;
            try
            {
                ds = ReportDAL.GetLeadTotalScoreboard(dealerKey, range, status,
                    Security.GetLoggedUserID(), Security.UserIsInGroup(Enumerations.AuthUserGroup.GroupDealerAdmin),
                    (int)Security.GetDealerTypeLoggedUser(Security.GetGroupLoggedUser()), leadResultType, layout, scale);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static DataSet GetDetailsLeadData(string dealerKey, DateTime sDate, DateTime eDate, string firstName, string lastName, string eMail, string phone, int LeadResultType)
        {
            DataSet ds = null;
            try
            {
                ds = ReportDAL.GetDetailsLeadData(dealerKey, sDate, eDate, firstName, lastName, eMail, phone, LeadResultType);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static DataSet GetResellerDetailsLeadData(string dealerKey, DateTime sDate, DateTime eDate, int authUserID)
        {
            DataSet ds = null;
            try
            {
                ds = ReportDAL.GetResellerLeadData(dealerKey, sDate, eDate, authUserID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static DataSet GetDealerAppraisalsData(string dealerKey, DateTime sDate, DateTime eDate, string firstName, string lastName, string userEMail, string phone)
        {
            DataSet ds = null;
            try
            {
                ds = ReportDAL.GetDealerAppraisalsData(dealerKey, sDate, eDate, firstName, lastName, userEMail, phone);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static Int32 GetNewDealers(int dealerTypeID)
        {
            Int32 ds = 0;
            try
            {
                ds = ReportDAL.GetNewDealers(dealerTypeID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static DataTable GetNewDealersForGrid(int dealerTypeID)
        {
            var dt = new DataTable();
            try
            {
                dt = ReportDAL.GetNewDealersForGrid(dealerTypeID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return dt;
        }

        public static Int32 GetDeactivatedDealers(int dealerTypeID)
        {
            Int32 ds = 0;
            try
            {
                ds = ReportDAL.GetDeactivatedDealers(dealerTypeID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static DataTable GetDeactivatedDealersForGrid(int dealerTypeID)
        {
            var dt = new DataTable();
            try
            {
                dt = ReportDAL.GetDeactivatedDealersForGrid(dealerTypeID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return dt;
        }

        public static DataSet GetBillableYears()
        {
            DataSet ds = null;
            try
            {
                ds = ReportDAL.GetBillableYears();
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static DataSet GetBillableMonths(int year)
        {
            DataSet ds = null;
            try
            {
                ds = ReportDAL.GetBillableMonths(year);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        public static DataSet GetBillableDealers(int year, int month, int dealerTypeID)
        {
            DataSet ds = null;
            try
            {
                ds = ReportDAL.GetBillableDealers(year, month, dealerTypeID);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return ds;
        }

        #endregion
    }
}