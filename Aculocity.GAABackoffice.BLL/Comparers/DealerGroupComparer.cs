﻿using System;
using System.Collections;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL.Comparers
{
    public class DealerGroupComparer: IComparer
    {
        #region ' Constructors '
        public DealerGroupComparer(){}
        public DealerGroupComparer(Enumerations.DealerGroupComparisonType comparisonType)
        {
            ComparisonType = comparisonType;
        }
        #endregion

        private Enumerations.DealerGroupComparisonType _comparisonType;
        public Enumerations.DealerGroupComparisonType ComparisonType
        {
            get { return _comparisonType; }
            set {_comparisonType = value; }
        }

        public int Compare(object x, object y)
        {
            DealerGroupBLL dealerGroup1;
            DealerGroupBLL dealerGroup2;

            if (x is DealerGroupBLL)
            {
                dealerGroup1 = x as DealerGroupBLL;
            }
            else
            {
                throw new ArgumentException("Object first is not of type DealerGroupBLL.");
            }

            if (y is DealerGroupBLL)
            {
                dealerGroup2 = y as DealerGroupBLL;
            }
            else
            {
                throw new ArgumentException("Object second is not of type DealerGroupBLL.");
            }

            return dealerGroup1.CompareTo(dealerGroup2, _comparisonType);
        }
    }

    public class DealerGroupComparerReverse : IComparer
    {
        #region ' Constructors '
        public DealerGroupComparerReverse() { }
        public DealerGroupComparerReverse(Enumerations.DealerGroupComparisonType comparisonType)
        {
            ComparisonType = comparisonType;
        }
        #endregion

        private Enumerations.DealerGroupComparisonType _comparisonType;
        public Enumerations.DealerGroupComparisonType ComparisonType
        {
            get { return _comparisonType; }
            set { _comparisonType = value; }
        }

        public int Compare(object x, object y)
        {
            DealerGroupBLL dealerGroup1;
            DealerGroupBLL dealerGroup2;

            if (x is DealerGroupBLL)
            {
                dealerGroup1 = x as DealerGroupBLL;
            }
            else
            {
                throw new ArgumentException("Object first is not of type DealerGroupBLL.");
            }

            if (y is DealerGroupBLL)
            {
                dealerGroup2 = y as DealerGroupBLL;
            }
            else
            {
                throw new ArgumentException("Object second is not of type DealerGroupBLL.");
            }

            return -(dealerGroup1.CompareTo(dealerGroup2, _comparisonType));
        }
    }
}