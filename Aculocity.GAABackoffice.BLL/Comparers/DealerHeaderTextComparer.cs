﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL.Comparers
{
    public class DealerHeaderTextComparer : IComparer
    {
        #region ' Constructors '
        public DealerHeaderTextComparer(){}
        public DealerHeaderTextComparer(Enumerations.DealerHeaderTextComparisonType comparisonType)
        {
            ComparisonType = comparisonType;
        }
        #endregion

        private Enumerations.DealerHeaderTextComparisonType _comparisonType;
        public Enumerations.DealerHeaderTextComparisonType ComparisonType
        {
            get { return _comparisonType; }
            set { _comparisonType = value; }
        }

        public int Compare(object x, object y)
        {
            DealerHeaderTextBLL dealerHeaderText1;
            DealerHeaderTextBLL dealerHeaderText2;

            if (x is DealerHeaderTextBLL)
            {
                dealerHeaderText1 = x as DealerHeaderTextBLL;
            }
            else
            {
                throw new ArgumentException("Object first is not of type DealerHeaderTextBLL.");
            }

            if (y is DealerHeaderTextBLL)
            {
                dealerHeaderText2 = y as DealerHeaderTextBLL;
            }
            else
            {
                throw new ArgumentException("Object second is not of type DealerHeaderTextBLL.");
            }

            return dealerHeaderText1.CompareTo(dealerHeaderText2, _comparisonType);
        }
    }

    public class DealerHeaderTextComparerReverse : IComparer
    {
        #region ' Constructors '
        public DealerHeaderTextComparerReverse() { }
        public DealerHeaderTextComparerReverse(Enumerations.DealerHeaderTextComparisonType comparisonType)
        {
            ComparisonType = comparisonType;
        }
        #endregion

        private Enumerations.DealerHeaderTextComparisonType _comparisonType;
        public Enumerations.DealerHeaderTextComparisonType ComparisonType
        {
            get { return _comparisonType; }
            set { _comparisonType = value; }
        }

        public int Compare(object x, object y)
        {
            DealerHeaderTextBLL dealerHeaderText1;
            DealerHeaderTextBLL dealerHeaderText2;

            if (x is DealerHeaderTextBLL)
            {
                dealerHeaderText1 = x as DealerHeaderTextBLL;
            }
            else
            {
                throw new ArgumentException("Object first is not of type DealerHeaderTextBLL.");
            }

            if (y is DealerHeaderTextBLL)
            {
                dealerHeaderText2 = y as DealerHeaderTextBLL;
            }
            else
            {
                throw new ArgumentException("Object second is not of type DealerHeaderTextBLL.");
            }

            return -(dealerHeaderText1.CompareTo(dealerHeaderText2, _comparisonType));
        }
    }
    
}
