﻿using System;
using System.Collections;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL.Comparers
{
    public class UserComparer: IComparer
    {
        #region ' Constructors '
        public UserComparer(){}
        public UserComparer(Enumerations.UsersComparisonType comparisonType)
        {
            ComparisonType = comparisonType;
        }
        #endregion
        
        #region ' Methods '
        
        #endregion


        private Enumerations.UsersComparisonType _comparisonType;
        public Enumerations.UsersComparisonType ComparisonType
        {
            get { return _comparisonType; }
            set {_comparisonType = value; }
        }

        public int Compare(object x, object y)
        {
            UserBLL user1;
            UserBLL user2;

            if (x is UserBLL)
            {
                user1 = x as UserBLL;
            }
            else
            {
                throw new ArgumentException("Object first is not of type UserBLL.");
            }

            if (y is UserBLL)
            {
                user2 = y as UserBLL;
            }
            else
            {
                throw new ArgumentException("Object second is not of type UserBLL.");
            }

            return user1.CompareTo(user2, _comparisonType);
        }
    }

    public class UserComparerReverse : IComparer
    {
        #region ' Constructors '
        public UserComparerReverse() { }
        public UserComparerReverse(Enumerations.UsersComparisonType comparisonType)
        {
            ComparisonType = comparisonType;
        }
        #endregion

        #region ' Methods '

        #endregion


        private Enumerations.UsersComparisonType _comparisonType;
        public Enumerations.UsersComparisonType ComparisonType
        {
            get { return _comparisonType; }
            set { _comparisonType = value; }
        }

        public int Compare(object x, object y)
        {
            UserBLL user1;
            UserBLL user2;

            if (x is UserBLL)
            {
                user1 = x as UserBLL;
            }
            else
            {
                throw new ArgumentException("Object first is not of type UserBLL.");
            }

            if (y is UserBLL)
            {
                user2 = y as UserBLL;
            }
            else
            {
                throw new ArgumentException("Object second is not of type UserBLL.");
            }
            
            return -(user1.CompareTo(user2, _comparisonType));
        }
    }
}