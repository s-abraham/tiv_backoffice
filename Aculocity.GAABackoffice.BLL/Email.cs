﻿using System;
using System.Data;
using System.Net.Mail;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class Email
    {
        #region ' Common Methods '
        public static string GetBody(int aprID, string num)
        {
            DataTable dt = null;
            string emailBody = string.Empty;
            try
            {
                dt = EmailDAL.GetBody(aprID, num);
                if (dt != null && dt.Rows.Count > 0)
                {
                    emailBody = (string)dt.Rows[0][0];
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) { dt.Dispose(); }
            }
            return emailBody;
        }

        public static bool SendEmail(MailMessage message)
        {
            bool res = false;
            try
            {
                res = EmailDAL.SendMail(message);
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
            return res;
        }

        #endregion
    }
}