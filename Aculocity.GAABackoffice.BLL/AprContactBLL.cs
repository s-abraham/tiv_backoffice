﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class AprContactBLL: AprContactEntity
    {
        #region Constructors
        public AprContactBLL() { }
        public AprContactBLL(int aprID)
        {
            GetAprContactAprID(aprID);
        }
        #endregion

        #region ' Custom Methods '
        public void GetAprContactAprID(int aprID)
        {
            DataTable dt = null;
            try
            {
                dt = AppraiseDAL.GetAprContactByAprID(aprID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //populate trade:
                    PopulateFromDataRow(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) { dt.Dispose(); }
            }

        }
        #endregion
    }
}