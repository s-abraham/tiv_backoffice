﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.BLL
{
    public class NewVehicleBLL : VehicleEntity
    {
        #region Constructors
        public NewVehicleBLL() { }
        public NewVehicleBLL(string style, string model)
        {
            GetVehicleByModel(style, model);
        }
        #endregion

        #region ' Custom Methods '
        public void GetVehicleByModel(string style, string model)
        {
            DataTable dt = null;
            try
            {
                dt = VehicleDAL.GetVehicleByModel(style, model, "new");
                if (dt != null && dt.Rows.Count > 0)
                {
                    //populate vehicle:
                    PopulateFromDataRow(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) { dt.Dispose(); }
            }
        }
        #endregion
    }

    public class UsedVehicleBLL : VehicleEntity
    {
        #region Constructors
        public UsedVehicleBLL() { }
        public UsedVehicleBLL(string style, string model)
        {
            GetVehicleByModel(style, model);
        }
        #endregion

        #region ' Custom Methods '
        public void GetVehicleByModel(string style, string model)
        {
            DataTable dt = null;
            try
            {
                dt = VehicleDAL.GetVehicleByModel(style, model, "used");
                if (dt != null && dt.Rows.Count > 0)
                {
                    //populate vehicle:
                    PopulateFromDataRow(dt.Rows[0]);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            finally
            {
                if (dt != null) { dt.Dispose(); }
            }
        }
        #endregion
    }
}