﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class BaseDAL
    {
        /// <summary>
        /// Returns data as DataTable
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public static DataTable GetData(string spName)
        {
            var dt = new DataTable();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        conn.Open();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr != null) dt.Load(dr);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, 0);}
            return dt;
        }

        public static List<String> GetDataList(string spName, SqlParameter[] parameters)
        {
            List<String> dt = new List<String>();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr != null)
                        {
                            while (dr.Read())
                            {
                                dt.Add(dr.GetValue(0).ToString());
                            }
                        }

                        if (dr != null) dr.Close();
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static List<String> GetDataList(string spName)
        {
            List<String> dt = new List<String>();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        conn.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr != null)
                        {
                            while (dr.Read())
                            {
                                dt.Add(dr.GetValue(0).ToString());
                            }
                        }

                        if (dr != null) dr.Close();
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        
        /// <summary>
        /// Returns data as DataTable
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DataTable GetData(string spName, SqlParameter[] parameters)
        {
            var dt = new DataTable();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr != null) dt.Load(dr);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        /// <summary>
        /// Returns data as DataTable
        /// </summary>
        /// <param name="sql">Command Text</param>
        /// <param name="cmdType">Command Type</param>
        /// <returns></returns>
        public static DataTable GetData(string sql, CommandType cmdType)
        {
            var dt = new DataTable();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(sql, conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        conn.Open();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr != null) dt.Load(dr);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        /// <summary>
        /// Returns data as DataSet
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(string spName, SqlParameter[] parameters)
        {
            var ds = new DataSet();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex){
                ExceptionHandler.HandleException(ref ex, 0);
            }
            return ds;
        }

        public static String GetResult(string SQL)
        {
            String Result = "0";
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = SQL;
                        cmd.Connection = conn;
                        conn.Open();
                        Result = Convert.ToString(cmd.ExecuteScalar());
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return Result;
        }

        /// <summary>
        /// Returns data as DataSet without input parameters
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public static DataSet GetDataSet(string spName)
        {
            var ds = new DataSet();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        conn.Open();
                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }
        /// <summary>
        /// Execute stored procedure without return data
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parameters"></param>
        public static void ExecuteSP(string spName, SqlParameter[] parameters)
        {
            try
            {
                using(var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb) )
                {
                    using(var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void ExecuteSPWithoutLogging(string spName, SqlParameter[] parameters)
        {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
        }

        public static Int32 ExecuteWithResult(string spName, SqlParameter[] parameters)
        {
            Int32 Count = 1;

            try
            {
                using (SqlConnection conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    using (SqlCommand cmd = new SqlCommand(spName, conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        Count = Convert.ToInt32(cmd.ExecuteScalar());
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return Count;
        }
        
    }
}