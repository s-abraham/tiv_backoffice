﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class ReportDAL
    {
        #region ' Constructors '
        public ReportDAL() { }
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Returns Data For 'Total Leads' chart as xml string
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <param name="LeadResultType"></param>
        /// <returns>DataSet</returns>
        public static DataSet GetTotalLeadAsXML(string dealerKey, string LeadResultType)
        {
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 100) { Value = dealerKey };
                parameters[1] = new SqlParameter("@LeadResultType", SqlDbType.Int) { Value = LeadResultType };
                //return data
                ds = BaseDAL.GetDataSet("BO_Report_GetNADADealerLeadCount_XML", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        /// <summary>
        /// Returns billable dealers for a specified type
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="dealerTypeID"></param>
        /// <returns></returns>
        public static DataSet GetBillableDealersXML(int year, int month, int dealerTypeID)
        {
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@Year", SqlDbType.Int) { Value = year };
                parameters[1] = new SqlParameter("@Month", SqlDbType.Int) { Value = month };
                parameters[2] = new SqlParameter("@DealerType", SqlDbType.Int) { Value = dealerTypeID };
                //return data
                ds = BaseDAL.GetDataSet("BO_Report_GetBillableDealers_XML", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }
        /// <summary>
        /// Returns Data For 'NADA Top 10 Models' chart as xml string
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <param name="LeadResultType"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetNADATop10ModelsAsXML(string dealerKey, string LeadResultType)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 100) { Value = dealerKey };
                parameters[1] = new SqlParameter("@LeadResultType", SqlDbType.Int) { Value = LeadResultType };
                //return data
                dt = BaseDAL.GetData("BO_Report_GetNADATop10Models_XML", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Returns the dealer lead report Vertical mode
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <param name="range"></param>
        /// <param name="status"></param>
        /// <param name="userID"></param>
        /// <param name="limit"></param>
        /// <param name="LeadResultType"></param>
        /// <param name="dealerTypeID"></param>
        /// <returns>DataSet</returns>
        public static DataSet GetLeadTotalScoreboard(string dealerKey, string range, string status, int userID, bool limit,
            int dealerTypeID, string LeadResultType, Enumerations.Layout layout, Enumerations.ScaleType scale)
        {
            DateTime? startDate = null, endDate = null;
            if (range == "CurrentMonth") startDate = DateTime.Today;
            else if (range == "LastMonth") startDate = DateTime.Today - new TimeSpan(DateTime.Today.Day + 2, 0, 0, 0, 0);
            else if (range == "CompleteHistory")
            {
                startDate = new DateTime(2006, 12, 1);
                endDate = DateTime.Today;
            }
            else startDate = DateTime.Parse(range);

            Boolean active = status == "Active";
            Boolean billable = status == "Billable";
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@DealerKey", SqlDbType.VarChar, 8000) { Value = dealerKey });
                parameters.Add(new SqlParameter("@StartDate", SqlDbType.Date, 15) { Value = startDate });
                if (endDate != null)
                    parameters.Add(new SqlParameter("@EndDate", SqlDbType.Date, 15) { Value = endDate });

                parameters.Add(new SqlParameter("@Active", SqlDbType.Bit, 10) { Value = active });
                parameters.Add(new SqlParameter("@Billable", SqlDbType.Bit) { Value = billable });
                if (limit)
                    parameters.Add(new SqlParameter("@DGroupUserID", SqlDbType.Int) { Value = userID });
                parameters.Add(new SqlParameter("@DealerType", SqlDbType.Int, 100) { Value = dealerTypeID });
                //return data

                if (layout == Enumerations.Layout.Vertical)
                    if (scale == Enumerations.ScaleType.Daily)
                        ds = BaseDAL.GetDataSet("scoreboard_daily_vertical", parameters.ToArray());
                    else
                        ds = BaseDAL.GetDataSet("scoreboard_monthly_vertical", parameters.ToArray());
                else
                    if (scale == Enumerations.ScaleType.Daily)
                        ds = BaseDAL.GetDataSet("scoreboard_daily_horizontal", parameters.ToArray());
                    else
                        ds = BaseDAL.GetDataSet("scoreboard_monthly_horizontal", parameters.ToArray());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        /// <summary>
        /// Returns the dealer details lead report data
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>DataSet</returns>
        public static DataSet GetDetailsLeadData(string dealerKey, DateTime startDate, DateTime endDate, string firstName, string lastName, string eMail, string phone, int LeadResultType)
        {
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[8];
                parameters[0] = new SqlParameter("@dealerKey", SqlDbType.VarChar, 100) { Value = dealerKey };
                parameters[1] = new SqlParameter("@startDate", SqlDbType.DateTime) { Value = startDate };
                parameters[2] = new SqlParameter("@endDate", SqlDbType.DateTime) { Value = endDate };
                parameters[3] = new SqlParameter("@firstName", firstName);
                parameters[4] = new SqlParameter("@lastName", lastName);
                parameters[5] = new SqlParameter("@eMail", eMail);
                parameters[6] = new SqlParameter("@phone", phone);
                parameters[7] = new SqlParameter("@LeadResultType", LeadResultType);
                //return data
                ds = BaseDAL.GetDataSet("getLeadDetails", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        public static DataSet GetResellerLeadData(string dealerKey, DateTime startDate, DateTime endDate, int authUserID)
        {
            var ds = new DataSet();
            try
            {
                endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                //set input parameters
                if (dealerKey == null) dealerKey = "";
                var parameters = new SqlParameter[4];
                parameters[0] = new SqlParameter("@dealerKey", SqlDbType.VarChar, 100) { Value = dealerKey };
                parameters[1] = new SqlParameter("@startDate", SqlDbType.DateTime) { Value = startDate };
                parameters[2] = new SqlParameter("@endDate", SqlDbType.DateTime) { Value = endDate };
                parameters[3] = new SqlParameter("@authUserID", SqlDbType.Int) { Value = authUserID };
                
                //return data
                ds = BaseDAL.GetDataSet("getResellerLeadDetails", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        /// <summary>
        /// Returns the dealer appraisal report data
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>DataSet</returns>
        public static DataSet GetDealerAppraisalsData(string dealerKey, DateTime startDate, DateTime endDate, string firstName, string lastName, string userMail, string phone)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters
                var parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@dealerKey", dealerKey);
                parameters[1] = new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate);
                parameters[2] = new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate);
                parameters[3] = new SqlParameter("@firstName", firstName);
                parameters[4] = new SqlParameter("@lastName", lastName);
                parameters[5] = new SqlParameter("@userMail", userMail);
                parameters[6] = new SqlParameter("@phone", phone);
                //return data
                ds = BaseDAL.GetDataSet("BO_Report_DealerAppraisalsReport", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        // Get a list of salesman and count of sales
        public static DataSet GetSalesmanStats(DateTime startDate, DateTime endDate)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate);
                parameters[1] = new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate);
                //return data
                ds = BaseDAL.GetDataSet("getSalesmanStats", parameters);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, 0);
                Debugger.Log(0, "Test", "Some Message: " + ex.ToString() + "\n");
            }
            return ds;
        }

        // Get a list of salesman and count of sales
        public static DataSet GetDealerSignupLevels(DateTime startDate, DateTime endDate)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate);
                parameters[1] = new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate);

                //return data
                ds = BaseDAL.GetDataSet("getDealerSignupLevels", parameters);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, 0);
                Debugger.Log(0, "Test", "Some Message: " + ex.ToString() + "\n");
            }
            return ds;
        }

        // three single-value result sets
        public static DataSet GetLeadStats(DateTime startDate, DateTime endDate, int userID = -1)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters

                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate));
                parameters.Add(new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate));
                if (userID > -1)
                {
                    parameters.Add(new SqlParameter("@UserID", userID));
                }
                //return data
                ds = BaseDAL.GetDataSet("getLeadStats", parameters.ToArray());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        // three single-value result sets: this_month, last_month and potential
        public static DataSet GetSignupStats(int userID = -1)
        {
            var ds = new DataSet();

            try
            {
                //return data
                ds = BaseDAL.GetDataSet("getSignupStats");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        // returns 10 rows of (year,make,model,count)
        public static DataSet GetTopReplacementVehicles(DateTime startDate, DateTime endDate, int userID = -1)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate));
                parameters.Add(new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate));
                if (userID > -1)
                {
                    parameters.Add(new SqlParameter("@UserID", userID));
                }

                //return data
                ds = BaseDAL.GetDataSet("getTopReplacementVehicles", parameters.ToArray());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        // returns a single dataset with year, make, model, count
        public static DataSet GetTopTradeinVehicles(DateTime startDate, DateTime endDate, int userID = -1)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate));
                parameters.Add(new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate));
                if (userID > -1)
                {
                    parameters.Add(new SqlParameter("@UserID", userID));
                }
                //return data
                ds = BaseDAL.GetDataSet("getTopTradeinVehicles", parameters.ToArray());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        // returns 6 single-value result sets: (active_dealers, inactive_dealers, total_leads, leads_with_photos, leads_with_videos, and facebook_leads)
        public static DataSet GetDashboardStats(DateTime startDate, DateTime endDate, int userID = -1)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate));
                parameters.Add(new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate));
                if (userID > -1)
                {
                    parameters.Add(new SqlParameter("@UserID", userID));
                }
                //return data
                ds = BaseDAL.GetDataSet("getDashboardStats", parameters.ToArray());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        // returns one result set of (new, count) -  new can be one of three values: (new, used, unsure)
        public static DataSet GetDesiredVehicleNewUSedUnsure(DateTime startDate, DateTime endDate, int userID = -1)
        {
            var ds = new DataSet();

            try
            {
                //set input parameters
                var parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@startDate", startDate == DateTime.MinValue ? DBNull.Value : (object)startDate));
                parameters.Add(new SqlParameter("@endDate", endDate == DateTime.MinValue ? DBNull.Value : (object)endDate));
                if (userID > -1)
                {
                    parameters.Add(new SqlParameter("@UserID", userID));
                }
                //return data
                ds = BaseDAL.GetDataSet("getDesiredVehicleNewUSedUnsure", parameters.ToArray());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        public static DataSet GetBillableMonths(int year)
        {
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Year", SqlDbType.Int) { Value = year };
                //return data
                ds = BaseDAL.GetDataSet("BO_Report_GetBillableMonths", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        public static DataSet GetBillableYears()
        {
            var ds = new DataSet();
            try
            {
                //return data
                return BaseDAL.GetDataSet("BO_Report_GetBillableYears");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        public static Int32 GetNewDealers(int dealerTypeID)
        {
            Int32 ds = 0;
            try
            {
                //return data
                return Convert.ToInt32(BaseDAL.GetResult(string.Format("Select Count(*) from tblDealerMaster where DateCreated >= (GETDATE() - DAY(30)) {0}", dealerTypeID == 1 ? " AND (DealerType <> 4 AND DealerType <> 9) " : string.Format(" AND  DealerType = {0}", dealerTypeID))));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }
        public static DataTable GetNewDealersForGrid(int dealerTypeID)
        {
            var dt = new DataTable();
            try
            {
                //return data
                return BaseDAL.GetData(string.Format("Select DealerName, DealerKey, DateCreated from tblDealerMaster where DateCreated >= (GETDATE() - DAY(30)) {0} ORDER BY DealerName", dealerTypeID == 1 ? " AND (DealerType <> 4 AND DealerType <> 9) " : string.Format(" AND  DealerType = {0}", dealerTypeID)), CommandType.Text);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static Int32 GetDeactivatedDealers(int dealerTypeID)
        {
            Int32 ds = 0;
            try
            {
                //return data
                return Convert.ToInt32(BaseDAL.GetResult(string.Format("SELECT Count(*) from tblDealerMaster where Active = 0 AND DeactivationDate >= (GETDATE() - DAY(30)) {0}", dealerTypeID == 1 ? " AND (DealerType <> 4 AND DealerType <> 9) " : string.Format(" AND  DealerType = {0}", dealerTypeID))));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        public static DataTable GetDeactivatedDealersForGrid(int dealerTypeID)
        {
            var dt = new DataTable();
            try
            {
                //return data
                return BaseDAL.GetData(string.Format("SELECT DealerName, DealerKey, DeactivationDate from tblDealerMaster where Active = 0 AND DeactivationDate >= (GETDATE() - DAY(30)) {0}", dealerTypeID == 1 ? " AND (DealerType <> 4 AND DealerType <> 9) " : string.Format(" AND  DealerType = {0}", dealerTypeID)), CommandType.Text);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataSet GetBillableDealers(int year, int month, int dealerTypeID)
        {
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@Year", SqlDbType.Int) { Value = year };
                parameters[1] = new SqlParameter("@Month", SqlDbType.Int) { Value = month };
                parameters[2] = new SqlParameter("@DealerType", SqlDbType.Int) { Value = dealerTypeID };
                //return data
                ds = BaseDAL.GetDataSet("BO_Report_GetBillableDealers", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        #endregion
    }
}