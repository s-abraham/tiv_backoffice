﻿using System;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class VehicleDAL
    {
        #region ' Custom methods '
        public static DataTable GetVehicleByModel(string style, string model, string @new)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@Style", SqlDbType.VarChar, 255) { Value = style };
                parameters[1] = new SqlParameter("@Model", SqlDbType.VarChar, 255) { Value = model };
                
                string spName = @new == "new" ? "BO_NewVehicle_GetNewVehicleByModel" : "BO_UsedVehicle_GetUsedVehicleByModel";
                //return data
                dt = BaseDAL.GetData(spName, parameters);
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static int[] GetImagesID(int aprID, string dealerKey)
        {
            var imagesID = new int[0];
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@AprID", SqlDbType.Int) { Value = aprID };
                parameters[1] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 100) { Value = dealerKey };
                
                DataTable dt = BaseDAL.GetData("BO_Vehicle_GetImagesID", parameters);
                imagesID = new int[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    imagesID[i] = (int)dt.Rows[i][0];
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return imagesID;
        }

        public static string[] GetOptionalsID(int aprID)
        {
            var optionalsID = new string[0];
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AprID", SqlDbType.Int) { Value = aprID };

                DataTable dt = BaseDAL.GetData("BO_Vehicle_GetOptionalsID", parameters);
                optionalsID = new string[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    optionalsID[i] = (string)dt.Rows[i][0];
                }

                //return data
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return optionalsID;
        }
        #endregion
    }
}