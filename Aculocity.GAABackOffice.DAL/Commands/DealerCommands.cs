﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;
using Aculocity.GAABackOffice.Global;

namespace  Aculocity.GAABackOffice.DAL {
    public class DealerCommands {

        public static DataTable GetDealershipFilterDataTable() {
            
            DataTable filters = new DataTable();
            filters.Columns.Add("ID", typeof(string));
            filters.Columns.Add("Description", typeof(string));

            filters.Rows.Add(new object[] { "DealerCity", "Dealer City" });
            filters.Rows.Add(new object[] { "DealerContactEmail", "Dealer Contact Email" });
            filters.Rows.Add(new object[] { "DealerContactName", "Dealer Contact Name" });
            filters.Rows.Add(new object[] { "DealerGroupDescription", "Dealer Group" });
            filters.Rows.Add(new object[] { "DealerKey", "Dealer Key" });
            filters.Rows.Add(new object[] { "DealerName", "Dealer Name" });
            filters.Rows.Add(new object[] { "DealerVideoID", "Has Video" });
            filters.Rows.Add(new object[] { "LeadEmail", "Lead Email" });
            filters.Rows.Add(new object[] { "AdminAutoAppraisalCost", "Monthly Cost Of Dealer Tool" });
            filters.Rows.Add(new object[] { "Salesperson", "Sales Person" });
            filters.Rows.Add(new object[] { "DealerState", "Dealer State" });
            filters.Rows.Add(new object[] { "WebsiteCompany", "Web Company" });
            filters.Rows.Add(new object[] { "DealerZip", "Zip" });
            
            return filters;
        
        }

        public static DataTable GetDealershipFilterDataTableOnlyDealerKeyDealerName() {

            DataTable filters = new DataTable();
            filters.Columns.Add("ID", typeof(string));
            filters.Columns.Add("Description", typeof(string));

           
            filters.Rows.Add(new object[] { "DealerKey", "Dealer Key" });
            filters.Rows.Add(new object[] { "DealerName", "Dealer Name" });
            

            return filters;

        }

        public static DataManager GetDealersByFilterCriteria(DataManager dataManager, string parameterName, object value, bool showOnlyActiveDealer, bool showDealerWithoutImageOrSpecialOffer, Enumerations.AuthUserGroup authUserGroup, int userId, bool showOnlyError) {

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@dealerCity", string.Empty),
                new SqlParameter("@dealerContactEmail", string.Empty),
                new SqlParameter("@dealerContactName", string.Empty),
                new SqlParameter("@dealerGroupDescription", string.Empty),
                new SqlParameter("@dealerKey", string.Empty),
                new SqlParameter("@dealerName", string.Empty),
                new SqlParameter("@dealerVideoID", DBNull.Value),    
                new SqlParameter("@adminAutoAppraisalCost", DBNull.Value),
                new SqlParameter("@salesperson", string.Empty),
                new SqlParameter("@dealerState", string.Empty),    
                new SqlParameter("@websiteCompany", string.Empty),     
                new SqlParameter("@dealerZip", string.Empty),
                new SqlParameter("@leadEmail", string.Empty),
                new SqlParameter("@showOnlyActiveDealer", showOnlyActiveDealer),
                new SqlParameter("@showDealerWithoutImageOrSpecialOffer", showDealerWithoutImageOrSpecialOffer),
                new SqlParameter("@authUserGroupId", (int)authUserGroup),
                new SqlParameter("@userID", userId),
                new SqlParameter("@showOnlyError", showOnlyError) 
            };

            if (!string.Empty.Equals(parameterName)) {
                parameterName = string.Format("@{0}", parameterName);
                Array.Find<SqlParameter>(parameters, parameter => parameter.ParameterName.Equals(parameterName, StringComparison.InvariantCultureIgnoreCase)).Value = value;
            }

            return dataManager.SetQuery("GetDealersByFilterCriteria", parameters);
        
        }

        public static DataManager GetAllDealerGroups(DataManager dataManager, string authUserGroup)
        {
            //return dataManager.SetQuery("GetAllDealerGroups", new SqlParameter[]{new SqlParameter("@isHomeNetUser", isHomeNetDealer)});
            return dataManager.SetQuery("BO_DealerGroup_GetDealerGroupsByDealerAdmin", new SqlParameter[] { new SqlParameter("@AuthUserGroup", authUserGroup) });
        }

        public static DataManager GetAllCreditAppTexts(DataManager dataManager) {
            return dataManager.SetQuery("GetAllCreditAppTexts", new SqlParameter[0]);
        }

        public static DataManager GetAllDefaultMakesForDealer(DataManager dataManager, string dealerKey) {
            return dataManager.SetQuery("GetAllDefaultMakesForDealer", new SqlParameter[] { new SqlParameter("@dealerKey", dealerKey) }); 
        }

        public static DataManager GetAllDealershipHNIMappingsForDealer(DataManager dataManager, string dealerKey) {
            return dataManager.SetQuery("GetAllDealershipHNIMappingsForDealer", new SqlParameter[] {new SqlParameter("@dealerKey", dealerKey) });
        }

        public static DataManager GetCarsRangeCategories(DataManager dataManager) {
            return dataManager.SetQuery("GetCarsRangeCategories", new SqlParameter[0]);
        }

        public static DataManager GetTrucksRangeCategories(DataManager dataManager) {
            return dataManager.SetQuery("GetTrucksRangeCategories", new SqlParameter[0]);
        }

        public static DataManager GetLeadProviders(DataManager dataManager) {
            return dataManager.SetQuery("GetLeadProviders", new SqlParameter[0]); 
        }

        public static DataManager GetAllNewReplacementVehicleForModelYear(DataManager dataManager) {
            return dataManager.SetQuery("GetAllNewReplacementVehicleForModelYear", new SqlParameter[0]);
        }

        public static DataManager GetAllowedCompanyDealers(DataManager dataManager) {
            return dataManager.SetQuery("spGetAllowedCompanyDealers", new SqlParameter[0]);
        }

        public static DataManager GetTriMonthlyLeads(DataManager dataManager, string dealerKey) {
            return dataManager.SetQuery("GetTriMonthlyLeads", new SqlParameter[] { new SqlParameter("@dealerKey", dealerKey)});
        }

        public static DataManager GetDealerKeyByUserId(DataManager dataManager, int userId) {
            return dataManager.SetQuery("GetDealerKeyByUserId", new SqlParameter[]{new SqlParameter("@userId", userId)});
        }


    }

}
