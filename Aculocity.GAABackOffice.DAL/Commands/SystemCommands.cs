﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    
    public class SystemCommands {

        public static void HN_LogException(DataManager dataManager, string message, string stackTrace, string source) {
            dataManager.SetQuery("HN_LogException", new SqlParameter[]{
                new SqlParameter("@message", message),
                new SqlParameter("@stackTrace", stackTrace),
                new SqlParameter("@source", source)
            }).ExecuteNonQuery();
        }

        public static DataManager GetAllExceptions(DataManager dataManager) {
            return dataManager.SetQuery("GetAllExceptions", new SqlParameter[0]);
        }

    }

}
