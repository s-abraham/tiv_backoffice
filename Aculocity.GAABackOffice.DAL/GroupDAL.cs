﻿using System;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class GroupDAL
    {
        #region ' Collection Methods '
        /// <summary>
        /// 
        /// </summary>
        /// <param name="authUserGroup"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetGroupsByAuthUserGroup(string authUserGroup)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AuthUserGroup", SqlDbType.VarChar, 50) { Value = authUserGroup };
                //return data
                dt = BaseDAL.GetData("BO_Groups_GetGroupsByAuthUserGroup", parameters);
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        #endregion
    }
}