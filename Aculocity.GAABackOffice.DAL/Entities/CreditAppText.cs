﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    
    
    public class CreditAppText : SingleIdKeyEntity {

        [DataField]
        public string Engilsh {
            get;
            set;
        }

        [DataField]
        public string Spanish {
            get;
            set;
        }

        public CreditAppText(DataManager dataManager)
            : base(dataManager) {
        }

    }


}
