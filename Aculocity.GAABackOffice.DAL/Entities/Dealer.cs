﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    public class Dealer : Entity { 

        DealerGroup dealerGroup;
        CreditAppText creditAppText;
        GalvesRangeCategory galvesRangeCategory;
        GalvesRangeCategory truckGalvesRangeCategory;
        DealerModelYears dealerModelYears;
        AverageValueRange averageValueRange;
        DealerType dealerType;

        #region Properties

        [PrimaryKey]
        [DataField]
        public string DealerKey {
            get;
            set;
        }

        [DataField]
        public string DealerName {
            get;
            set;
        }

        [DataField]
        public string DealerStreet {
            get;
            set;
        }

        [DataField]
        public string DealerCity {
            get;
            set;
        }

        [DataField]
        public string DealerState {
            get;
            set;
        }

        [DataField]
        public string DealerTitle {
            get;
            set;
        }

        [DataField]
        public string DealerZip {
            get;
            set;
        }

        [DataField]
        public string DealerContactName {
            get;
            set;
        }

        [DataField]
        public string DealerContactEmail {
            get;
            set;
        }

        [DataField]
        public string DealerContactPhone {
            get;
            set; 
        }

        [DataField]
        public string DealerContactFax {
            get;
            set;
        }

        [DataField]
        public string DealerWebsite {
            get;
            set;
        }

        [DataField]
        public string DealerPublicSite {
            get;
            set;
        }

        [DataField]
        public string DealerAutoGroup {
            get;
            set;
        }

        [DataField]
        public string DealerLogo {
            get;
            set;
        }

        [DataField]
        public string DealerPromo {
            get;
            set;
        }

        [DataField]
        public string DealerDefaultMakes {
            get;
            set;
        }

        [DataField]
        public string LeadProvider {
            get;
            set;
        }

        [DataField]
        public string LeadFormat {
            get;
            set;
        }

        [DataField]
        public string LeadFormat2 {
            get;
            set;
        }

        [DataField]
        public string LeadEmail1 {
            get;
            set;
        }

        [DataField]
        public string LeadEmail2 {
            get;
            set;
        }

        [DataField]
        public string LeadContactName {
            get;
            set;
        }

        [DataField]
        public string LeadContactEmail {
            get;
            set;
        }

        [DataField]
        public string LeadContactPhone {
            get;
            set;
        }

        [DataField]
        public string LeadContactFax {
            get;
            set;
        }

        [DataField]
        public string WebsiteCompany {
            get;
            set;
        }

        [DataField]
        public string WebsiteContactName {
            get;
            set;
        }

        [DataField]
        public string WebsiteContactEmail {
            get;
            set;
        }

        [DataField]
        public string WebsiteContactPhone {
            get;
            set;
        }

        [DataField]
        public DateTime AdminDate {
            get;
            set;
        }

        [DataField]
        public DateTime AdminActivationDate {
            get;
            set;
        }

        [DataField]
        public string AdminDWTProduct {
            get;
            set;
        }

        [DataField]
        public decimal AdminAutoAppraisalCost {
            get;
            set;
        }

        [DataField]
        public decimal AdminSetupFees {
            get;
            set;
        }

        [DataField]
        public bool Active {
            get;
            set;
        }

        [DataField]
        public bool EarlyAppraise {
            get;
            set;
        }

        [DataField]
        public string Theme {
            get;
            set;
        }

        [DataField]
        public bool Billable {
            get;
            set;
        }

        [DataField]
        public DateTime BillableStartDate {
            get;
            set;
        }

        [DataField]
        public DateTime DateUpdated {
            get;
            set;
        }

        [DataField]
        public DateTime DateCreated {
            get;
            set;
        }

        [DataField]
        public bool InstantOptionPricing {
            get;
            set;
        }

        [DataField]
        public string Language {
            get;
            set;
        }

        [DataField]
        public string CreditApp {
            get;
            set;
        }

        [DataField]
        public string LeadEmail3 {
            get;
            set;
        }

        [DataField]
        public string LeadEmail4 {
            get;
            set;
        }

        [DataField]
        public string LeadFormat3 {
            get;
            set;
        }


        [DataField]
        public string LeadFormat4 {
            get;
            set;
        }

        [DataField]
        public bool CreditEnabled {
            get;
            set;
        }

        [DataField]
        public string InvUsed {
            get;
            set;
        }

        [DataField]
        public bool InvUsedEnabled {
            get;
            set;
        }

        [DataField]
        public string InvNew {
            get;
            set;
        }

        [DataField]
        public bool InvNewEnabled {
            get;
            set;
        }

        [DataField]
        public bool EmailAppraisal {
            get;
            set;
        }

        [DataField]
        public bool FourStep {
            get;
            set;
        }

        [DataField]
        public bool UnsureOption {
            get;
            set;
        }


        [DataField]
        public string GoogleUrchin {
            get;
            set;
        }

        [DataField]
        public bool IgnoreOption {
            get;
            set;
        }

        [DataField]
        public string Comments {
            get;
            set;
        }

        [DataField]
        public string Salesperson {
            get;
            set;
        }

        [DataField]
        public bool HideTradeIn {
            get;
            set;
        }

        [DataField]
        public string EmailTitle {
            get;
            set;
        }

        [DataField]
        public string OperatingHours {
            get;
            set;
        }

        [DataField]
        public bool HideCondition {
            get;
            set;
        }

        [DataField]
        public bool HideEmailTradeIn {
            get;
            set;
        }

        [DataField]
        public bool HideSpecialOffer {
            get;
            set;
        }

        [DataField]
        public bool HideAddress {
            get;
            set;
        }

        [DataField]
        public string DealerContactPhone2 {
            get;
            set;
        }

        [DataField]
        public string SecondDealerTitle {
            get;
            set;
        }

        [DataField]
        public string SecondDealerContactName {
            get;
            set;
        }

        [DataField]
        public string SecondDealerContactEmail {
            get;
            set;
        }

        [DataField]
        public string SecondDealerContactPhone {
            get;
            set;
        }

        [DataField]
        public string SecondDealerContactFax {
            get;
            set;
        }

        [DataField]
        public string SecondDealerContactPhone2 {
            get;
            set;
        }

        [DataField]
        public string AppointmentEmail {
            get;
            set;
        }
        
        [DataField]
        public string OriginalSalesPerson {
            get;
            set;
        }

        [DataField]
        public string DataProvider {
            get;
            set;
        }

        [DataField]
        public bool NewSelection {
            get;
            set;
        }

        [DataField]
        public string HNIDealers {
            get;
            set;
        }

        //[DataField]
        //public int DealerType {
        //    get;
        //    set;
        //}

        [DataField]
        public int NADARangeCategory {
            get;
            set;
        }

        [DataField]
        public bool NADABranding {
            get;
            set;
        }
        
        [DataField]
        public bool RetailPricing {
            get;
            set;
        }

        [DataField]
        public bool TradeImageUpload {
            get;
            set;
        }

        [DataField]
        public bool ShowIncentives {
            get;
            set;
        }

        [DataField]
        public bool ShowMonthlyPayment {
            get;
            set;
        }

        [DataField]
        public bool ShowAmountOwed {
            get;
            set;
        }


        [DataField]
        public int AppraisalExpiration {
            get;
            set;
        }

        [DataField]
        public bool RepaymentCalc {
            get;
            set;
        }

        [DataField]
        public DateTime BillingLastUpdated {
            get;
            set;
        }

        [DataField]
        public string BillingInfo {
            get;
            set;
        }

        [DataField]
        public string BillingExplanation {
            get;
            set;
        }

        [DataField]
        public DateTime SystemBillingDate {
            get;
            set;
        }

        [ForeignField]
        public DealerGroup DealerGroup {
            get {
                if (dealerGroup == null) 
                    dealerGroup = new DealerGroup(DataManager);
                    return dealerGroup;
            }
            set {
                dealerGroup = value;
            }
        }

        [ForeignField]
        public CreditAppText CreditAppText {
            get {
                if (creditAppText == null)
                    creditAppText = new CreditAppText(DataManager);
                return creditAppText;
            }
            set {
                creditAppText = value;
            }
        }

        [ForeignField]
        public GalvesRangeCategory GalvesRangeCategory {
            get {
                if (galvesRangeCategory == null)
                    galvesRangeCategory = new GalvesRangeCategory(DataManager,"C");
                return galvesRangeCategory;
            }
            set {
                galvesRangeCategory = value;
            }
        }

        [ForeignField]
        public GalvesRangeCategory TruckGalvesRangeCategory {
            get {
                if (truckGalvesRangeCategory == null)
                    truckGalvesRangeCategory = new GalvesRangeCategory(DataManager, "T");
                return truckGalvesRangeCategory;
            }
            set {
                truckGalvesRangeCategory = value;
            }
        }

        [ForeignField]
        public DealerModelYears DealerModelYears {
            get {
                if (dealerModelYears == null)
                    dealerModelYears = new DealerModelYears(DataManager);
                return dealerModelYears;
            }
            set {
                dealerModelYears = value;
            }
        }

        [ForeignField]
        public AverageValueRange AverageValueRange {
            get {
                if (averageValueRange == null) 
                    averageValueRange = new AverageValueRange(DataManager);
                return averageValueRange;
            }
            set {
                averageValueRange = value;
            }
        }

        [ForeignField]
        public DealerType DealerType {
            get {
                if (dealerType == null)
                    dealerType = new DealerType(DataManager);
                return dealerType;
            }
            set {
                dealerType = value;
            }
        }

        #endregion

        public void GenerateNewDealerKey() {
            DealerKey = Guid.NewGuid().ToString().Replace("-", string.Empty).ToUpper();
        }

        

        public override bool IsNew() {
            return string.IsNullOrEmpty(DealerKey);
        }

        public Dealer(DataManager dataManager)
            : base(dataManager) {
                        
        }
    
    }
}
