﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    public class AverageValueRange : Entity {

        [DataField]
        public string DealerKey {
            get;
            set;
        }

        [DataField]
        public int VUnderPercent {
            get;
            set;
        }

        [DataField]
        public int VOverPercent {
            get;
            set;
        }

        [DataField]
        public decimal VUnderAmount {
            get;
            set;
        }

        [DataField]
        public decimal VOverAmount {
            get;
            set;
        }

        [DataField]
        public string VUnderTypeSelection {
            get;
            set;
        }


        [DataField]
        public string VOverTypeSelection {
            get;
            set;
        }

        public bool ISEntityNew {
            get;
            set;
        }

        public override bool IsNew() {
            return ISEntityNew;
        }

        public AverageValueRange(DataManager dataManager)
            : base(dataManager) {
        }
    }
}
