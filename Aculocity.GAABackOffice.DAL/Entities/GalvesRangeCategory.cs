﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {

    public class GalvesRangeCategory : NamedSingleIdEntity {
        
        [DataField]
        public string TypeId {
            get;
            set;
        }

        public GalvesRangeCategory(DataManager dataManager, string typeId) : base(dataManager) {
            TypeId = typeId;
        }
    
    }
}
