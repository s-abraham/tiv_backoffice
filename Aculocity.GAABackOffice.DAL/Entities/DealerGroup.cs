﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {

    public class DealerGroup : NamedSingleIdEntity {

        DealerType dealerType; 

        [ForeignField]
        public DealerType DealerType {
            get {
                if (dealerType == null)
                    dealerType = new DealerType(DataManager);
                return dealerType;
            }
            set {
                dealerType = value;
            }
        }

       
        public DealerGroup(DataManager dataManager)
            : base(dataManager) {
        }
    }
}
