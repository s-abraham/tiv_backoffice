﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    
    public class DealerModelYears : Entity {

        Dealer dealer;

        [DataField]
        public string DealerKey {
            get;
            set;
        }

        [DataField]
        public string NewModelYears {
            get;
            set;
        }

        [ForeignField]
        public Dealer Dealer {
            get {
                if (dealer == null)
                    dealer = new Dealer(DataManager);
                return dealer;
            }
            set {
                dealer = value;
            }
        }

        public bool IsEntityNew {
            get;
            set;
        }

        public override bool IsNew() {
            return IsEntityNew;
        }

        public DealerModelYears(DataManager dataManager)
            : base(dataManager) {
        }
    
    }

}
