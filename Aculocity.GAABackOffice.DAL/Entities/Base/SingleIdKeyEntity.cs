﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    
    public class SingleIdKeyEntity : Entity {

        [PrimaryKey]
        [DataField]
        public int Id {
            get;
            set;
        }


        public override bool IsNew() {
            return Id == 0;
        }

        public SingleIdKeyEntity(DataManager dataManager)
            : base(dataManager) {
        }

    }
}
