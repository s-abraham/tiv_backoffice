﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    
    public class NamedSingleIdEntity : SingleIdKeyEntity {
        
        [DataField]
        public string Description {
            get;
            set;
        }
        
        public NamedSingleIdEntity(DataManager dataManager)
            : base(dataManager) {
        }
    }

}
