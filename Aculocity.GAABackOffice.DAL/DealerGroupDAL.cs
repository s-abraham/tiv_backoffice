﻿using System;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class DealerGroupDAL
    {
        #region ' Collection Methods '
        /// <summary>
        /// Returns all dealer groups
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllDealerGroups()
        {
            var dt = new DataTable();
            try
            {
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_GetAllDealerGroups");

            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable GetDealerGroupsByDealerType(int dealerTypeID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerTypeID", SqlDbType.Int) {Value = dealerTypeID};
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_GetDealerGroupsByDealerTypeID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable GetDealerGroupsByDealerAdmin(string authUserGroup)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AuthUserGroup", SqlDbType.VarChar, 50) { Value = authUserGroup };
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_GetDealerGroupsByDealerAdmin", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        #endregion
        #region ' Custom Methods '
        public static DataTable UpdateDealerGroup(int id, string groupName, int dealerTypeID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@ID", SqlDbType.Int) {Value = id};
                parameters[1] = new SqlParameter("@GroupName", SqlDbType.VarChar, 255) {Value = groupName};
                parameters[2] = new SqlParameter("@DealerTypeID", SqlDbType.Int) {Value = dealerTypeID};
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_Update", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable InsertDealerGroup(string groupName, int dealerTypeID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@GroupName", SqlDbType.VarChar, 255) { Value = groupName };
                parameters[1] = new SqlParameter("@DealerTypeID", SqlDbType.Int) { Value = dealerTypeID };
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_Insert", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable DeleteDealerGroup(int id)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ID", SqlDbType.Int) { Value = id };
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_Delete", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable GetDealerGroupIDByUserID(int userID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserID", SqlDbType.Int) { Value = userID };
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_GetDealerGroupIDByUserID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable GetDealerGroupByID(int groupID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@GroupID", SqlDbType.Int) { Value = groupID };
                //return data
                dt = BaseDAL.GetData("BO_DealerGroup_GetDealerGroupByID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        #endregion
    }
}