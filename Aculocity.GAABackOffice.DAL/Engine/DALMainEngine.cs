﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Aculocity.SimpleData;

namespace Aculocity.GAABackOffice.DAL {
    public class DALMainEngine {
        public static void Init() {
        
            EntityDescriptor.RegisterEntityDescriptor(typeof(Dealer));
            EntityDescriptor.RegisterEntityDescriptor(typeof(DealerType));
            EntityDescriptor.RegisterEntityDescriptor(typeof(DealerGroup));
            EntityDescriptor.RegisterEntityDescriptor(typeof(CreditAppText));
            EntityDescriptor.RegisterEntityDescriptor(typeof(GalvesRangeCategory));
            EntityDescriptor.RegisterEntityDescriptor(typeof(DealerModelYears));
            EntityDescriptor.RegisterEntityDescriptor(typeof(AverageValueRange));

        }
    }
}
