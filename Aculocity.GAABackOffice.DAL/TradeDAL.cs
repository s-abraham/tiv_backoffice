﻿using System;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class TradeDAL
    {
        #region ' Custom methods '
        public static DataTable GetTradeWantedByAprID(int aprID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AprID", SqlDbType.Int) { Value = aprID };
                //return data
                dt = BaseDAL.GetData("BO_TradeWanted_GetTradeWantedByAprID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable GetTradeInByAprID(int aprID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AprID", SqlDbType.Int) { Value = aprID };
                //return data
                dt = BaseDAL.GetData("BO_Trade_GetTradeInByAprID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        #endregion
    }
}