﻿using System;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class UserDAL
    {
        #region ' Collection Methods '
        /// <summary>
        /// Returns Users by specific admin account
        /// </summary>
        /// <param name="userGroup"></param>
        /// <param name="searchBy"></param>
        /// <param name="serchValue"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetUsersByAuthUserGroup(string userGroup, string searchBy, string serchValue)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@AuthUserGroup", SqlDbType.VarChar, 50) { Value = userGroup };
                parameters[1] = new SqlParameter("@SearchBy", SqlDbType.VarChar, 20) { Value = searchBy };
                parameters[2] = new SqlParameter("@SerchValue", SqlDbType.VarChar, 255) { Value = serchValue };
                //return data
                dt = BaseDAL.GetData("BO_Users_GetUsersByAuthUserGroup", parameters);
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable GetUsersByDealerKey(string dealerKey)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                dt = BaseDAL.GetData("BO_Users_GetUsersByDealerKey", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        #endregion
        
        #region ' Custom methods '
        /// <summary>
        /// Returns a user by user's name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static DataTable GetUserByUserName(string userName)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserName", SqlDbType.VarChar, 50) { Value = userName };
                //return data
                dt = BaseDAL.GetData("BO_AuthUser_GetUserByUserName", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Returns user's group ID by user's ID 
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static DataTable GetGroupIDByUserID(int userid)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserID", SqlDbType.Int) { Value = userid };
                //return data
                dt = BaseDAL.GetData("BO_AuthUser_GetGroupIDByUserID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Returns user's sub group dealer type ID by user's ID
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static DataTable GetSubGroupDealerTypeIDByUserID(int userid)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserID", SqlDbType.Int) { Value = userid };
                //return data
                dt =  BaseDAL.GetData("BO_AuthUser_GetDealerTypeIDByUserID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable UpdateUser(int userID, string userName, string password, string dealerKey, int groupID, int dealerGroupID, bool isActive)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@UserID", SqlDbType.Int) { Value = userID };
                parameters[1] = new SqlParameter("@UserName", SqlDbType.VarChar, 50) { Value = userName };
                parameters[2] = new SqlParameter("@Password", SqlDbType.VarChar, 50) { Value = password };
                parameters[3] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[4] = new SqlParameter("@GroupId", SqlDbType.Int) { Value = groupID };
                parameters[5] = new SqlParameter("@DealerGroupId", SqlDbType.Int) { Value = dealerGroupID };
                parameters[6] = new SqlParameter("@IsActive", SqlDbType.Bit) { Value = isActive };
                //return data
                dt = BaseDAL.GetData("BO_Users_Update", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable InsertUser(string userName, string password, string dealerKey, int groupID, int dealerGroupID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@UserName", SqlDbType.VarChar, 50) { Value = userName };
                parameters[1] = new SqlParameter("@Password", SqlDbType.VarChar, 50) { Value = password };
                parameters[2] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[3] = new SqlParameter("@GroupId", SqlDbType.Int) { Value = groupID };
                parameters[4] = new SqlParameter("@DealerGroupId", SqlDbType.Int) { Value = dealerGroupID };
                //return data
                return BaseDAL.GetData("BO_Users_Insert", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable DeleteUser(int userID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserID", SqlDbType.Int) { Value = userID };
                //return data
                dt = BaseDAL.GetData("BO_Users_Delete", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        #endregion

        #region ' Additional Methods '
        public static DataTable InsertLoggedUserIPData(string userIP, int userID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@UserIP", SqlDbType.VarChar, 15) { Value = userIP };
                parameters[1] = new SqlParameter("@UserID", SqlDbType.Int) { Value = userID};
                //return data
                return BaseDAL.GetData("BO_Users_InsertLoggedUserIPData", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable DeleteLoggedUserIPData(string userIP)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserIP", SqlDbType.VarChar, 15) { Value = userIP };
                //return data
                dt = BaseDAL.GetData("BO_Users_DeleteLoggedUserIPData", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable GetUserBySavedUserIP(string userIP)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserIP", SqlDbType.VarChar, 15) { Value = userIP };
                //return data
                dt = BaseDAL.GetData("BO_Users_GetUserBySavedUserIP", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        #endregion
    }
}