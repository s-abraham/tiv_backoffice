﻿using System;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class AppraiseDAL
    {
        #region ' Castom Methods '
        public static DataTable GetAppraiseDateCreatedByAprID(int aprID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AprID", SqlDbType.Int) { Value = aprID };
                //return data
                dt = BaseDAL.GetData("BO_Appraise_GetDateCreated", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable GetAprContactByAprID(int aprID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AprID", SqlDbType.Int) { Value = aprID };
                //return data
                dt = BaseDAL.GetData("BO_AprContact_GetAprContact", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        public static DataTable GetEmailID(int aprID, int num)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@AprID", SqlDbType.Int) { Value = aprID };
                parameters[1] = new SqlParameter("@Num", SqlDbType.Int) { Value = num };
                //return data
                dt = BaseDAL.GetData("BO_Appraise_GetEmailID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        #endregion
    }
}