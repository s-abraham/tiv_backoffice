﻿using System;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class DealerHeaderTextDAL
    {
        #region ' Collection Methods '
        /// <summary>
        /// Returns all dealer headers
        /// </summary>
        /// <returns></returns>
        public static DataTable GetAllDealerHeaders(string dealerKey)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetAllDealerHeaders", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        #endregion

        #region ' Custom Methods '
        public static void UpdateDealerHeader(string dealerKey, int pageNo, string lang, string headerText, bool isActive)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@PageNo", SqlDbType.Int) { Value = pageNo };
                parameters[2] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                parameters[3] = new SqlParameter("@HeaderText", SqlDbType.Text) { Value = headerText };
                parameters[4] = new SqlParameter("@Active", SqlDbType.Bit) { Value = isActive };
                //execute stored procedure
                BaseDAL.ExecuteSP("BO_Dealer_UpdateDealerHeader", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }
        public static DataTable GetAllowedCompanyDealers()
        {
            var dt = new DataTable();
            try
            {
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetAllowedCompanyDealers");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        #endregion
    }
}
