﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class DealerDAL
    {
        #region ' Constructors '

        #endregion

        #region ' Collection Methods '

        /// <summary>
        /// Returns all non video dealers
        /// </summary>
        /// <param name="authUserGroupID"></param>
        /// <returns></returns>
        public static DataTable GetNonVideoDealers(int authUserGroupID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AuthUserGroupID", SqlDbType.Int) { Value = authUserGroupID };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetNonVideoDealers", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Returns all allowed video dealers
        /// </summary>
        /// <param name="authUserGroupID"></param>
        /// <returns></returns>
        public static DataTable GetAllowedVideoDealers(int authUserGroupID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AuthUserGroupID", SqlDbType.Int) { Value = authUserGroupID };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetAllowedVideoDealers", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Set dealer as allowed video
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <returns></returns>
        public static void AllowVideo(string dealerKey)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_AllowVideoForDealer", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        /// <summary>
        /// Set dealer as allowed video
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <returns></returns>
        public static void DenyVideo(string dealerKey)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_DenyVideoForDealer", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }
        /// <summary>
        /// Returns all selected dealers by dealer group ID
        /// </summary>
        /// <param name="dealerGroupID"></param>
        /// <returns></returns>
        public static DataTable GetAllSelectedDealers(int dealerGroupID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerGroupID", SqlDbType.Int) { Value = dealerGroupID };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetAllSelectedDealers", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Returns all free dealers by dealer group ID
        /// </summary>
        /// <param name="dealerGroupID"></param>
        /// <returns></returns>
        public static DataTable GetAllFreeDealers(int dealerGroupID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerTypeID", SqlDbType.Int) { Value = dealerGroupID };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetAllFreeDealers", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Returns Dealer Name by Dealer Key
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetDealerNameByPrimaryKey(string dealerKey)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetDealerNameByPrimaryKey", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable GetDraftAllDealers()
        {
            DataTable DraftDealers = new DataTable();
            try
            {
                DraftDealers = BaseDAL.GetData("BO_Dealer_GetAllDraftDealers");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return DraftDealers;
        }

        public static DataTable GetBlacklistedEmails()
        {
            DataTable Emails = new DataTable();
            try
            {
                Emails = BaseDAL.GetData("BO_Dealer_GetAllBlacklistedEmails");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return Emails;
        }

        public static Int32 BlackListEmail(string Email)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Email", SqlDbType.NVarChar) { Value = Email };
                //execute query
                BaseDAL.ExecuteSPWithoutLogging("BO_Dealer_BlackListEmail", parameters);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("PRIMARY KEY"))
                {
                    return 2;
                }
                ExceptionHandler.HandleException(ref ex, 0);
                return 0;
            }

            return 1;
        }

        public static void UnBlackListEmail(string Email)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Email", SqlDbType.NVarChar) { Value = Email };
                //execute query
                BaseDAL.ExecuteSP("BO_Dealer_UnBlackListEmail", parameters);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, 0);
            }
        }

        public static DataTable GetImages(string dealerKey, string lang)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetImages", parameters);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, 0);
            }
            return dt;
        }

        /// <summary>
        /// returns image file for dealer by imageID
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <param name="imageID"></param>
        /// <returns></returns>
        public static DataTable GetImageFile(string dealerKey, int imageID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@ImageID", SqlDbType.Int) { Value = imageID };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetImageFile", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Returns the dealer offer text
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <returns></returns>
        public static DataSet GetOfferText(string dealerKey)
        {
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                ds = BaseDAL.GetDataSet("BO_Dealer_GetOfferText", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        /// <summary>
        /// Returns the dealer's header for FaceBook
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <returns></returns>
        public static DataSet GetFaceBookHeader(string dealerKey)
        {
            var ds = new DataSet();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                ds = BaseDAL.GetDataSet("BO_Dealer_GetFacebookHeader", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return ds;
        }

        /// <summary>
        /// Returns the date range for dealer offer text
        /// </summary>
        /// <param name="dealerKey"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetOfferTextDate(string dealerKey)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetOfferTextDate", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static void UpdateOfferTextDate(string dealerKey, DateTime startDate, DateTime stopdate, bool enabled)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[4];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@StartDate", SqlDbType.SmallDateTime) { Value = startDate };
                parameters[2] = new SqlParameter("@EndDate", SqlDbType.SmallDateTime) { Value = stopdate };
                parameters[3] = new SqlParameter("@Enabled", SqlDbType.Bit) { Value = enabled };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_UpdateOfferTextDate", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void DeleteDraftDealer(Int32 DealerID)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerID", SqlDbType.Int) { Value = DealerID };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_DeleteDraftDealer", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void DeleteLeadProvider(String ProviderName)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ProviderName", SqlDbType.VarChar) { Value = ProviderName };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_DeleteLeadProvider", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static Int32 CheckLeadProviderUsage(String ProviderName)
        {
            Int32 Count = 1;
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ProviderName", SqlDbType.VarChar) { Value = ProviderName };
                //return data
                Count = BaseDAL.ExecuteWithResult("BO_Dealer_CheckLeadProviderUsage", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return Count;
        }

        public static void AddLeadProvider(String provider)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ProviderName", SqlDbType.VarChar) { Value = provider };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_AddLeadProvider", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void EnableDealerImageDate(string dealerKey, string lang, int imageID)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                parameters[2] = new SqlParameter("@ImageID", SqlDbType.Int) { Value = imageID };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_EnableImageDate", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void DeleteDealerImage(string dealerKey, string lang, string imageID)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                parameters[2] = new SqlParameter("@ImageID", SqlDbType.VarChar, 255) { Value = imageID };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_DeleteImage", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void UpdateDealerImageDate(string dealerKey, string lang, int imageID, DateTime sDate, DateTime eDate)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                parameters[2] = new SqlParameter("@ImageID", SqlDbType.Int) { Value = imageID };
                parameters[3] = new SqlParameter("@StartDate", SqlDbType.SmallDateTime) { Value = sDate };
                parameters[4] = new SqlParameter("@EndDate", SqlDbType.SmallDateTime) { Value = eDate };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_UpdateImageDate", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void ActivateDraftDealer(Int32 DealerID)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerID", SqlDbType.Int) { Value = DealerID };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_ActivateDraftDealer", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void SetDealerImageActive(string dealerKey, string lang, string imageID)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                parameters[2] = new SqlParameter("@ImageID", SqlDbType.VarChar, 255) { Value = imageID };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_SetImageActive", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void InsertDealerImage(string dealerKey, string lang, byte[] data)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                parameters[2] = new SqlParameter("@Data", SqlDbType.Image) { Value = data };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_InsertImage", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void UpdateOfferText(string dealerKey, string lang, string html)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@Language", SqlDbType.VarChar, 3) { Value = lang };
                parameters[2] = new SqlParameter("@HTML", SqlDbType.NText) { Value = html };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_UpdateOfferText", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void UpdateFaceBookHeader(string dealerKey, string html)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@HTML", SqlDbType.NText) { Value = html };
                //return data
                BaseDAL.ExecuteSP("BO_Dealer_UpdateFacebookHeader", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        /// <summary>
        /// Returns dealers list for specific admin
        /// </summary>
        /// <param name="authUserGroup"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetDealersListByAuthUserGroup(string authUserGroup)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@AuthUserGroup", SqlDbType.VarChar, 50) { Value = authUserGroup };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetDealersListByAuthUserGroup", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }
        /// <summary>
        /// Returns a list of current active dealers
        /// </summary>
        /// <param name="dealerType"></param>
        /// <returns></returns>
        public static DataTable GetDealersListByDealerType(string dealerType)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerType", SqlDbType.Int) { Value = dealerType };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetDealerListByGroupID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        /// <summary>
        /// Returns a list of current active dealers for group dealer admin 
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static DataTable GetDealersListByUserID(string userID)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UserID", SqlDbType.Int) { Value = userID };
                //return data
                dt = BaseDAL.GetData("BO_Dealer_GetDealerListByUserID", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static DataTable GetCreditAppTexts()
        {
            var creditTexts = new DataTable();

            try
            {
                creditTexts = BaseDAL.GetData("GetAllCreditAppTexts");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return creditTexts;
        }

        public static List<String> GetFreeMakes(String dealerKey)
        {
            var freeMakes = new List<String>();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                freeMakes = BaseDAL.GetDataList("GetAllDefaultMakesForDealer", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return freeMakes;
        }

        #region new table based lists

        public static List<String> GetAvailableExcludedUsedCarMakes(String dealerKey)
        {
            var freeMakes = new List<String>();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                freeMakes = BaseDAL.GetDataList("GetAvailableUsedMakesForDealer", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return freeMakes;
        }

        public static List<String> GetExcludedUsedCarMakes(String dealerKey)
        {
            var freeMakes = new List<String>();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                freeMakes = BaseDAL.GetDataList("BO_Dealer_GetExcludedUsedCarMakes", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return freeMakes;
        }

        public static List<String> GetAvailableDefaultMakes(String dealerKey)
        {
            var freeMakes = new List<String>();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                freeMakes = BaseDAL.GetDataList("BO_Dealer_GetAvailableDefaultMakes", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return freeMakes;
        }

        public static List<String> GetDefaultMakes(String dealerKey)
        {
            var freeMakes = new List<String>();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                freeMakes = BaseDAL.GetDataList("BO_Dealer_GetDefaultMakes", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return freeMakes;
        }

        public static List<String> GetAvailableHNIFeeds(String dealerKey)
        {
            var freeMakes = new List<String>();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                freeMakes = BaseDAL.GetDataList("BO_Dealer_GetAvailableHNIFeeds", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return freeMakes;
        }

        public static List<String> GetHNIFeeds(String dealerKey)
        {
            var freeMakes = new List<String>();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                freeMakes = BaseDAL.GetDataList("BO_Dealer_GetHNIFeeds", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return freeMakes;
        }

        //spAssignPrimaryDealerToHNIFeed

        public static void AssignPrimaryDealerToHNIFeed(string hniDealerID, string dealerKey)
        {
            try
            {
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@HNIDealerID", SqlDbType.VarChar, 255) { Value = hniDealerID };
                parameters[1] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                BaseDAL.ExecuteSP("spAssignPrimaryDealerToHNIFeed", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        public static DataTable GetHNIFeedDealerList()
        {            
            try
            {
                return BaseDAL.GetDataSet("spGetHNIFeedPrimaryDealerList").Tables[0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return null;
        }

        public static DataTable GetDealerList()
        {
            try
            {
                return BaseDAL.GetDataSet("spGetDealerList2").Tables[0];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return null;
        }


        #endregion

        //public static List<String> GetFreeHNIDealers(String dealerKey)
        //{
        //    var freeHNIDealers = new List<String>();

        //    try
        //    {
        //        var parameters = new SqlParameter[1];
        //        parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
        //        freeHNIDealers = BaseDAL.GetDataList("GetAllDealershipHNIMappingsForDealer", parameters);
        //    }
        //    catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        //    return freeHNIDealers;
        //}

        public static List<String> GetAllProviders()
        {
            var providers = new List<String>();

            try
            {
                providers = BaseDAL.GetDataList("GetLeadProviders");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return providers;
        }

        public static DataTable GetGalvesCarRanges()
        {
            var carRanges = new DataTable();

            try
            {
                carRanges = BaseDAL.GetData("GetCarsRangeCategories");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return carRanges;
        }

        public static DataTable GetGalvesTruckRanges()
        {
            var truckRanges = new DataTable();

            try
            {
                truckRanges = BaseDAL.GetData("GetTrucksRangeCategories");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return truckRanges;
        }

        public static DataTable LoadDealer(String dealerkey)
        {
            var dealer = new DataTable();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@dealerKey", SqlDbType.VarChar, 50) { Value = dealerkey };
                dealer = BaseDAL.GetData("BO_Dealer_GetDealer", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return dealer;
        }

        public static DataTable LoadDealerFacebookSettings(String dealerkey)
        {
            var Settings = new DataTable();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@dealerKey", SqlDbType.VarChar, 50) { Value = dealerkey };
                Settings = BaseDAL.GetData("BO_Dealer_GetDealerFacebookSettingsBO", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return Settings;
        }

        public static DataTable GetDealerGroupByID(int groupID)
        {
            var group = new DataTable();
            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@GroupID", SqlDbType.VarChar, 50) { Value = groupID };
                group = BaseDAL.GetData("BO_DealerGroup_GetDealerGroupByID", parameters);

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return group;
        }

        public static DataTable GetAllReplacementYears()
        {
            var replacementYears = new DataTable();

            try
            {
                replacementYears = BaseDAL.GetData("GetAllNewReplacementVehicleForModelYear");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return replacementYears;
        }

        public static DataTable GetEditLog(String DealerKey)
        {
            DataTable EditLog = new DataTable();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = DealerKey };
                EditLog = BaseDAL.GetData("BO_Dealer_GetEditResults", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return EditLog;
        }

        public static DataTable GetTriMonthlyLeads(String dealerkey)
        {
            var leads = new DataTable();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@dealerKey", SqlDbType.VarChar, 50) { Value = dealerkey };
                leads = BaseDAL.GetData("GetTriMonthlyLeads", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return leads;
        }

        public static DataTable GetCountAvailableModelsForSpecifiedYear(string year, string makes)
        {
            var countModels = new DataTable();

            try
            {
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@Year", SqlDbType.VarChar, 4) { Value = year };
                parameters[1] = new SqlParameter("@Makes", SqlDbType.VarChar, 1000) { Value = makes };
                countModels = BaseDAL.GetData("BO_Dealer_CheckModelsFoeSpecifiedYear", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return countModels;
        }

        public static DataTable GetNewMakeYears(String SelectedMake)
        {
            DataTable dtYears = new DataTable();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Make", SqlDbType.VarChar, 50) { Value = SelectedMake };
                dtYears = BaseDAL.GetData("BO_Dealer_GetNewMakeYears", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return dtYears;
        }

        public static void SaveAuditResults(String DealerKey, String AuditString, String UserName)
        {
            try
            {
                var parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 255) { Value = DealerKey };
                parameters[1] = new SqlParameter("@AuditString", SqlDbType.VarChar, 1000) { Value = AuditString };
                parameters[2] = new SqlParameter("@Username", SqlDbType.VarChar, 1000) { Value = UserName };
                BaseDAL.ExecuteSP("BO_Dealer_SaveEditResults", parameters);


            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void SaveDealer(DealerHolder dealerToSave)
        {
            try
            {
                var parameters = new SqlParameter[123];

                parameters[0] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);
                parameters[1] = new SqlParameter("@DealerName", dealerToSave.DealerName);
                parameters[2] = new SqlParameter("@Active", dealerToSave.Active);
                parameters[3] = new SqlParameter("@Salesperson", dealerToSave.Salesperson);
                parameters[4] = new SqlParameter("@OriginalSalesPerson", dealerToSave.OriginalSalesPerson);
                parameters[5] = new SqlParameter("@DealerGroup_Id", dealerToSave.GroupID);
                parameters[6] = new SqlParameter("@DealerStreet", dealerToSave.DealerStreet);
                parameters[7] = new SqlParameter("@DealerCity", dealerToSave.DealerCity);
                parameters[8] = new SqlParameter("@DealerState", dealerToSave.DealerState);
                parameters[9] = new SqlParameter("@DealerZip", dealerToSave.DealerZip);
                parameters[10] = new SqlParameter("@DealerAutoGroup", dealerToSave.DealerAutoGroup);
                parameters[11] = new SqlParameter("@DealerContactEmail", dealerToSave.DealerContactEmail);
                parameters[12] = new SqlParameter("@DealerContactName", dealerToSave.DealerContactName);
                parameters[13] = new SqlParameter("@DealerContactPhone", dealerToSave.DealerContactPhone);
                parameters[14] = new SqlParameter("@AppointmentEmail", dealerToSave.AppointmentEmail);
                parameters[15] = new SqlParameter("@DealerContactPhone2", dealerToSave.DealerContactPhone2);
                parameters[16] = new SqlParameter("@DealerContactFax", dealerToSave.DealerContactFax);
                parameters[17] = new SqlParameter("@DealerTitle", dealerToSave.DealerTitle);
                parameters[18] = new SqlParameter("@SecondDealerContactName", dealerToSave.SecondDealerContactName);
                parameters[19] = new SqlParameter("@SecondDealerContactEmail", dealerToSave.SecondDealerContactEmail);
                parameters[20] = new SqlParameter("@SecondDealerContactPhone", dealerToSave.SecondDealerContactPhone);
                parameters[21] = new SqlParameter("@SecondDealerContactPhone2", dealerToSave.SecondDealerContactPhone2);
                parameters[22] = new SqlParameter("@SecondDealerContactFax", dealerToSave.SecondDealerContactFax);
                parameters[23] = new SqlParameter("@SecondDealerTitle", dealerToSave.SecondDealerTitle);
                parameters[24] = new SqlParameter("@DealerWebSite", dealerToSave.DealerWebSite);
                parameters[25] = new SqlParameter("@CreditApp", dealerToSave.CreditApp);
                parameters[26] = new SqlParameter("@CreditEnabled", dealerToSave.CreditEnabled);
                parameters[27] = new SqlParameter("@CreditAppText_Id", dealerToSave.CreditMsg);
                parameters[28] = new SqlParameter("@InvUsed", dealerToSave.InvUsed);
                parameters[29] = new SqlParameter("@InvUsedEnabled", dealerToSave.InvUsedEnabled);
                parameters[30] = new SqlParameter("@InvNew", dealerToSave.InvNew);
                parameters[31] = new SqlParameter("@InvNewEnabled", dealerToSave.InvNewEnabled);
                parameters[32] = new SqlParameter("@DealerPublicSite", dealerToSave.DealerPublicSite);
                parameters[33] = new SqlParameter("@InstantOptionPricing", dealerToSave.InstantOptionPricing);
                parameters[34] = new SqlParameter("@EmailAppraisal", dealerToSave.EmailAppraisal);
                parameters[35] = new SqlParameter("@FourStep", dealerToSave.FourStep);
                parameters[36] = new SqlParameter("@NewSelection", dealerToSave.NewSelection);
                parameters[37] = new SqlParameter("@ShowAmountOwed", dealerToSave.ShowAmountOwed);
                parameters[38] = new SqlParameter("@ShowIncentives", dealerToSave.ShowIncentives);
                parameters[39] = new SqlParameter("@ShowMonthlyPayment", dealerToSave.ShowMonthlyPayment);
                parameters[40] = new SqlParameter("@UnsureOption", dealerToSave.UnsureOption);
                parameters[41] = new SqlParameter("@RetailPricing", dealerToSave.RetailPricing);
                parameters[42] = new SqlParameter("@TradeImageUpload", dealerToSave.TradeImageUpload);
                parameters[43] = new SqlParameter("@TradeVideoUpload", dealerToSave.TradeVideoUpload);
                parameters[44] = new SqlParameter("@GoogleUrchin", dealerToSave.GoogleUrchin);
                parameters[45] = new SqlParameter("@AppraisalExpiration", dealerToSave.AppraisalExpiration);
                parameters[46] = new SqlParameter("@Comments", dealerToSave.Comments);
                parameters[47] = new SqlParameter("@GalvesRangeCategory_Id", dealerToSave.GalvesRangeCategory);
                parameters[48] = new SqlParameter("@TruckGalvesRangeCategory_Id", dealerToSave.TruckGalvesRangeCategory);
                parameters[49] = new SqlParameter("@Language", dealerToSave.Language);
                parameters[50] = new SqlParameter("@NADABranding", dealerToSave.NADABranding);
                parameters[51] = new SqlParameter("@DataProvider", dealerToSave.DataProvider);
                parameters[52] = new SqlParameter("@Theme", dealerToSave.Theme);
                parameters[53] = new SqlParameter("@OperatingHours", dealerToSave.OperatingHours);
                parameters[54] = new SqlParameter("@HideCondition", dealerToSave.HideCondition);
                parameters[55] = new SqlParameter("@HideEmailTradeIn", dealerToSave.HideEmailTradeIn);
                parameters[56] = new SqlParameter("@EmailTitle", dealerToSave.EmailTitle);
                parameters[57] = new SqlParameter("@HideSpecialOffer", dealerToSave.HideSpecialOffer);
                parameters[58] = new SqlParameter("@HideAddress", dealerToSave.HideAddress);
                parameters[59] = new SqlParameter("@WebsiteCompany", dealerToSave.WebsiteCompany);
                parameters[60] = new SqlParameter("@WebsiteContactName", dealerToSave.WebsiteContactName);
                parameters[61] = new SqlParameter("@WebsiteContactPhone", dealerToSave.WebsiteContactPhone);
                parameters[62] = new SqlParameter("@WebsiteContactEmail", dealerToSave.WebsiteContactEmail);
                parameters[63] = new SqlParameter("@LeadProvider", dealerToSave.LeadProvider);
                parameters[64] = new SqlParameter("@LeadEmail1", dealerToSave.LeadEmail);
                parameters[65] = new SqlParameter("@LeadEmail2", dealerToSave.LeadEmail2);
                parameters[66] = new SqlParameter("@LeadEmail3", dealerToSave.LeadEmail3);
                parameters[67] = new SqlParameter("@LeadEmail4", dealerToSave.LeadEmail4);
                parameters[68] = new SqlParameter("@LeadFormat", dealerToSave.LeadFormat);
                parameters[69] = new SqlParameter("@LeadFormat2", dealerToSave.LeadFormat2);
                parameters[70] = new SqlParameter("@LeadFormat3", dealerToSave.LeadFormat3);
                parameters[71] = new SqlParameter("@LeadFormat4", dealerToSave.LeadFormat4);
                parameters[72] = new SqlParameter("@LeadContactName", dealerToSave.LeadContactName);
                parameters[73] = new SqlParameter("@LeadContactEmail", dealerToSave.LeadContactEmail);
                parameters[74] = new SqlParameter("@LeadContactPhone", dealerToSave.LeadContactPhone);
                parameters[75] = new SqlParameter("@LeadContactFax", dealerToSave.LeadContactFax);
                parameters[76] = new SqlParameter("@Billable", dealerToSave.Billable);
                parameters[77] = new SqlParameter("@AdminAutoAppraisalCost", dealerToSave.AdminAutoAppraisalCost);
                parameters[78] = new SqlParameter("@BillingInfo", dealerToSave.BillingInfo);
                parameters[79] = new SqlParameter("@BillingLastUpdated", dealerToSave.BillingLastUpdated == DateTime.MinValue ? DBNull.Value : (object)dealerToSave.BillingLastUpdated);
                parameters[80] = new SqlParameter("@DateCreated", dealerToSave.DateCreated == DateTime.MinValue ? DBNull.Value : (object)dealerToSave.DateCreated);
                parameters[81] = new SqlParameter("@SystemBillingDate", dealerToSave.SystemBillingDate == DateTime.MinValue ? DBNull.Value : (object)dealerToSave.SystemBillingDate);
                parameters[82] = new SqlParameter("@AdminSetupFees", dealerToSave.AdminSetupFees);
                parameters[83] = new SqlParameter("@BillableStartDate", dealerToSave.BillableStartDate == DateTime.MinValue ? DBNull.Value : (object)dealerToSave.BillableStartDate);
                parameters[84] = new SqlParameter("@BillingExplanation", dealerToSave.BillingExplanation);
                parameters[85] = new SqlParameter("@DealerDefaultMakes", dealerToSave.DealerDefaultMakes);
                parameters[86] = new SqlParameter("@HNIDealers", dealerToSave.HNIDealers);
                parameters[87] = new SqlParameter("@DealerType_Id", dealerToSave.DealerType);
                parameters[88] = new SqlParameter("@AverageValueRange_VUnderPercent1", dealerToSave.AverageValueRange_VUnderPercent1);
                parameters[89] = new SqlParameter("@AverageValueRange_VUnderAmount1", dealerToSave.AverageValueRange_VUnderAmount1);
                parameters[90] = new SqlParameter("@AverageValueRange_VOverPercent1", dealerToSave.AverageValueRange_VOverPercent1);
                parameters[91] = new SqlParameter("@AverageValueRange_VOverAmount1", dealerToSave.AverageValueRange_VOverAmount1);
                parameters[92] = new SqlParameter("@AverageValueRange_VOverTypeSelection1", dealerToSave.AverageValueRange_VOverTypeSelection1);
                parameters[93] = new SqlParameter("@AverageValueRange_VUnderTypeSelection1", dealerToSave.AverageValueRange_VUnderTypeSelection1);
                parameters[94] = new SqlParameter("@AverageValueRange_VUnderPercent2", dealerToSave.AverageValueRange_VUnderPercent2);
                parameters[95] = new SqlParameter("@AverageValueRange_VUnderAmount2", dealerToSave.AverageValueRange_VUnderAmount2);
                parameters[96] = new SqlParameter("@AverageValueRange_VOverPercent2", dealerToSave.AverageValueRange_VOverPercent2);
                parameters[97] = new SqlParameter("@AverageValueRange_VOverAmount2", dealerToSave.AverageValueRange_VOverAmount2);
                parameters[98] = new SqlParameter("@AverageValueRange_VOverTypeSelection2", dealerToSave.AverageValueRange_VOverTypeSelection2);
                parameters[99] = new SqlParameter("@AverageValueRange_VUnderTypeSelection2", dealerToSave.AverageValueRange_VUnderTypeSelection2);

                parameters[100] = new SqlParameter("@AverageValueRange_VUnderPercent3", dealerToSave.AverageValueRange_VUnderPercent3);
                parameters[101] = new SqlParameter("@AverageValueRange_VUnderAmount3", dealerToSave.AverageValueRange_VUnderAmount3);
                parameters[102] = new SqlParameter("@AverageValueRange_VOverPercent3", dealerToSave.AverageValueRange_VOverPercent3);
                parameters[103] = new SqlParameter("@AverageValueRange_VOverAmount3", dealerToSave.AverageValueRange_VOverAmount3);
                parameters[104] = new SqlParameter("@AverageValueRange_VOverTypeSelection3", dealerToSave.AverageValueRange_VOverTypeSelection3);
                parameters[105] = new SqlParameter("@AverageValueRange_VUnderTypeSelection3", dealerToSave.AverageValueRange_VUnderTypeSelection3);

                parameters[106] = new SqlParameter("@DealerModelYears_NewModelYears", dealerToSave.NewModelYears);
                parameters[107] = new SqlParameter("@HideTradeIn", dealerToSave.HideTradeIn);
                parameters[108] = new SqlParameter("@HyperLead", dealerToSave.HyperLead);
                parameters[109] = new SqlParameter("@TaxIncentive", dealerToSave.TaxIncentive);
                parameters[110] = new SqlParameter("@DateDeactivated", dealerToSave.DateDeactivated);
                parameters[111] = new SqlParameter("@Defaultbackground", dealerToSave.Defaultbackground);
                parameters[112] = new SqlParameter("@WebsiteComments", dealerToSave.WebsiteComments);
                parameters[113] = new SqlParameter("@SocialMedia", dealerToSave.SocialMedia);
                parameters[114] = new SqlParameter("@AppID", dealerToSave.AppID);
                parameters[115] = new SqlParameter("@AppSecret", dealerToSave.AppSecret);
                parameters[116] = new SqlParameter("@APIKey", dealerToSave.APIKey);
                parameters[117] = new SqlParameter("@WallURL", dealerToSave.WallURL);
                parameters[118] = new SqlParameter("@DisableLeadEmails", dealerToSave.DisableLeadEmails);

                parameters[119] = new SqlParameter("@ProductType", dealerToSave.DealerProductType);
                parameters[120] = new SqlParameter("@EnableExcludedUsedCarMakes", dealerToSave.EnableExcludedUsedCarMakes);
                parameters[121] = new SqlParameter("@EnableValueRanges", dealerToSave.EnableValueRanges);
                parameters[122] = new SqlParameter("@EnableWidget", dealerToSave.EnableWidget);
                parameters[122] = new SqlParameter("@IsReseller", dealerToSave.IsReseller);
                BaseDAL.ExecuteSP("BO_Dealer_SaveDealer", parameters);

                //save the excluded used car makes
                BaseDAL.ExecuteSP("BO_Dealer_ClearExcludedUsedCarMakes", new SqlParameter[] { new SqlParameter("@DealerKey", dealerToSave.DealerKey) });
                if (dealerToSave.ExcludedUsedCarMakes != null)
                {
                    foreach (string make in dealerToSave.ExcludedUsedCarMakes)
                    {
                        BaseDAL.ExecuteSP("BO_Dealer_AddExcludedUsedCarMake", new SqlParameter[] { new SqlParameter("@DealerKey", dealerToSave.DealerKey), new SqlParameter("@Make", make) });
                    }
                }

                //save the default makes
                BaseDAL.ExecuteSP("BO_Dealer_ClearDefaultMakes", new SqlParameter[] { new SqlParameter("@DealerKey", dealerToSave.DealerKey) });
                foreach (string make in dealerToSave.DefaultMakes)
                {
                    BaseDAL.ExecuteSP("BO_Dealer_AddDefaultMake", new SqlParameter[] { new SqlParameter("@DealerKey", dealerToSave.DealerKey), new SqlParameter("@Make", make) });
                }

                //save the hni feeds
                BaseDAL.ExecuteSP("BO_Dealer_ClearHNIFeeds", new SqlParameter[] { new SqlParameter("@DealerKey", dealerToSave.DealerKey) });
                foreach (string item in dealerToSave.HNIFeeds)
                {
                    BaseDAL.ExecuteSP("BO_Dealer_AddHNIFeed", new SqlParameter[] { new SqlParameter("@DealerKey", dealerToSave.DealerKey), new SqlParameter("@Dealer_ID", item) });
                }


            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        public static void SaveDealerInfoTab(DealerHolder dealerToSave)
        {
            try
            {
                var parameters = new SqlParameter[26];

                parameters[0] = new SqlParameter("@DealerName", dealerToSave.DealerName);
                parameters[1] = new SqlParameter("@Active", dealerToSave.Active);
                parameters[2] = new SqlParameter("@Salesperson", dealerToSave.Salesperson);
                parameters[3] = new SqlParameter("@OriginalSalesPerson", dealerToSave.OriginalSalesPerson);
                parameters[4] = new SqlParameter("@DealerGroup_Id", dealerToSave.GroupID);
                parameters[5] = new SqlParameter("@DealerStreet", dealerToSave.DealerStreet);
                parameters[6] = new SqlParameter("@DealerCity", dealerToSave.DealerCity);
                parameters[7] = new SqlParameter("@DealerState", dealerToSave.DealerState);
                parameters[8] = new SqlParameter("@DealerZip", dealerToSave.DealerZip);
                parameters[9] = new SqlParameter("@DealerAutoGroup", dealerToSave.DealerAutoGroup);
                parameters[10] = new SqlParameter("@DealerContactEmail", dealerToSave.DealerContactEmail);
                parameters[11] = new SqlParameter("@DealerContactName", dealerToSave.DealerContactName);
                parameters[12] = new SqlParameter("@DealerContactPhone", dealerToSave.DealerContactPhone);
                parameters[13] = new SqlParameter("@AppointmentEmail", dealerToSave.AppointmentEmail);
                parameters[14] = new SqlParameter("@DealerContactPhone2", dealerToSave.DealerContactPhone2);
                parameters[15] = new SqlParameter("@DealerContactFax", dealerToSave.DealerContactFax);
                parameters[16] = new SqlParameter("@DealerTitle", dealerToSave.DealerTitle);
                parameters[17] = new SqlParameter("@SecondDealerContactName", dealerToSave.SecondDealerContactName);
                parameters[18] = new SqlParameter("@SecondDealerContactEmail", dealerToSave.SecondDealerContactEmail);
                parameters[19] = new SqlParameter("@SecondDealerContactPhone", dealerToSave.SecondDealerContactPhone);
                parameters[20] = new SqlParameter("@SecondDealerContactPhone2", dealerToSave.SecondDealerContactPhone2);
                parameters[21] = new SqlParameter("@SecondDealerContactFax", dealerToSave.SecondDealerContactFax);
                parameters[22] = new SqlParameter("@SecondDealerTitle", dealerToSave.SecondDealerTitle);
                parameters[23] = new SqlParameter("@DealerWebSite", dealerToSave.DealerWebSite);
                parameters[24] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);
                parameters[25] = new SqlParameter("@DateDeactivated", dealerToSave.DateDeactivated);

                BaseDAL.ExecuteSP("BO_Dealer_UpdateInfoPanel", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        public static void SaveDataTab(DealerHolder dealerToSave)
        {
            try
            {
                var parameters = new SqlParameter[23];

                parameters[0] = new SqlParameter("@GalvesRangeCategory_Id", dealerToSave.GalvesRangeCategory);
                parameters[1] = new SqlParameter("@TruckGalvesRangeCategory_Id", dealerToSave.TruckGalvesRangeCategory);
                parameters[2] = new SqlParameter("@DataProvider", dealerToSave.DataProvider);
                parameters[3] = new SqlParameter("@AverageValueRange_VUnderPercent1", dealerToSave.AverageValueRange_VUnderPercent1);
                parameters[4] = new SqlParameter("@AverageValueRange_VUnderAmount1", dealerToSave.AverageValueRange_VUnderAmount1);
                parameters[5] = new SqlParameter("@AverageValueRange_VOverPercent1", dealerToSave.AverageValueRange_VOverPercent1);
                parameters[6] = new SqlParameter("@AverageValueRange_VOverAmount1", dealerToSave.AverageValueRange_VOverAmount1);
                parameters[7] = new SqlParameter("@AverageValueRange_VOverTypeSelection1", dealerToSave.AverageValueRange_VOverTypeSelection1);
                parameters[8] = new SqlParameter("@AverageValueRange_VUnderTypeSelection1", dealerToSave.AverageValueRange_VUnderTypeSelection1);

                parameters[9] = new SqlParameter("@AverageValueRange_VUnderPercent2", dealerToSave.AverageValueRange_VUnderPercent2);
                parameters[10] = new SqlParameter("@AverageValueRange_VUnderAmount2", dealerToSave.AverageValueRange_VUnderAmount2);
                parameters[11] = new SqlParameter("@AverageValueRange_VOverPercent2", dealerToSave.AverageValueRange_VOverPercent2);
                parameters[12] = new SqlParameter("@AverageValueRange_VOverAmount2", dealerToSave.AverageValueRange_VOverAmount2);
                parameters[13] = new SqlParameter("@AverageValueRange_VOverTypeSelection2", dealerToSave.AverageValueRange_VOverTypeSelection2);
                parameters[14] = new SqlParameter("@AverageValueRange_VUnderTypeSelection2", dealerToSave.AverageValueRange_VUnderTypeSelection2);

                parameters[15] = new SqlParameter("@AverageValueRange_VUnderPercent3", dealerToSave.AverageValueRange_VUnderPercent3);
                parameters[16] = new SqlParameter("@AverageValueRange_VUnderAmount3", dealerToSave.AverageValueRange_VUnderAmount3);
                parameters[17] = new SqlParameter("@AverageValueRange_VOverPercent3", dealerToSave.AverageValueRange_VOverPercent3);
                parameters[18] = new SqlParameter("@AverageValueRange_VOverAmount3", dealerToSave.AverageValueRange_VOverAmount3);
                parameters[19] = new SqlParameter("@AverageValueRange_VOverTypeSelection3", dealerToSave.AverageValueRange_VOverTypeSelection3);
                parameters[20] = new SqlParameter("@AverageValueRange_VUnderTypeSelection3", dealerToSave.AverageValueRange_VUnderTypeSelection3);

                parameters[21] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);
                parameters[22] = new SqlParameter("@DealerType_Id", dealerToSave.DealerType);

                BaseDAL.ExecuteSP("BO_Dealer_UpdateDataPanel", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        //public static void SaveCustomizationTab(DealerHolder dealerToSave)
        //{
        //    try
        //    {
        //        var parameters = new SqlParameter[33];

        //        parameters[0] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);
        //        parameters[1] = new SqlParameter("@CreditApp", dealerToSave.CreditApp);
        //        parameters[2] = new SqlParameter("@CreditEnabled", dealerToSave.CreditEnabled);
        //        parameters[3] = new SqlParameter("@CreditAppText_Id", dealerToSave.CreditMsg);
        //        parameters[4] = new SqlParameter("@InvUsed", dealerToSave.InvUsed);
        //        parameters[5] = new SqlParameter("@InvUsedEnabled", dealerToSave.InvUsedEnabled);
        //        parameters[6] = new SqlParameter("@InvNew", dealerToSave.InvNew);
        //        parameters[7] = new SqlParameter("@InvNewEnabled", dealerToSave.InvNewEnabled);
        //        parameters[8] = new SqlParameter("@InstantOptionPricing", dealerToSave.InstantOptionPricing);
        //        parameters[9] = new SqlParameter("@EmailAppraisal", dealerToSave.EmailAppraisal);
        //        parameters[10] = new SqlParameter("@FourStep", dealerToSave.FourStep);
        //        parameters[11] = new SqlParameter("@NewSelection", dealerToSave.NewSelection);
        //        parameters[12] = new SqlParameter("@ShowAmountOwed", dealerToSave.ShowAmountOwed);
        //        parameters[13] = new SqlParameter("@ShowIncentives", dealerToSave.ShowIncentives);
        //        parameters[14] = new SqlParameter("@ShowMonthlyPayment", dealerToSave.ShowMonthlyPayment);
        //        parameters[15] = new SqlParameter("@UnsureOption", dealerToSave.UnsureOption);
        //        parameters[16] = new SqlParameter("@RetailPricing", dealerToSave.RetailPricing);
        //        parameters[17] = new SqlParameter("@TradeImageUpload", dealerToSave.TradeImageUpload);
        //        parameters[18] = new SqlParameter("@TradeVideoUpload", dealerToSave.TradeVideoUpload);
        //        parameters[19] = new SqlParameter("@GoogleUrchin", dealerToSave.GoogleUrchin);
        //        parameters[20] = new SqlParameter("@AppraisalExpiration", dealerToSave.AppraisalExpiration);
        //        parameters[21] = new SqlParameter("@Comments", dealerToSave.Comments);
        //        parameters[22] = new SqlParameter("@Language", dealerToSave.Language);
        //        parameters[23] = new SqlParameter("@NADABranding", dealerToSave.NADABranding);
        //        parameters[24] = new SqlParameter("@Theme", dealerToSave.Theme);
        //        parameters[25] = new SqlParameter("@DealerDefaultMakes", dealerToSave.DealerDefaultMakes);
        //        parameters[26] = new SqlParameter("@HNIDealers", dealerToSave.HNIDealers);
        //        parameters[27] = new SqlParameter("@HideTradeIn", dealerToSave.HideTradeIn);
        //        parameters[28] = new SqlParameter("@DealerModelYears_NewModelYears", dealerToSave.NewModelYears);
        //        parameters[29] = new SqlParameter("@HyperLead", dealerToSave.HyperLead);
        //        parameters[30] = new SqlParameter("@TaxIncentive", dealerToSave.TaxIncentive);
        //        parameters[31] = new SqlParameter("@Defaultbackground", dealerToSave.Defaultbackground);
        //        parameters[32] = new SqlParameter("@SocialMedia", dealerToSave.SocialMedia);

        //        BaseDAL.ExecuteSP("BO_Dealer_UpdateCutomizationPanel", parameters);
        //    }
        //    catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        //}

        public static void SaveLeadTab(DealerHolder dealerToSave)
        {
            try
            {
                var parameters = new SqlParameter[15];

                parameters[0] = new SqlParameter("@LeadProvider", dealerToSave.LeadProvider);
                parameters[1] = new SqlParameter("@LeadEmail1", dealerToSave.LeadEmail);
                parameters[2] = new SqlParameter("@LeadEmail2", dealerToSave.LeadEmail2);
                parameters[3] = new SqlParameter("@LeadEmail3", dealerToSave.LeadEmail3);
                parameters[4] = new SqlParameter("@LeadEmail4", dealerToSave.LeadEmail4);
                parameters[5] = new SqlParameter("@LeadFormat", dealerToSave.LeadFormat);
                parameters[6] = new SqlParameter("@LeadFormat2", dealerToSave.LeadFormat2);
                parameters[7] = new SqlParameter("@LeadFormat3", dealerToSave.LeadFormat3);
                parameters[8] = new SqlParameter("@LeadFormat4", dealerToSave.LeadFormat4);
                parameters[9] = new SqlParameter("@LeadContactName", dealerToSave.LeadContactName);
                parameters[10] = new SqlParameter("@LeadContactEmail", dealerToSave.LeadContactEmail);
                parameters[11] = new SqlParameter("@LeadContactPhone", dealerToSave.LeadContactPhone);
                parameters[12] = new SqlParameter("@LeadContactFax", dealerToSave.LeadContactFax);
                parameters[13] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);
                parameters[14] = new SqlParameter("@DisableLeadEmails", dealerToSave.DisableLeadEmails);

                BaseDAL.ExecuteSP("BO_Dealer_UpdateLeadPanel", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        public static void SaveBillingTab(DealerHolder dealerToSave)
        {
            try
            {
                var parameters = new SqlParameter[10];

                parameters[0] = new SqlParameter("@Billable", dealerToSave.Billable);
                parameters[1] = new SqlParameter("@AdminAutoAppraisalCost", dealerToSave.AdminAutoAppraisalCost);
                parameters[2] = new SqlParameter("@BillingInfo", dealerToSave.BillingInfo);
                parameters[3] = new SqlParameter("@BillingLastUpdated", dealerToSave.BillingLastUpdated);
                parameters[4] = new SqlParameter("@DateCreated", dealerToSave.DateCreated);
                parameters[5] = new SqlParameter("@SystemBillingDate", dealerToSave.SystemBillingDate);
                parameters[6] = new SqlParameter("@AdminSetupFees", dealerToSave.AdminSetupFees);
                parameters[7] = new SqlParameter("@BillableStartDate", dealerToSave.BillableStartDate);
                parameters[8] = new SqlParameter("@BillingExplanation", dealerToSave.BillingExplanation);
                parameters[9] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);

                BaseDAL.ExecuteSP("BO_Dealer_UpdateBillingPanel", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        public static void SaveFacebookTab(DealerHolder dealerToSave)
        {
            try
            {
                var parameters = new SqlParameter[5];

                parameters[0] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);
                parameters[1] = new SqlParameter("@AppID", dealerToSave.AppID);
                parameters[2] = new SqlParameter("@AppSecret", dealerToSave.AppSecret);
                parameters[3] = new SqlParameter("@APIKey", dealerToSave.APIKey);
                parameters[4] = new SqlParameter("@WallURL", dealerToSave.WallURL);

                BaseDAL.ExecuteSP("BO_Dealer_UpdateFacebooksettings", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        public static void SaveWebsiteTab(DealerHolder dealerToSave)
        {
            try
            {
                var parameters = new SqlParameter[6];

                parameters[0] = new SqlParameter("@WebsiteCompany", dealerToSave.WebsiteCompany);
                parameters[1] = new SqlParameter("@WebsiteContactName", dealerToSave.WebsiteContactName);
                parameters[2] = new SqlParameter("@WebsiteContactPhone", dealerToSave.WebsiteContactPhone);
                parameters[3] = new SqlParameter("@WebsiteContactEmail", dealerToSave.WebsiteContactEmail);
                parameters[4] = new SqlParameter("@DealerKey", dealerToSave.DealerKey);
                parameters[5] = new SqlParameter("@WebsiteCOmments", dealerToSave.WebsiteComments);

                BaseDAL.ExecuteSP("BO_Dealer_UpdateWebsitePanel", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

        }

        public static void UpdateDealerGroup(int dealerGroupID, string dealerKey)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@DealerGroupID", SqlDbType.Int) { Value = dealerGroupID };
                parameters[1] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //execute query
                BaseDAL.ExecuteSP("BO_Dealer_UpdateDealerGroup", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void ActivateDealerByMainAdmin(string dealerKey)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                //execute query
                BaseDAL.ExecuteSP("BO_Dealer_ActivateDealerByMainAdmin", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void UpdateDeactivatedDealerData(string dealerKey, string dealerName, string leadEmail1, string leadEmail2, string leadEmail3, string leadEmail4)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[6];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@DealerName", SqlDbType.VarChar, 255) { Value = dealerName };
                parameters[2] = new SqlParameter("@leadEmail1", SqlDbType.VarChar, 100) { Value = leadEmail1 };
                parameters[3] = new SqlParameter("@leadEmail2", SqlDbType.VarChar, 100) { Value = leadEmail2 };
                parameters[4] = new SqlParameter("@leadEmail3", SqlDbType.VarChar, 100) { Value = leadEmail3 };
                parameters[5] = new SqlParameter("@leadEmail4", SqlDbType.VarChar, 100) { Value = leadEmail4 };

                //execute query
                BaseDAL.ExecuteSP("BO_Dealer_UpdateDeactivatedDealerData", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static void DeactivateDealerByMainAdmin(string dealerKey, string dealerName, string leadEmail1, string leadEmail2, string leadEmail3, string leadEmail4)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[6];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                parameters[1] = new SqlParameter("@DealerName", SqlDbType.VarChar, 255) { Value = dealerName };
                parameters[2] = new SqlParameter("@leadEmail1", SqlDbType.VarChar, 100) { Value = leadEmail1 };
                parameters[3] = new SqlParameter("@leadEmail2", SqlDbType.VarChar, 100) { Value = leadEmail2 };
                parameters[4] = new SqlParameter("@leadEmail3", SqlDbType.VarChar, 100) { Value = leadEmail3 };
                parameters[5] = new SqlParameter("@leadEmail4", SqlDbType.VarChar, 100) { Value = leadEmail4 };

                //execute query
                BaseDAL.ExecuteSP("BO_Dealer_DeactivateDealerByMainAdmin", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        public static DataTable IsDealerExists(string dealerKey)
        {
            var dt = new DataTable();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                dt = BaseDAL.GetData("BO_Dealer_IsDealerExists", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return dt;
        }

        public static DataTable IsDeactivated(string dealerKey)
        {
            var dt = new DataTable();

            try
            {
                var parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@DealerKey", SqlDbType.VarChar, 50) { Value = dealerKey };
                dt = BaseDAL.GetData("BO_Dealer_IsDeactivated", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return dt;
        }

        public static DataTable GetAllDeactivatedDealers()
        {
            var dt = new DataTable();

            try
            {
                dt = BaseDAL.GetData("BO_Dealer_GetAllDeactivatedDealers");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return dt;
        }

        public static DataTable GetMatchesFromDeactivatedDealers(string dealerName, string leadEmail1, string leadEmail2, string leadEmail3, string leadEmail4)
        {
            var dt = new DataTable();
            try
            {
                //set input parameters
                var parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@DealerName", SqlDbType.VarChar, 255) { Value = dealerName };
                parameters[1] = new SqlParameter("@leadEmail1", SqlDbType.VarChar, 100) { Value = leadEmail1 };
                parameters[2] = new SqlParameter("@leadEmail2", SqlDbType.VarChar, 100) { Value = leadEmail2 };
                parameters[3] = new SqlParameter("@leadEmail3", SqlDbType.VarChar, 100) { Value = leadEmail3 };
                parameters[4] = new SqlParameter("@leadEmail4", SqlDbType.VarChar, 100) { Value = leadEmail4 };

                //execute query
                dt = BaseDAL.GetData("BO_Dealer_GetMatchesFromDeactivatedDealers", parameters);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            return dt;
        }

        #endregion
    }

    [Serializable]
    public class DealerHolder
    {
        public string DealerKey;
        public string DealerName;
        public string DealerStreet;
        public string DealerCity;
        public string DealerState;
        public string DealerTitle;
        public string DealerZip;
        public string DealerContactName;
        public string DealerContactEmail;
        public string DealerContactPhone;
        public string DealerContactPhone2;
        public string DealerContactFax;
        public string DealerWebSite;
        public int DealerProductType;
        public string DealerPublicSite;
        public string DealerAutoGroup;
        public string DealerLogo;
        public string DealerPromo;
        public string DealerDefaultMakes;
        public int DealerType;
        public string LeadProvider;
        public string LeadFormat;
        public string LeadFormat2;
        public string LeadFormat3;
        public string LeadFormat4;
        public string LeadEmail;
        public string LeadEmail2;
        public string LeadEmail3;
        public string LeadEmail4;
        public string LeadContactName;
        public string LeadContactEmail;
        public string LeadContactPhone;
        public string LeadContactFax;
        public string WebsiteCompany;
        public string WebsiteContactName;
        public string WebsiteContactEmail;
        public string WebsiteContactPhone;
        public DateTime AdminDate;
        public DateTime AdminActivationDate;
        public string AdminDWTProduct;
        public decimal AdminAutoAppraisalCost;
        public decimal AdminSetupFees;
        public bool Active;
        public bool EarlyAppraise;
        public string Theme;
        public bool Billable;
        public DateTime BillableStartDate;
        public DateTime DateUpdated;
        public DateTime DateCreated;
        public String DateDeactivated;
        public int GalvesRangeCategory;
        public bool InstantOptionPricing;
        public int GroupID;
        public string Language;
        public string CreditApp;
        public int CreditMsg;
        public bool CreditEnabled;
        public string InvNew;
        public bool InvNewEnabled;
        public string InvUsed;
        public bool InvUsedEnabled;
        public bool EmailAppraisal;
        public bool FourStep;
        public bool UnsureOption;
        public string GoogleUrchin;
        public bool IgnoreOption;
        public string Comments;
        public string Salesperson;
        public bool HideTradeIn;
        public string EmailTitle;
        public string OperatingHours;
        public bool HideCondition;
        public bool HideEmailTradeIn;
        public bool HideAddress;
        public bool HideSpecialOffer;
        public string SecondDealerTitle;
        public string SecondDealerContactName;
        public string SecondDealerContactEmail;
        public string SecondDealerContactPhone;
        public string SecondDealerContactFax;
        public string SecondDealerContactPhone2;
        public int TruckGalvesRangeCategory;
        public string AppointmentEmail;
        public string OriginalSalesPerson;
        public string DataProvider;
        public bool NewSelection;
        public string HNIDealers;
        public int NADARangeCategory;
        public bool NADABranding;
        public bool RetailPricing;
        public bool TradeImageUpload;
        public bool ShowIncentives;
        public bool ShowMonthlyPayment;
        public bool ShowAmountOwed;
        public int AppraisalExpiration;
        public bool RepaymentCalc;
        public DateTime BillingLastUpdated;
        public DateTime SystemBillingDate;
        public string BillingInfo;
        public string BillingExplanation;
        public bool TradeVideoUpload;

        public string AverageValueRange_VUnderPercent1;
        public string AverageValueRange_VUnderAmount1;
        public string AverageValueRange_VOverPercent1;
        public string AverageValueRange_VOverAmount1;
        public string AverageValueRange_VUnderTypeSelection1;
        public string AverageValueRange_VOverTypeSelection1;

        public string AverageValueRange_VUnderPercent2;
        public string AverageValueRange_VUnderAmount2;
        public string AverageValueRange_VOverPercent2;
        public string AverageValueRange_VOverAmount2;
        public string AverageValueRange_VUnderTypeSelection2;
        public string AverageValueRange_VOverTypeSelection2;

        public string AverageValueRange_VUnderPercent3;
        public string AverageValueRange_VUnderAmount3;
        public string AverageValueRange_VOverPercent3;
        public string AverageValueRange_VOverAmount3;
        public string AverageValueRange_VUnderTypeSelection3;
        public string AverageValueRange_VOverTypeSelection3;

        public int ValueRange;
        public string NewModelYears;
        public bool HyperLead;
        public bool TaxIncentive;
        public string Defaultbackground;
        public string WebsiteComments;
        public bool SocialMedia;

        public string AppID;
        public string AppSecret;
        public string APIKey;
        public string WallURL;
        public bool DisableLeadEmails;

        public bool EnableExcludedUsedCarMakes;
        public List<string> ExcludedUsedCarMakes;
        public List<string> HNIFeeds;
        public List<string> DefaultMakes;
        public bool EnableValueRanges;
        public bool EnableWidget;
        public bool IsReseller;
    }
}