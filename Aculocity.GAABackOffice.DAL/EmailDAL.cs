﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.DAL
{
    public class EmailDAL
    {
        #region ' Castom Methods '
        public static DataTable GetBody(int aprID, string num)
        {
            var dt = new DataTable();
            try
            {
                var sql = new StringBuilder();
                string emailIDLead = string.Empty;
                switch(num)
                {
                    case "1":
                        emailIDLead = "EmailID_Lead";
                        break;
                    case "2":
                        emailIDLead = "EmailID_Lead2";
                        break;
                    case "3":
                        emailIDLead = "EmailID_Lead3";
                        break;
                    case "4":
                        emailIDLead = "EmailID_Lead4";
                        break;
                    case "summary":
                        emailIDLead = "EmailID_Summary";
                        break;
                }

                sql.AppendLine(string.Format("SELECT {0} FROM tblApr WHERE [Id]= {1}", emailIDLead, aprID));
                //return emailIDLead
                int emailID = 0;
                dt = GetData(sql.ToString(), false);
                if (dt.Rows.Count > 0) emailID = (int)dt.Rows[0][0];

                sql = new StringBuilder();
                sql.AppendLine(string.Format("SELECT body FROM Outbox WHERE [id] = {0}", emailID));
                //return data
                dt = GetData(sql.ToString(), true);
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        private static DataTable GetData(string sql, bool isMailServer)
        {
            var dt = new DataTable();
            try
            {
                using (var conn = new SqlConnection(isMailServer ? SettingManager.ConnectionStringToMailServer : SettingManager.ConnectionStringToGalvesDb))
                {
                    using (var cmd = new SqlCommand(sql, conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        conn.Open();
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr != null) dt.Load(dr);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
            return dt;
        }

        public static bool SendMail(MailMessage message)
        {
            try
            {
                //set input parameters
                var parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@profile_name", SqlDbType.VarChar, 1000) { Value = "TIVMAIL" };
                parameters[1] = new SqlParameter("@recipients", SqlDbType.VarChar, 1000) { Value = message.To.ToString() };
                parameters[2] = new SqlParameter("@copy_recipients", SqlDbType.VarChar, 1000) { Value = message.CC.ToString() };
                parameters[3] = new SqlParameter("@blind_copy_recipients", SqlDbType.VarChar, 1000) { Value = message.Bcc.ToString() };
                parameters[4] = new SqlParameter("@from_address", SqlDbType.VarChar, 255) { Value = message.From.ToString() };
                parameters[5] = new SqlParameter("@subject", SqlDbType.VarChar, 500) { Value = message.Subject };
                parameters[6] = new SqlParameter("@body", SqlDbType.Text) { Value = message.Body };

                using (var conn = new SqlConnection(SettingManager.ConnectionStringToMailServer))
                {
                    using (var cmd = new SqlCommand("msdb.dbo.sp_send_dbmail", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddRange(parameters);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, 0);
                throw;
            }
        }

        #endregion
    }
}