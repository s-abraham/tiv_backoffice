﻿using System;
using System.Configuration;

namespace Aculocity.GAABackOffice.Global
{
    public class SettingManager
    {
        public static string ConnectionStringToGalvesDb
        {
            get
            {
                try
                {
                    return ConfigurationManager.ConnectionStrings["GAABackOffice.GalvesDB"].ToString();
                }
                catch{throw new Exception("Property not found in configuration file.");}
            }
        }
        public static string ConnectionStringToMailServer
        {
            get
            {
                try
                {
                    return ConfigurationManager.ConnectionStrings["GAABackOffice.MailServer"].ToString();
                }
                catch{throw new Exception("Property not found in configuration file.");}
            }
        }
        public static int LoginTimeout
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ConfigurationManager.AppSettings["GAABackOffice.LoginTimeout"]);
                }
                catch{throw new Exception("Property not found in configuration file.");}
            }
        }
        public static string AppURL
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["GAABackOffice.AppURL"];
                }
                catch{throw new Exception("Property not found in configuration file.");}
            }
        }
        public static string AppName
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["GAABackOffice.AppName"];
                }
                catch{throw new Exception("Property not found in configuration file.");}
            }
        }
        public static string AppInitialPage
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["AppraisalInitialPage"];
                }
                catch{throw new Exception("Property not found in configuration file.");}
            }
        }
        public static string DefaultImage
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["GAABackOffice.DefaultImage"];
                }
                catch { throw new Exception("Property not found in configuration file."); }
            }
        }
        public static string MailFrom
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["GAABackOffice.MailFrom"];
                }
                catch { throw new Exception("Property not found in configuration file."); }
            }
        }

    }
}
