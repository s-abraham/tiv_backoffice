﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Aculocity.GAABackOffice.Global
{
    public class ExceptionHandler
    {
        public static void HandleException(ref Exception exception, int userID)
        {
            try
            {
                LogExceptionToDB(exception.Message, exception.StackTrace, exception.Source, userID, false, null);
            }
            catch(Exception ex){ExceptionHandler.HandleException(ref ex, 0);}
        }
        public static void HandleException(ref ExceptionJS exception, int userID)
        {
            try
            {
                LogExceptionToDB(exception.Msg, null, exception.URL, userID, true, exception.LineNumber);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }

        private static void LogExceptionToDB(object massage, object stackTrace, object source, object userid, object isJSerror, object lineNumber)
        {
            using(var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
            {
                using(var cmd = new SqlCommand("BO_LogException", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Message", SqlDbType.VarChar, 500).Value = massage ?? DBNull.Value;
                    cmd.Parameters.Add("@StackTrace", SqlDbType.VarChar, 5000).Value = stackTrace ?? DBNull.Value;
                    cmd.Parameters.Add("@Source", SqlDbType.VarChar, 500).Value = source ?? DBNull.Value;
                    cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = userid ?? DBNull.Value;
                    cmd.Parameters.Add("@IsJSError", SqlDbType.Bit).Value = isJSerror ?? DBNull.Value;
                    cmd.Parameters.Add("@LineNumber", SqlDbType.Int).Value = lineNumber ?? DBNull.Value;

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
    }

    public class ExceptionJS: Exception
    {
        #region Constructors
            public ExceptionJS(string msg, string url, int linenumber)
            {
                Msg = msg;
                URL = url;
                LineNumber = linenumber;
            }
        #endregion
        #region Properties
            public string Msg { get; private set; }
            public string URL { get; private set; }
            public int LineNumber { get; private set; }
        #endregion
    }
}
