﻿namespace Aculocity.GAABackOffice.Global
{
    public class Enumerations
    {
        public enum AuthUserGroup
        {
            None = 0,
            AllDealerAdmin = 1,
            SpecificDealerAdmin = 2,
            GroupDealerAdmin = 3,
            HomeNetSuperUser = 4,
            GalvesFullAdmin = 5,
            BSBConsultingFullAdmin = 6,
            GalvesDealerAdmin = 7,
            BSBConsultingDealerAdmin = 8,
            HomeNetDealerAdmin = 9,
            ResellerAdmin = 10
        }

        public enum GroupDealerAdmin
        {
            None = 0,
            Admin = 1,
            HomeNet = 4,
            Galves = 5,
            BSB = 6
        }

        public enum DateRange
        {
            CurrentMonth = 0,
            LastMonth = 1
        }

        public enum ScaleType
        { 
            Daily = 0,
            Monthly = 1
        }

        public enum Layout
        {
            Horizontal = 0,
            Vertical = 1
        }

        public enum UsersComparisonType
        {
            DealerName = 1,
            UserName = 2,
            Password = 3,
            Active = 4
        }

        public enum DealerGroupComparisonType
        {
            Description = 1,
            DealerTypeName = 2
        }

        public enum DealerHeaderTextComparisonType
        {
            PageNo = 1,
            TextLanguage = 2,
            Active = 3
        }

        public enum Language
        {
            Eng = 1,
            Esp = 2
        }

        public enum DataProvider
        {
            Galves = 1,
            Nada = 2
        }
    }
}
