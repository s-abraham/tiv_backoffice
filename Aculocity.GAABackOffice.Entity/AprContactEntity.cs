﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class AprContactEntity
    {
        #region ' Constructors '
        public AprContactEntity() { }
        #endregion

        #region ' Properties '
        private int _aprID = 0;
        public int AprID
        {
            get { return _aprID; }
            set { _aprID = value; }
        }

        private string _firstName = string.Empty;
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName = string.Empty;
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _email = string.Empty;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        private string _phone = string.Empty;
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        private string _address = string.Empty;
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private string _zipCode = string.Empty;
        public string ZipCode
        {
            get { return _zipCode; }
            set { _zipCode = value; }
        }

        private string _buyWithin = string.Empty;
        public string BuyWithin
        {
            get { return _buyWithin; }
            set { _buyWithin = value; }
        }

        private string _comments = string.Empty;
        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Populate new vehicle from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRow(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["AprID"] != DBNull.Value) { this._aprID = (int)drow["AprID"]; }
                if (drow["FirstName"] != DBNull.Value) { this._firstName = (string)drow["FirstName"]; }
                if (drow["LastName"] != DBNull.Value) { this._lastName = (string)drow["LastName"]; }
                if (drow["Email"] != DBNull.Value) { this._email = (string)drow["Email"]; }
                if (drow["Phone"] != DBNull.Value) { this._phone = (string)drow["Phone"]; }
                if (drow["Address"] != DBNull.Value) { this._address = (string)drow["Address"]; }
                if (drow["ZipCode"] != DBNull.Value) { this._zipCode = (string)drow["ZipCode"]; }
                if (drow["BuyWithin"] != DBNull.Value) { this._buyWithin = (string)drow["BuyWithin"]; }
                if (drow["Comments"] != DBNull.Value) { this._comments = (string)drow["Comments"]; }
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
        }
        #endregion
    }
}