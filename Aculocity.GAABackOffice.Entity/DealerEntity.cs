﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class DealerEntity
    {
        #region ' Constructors '
        public DealerEntity(){}
        #endregion

        #region ' Properties '

        private string _dealerKey = string.Empty;
        public string DealerKey
        {
            get { return _dealerKey; }
            set { _dealerKey = value; }
        }

        private string _dealerName = string.Empty;
        public string DealerName
        {
            get { return _dealerName; }
            set { _dealerName = value; }
        }

        private string _dealerStreet = string.Empty;
        public string DealerStreet
        {
            get { return _dealerStreet; }
            set { _dealerStreet = value; }
        }

        private string _dealerCity = string.Empty;
        public string DealerCity
        {
            get { return _dealerCity; }
            set { _dealerCity = value; }
        }

        private string _dealerState = string.Empty;
        public string DealerState
        {
            get { return _dealerState; }
            set { _dealerState = value; }
        }

        private string _dealerTitle = string.Empty;
        public string DealerTitle
        {
            get { return _dealerTitle; }
            set { _dealerTitle = value; }
        }

        private string _dealerZip = string.Empty;
        public string DealerZip
        {
            get { return _dealerZip; }
            set { _dealerZip = value; }
        }

        private string _dealerContactName = string.Empty;
        public string DealerContactName
        {
            get { return _dealerContactName; }
            set { _dealerContactName = value; }
        }

        private string _dealerContactEmail = string.Empty;
        public string DealerContactEmail
        {
            get { return _dealerContactEmail; }
            set { _dealerContactEmail = value; }
        }

        private string _dealerContactePhone = string.Empty;
        public string DealerContactPhone
        {
            get { return _dealerContactePhone; }
            set { _dealerContactePhone = value; }
        }

        private string _dealerContactPhone2 = string.Empty;
        public string DealerContactPhone2
        {
            get { return _dealerContactPhone2; }
            set { _dealerContactPhone2 = value; }
        }

        private string _dealerContacteFax = string.Empty;
        public string DealerContactFax
        {
            get { return _dealerContacteFax; }
            set { _dealerContacteFax = value; }
        }

        private string _dealerWebSite = string.Empty;
        public string DealerWebSite
        {
            get { return _dealerWebSite; }
            set { _dealerWebSite = value; }
        }

        private string _dealerPublicState = string.Empty;
        public string DealerPublicSite
        {
            get { return _dealerPublicState; }
            set { _dealerPublicState = value; }
        }

        private string _dealerAutoGroup = string.Empty;
        public string DealerAutoGroup
        {
            get { return _dealerAutoGroup; }
            set { _dealerAutoGroup = value; }
        }

        private string _dealerLogo = string.Empty;
        public string DealerLogo
        {
            get { return _dealerLogo; }
            set { _dealerLogo = value; }
        }

        private string _dealerPromo = string.Empty;
        public string DealerPromo
        {
            get { return _dealerPromo; }
            set { _dealerPromo = value; }
        }

        private string _dealerDefaultMakes = string.Empty;
        public string DealerDefaultMakes
        {
            get { return _dealerDefaultMakes; }
            set { _dealerDefaultMakes = value; }
        }

        private int _dealerType;
        public int DealerType
        {
            get { return _dealerType; }
            set { _dealerType = value; }
        }

        private string _leadProvider = string.Empty;
        public string LeadProvider
        {
            get { return _leadProvider; }
            set { _leadProvider = value; }
        }

        private string _leadFormat = string.Empty;
        public string LeadFormat
        {
            get { return _leadFormat; }
            set { _leadFormat = value; }
        }

        private string _leadFormat2 = string.Empty;
        public string LeadFormat2
        {
            get { return _leadFormat2; }
            set { _leadFormat2 = value; }
        }

        private string _leadFormat3 = string.Empty;
        public string LeadFormat3
        {
            get { return _leadFormat3; }
            set { _leadFormat3 = value; }
        }

        private string _leadFormat4 = string.Empty;
        public string LeadFormat4
        {
            get { return _leadFormat4; }
            set { _leadFormat4 = value; }
        }

        private string _leadEmail = string.Empty;
        public string LeadEmail
        {
            get { return _leadEmail; }
            set { _leadEmail = value; }
        }

        private string _leadEmail2 = string.Empty;
        public string LeadEmail2
        {
            get { return _leadEmail2; }
            set { _leadEmail2 = value; }
        }

        private string _leadEmail3 = string.Empty;
        public string LeadEmail3
        {
            get { return _leadEmail3; }
            set { _leadEmail3 = value; }
        }

        private string _leadEmail4 = string.Empty;
        public string LeadEmail4
        {
            get { return _leadEmail4; }
            set { _leadEmail4 = value; }
        }

        private string _leadContactName = string.Empty;
        public string LeadContactName
        {
            get { return _leadContactName; }
            set { _leadContactName = value; }
        }

        private string _leadContactEmail = string.Empty;
        public string LeadContactEmail
        {
            get { return _leadContactEmail; }
            set { _leadContactEmail = value; }
        }

        private string _leadContactPhone = string.Empty;
        public string LeadContactPhone
        {
            get { return _leadContactPhone; }
            set { _leadContactPhone = value; }
        }

        private string _leadContactFax = string.Empty;
        public string LeadContactFax
        {
            get { return _leadContactFax; }
            set { _leadContactFax = value; }
        }

        private string _webSiteCompany = string.Empty;
        public string WebsiteCompany
        {
            get { return _webSiteCompany; }
            set { _webSiteCompany = value; }
        }

        private string _webSiteContactName = string.Empty;
        public string WebsiteContactName
        {
            get { return _webSiteContactName; }
            set { _webSiteContactName = value; }
        }

        private string _webSiteContactEmail = string.Empty;
        public string WebsiteContactEmail
        {
            get { return _webSiteContactEmail; }
            set { _webSiteContactEmail = value; }
        }

        private string _webSiteContactPhone = string.Empty;
        public string WebsiteContactPhone
        {
            get { return _webSiteContactPhone; }
            set { _webSiteContactPhone = value; }
        }

        private DateTime _adminDate;
        public DateTime AdminDate
        {
            get { return _adminDate; }
            set { _adminDate = value; }
        }

        private DateTime _adminActivationDate;
        public DateTime AdminActivationDate
        {
            get { return _adminActivationDate; }
            set { _adminActivationDate = value; }
        }

        private string _adminDWTProduct = string.Empty;
        public string AdminDWTProduct
        {
            get { return _adminDWTProduct; }
            set { _adminDWTProduct = value; }
        }

        private decimal _adminAutoAppraisalCost;
        public decimal AdminAutoAppraisalCost
        {
            get { return _adminAutoAppraisalCost; }
            set { _adminAutoAppraisalCost = value; }
        }

        private decimal _adminSetupFees;
        public decimal AdminSetupFees
        {
            get { return _adminSetupFees; }
            set { _adminSetupFees = value; }
        }

        private bool _active;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        private bool _earlyAppraise;
        public bool EarlyAppraise
        {
            get { return _earlyAppraise; }
            set { _earlyAppraise = value; }
        }

        private string _theme = string.Empty;
        public string Theme
        {
            get { return _theme; }
            set { _theme = value; }
        }

        private bool _billable;
        public bool Billable
        {
            get { return _billable; }
            set { _billable = value; }
        }

        private DateTime _billableStartDate;
        public DateTime BillableStartDate
        {
            get { return _billableStartDate; }
            set { _billableStartDate = value; }
        }

        private DateTime _dateUpdated;
        public DateTime DateUpdated
        {
            get { return _dateUpdated; }
            set { _dateUpdated = value; }
        }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        private int _galvesRangeCategory;
        public int GalvesRangeCategory
        {
            get { return _galvesRangeCategory; }
            set { _galvesRangeCategory = value; }
        }

        private bool _instantOptionPricing;
        public bool InstantOptionPricing
        {
            get { return _instantOptionPricing; }
            set { _instantOptionPricing = value; }
        }

        private int _groupID;
        public int GroupID
        {
            get { return _groupID; }
            set { _groupID = value; }
        }

        private string _language = string.Empty;
        public string Language
        {
            get { return _language; }
            set { _language = value; }
        }

        private string _creditApp = string.Empty;
        public string CreditApp
        {
            get { return _creditApp; }
            set { _creditApp = value; }
        }

        private int _creditMsg;
        public int CreditMsg
        {
            get { return _creditMsg; }
            set { _creditMsg = value; }
        }

        private bool _creditEnabled;
        public bool CreditEnabled
        {
            get { return _creditEnabled; }
            set { _creditEnabled = value; }
        }

        private string _invNew = string.Empty;
        public string InvNew
        {
            get { return _invNew; }
            set { _invNew = value; }
        }

        private bool _invNewEnabled;
        public bool InvNewEnabled
        {
            get { return _invNewEnabled; }
            set { _invNewEnabled = value; }
        }

        private string _invUsed = string.Empty;
        public string InvUsed
        {
            get { return _invUsed; }
            set { _invUsed = value; }
        }

        private bool _invUsedEnabled;
        public bool InvUsedEnabled
        {
            get { return _invUsedEnabled; }
            set { _invUsedEnabled = value; }
        }

        private bool _emailAppraisal;
        public bool EmailAppraisal
        {
            get { return _emailAppraisal; }
            set { _emailAppraisal = value; }
        }

        private bool _fourStep;
        public bool FourStep
        {
            get { return _fourStep; }
            set { _fourStep = value; }
        }

        private bool _unsureOption;
        public bool UnsureOption
        {
            get { return _unsureOption; }
            set { _unsureOption = value; }
        }

        private string _googleUrchin = string.Empty;
        public string GoogleUrchin
        {
            get { return _googleUrchin; }
            set { _googleUrchin = value; }
        }

        private bool _ignoreOption;
        public bool IgnoreOption
        {
            get { return _ignoreOption; }
            set { _ignoreOption = value; }
        }

        private string _comments = string.Empty;
        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        private string _salesperson = string.Empty;
        public string Salesperson
        {
            get { return _salesperson; }
            set { _salesperson = value; }
        }

        private bool _hideTradeIn;
        public bool HideTradeIn
        {
            get { return _hideTradeIn; }
            set { _hideTradeIn = value; }
        }

        private string _emailTitle = string.Empty;
        public string EmailTitle
        {
            get { return _emailTitle; }
            set { _emailTitle = value; }
        }

        private string _operatingHours = string.Empty;
        public string OperatingHours
        {
            get { return _operatingHours; }
            set { _operatingHours = value; }
        }

        private bool _hideCondition;
        public bool HideCondition
        {
            get { return _hideCondition; }
            set { _hideCondition = value; }
        }

        private bool _hideEmailTradeIn;
        public bool HideEmailTradeIn
        {
            get { return _hideEmailTradeIn; }
            set { _hideEmailTradeIn = value; }
        }

        private bool _hideSpecialOffer;
        public bool HideSpecialOffer
        {
            get { return _hideSpecialOffer; }
            set { _hideSpecialOffer = value; }
        }

        private bool _hideAddress;
        public bool HideAddress
        {
            get { return _hideAddress; }
            set { _hideAddress = value; }
        }

        private string _secondDealerTitle = string.Empty;
        public string SecondDealerTitle
        {
            get { return _secondDealerTitle; }
            set { _secondDealerTitle = value; }
        }

        private string _secondDealerContactName = string.Empty;
        public string SecondDealerContactName
        {
            get { return _secondDealerContactName; }
            set { _secondDealerContactName = value; }
        }

        private string _secondDealerContactEmail = string.Empty;
        public string SecondDealerContactEmail
        {
            get { return _secondDealerContactEmail; }
            set { _secondDealerContactEmail = value; }
        }

        private string _secondDealerContactPhone = string.Empty;
        public string SecondDealerContactPhone
        {
            get { return _secondDealerContactPhone; }
            set { _secondDealerContactPhone = value; }
        }

        private string _secondDealerContactFax = string.Empty;
        public string SecondDealerContactFax
        {
            get { return _secondDealerContactFax; }
            set { _secondDealerContactFax = value; }
        }

        private string _secondDealerContactPhone2 = string.Empty;
        public string SecondDealerContactPhone2
        {
            get { return _secondDealerContactPhone2; }
            set { _secondDealerContactPhone2 = value; }
        }

        private int _truckGalvesRangeCategory;
        public int TruckGalvesRangeCategory
        {
            get { return _truckGalvesRangeCategory; }
            set { _truckGalvesRangeCategory = value; }
        }

        private string _appointmentEmail = string.Empty;
        public string AppointmentEmail
        {
            get { return _appointmentEmail; }
            set { _appointmentEmail = value; }
        }

        private string _originalSalesPerson = string.Empty;
        public string OriginalSalesPerson
        {
            get { return _originalSalesPerson; }
            set { _originalSalesPerson = value; }
        }

        private string _dataProvider = string.Empty;
        public string DataProvider
        {
            get { return _dataProvider; }
            set { _dataProvider = value; }
        }

        private bool _newSelection;
        public bool NewSelection
        {
            get { return _newSelection; }
            set { _newSelection = value; }
        }

        private string _hniDealers = string.Empty;
        public string HNIDealers
        {
            get { return _hniDealers; }
            set { _hniDealers = value; }
        }

        private int _nadaRangeCategory;
        public int NADARangeCategory
        {
            get { return _nadaRangeCategory; }
            set { _nadaRangeCategory = value; }
        }

        private bool _nadaBranding;
        public bool NADABranding
        {
            get { return _nadaBranding; }
            set { _nadaBranding = value; }
        }

        private bool _retailPricing;
        public bool RetailPricing
        {
            get { return _retailPricing; }
            set { _retailPricing = value; }
        }

        private bool _tradeImageUpload;
        public bool TradeImageUpload
        {
            get { return _tradeImageUpload; }
            set { _tradeImageUpload = value; }
        }

        private bool _showIncentives;
        public bool ShowIncentives
        {
            get { return _showIncentives; }
            set { _showIncentives = value; }
        }

        private bool _showMonthlyPayment;
        public bool ShowMonthlyPayment
        {
            get { return _showMonthlyPayment; }
            set { _showMonthlyPayment = value; }
        }

        private bool _showAmountOwed;
        public bool ShowAmountOwed
        {
            get { return _showAmountOwed; }
            set { _showAmountOwed = value; }
        }

        private int _appraisalExpiration;
        public int AppraisalExpiration
        {
            get { return _appraisalExpiration; }
            set { _appraisalExpiration = value; }
        }

        private bool _repaymentCalc;
        public bool RepaymentCalc
        {
            get { return _repaymentCalc; }
            set { _repaymentCalc = value; }
        }

        private DateTime _billingLastUpdated;
        public DateTime BillingLastUpdated
        {
            get { return _billingLastUpdated; }
            set { _billingLastUpdated = value; }
        }

        private DateTime _systemBillingDate;
        public DateTime SystemBillingDate
        {
            get { return _systemBillingDate; }
            set { _systemBillingDate = value; }
        }

        private string _billingInfo = string.Empty;
        public string BillingInfo
        {
            get { return _billingInfo; }
            set { _billingInfo = value; }
        }

        private string _billingExplanation = string.Empty;
        public string BillingExplanation
        {
            get { return _billingExplanation; }
            set { _billingExplanation = value; }
        }

        private string _AverageValueRange_VUnderPercent = string.Empty;
        public string AverageValueRange_VUnderPercent
        {
            get { return AverageValueRange_VUnderPercent; }
            set { _AverageValueRange_VUnderPercent = value; }
        }

        private string _AverageValueRange_VUnderAmount = string.Empty;
        public string AverageValueRange_VUnderAmount
        {
            get { return AverageValueRange_VUnderAmount; }
            set { _AverageValueRange_VUnderAmount = value; }
        }

        private string _AverageValueRange_VOverPercent = string.Empty;
        public string AverageValueRange_VOverPercent
        {
            get { return AverageValueRange_VOverPercent; }
            set { _AverageValueRange_VOverPercent = value; }
        }

        private string _AverageValueRange_VOverAmount = string.Empty;
        public string AverageValueRange_VOverAmount
        {
            get { return AverageValueRange_VOverAmount; }
            set { _AverageValueRange_VOverAmount = value; }
        }

        private string _AverageValueRange_VUnderTypeSelection = string.Empty;
        public string AverageValueRange_VUnderTypeSelection
        {
            get { return AverageValueRange_VUnderTypeSelection; }
            set { _AverageValueRange_VUnderTypeSelection = value; }
        }

        private string _AverageValueRange_VOverTypeSelection = string.Empty;
        public string AverageValueRange_VOverTypeSelection
        {
            get { return AverageValueRange_VOverTypeSelection; }
            set { _AverageValueRange_VOverTypeSelection = value; }
        }

        #endregion

        #region ' Custom Populate Methods '
        /// <summary>
        /// Populate two properties DealerKey and DealerName from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRowAsDealerList(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["DealerKey"] != DBNull.Value) { _dealerKey = (string) drow["DealerKey"]; }
                if (drow["DealerName"] != DBNull.Value) { _dealerName = (string) drow["DealerName"]; }
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, 0);}
        }

        #endregion
    }
}