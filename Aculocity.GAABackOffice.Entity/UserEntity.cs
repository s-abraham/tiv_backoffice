﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class UserEntity
    {
        #region ' Constructors '
        public UserEntity(){}
        #endregion

        #region ' Properties '

        private int _userid = 0;
        public int UserID
        {
            get { return _userid; }
            set { _userid = value; }
        }

        private string _username = string.Empty;
        public string UserName
        {
            get { return _username; }
            set { _username = value; }
        }

        private string _password = string.Empty;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private bool _active = false;
        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        private string _dealerkey = string.Empty;
        public string DealerKey
        {
            get { return _dealerkey; }
            set { _dealerkey = value;}
        }
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Populate user from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRow(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["UserID"] != DBNull.Value) { this._userid = (int)drow["UserID"]; }
                if (drow["UserName"] != DBNull.Value) { this._username = (string)drow["UserName"]; }
                if (drow["Password"] != DBNull.Value) { this._password = (string)drow["Password"]; }
                if (drow["Active"] != DBNull.Value) { this._active = (bool)drow["Active"]; }
                if (drow["DealerKey"] != DBNull.Value) { this._dealerkey = (string)drow["DealerKey"]; }
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, this._userid);}
        }

        #endregion
    }
}