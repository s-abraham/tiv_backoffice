﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class TradeWantedEntity
    {
        #region ' Constructors '
        public TradeWantedEntity() { }
        #endregion

        #region ' Properties '
        private int _aprID = 0;
        public int AprID
        {
            get { return _aprID; }
            set { _aprID = value; }
        }

        private string _new = string.Empty;
        public string New
        {
            get { return _new; }
            set { _new = value; }
        }

        private string _modelYear = string.Empty;
        public string ModelYear
        {
            get { return _modelYear; }
            set { _modelYear = value; }
        }

        private string _make = string.Empty;
        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }

        private string _model = string.Empty;
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private string _style = string.Empty;
        public string Style
        {
            get { return _style; }
            set { _style = value; }
        }

        private string _manufacturerColor = string.Empty;
        public string ManufacturerColor
        {
            get { return _manufacturerColor; }
            set { _manufacturerColor = value; }
        }

        private string _buyWithin = string.Empty;
        public string BuyWithin
        {
            get { return _buyWithin; }
            set { _buyWithin = value; }
        }

        private string _comments = string.Empty;
        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        private string _vin = string.Empty;
        public string VIN
        {
            get { return _vin; }
            set { _vin = value; }
        }

        private string _miles = string.Empty;
        public string Miles
        {
            get { return _miles; }
            set { _miles = value; }
        }

        private string _dealerID = string.Empty;
        public string DealerID
        {
            get { return _dealerID; }
            set { _dealerID = value; }
        }
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Populate TradeWanted from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRow(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["AprID"] != DBNull.Value) { this._aprID = (int)drow["AprID"]; }
                if (drow["New"] != DBNull.Value) { this._new = (string)drow["New"]; }
                if (drow["ModelYear"] != DBNull.Value) { this._modelYear = (string)drow["ModelYear"]; }
                if (drow["Make"] != DBNull.Value) { this._make = (string)drow["Make"]; }
                if (drow["Model"] != DBNull.Value) { this._model = (string)drow["Model"]; }
                if (drow["Style"] != DBNull.Value) { this._style = (string)drow["Style"]; }
                if (drow["ManufacturerColor"] != DBNull.Value) { this._manufacturerColor = (string)drow["ManufacturerColor"]; }
                if (drow["BuyWithin"] != DBNull.Value) { this._buyWithin = (string)drow["BuyWithin"]; }
                if (drow["Comments"] != DBNull.Value) { this._comments = (string)drow["Comments"]; }
                if (drow["DateCreated"] != DBNull.Value) { this._dateCreated = (DateTime)drow["DateCreated"]; }
                if (drow["Miles"] != DBNull.Value) { this._miles = (string)drow["Miles"]; }
                if (drow["DealerID"] != DBNull.Value) { this._dealerID = (string)drow["DealerID"]; }
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
        }
        #endregion
    }

    public class TradeInEntity
    {
        #region ' Constructors '
        public TradeInEntity() { }
        #endregion

        #region ' Properties '
        private string _year = string.Empty;
        public string Year
        {
            get { return _year; }
            set { _year = value; }
        }

        private string _manufacturer = string.Empty;
        public string Manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }

        private string _model = string.Empty;
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private string _body = string.Empty;
        public string Body
        {
            get { return _body; }
            set { _body = value; }
        }

        private string _engine = string.Empty;
        public string Engine
        {
            get { return _engine; }
            set { _engine = value; }
        }

        private string _mileage = string.Empty;
        public string Mileage
        {
            get { return _mileage; }
            set { _mileage = value; }
        }

        private string _baseValueRange = string.Empty;
        public string BaseValueRange
        {
            get { return _baseValueRange; }
            set { _baseValueRange = value; }
        }

        private string _totalValueRange = string.Empty;
        public string TotalValueRange
        {
            get { return _totalValueRange; }
            set { _totalValueRange = value; }
        }

        private string _condition = string.Empty;
        public string Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        private string _language = string.Empty;
        public string Language
        {
            get { return _language; }
            set { _language = value; }
        }

        private string _monthlyPayment = string.Empty;
        public string MonthlyPayment
        {
            get { return _monthlyPayment; }
            set { _monthlyPayment = value; }
        }

        private string _settlementValue = string.Empty;
        public string SettlementValue
        {
            get { return _settlementValue; }
            set { _settlementValue = value; }
        }
        
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Populate TradeIn from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRow(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["Year"] != DBNull.Value) { this._year = (string)drow["Year"]; }
                if (drow["Manufacturer"] != DBNull.Value) { this._manufacturer = (string)drow["Manufacturer"]; }
                if (drow["Model"] != DBNull.Value) { this._model = (string)drow["Model"]; }
                if (drow["Body"] != DBNull.Value) { this._body = (string)drow["Body"]; }
                if (drow["Engine"] != DBNull.Value) { this._engine = (string)drow["Engine"]; }
                if (drow["Mileage"] != DBNull.Value) { this._engine = (string)drow["Mileage"]; }
                if (drow["BaseValueRange"] != DBNull.Value) { this._baseValueRange = (string)drow["BaseValueRange"]; }
                if (drow["TotalValueRange"] != DBNull.Value) { this._totalValueRange = (string)drow["TotalValueRange"]; }
                if (drow["Condition"] != DBNull.Value) { this._condition = (string)drow["Condition"]; }
                if (drow["Language"] != DBNull.Value) { this._language = (string)drow["Language"]; }
                if (drow["MonthlyPayment"] != DBNull.Value) { this._monthlyPayment = (string)drow["MonthlyPayment"]; }
                if (drow["SettlementValue"] != DBNull.Value) { this._settlementValue = (string)drow["SettlementValue"]; }
            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
    }
}