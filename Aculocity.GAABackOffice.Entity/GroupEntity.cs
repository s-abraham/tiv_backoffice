﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class GroupEntity
    {
        #region ' Constructors '
        public GroupEntity() { }
        #endregion
        
        #region ' Properties '
        private int _groupid = 0;
        public int GroupID
        {
            get { return _groupid; }
            set { _groupid = value; }
        }
        private string _description = string.Empty;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Populate user from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRow(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["GroupID"] != DBNull.Value) { this._groupid = (int)drow["GroupID"]; }
                if (drow["FunctionDescription"] != DBNull.Value) { this._description = (string)drow["FunctionDescription"]; }
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
        }
        #endregion
    }
}