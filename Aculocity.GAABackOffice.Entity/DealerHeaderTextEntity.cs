﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class DealerHeaderTextEntity
    {
        #region ' Constructors '
        public DealerHeaderTextEntity() { }
        #endregion

        #region ' Properties '
        public string DealerKey { get; set; }
        public int PageNo { get; set; }
		public string TextLanguage { get; set; }
		public string HeaderText { get; set; }
		public bool Active { get; set; }
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Populate user from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRow(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["DealerKey"] != DBNull.Value) { DealerKey = (string)drow["DealerKey"]; }
                if (drow["PageNo"] != DBNull.Value) { PageNo = (int)drow["PageNo"]; }
                if (drow["TextLanguage"] != DBNull.Value) { TextLanguage = (string)drow["TextLanguage"]; }
                if (drow["HeaderText"] != DBNull.Value) { HeaderText = (string)drow["HeaderText"]; }
                if (drow["Active"] != DBNull.Value) { Active = (bool)drow["Active"]; }
                
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }
        }
        #endregion
    }
}
