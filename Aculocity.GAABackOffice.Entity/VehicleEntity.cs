﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class VehicleEntity
    {
        #region ' Constructors '
        public VehicleEntity() { }
        #endregion

        #region ' Properties '
        private string _year = string.Empty;
        public string Year
        {
            get { return _year; }
            set { _year = value; }
        }

        private string _make = string.Empty;
        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }

        private string _model = string.Empty;
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private string _trim = string.Empty;
        public string Trim
        {
            get { return _trim; }
            set { _trim = value; }
        }

        private string _style = string.Empty;
        public string Style
        {
            get { return _style; }
            set { _style = value; }
        }
        #endregion

        #region ' Custom Methods '
        /// <summary>
        /// Populate new vehicle from DataRow
        /// </summary>
        /// <param name="drow"></param>
        public void PopulateFromDataRow(DataRow drow)
        {
            try
            {
                //populate object:
                if (drow["YEAR"] != DBNull.Value) { this._year = (string)drow["YEAR"]; }
                if (drow["MAKE"] != DBNull.Value) { this._make = (string)drow["MAKE"]; }
                if (drow["Model"] != DBNull.Value) { this._model = (string)drow["Model"]; }
                if (drow["TRIM"] != DBNull.Value) { this._trim = (string)drow["TRIM"]; }
                if (drow["STYLE"] != DBNull.Value) { this._style = (string)drow["STYLE"]; }
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
        }
        #endregion
    }
}