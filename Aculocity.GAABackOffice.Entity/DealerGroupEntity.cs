﻿using System;
using System.Data;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.Entity
{
    public class DealerGroupEntity
    {
        #region ' Constructors '
        public DealerGroupEntity() { }
        #endregion

        #region ' Properties '

        private int _id = 0;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _description = string.Empty;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _dealerTypeID = 0;
        public int DealerTypeID
        {
            get { return _dealerTypeID; }
            set { _dealerTypeID = value; }
        }
        #endregion

        #region ' Custom Methods '
        public void PupulateFromDataRow(DataRow drow)
        {
            try
            {
                //pupulate object:
                if (drow["ID"] != DBNull.Value) { _id = (int) drow["ID"]; }
                if (drow["Description"] != DBNull.Value) {_description = (string) drow["Description"]; }
                if (drow["DealerTypeID"] != DBNull.Value) {_dealerTypeID = (int) drow["DealerTypeID"];}
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, 0); }
        }
        #endregion
    }
}