﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web
{
    public partial class SSOPortal : System.Web.UI.Page
    {

        protected void Page_PreInit(object sener, EventArgs e)
        {
            Response.AddHeader("P3P", "CP=\"CAO PSA OUR\"");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Build the string to encode
                var sb = new StringBuilder();

                sb.Append(Request.QueryString["Token"]);
                sb.Append(Request.QueryString["DealerKey"]);
                sb.Append("HomeNet");
                //Our Salt Value

                //Encode the string
                var md5Obj = new MD5CryptoServiceProvider();

                byte[] bytesToHash = System.Text.Encoding.ASCII.GetBytes(sb.ToString());
                byte[] hashedBytes = md5Obj.ComputeHash(bytesToHash);

                var compareSB = new StringBuilder();
                foreach (byte b in hashedBytes)
                {
                    compareSB.Append(b.ToString("x2"));
                }

                var calculatedMD5 = compareSB.ToString();
                //Compare the encoded string to the key in the query string
                if ((Request.QueryString["key"] != null))
                {
                    if (calculatedMD5.Equals(Request.QueryString["Key"]))
                    {
                        LoginUser(Request.QueryString["DealerKey"]);
                        Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                    }
                    else
                    {
                        GoToLogin();
                    }
                }
                else
                {
                    GoToLogin();
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void LoginUser(string dealerKey)
        {
            try
            {
                var users = new Users();
                users.GetUsersByDealerKey(dealerKey);
                if(users.Count == 1)
                {
                    SetUser(users[0]);
                }
                else
                {
                    GoToLogin();
                }

            }
            catch (Exception ex)
            {
                GoToLogin();
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        private void SetUser(UserEntity user)
        {
            try
            {
                UserBLL authenticatedUser = Security.PerformLogin(user.UserName, user.Password);
                //set up group data
                string customData = string.Format("Group:{0};SubGroup:{1};UserID:{2}",
                                                  authenticatedUser.AuthGroup,
                                                  authenticatedUser.SubGroupDealerAdmin,
                                                  authenticatedUser.UserID);

                Response.Cookies.Add(Security.CreateAuthCookie(authenticatedUser.UserName, customData));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void GoToLogin()
        {
            try
            {
                Session.Abandon();
                Security.PerformLogout();
                Response.Redirect("~/Login.aspx", false);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
    }
}