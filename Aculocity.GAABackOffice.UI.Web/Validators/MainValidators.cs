﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aculocity.SimpleData;
using Aculocity.Controls.Web;

namespace Aculocity.GAABackOffice.UI.Web {
    
    public class EntityZipCodeValidator : EntityEditorRegExValidator {

        public EntityZipCodeValidator(string fieldName, Aculocity.SimpleData.Entity entity, AdditionalConditionCallback additionalConditionCallBack)
            : base("\\d{5}(-\\d{4})?", fieldName, entity, additionalConditionCallBack) {
        }

        public EntityZipCodeValidator(string fieldName, Aculocity.SimpleData.Entity entity)
            : base("\\d{5}(-\\d{4})?", fieldName, entity) {
        }

        public EntityZipCodeValidator(string fieldName, AdditionalConditionCallback additionalConditionCallBack)
            : base("\\d{5}(-\\d{4})?", fieldName, additionalConditionCallBack) {   
        }

        public EntityZipCodeValidator(string fieldName)
            : base("\\d{5}(-\\d{4})?", fieldName) {
        }
    }


    public class EntityMailAddressValidator : EntityEditorRegExValidator {

        public EntityMailAddressValidator(string fieldName, Aculocity.SimpleData.Entity entity, AdditionalConditionCallback additionalConditionCallBack)
            : base("(((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)[;]?[\\s]*)*)", fieldName, entity, additionalConditionCallBack) {
        }

        public EntityMailAddressValidator(string fieldName, Aculocity.SimpleData.Entity entity)
            : base("(((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)[;]?[\\s]*)*)", fieldName, entity) {
        }

        public EntityMailAddressValidator(string fieldName, AdditionalConditionCallback additionalConditionCallBack)
            : base("(((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)[;]?[\\s]*)*)", fieldName, additionalConditionCallBack) {
            
        }

        public EntityMailAddressValidator(string fieldName)
            : base("(((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)[;]?[\\s]*)*)", fieldName) {

        }  
    
    }

    public class EntityPhoneValidator : EntityEditorRegExValidator {

        public EntityPhoneValidator(string fieldName, Aculocity.SimpleData.Entity entity, AdditionalConditionCallback additionalConditionCallBack)
            : base("^(?:\\([2-9]\\d{2}\\)\\ ?|[2-9]\\d{2}(?:\\-?|\\ ?))[2-9]\\d{2}[- ]?\\d{4}$", fieldName, entity, additionalConditionCallBack) {
        }

        public EntityPhoneValidator(string fieldName, Aculocity.SimpleData.Entity entity)
            : base("^(?:\\([2-9]\\d{2}\\)\\ ?|[2-9]\\d{2}(?:\\-?|\\ ?))[2-9]\\d{2}[- ]?\\d{4}$", fieldName, entity) {
        }

        public EntityPhoneValidator(string fieldName, AdditionalConditionCallback additionalConditionCallBack)
            : base("^(?:\\([2-9]\\d{2}\\)\\ ?|[2-9]\\d{2}(?:\\-?|\\ ?))[2-9]\\d{2}[- ]?\\d{4}$", fieldName, additionalConditionCallBack) {
        }

        public EntityPhoneValidator(string fieldName)
            : base("^(?:\\([2-9]\\d{2}\\)\\ ?|[2-9]\\d{2}(?:\\-?|\\ ?))[2-9]\\d{2}[- ]?\\d{4}$", fieldName) {
        }  
    
    }

}
