﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true"
    CodeBehind="ManageUsers.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesUsers.ManageUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        table.text,
        table.text td,
        table.text th {
            border-color: #666;

        }
        table.text th {
            text-align: left;
        }
        table.text th,
        table.text td {
            text-align: left;
            padding: 6px 6px 5px 7px;
        }

        table.text td {
            font-weight: lighter;
            color: #333;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">

    <script type="text/javascript" language="JavaScript">
    changeclass("ctl00_liManageUsers");
    </script>

    <table width="100%">
        <tr>
            <td align="center">
                <table width="1150px">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblError" runat="server" CssClass="errorText" EnableViewState="False"
                                ForeColor="Red"></asp:Label>
                            <asp:Label ID="lblInfo" runat="server" CssClass="infoText" EnableViewState="False"
                                ForeColor="Gray"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%">
                            User Filter:
                            <asp:DropDownList ID="ddlSearchBy" runat="server" OnSelectedIndexChanged="ddlSearchBy_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Text="User Name" Value="UserName"></asp:ListItem>
                                <asp:ListItem Text="Dealership" Value="DealerName"></asp:ListItem>
                                <asp:ListItem Text="Group Name" Value="GroupName"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="50%" align="right">
                            <asp:TextBox runat="server" ID="txtSearchCriteria"></asp:TextBox>
                            <asp:DropDownList ID="ddlSearchByDealerGroup" runat="server" Width="55%" DataTextField="Description"
                                DataValueField="ID" Visible="false">
                            </asp:DropDownList>
                            <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
                <table width="1150px">
                    <tr>
                        <td>
                            <asp:GridView ID="gvUsers" runat="server" AllowPaging="True" AllowSorting="True"
                                ShowFooter="True" AutoGenerateColumns="False" Width="100%" CssClass="text" 
                                DataKeyNames="UserId"
                                PageSize="20" OnRowCommand="gvUsers_RowCommand" OnRowEditing="gvUsers_RowEditing"
                                OnRowCancelingEdit="gvUsers_RowCancelingEdit" OnRowDeleting="gvUsers_RowDeleting"
                                OnRowDataBound="gvUsers_RowDataBound" OnPageIndexChanging="gvUsers_PageIndexChanging"
                                OnSorting="gvUsers_Sorting">
                                <Columns>
                                    <asp:TemplateField HeaderText="User ID" InsertVisible="False" SortExpression="UserID"
                                        Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUserId" runat="server" Text='<%# Bind("UserID") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <FooterStyle VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGroupID" runat="server" Text='<%# Bind("GroupID") %>' Visible="false" />
                                            <asp:Label ID="lblSubGroupDealerAdmin" runat="server" Text='<%# Bind("SubGroupDealerAdminID") %>'
                                                Visible="false" />
                                            <asp:DropDownList ID="ddlGroup" runat="server" DataTextField="Description" DataValueField="GroupID"
                                                Enabled="false" OnSelectedIndexChanged="DDLGroupEditSelectedIndexChanged" AutoPostBack="true" />
                                            <asp:DropDownList ID="ddlSubGroup" runat="server" DataTextField="Description" DataValueField="ID"
                                                Enabled="false">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlGroupAdd" runat="server" DataTextField="Description" DataValueField="GroupID"
                                                OnSelectedIndexChanged="DDLGroupAddSelectedIndexChanged" AutoPostBack="true" />
                                            <asp:DropDownList ID="ddlSubGroupAdd" runat="server" DataTextField="Description"
                                                DataValueField="ID" Enabled="false" Visible="false">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <FooterStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dealer" SortExpression="DealerName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDealerKey" runat="server" Text='<%# Bind("DealerKey") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDealerNameEdit" runat="server" Text='<%# Bind("DealerName") %>'></asp:Label>
                                            <asp:DropDownList ID="ddlDealersEdit" runat="server" DataTextField="DealerName" Visible="false"
                                                DataValueField="DealerKey" Width="200px">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlDealersAdd" runat="server" DataTextField="DealerName" Enabled="false"
                                                DataValueField="DealerKey" Width="200px">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <FooterStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="UserName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUsernameEdit" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                            <asp:TextBox ID="txtUsernameEdit" runat="server" Text='<%# Bind("UserName") %>' Visible="false" />
                                            <asp:RequiredFieldValidator ID="reqUsernameEdit" runat="server" ControlToValidate="txtUsernameEdit"
                                                ErrorMessage="Please enter a username" ValidationGroup="GridRowVal">*</asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtUsernameAdd" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqUsernameAdd" runat="server" ControlToValidate="txtUsernameAdd"
                                                ErrorMessage="Please enter a username" ValidationGroup="GridFooterVal">*</asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <FooterStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Password" SortExpression="Password">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPasswordEdit" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
                                            <asp:TextBox ID="txtPasswordEdit" runat="server" Text='<%# Bind("Password") %>' Visible="false" />
                                            <asp:RequiredFieldValidator ID="reqPasswordEdit" runat="server" ControlToValidate="txtPasswordEdit"
                                                ErrorMessage="Please enter a password" ValidationGroup="GridRowVal">*</asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtPasswordAdd" runat="server" Text='<%# Bind("Password") %>' />
                                            <asp:RequiredFieldValidator ID="reqPasswordAdd" runat="server" ControlToValidate="txtPasswordAdd"
                                                ErrorMessage="Please enter a password" ValidationGroup="GridFooterVal">*</asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <FooterStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Active" SortExpression="Active">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkActivateUser" runat="server" Checked='<%# Bind("Active") %>'
                                                AutoPostBack="True" Enabled="false" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" />
                                        <FooterStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tools">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit" CommandArgument='<%# Bind("UserID") %>'
                                                Text="Edit" CausesValidation="true" ForeColor="Black" />&nbsp;
                                            <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                CommandArgument='<%# Bind("UserID") %>' OnClientClick='return confirm("Are you sure you want to delete this group?");'
                                                Text="Delete" ForeColor="Black" />
                                            <asp:LinkButton ID="lnlUpdateEdit" runat="server" CommandName="Edit_Update" Text="Update"
                                                Visible="false" CausesValidation="True" ValidationGroup="GridRowVal" ForeColor="Black" />&nbsp;
                                            <asp:LinkButton ID="lnkCancelEdit" runat="server" CommandName="Cancel" Text="Cancel"
                                                Visible="false" CausesValidation="false" ForeColor="Black" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:LinkButton ID="lnlUpdateAdd" runat="server" CommandName="Add_Update" Text="Insert"
                                                CausesValidation="true" ValidationGroup="GridFooterVal" ForeColor="Black" />
                                        </FooterTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <FooterStyle VerticalAlign="Top" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#CCC" ForeColor="#000" HorizontalAlign="Left" CssClass="HeaderClass"/>
                                <FooterStyle BackColor="WhiteSmoke" />
                                <AlternatingRowStyle BackColor="WhiteSmoke" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
