﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.BLL.Comparers;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesUsers
{
    public partial class ManageUsers : BasePage
    {
        private static Users _users = new Users();
        private static Dealers _dealers = new Dealers();
        private static Groups _groups = new Groups();
        private static DealerGroups _subGroups = new DealerGroups();

        #region Properties
        public string SortDirection
        {
            get { return ViewState["SortDirection"] as string; }
            set { ViewState["SortDirection"] = value; }
        }
        public string SortColumn
        {
            get { return ViewState["SortColumn"] != null ? ViewState["SortColumn"].ToString() : "DealerName"; }
            set { ViewState["SortColumn"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    _subGroups.GetDealerGroupsByDealerAdmin(Security.GetCurrentUserGroup().ToString());

                    ddlSearchByDealerGroup.DataSource = _subGroups;
                    ddlSearchByDealerGroup.DataBind();
                    ddlSearchByDealerGroup.Items.Insert(0, new ListItem() { Text = "Select Group Name", Value = "" });
                    txtSearchCriteria.Focus();

                    FillGrid(string.Empty, string.Empty);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' Control's Enents '

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string searchCriteria = ddlSearchBy.SelectedValue == "GroupName"
                                            ? ddlSearchByDealerGroup.SelectedItem.Text.Trim()
                                            : txtSearchCriteria.Text.Trim();
                FillGrid(ddlSearchBy.SelectedValue, searchCriteria);
                if (gvUsers.Rows.Count == 0) lblError.Text = "No Results, try to change the search criteria.";
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region ' Grid Events '
        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                var selectedRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                UserBLL user;
                switch (e.CommandName)
                {
                    case "Edit_Update":
                        Page.Validate("GridRowVal");
                        if (Page.IsValid)
                        {
                            int userID = Convert.ToInt32(((Label)gvUsers.Rows[selectedRow.RowIndex].Cells[0].FindControl("lblUserId")).Text);
                            string dealerKey = ((DropDownList)gvUsers.Rows[selectedRow.RowIndex].Cells[2].FindControl("ddlDealersEdit")).SelectedValue.Trim();
                            string userName = ((TextBox)gvUsers.Rows[selectedRow.RowIndex].Cells[3].FindControl("txtUsernameEdit")).Text.Trim();
                            string password = ((TextBox)gvUsers.Rows[selectedRow.RowIndex].Cells[4].FindControl("txtPasswordEdit")).Text.Trim();
                            int groupID = Convert.ToInt32(((DropDownList)gvUsers.Rows[selectedRow.RowIndex].Cells[1].FindControl("ddlGroup")).SelectedValue);
                            int dealergroupID = 0;
                            bool isActive = ((CheckBox)gvUsers.Rows[selectedRow.RowIndex].Cells[5].FindControl("chkActivateUser")).Checked;
                            if (groupID == (int)Enumerations.AuthUserGroup.GroupDealerAdmin)
                            {
                                if (((DropDownList)gvUsers.Rows[selectedRow.RowIndex].Cells[1].FindControl("ddlSubGroup")).SelectedValue != string.Empty)
                                    dealergroupID = Convert.ToInt32(((DropDownList)gvUsers.Rows[selectedRow.RowIndex].Cells[1].FindControl("ddlSubGroup")).SelectedValue);
                                else
                                {
                                    lblError.Text = "<li>Select Dealer Group.";
                                    return;
                                }
                            }
                            else if (groupID == (int)Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                                     groupID == (int)Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                                     groupID == (int)Enumerations.AuthUserGroup.ResellerAdmin ||
                                     groupID == (int)Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                            {
                                if (string.IsNullOrEmpty(dealerKey))
                                {
                                    lblError.Text = "<li>Select Dealer.";
                                    return;
                                }
                            }

                            user = new UserBLL { UserID = userID, DealerKey = dealerKey, UserName = userName, Password = password, GroupID = groupID, SubGroupDealerAdminID = dealergroupID, Active = isActive };

                            if (user.Update() < 0) lblError.Text = "<li>The user entered already exists.";
                            else
                            {
                                FillGrid(ddlSearchBy.SelectedValue, txtSearchCriteria.Text.Trim());
                                ShowHideControls(selectedRow.RowIndex, false);
                                lblInfo.Text = "<li>(User updated successfully)";
                            }
                        }
                        break;
                    case "Add_Update":
                        Page.Validate("GridFooterVal");
                        if (Page.IsValid)
                        {
                            string dealerKeyAdd = ((DropDownList)gvUsers.FooterRow.FindControl("ddlDealersAdd")).SelectedValue.Trim();
                            string userNameAdd = ((TextBox)gvUsers.FooterRow.FindControl("txtUsernameAdd")).Text.Trim();
                            string passwordAdd = ((TextBox)gvUsers.FooterRow.FindControl("txtPasswordAdd")).Text.Trim();
                            int groupIDAdd = Convert.ToInt32(((DropDownList)gvUsers.FooterRow.FindControl("ddlGroupAdd")).SelectedValue);
                            int dealergroupIDAdd = 0;

                            if (groupIDAdd == (int)Enumerations.AuthUserGroup.GroupDealerAdmin)
                            {
                                if (((DropDownList)gvUsers.FooterRow.FindControl("ddlSubGroupAdd")).SelectedValue != string.Empty)
                                    dealergroupIDAdd = Convert.ToInt32(((DropDownList)gvUsers.FooterRow.FindControl("ddlSubGroupAdd")).SelectedValue);
                                else
                                {
                                    lblError.Text = "<li>Select Deler Group.";
                                    return;
                                }
                            }
                            else if (groupIDAdd == (int)Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                                     groupIDAdd == (int)Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                                     groupIDAdd == (int)Enumerations.AuthUserGroup.ResellerAdmin ||
                                     groupIDAdd == (int)Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                            {
                                if (string.IsNullOrEmpty(dealerKeyAdd))
                                {
                                    lblError.Text = "<li>Select Dealer.";
                                    return;
                                }
                            }

                            user = new UserBLL { DealerKey = dealerKeyAdd, UserName = userNameAdd, Password = passwordAdd, GroupID = groupIDAdd, SubGroupDealerAdminID = dealergroupIDAdd };

                            if (user.Insert() < 0) lblError.Text = "<li>The user entered already exists.";
                            else
                            {
                                FillGrid(ddlSearchBy.SelectedValue, txtSearchCriteria.Text.Trim());
                                lblInfo.Text = "<li>(User inserted successfully)";
                            }
                        }
                        break;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvUsers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                //refresh all controls
                foreach (GridViewRow row in gvUsers.Rows) { ShowHideControls(row.RowIndex, false); }
                //visible controls for updated row only
                bool isVisible = (gvUsers.Rows[e.NewEditIndex].Cells[6].FindControl("lnkEdit")).Visible;
                string dealerKey = ((Label)gvUsers.Rows[e.NewEditIndex].Cells[2].FindControl("lblDealerKey")).Text;

                var ddlGroupEdit = (DropDownList)gvUsers.Rows[e.NewEditIndex].Cells[1].FindControl("ddlGroup");
                var ddlDealersEdit = (DropDownList)gvUsers.Rows[e.NewEditIndex].Cells[1].FindControl("ddlDealersEdit");

                try
                {
                    ddlDealersEdit.SelectedValue = dealerKey;
                }
                catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

                if (!string.IsNullOrEmpty(ddlGroupEdit.SelectedValue))
                {
                    if (Convert.ToInt32(ddlGroupEdit.SelectedValue) == (int)Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                        Convert.ToInt32(ddlGroupEdit.SelectedValue) == (int)Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                        Convert.ToInt32(ddlGroupEdit.SelectedValue) == (int)Enumerations.AuthUserGroup.ResellerAdmin ||
                        Convert.ToInt32(ddlGroupEdit.SelectedValue) == (int)Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                    {
                        FillDealersInDDL(ref ddlDealersEdit, (Enumerations.AuthUserGroup)Enum.Parse(typeof(Enumerations.AuthUserGroup), ddlGroupEdit.SelectedValue));
                        ddlDealersEdit.Enabled = true;
                    }
                    else
                    {
                        ddlDealersEdit.Enabled = false;
                    }
                }

                ShowHideControls(e.NewEditIndex, isVisible);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

        }
        protected void gvUsers_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                int groupID = Convert.ToInt32(((Label)gvUsers.Rows[e.RowIndex].Cells[1].FindControl("lblGroupID")).Text);
                var ddlGroup = (DropDownList)gvUsers.Rows[e.RowIndex].Cells[1].FindControl("ddlGroup");
                var ddlSubGroup = (DropDownList)gvUsers.Rows[e.RowIndex].Cells[1].FindControl("ddlSubGroup");

                ddlGroup.SelectedValue = groupID.ToString();
                if (groupID != (int)Enumerations.AuthUserGroup.GroupDealerAdmin)
                {
                    ddlSubGroup.Attributes.Add("style", "display:none;");
                }

                bool isVisible = (gvUsers.Rows[e.RowIndex].Cells[6].FindControl("lnkEdit")).Visible;
                ShowHideControls(e.RowIndex, isVisible);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int userID = Convert.ToInt32(((Label)gvUsers.Rows[e.RowIndex].Cells[0].FindControl("lblUserId")).Text);
                var user = new UserBLL() { UserID = userID };
                if (user.Delete() == 0) lblInfo.Text = "<li>(User deleted successfully)";
                //refill Grid
                FillGrid(ddlSearchBy.SelectedValue, txtSearchCriteria.Text.Trim());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                switch (e.Row.RowType)
                {
                    case DataControlRowType.DataRow:
                        {
                            var ddlGroup = (DropDownList)e.Row.Cells[1].FindControl("ddlGroup");
                            string groupID = ((Label)e.Row.Cells[1].FindControl("lblGroupID")).Text;

                            _groups.GetGroupsForSpecialGroup(Security.GetGroupLoggedUser());
                            ddlGroup.DataSource = _groups;
                            ddlGroup.DataBind();
                            ddlGroup.SelectedValue = groupID;

                            var ddlSubGroup = (DropDownList)e.Row.Cells[1].FindControl("ddlSubGroup");
                            string subGroupID = ((Label)e.Row.Cells[1].FindControl("lblSubGroupDealerAdmin")).Text;

                            //var dealerTypeID = (int)Security.GetDealerTypeLoggedUser(Security.GetGroupLoggedUser());
                            _subGroups.GetDealerGroupsByDealerAdmin(Security.GetCurrentUserGroup().ToString());
                            ddlSubGroup.DataSource = _subGroups;
                            ddlSubGroup.DataBind();
                            ddlSubGroup.Items.Insert(0, new ListItem() { Text = "None", Value = "" });
                            ddlSubGroup.SelectedValue = subGroupID;
                            if (Convert.ToInt32(groupID) == (int)Enumerations.AuthUserGroup.GroupDealerAdmin)
                                ddlSubGroup.Attributes.Add("style", "display:inline;");
                            else ddlSubGroup.Attributes.Add("style", "display:none;");

                        }
                        break;
                    default:
                        {
                            _groups.GetGroupsForSpecialGroup(Security.GetGroupLoggedUser());
                            //var dealerTypeID = (int)Security.GetDealerTypeLoggedUser(Security.GetGroupLoggedUser());
                            _subGroups.GetDealerGroupsByDealerAdmin(Security.GetCurrentUserGroup().ToString());
                        }
                        break;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                _users = SortUsers(_users);
                gvUsers.DataSource = _users;
                gvUsers.PageIndex = e.NewPageIndex;
                gvUsers.DataBind();

                FillControlsInGrid();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void gvUsers_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (SortColumn.ToUpper() == e.SortExpression.ToUpper() && this.SortDirection == "ASC")
                { SortDirection = "DESC"; }
                else { SortDirection = "ASC"; }
                //set sort column
                SortColumn = e.SortExpression;

                _users = SortUsers(_users);
                gvUsers.DataSource = _users;
                gvUsers.DataBind();

                FillControlsInGrid();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        #region ' Controls 's Events '
        protected void ddlSearchBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtSearchCriteria.Visible = ddlSearchBy.SelectedValue != "GroupName";
                if (txtSearchCriteria.Visible) txtSearchCriteria.Focus();
                ddlSearchByDealerGroup.Visible = ddlSearchBy.SelectedValue == "GroupName";
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void DDLGroupAddSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var ddlGroupAdd = (DropDownList)gvUsers.FooterRow.FindControl("ddlGroupAdd");
                var ddlSubGroupAdd = (DropDownList)gvUsers.FooterRow.FindControl("ddlSubGroupAdd");
                var ddlDealersAdd = (DropDownList)gvUsers.FooterRow.FindControl("ddlDealersAdd");
                if (!string.IsNullOrEmpty(ddlGroupAdd.SelectedValue))
                {
                    if (Convert.ToInt32(ddlGroupAdd.SelectedValue) == (int)Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                        Convert.ToInt32(ddlGroupAdd.SelectedValue) == (int)Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                        Convert.ToInt32(ddlGroupAdd.SelectedValue) == (int)Enumerations.AuthUserGroup.ResellerAdmin ||
                        Convert.ToInt32(ddlGroupAdd.SelectedValue) == (int)Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                    {
                        FillDealersInDDL(ref ddlDealersAdd, (Enumerations.AuthUserGroup)Enum.Parse(typeof(Enumerations.AuthUserGroup), ddlGroupAdd.SelectedValue));
                        ddlDealersAdd.Enabled = true;
                    }
                    else
                    {
                        ddlDealersAdd.Enabled = false;
                        ddlDealersAdd.SelectedValue = string.Empty;
                    }
                    if (Convert.ToInt32(ddlGroupAdd.SelectedValue) == (int)Enumerations.AuthUserGroup.GroupDealerAdmin)
                    {
                        //var dealerTypeID = (int)Security.GetDealerTypeLoggedUser(Security.GetGroupLoggedUser());
                        _subGroups.GetDealerGroupsByDealerAdmin(Security.GetCurrentUserGroup().ToString());

                        ddlSubGroupAdd.DataSource = _subGroups;
                        ddlSubGroupAdd.DataBind();
                        ddlSubGroupAdd.Visible = true;
                        ddlSubGroupAdd.Enabled = true;
                    }
                    else
                    {
                        ddlSubGroupAdd.Visible = false;
                        ddlSubGroupAdd.Enabled = false;
                    }
                }
                else
                {
                    ddlSubGroupAdd.Visible = false;
                    ddlSubGroupAdd.Enabled = false;
                }

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void DDLGroupEditSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedRow = (GridViewRow)(((DropDownList)sender).NamingContainer);
                var ddlGroup = (DropDownList)sender;
                var ddlSubGroup = (DropDownList)gvUsers.Rows[selectedRow.RowIndex].Cells[4].FindControl("ddlSubGroup");
                var ddlDealers = (DropDownList)gvUsers.Rows[selectedRow.RowIndex].Cells[1].FindControl("ddlDealersEdit");

                if (Convert.ToInt32(ddlGroup.SelectedValue) == (int)Enumerations.AuthUserGroup.GroupDealerAdmin)
                {
                    //var dealerTypeID = (int)Security.GetDealerTypeLoggedUser(Security.GetGroupLoggedUser());

                    _subGroups.GetDealerGroupsByDealerAdmin(Security.GetCurrentUserGroup().ToString());

                    ddlSubGroup.DataSource = _subGroups;
                    ddlSubGroup.DataBind();
                    ddlSubGroup.Attributes.Add("style", "display:inline");
                    ddlSubGroup.Enabled = true;
                }

                if (!string.IsNullOrEmpty(ddlGroup.SelectedValue))
                {
                    if (Convert.ToInt32(ddlGroup.SelectedValue) == (int)Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                        Convert.ToInt32(ddlGroup.SelectedValue) == (int)Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                        Convert.ToInt32(ddlGroup.SelectedValue) == (int)Enumerations.AuthUserGroup.ResellerAdmin ||
                        Convert.ToInt32(ddlGroup.SelectedValue) == (int)Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                    {
                        FillDealersInDDL(ref ddlDealers, (Enumerations.AuthUserGroup)Enum.Parse(typeof(Enumerations.AuthUserGroup), ddlGroup.SelectedValue));
                        ddlDealers.Enabled = true;
                    }
                    else
                    {
                        ddlDealers.Enabled = false;
                        ddlDealers.SelectedValue = string.Empty;
                    }
                    if (Convert.ToInt32(ddlGroup.SelectedValue) == (int)Enumerations.AuthUserGroup.GroupDealerAdmin)
                    {
                        //var dealerTypeID = (int)Security.GetDealerTypeLoggedUser(Security.GetGroupLoggedUser());
                        _subGroups.GetDealerGroupsByDealerAdmin(Security.GetCurrentUserGroup().ToString());

                        ddlSubGroup.DataSource = _subGroups;
                        ddlSubGroup.DataBind();
                        ddlSubGroup.Visible = true;
                        ddlSubGroup.Enabled = true;
                    }
                    else
                    {
                        ddlSubGroup.Visible = false;
                        ddlSubGroup.Enabled = false;
                    }
                }
                else
                {
                    ddlSubGroup.Visible = false;
                    ddlSubGroup.Enabled = false;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        #region ' Methods '
        private static void FillDealersInDDL(ref DropDownList ddlDealers, Enumerations.AuthUserGroup group)
        {
            try
            {
                _dealers.GetDealersForSpecialGroup(group);
                ddlDealers.DataSource = _dealers;
                ddlDealers.DataBind();
                ddlDealers.Items.Insert(0, new ListItem() { Text = "None", Value = "" });
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void FillGrid(string searchBy, string searchValue)
        {
            try
            {
                _users.GetUsersForSpecialGroup(Security.GetGroupLoggedUser(), searchBy, searchValue);

                _users = SortUsers(_users);
                gvUsers.DataSource = _users;
                gvUsers.DataBind();

                FillControlsInGrid();

                foreach (GridViewRow row in gvUsers.Rows)
                {
                    ShowHideControls(row.RowIndex, false);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

        }
        private void FillControlsInGrid()
        {
            try
            {
                _groups.GetGroupsForSpecialGroup(Security.GetGroupLoggedUser());

                if (gvUsers.FooterRow.FindControl("ddlGroupAdd") != null)
                {
                    var ddlGroupAdd = (DropDownList)gvUsers.FooterRow.FindControl("ddlGroupAdd");
                    ddlGroupAdd.DataSource = _groups;
                    ddlGroupAdd.DataBind();
                    ddlGroupAdd.Items.Insert(0, new ListItem() { Text = "Select User Type", Value = "" });
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        private void ShowHideControls(int rowIndex, bool isVisible)
        {
            try
            {
                //cell[1]
                (gvUsers.Rows[rowIndex].Cells[2].FindControl("lblDealerNameEdit")).Visible = !isVisible;
                (gvUsers.Rows[rowIndex].Cells[2].FindControl("ddlDealersEdit")).Visible = isVisible;
                //cell[2]
                (gvUsers.Rows[rowIndex].Cells[3].FindControl("lblUsernameEdit")).Visible = !isVisible;
                (gvUsers.Rows[rowIndex].Cells[3].FindControl("txtUsernameEdit")).Visible = isVisible;
                //cell[3]
                (gvUsers.Rows[rowIndex].Cells[4].FindControl("lblPasswordEdit")).Visible = !isVisible;
                (gvUsers.Rows[rowIndex].Cells[4].FindControl("txtPasswordEdit")).Visible = isVisible;
                //cell[4]
                ((DropDownList)gvUsers.Rows[rowIndex].Cells[1].FindControl("ddlGroup")).Enabled = isVisible;
                ((DropDownList)gvUsers.Rows[rowIndex].Cells[1].FindControl("ddlSubGroup")).Enabled = isVisible;
                //cell[5]
                ((CheckBox)gvUsers.Rows[rowIndex].Cells[5].FindControl("chkActivateUser")).Enabled = isVisible;
                //cell[6]
                (gvUsers.Rows[rowIndex].Cells[6].FindControl("lnkEdit")).Visible = !isVisible;
                (gvUsers.Rows[rowIndex].Cells[6].FindControl("lnkDelete")).Visible = !isVisible;
                (gvUsers.Rows[rowIndex].Cells[6].FindControl("lnlUpdateEdit")).Visible = isVisible;
                (gvUsers.Rows[rowIndex].Cells[6].FindControl("lnkCancelEdit")).Visible = isVisible;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private Users SortUsers(Users usersObj)
        {
            try
            {
                var type = (Enumerations.UsersComparisonType)Enum.Parse(typeof(Enumerations.UsersComparisonType), SortColumn);
                if (SortDirection == "DESC") usersObj.Sort(new UserComparerReverse(type));
                else usersObj.Sort(new UserComparer(type));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return usersObj;
        }

        #endregion
    }
}