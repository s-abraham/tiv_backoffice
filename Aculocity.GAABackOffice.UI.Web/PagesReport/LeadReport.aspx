﻿<%@ Page Title="Lead Reports" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true"
    CodeBehind="LeadReport.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesReport.LeadReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        .tivBtn {
            background: transparent; 
            background-image: url(../Images/Buttons/orangebutton2-small-short.png); 
            background-repeat: no-repeat; 
            width: 135px; 
            border: 0; 
            line-height: 30px;
            font-size: 12px; 
            font-weight: bold;
            color: #fff;
        }
        .reportBlock {
            border: 1px solid orange;
            min-height: 230px;
            padding: 15px;
            overflow: auto;
        }
        .reportBlockTitle {
            font-size: 16px;
            color: #F5831F;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">

    <script language="JavaScript">
        changeclass("ctl00_liLeadsReports");
    </script>

    <script type="text/javascript" src="../JScripts/wz_tooltip.js"></script>

    <script type="text/javascript" src="../JScripts/FusionCharts.js"></script>

    <script type="text/javascript">
        window.onerror = function(msg, url, linenumber) {
            //alert('Error message: ' + msg + '\nURL: ' + url + '\nLine Number: ' + linenumber);
            return true;
        }
    </script>

    <script type="text/javascript">
        function CheckRadioButtons() {
            var rb1 = document.getElementById("ctl00_PageContent_rblLastWeek_0");
            if (rb1.checked == true) {
                var StartDate = new Date();
                var dayNumber = StartDate.getDay();
                StartDate.setDate(StartDate.getDate() - (7 + (dayNumber - 1)));

                var EndDate = new Date();
                EndDate.setDate(EndDate.getDate() - (7 + (dayNumber - 1)));
                EndDate.setDate(EndDate.getDate() + 6);

                document.getElementById("ctl00_PageContent_dpStartDate_txtDate").value = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
                document.getElementById("ctl00_PageContent_dpEndDate_txtDate").value = (EndDate.getMonth() + 1) + "/" + EndDate.getDate() + "/" + EndDate.getFullYear();
            }

            var rb2 = document.getElementById("ctl00_PageContent_rblLastWeek_1");
            if (rb2.checked == true) {
                var StartDate = new Date();
                StartDate.setDate(StartDate.getDate() - 7);

                var EndDate = new Date();

                document.getElementById("ctl00_PageContent_dpStartDate_txtDate").value = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
                document.getElementById("ctl00_PageContent_dpEndDate_txtDate").value = (EndDate.getMonth() + 1) + "/" + EndDate.getDate() + "/" + EndDate.getFullYear();
            }
            
            var rb3 = document.getElementById("ctl00_PageContent_rlrLastWeek_1");
            if (rb3.checked == true) {
                var StartDate = new Date();
                StartDate.setDate(StartDate.getDate() - 7);

                var EndDate = new Date();

                document.getElementById("ctl00_PageContent_rlrDate1_txtDate").value = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
                document.getElementById("ctl00_PageContent_rlrDate2_txtDate").value = (EndDate.getMonth() + 1) + "/" + EndDate.getDate() + "/" + EndDate.getFullYear();
            }
            var rb4 = document.getElementById("ctl00_PageContent_rlrLastWeek_0");
            if (rb4.checked == true) {
                var StartDate = new Date();
                var dayNumber = StartDate.getDay();
                StartDate.setDate(StartDate.getDate() - (7 + (dayNumber - 1)));

                var EndDate = new Date();
                EndDate.setDate(EndDate.getDate() - (7 + (dayNumber - 1)));
                EndDate.setDate(EndDate.getDate() + 6);

                document.getElementById("ctl00_PageContent_rlrDate1_txtDate").value = (StartDate.getMonth() + 1) + "/" + StartDate.getDate() + "/" + StartDate.getFullYear();
                document.getElementById("ctl00_PageContent_rlrDate2_txtDate").value = (EndDate.getMonth() + 1) + "/" + EndDate.getDate() + "/" + EndDate.getFullYear();
            }
        }

    </script>

    <asp:UpdatePanel ID="upContent" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr align="center">
                    <td align="center">
                        <table width="100%">
                            <tr align="left">
                                <td>
                                    <table>
                                        <tr>
                                            <td valign="top" align="left" runat="server" id="tdLeadTotalsReport">
                                                <div class="reportBlock">
                                                    <asp:Label ID="lblTotalLeadReportHeader" runat="server" CssClass="reportBlockTitle" Text="Lead totals report"></asp:Label><br />
                                                
                                                    <div id="divLeadTotalsReport" runat="server" >
                                                    <table style="height: 100%">
                                                        <tr>
                                                            <td>
                                                                <table width="400px" style="height: 100%">
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:RadioButtonList ID="rbScale" runat="server" RepeatDirection="Horizontal" 
                                                                                AutoPostBack="true" OnSelectedIndexChanged="rbScale_SelectedIndexChanged">
                                                                                <asp:ListItem Text="Daily" Enabled="true" Selected="False" Value="0"></asp:ListItem>
                                                                                <asp:ListItem Text="Monthly" Enabled="true" Selected="False" Value="1"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="100px">
                                                                            Date Range:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlDateRange" runat="server" Width="250px" >
                                                                            </asp:DropDownList>
                                                                            <asp:DropDownList ID="ddlYearRange" runat="server" Width="250px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chbMultipleSelection" runat="server" Checked="false" Text="Multiple Dealers"
                                                                                AutoPostBack="true" OnCheckedChanged="chbMultipleSelection_Clicked" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Dealership:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlDealerShip" runat="server" Width="250px">
                                                                            </asp:DropDownList>
                                                                            <asp:ListBox ID="lstDealerShip" runat="server" Width="250px" Height="100px" Visible="false"
                                                                                SelectionMode="Multiple" ToolTip="Hold down the ctrl key to select multiple items">
                                                                            </asp:ListBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Report Layout:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlLayout" runat="server" Width="250px">
                                                                                <asp:ListItem Text="Horizontal" Value="0" Selected="True" />
                                                                                <asp:ListItem Text="Vertical" Value="1" />
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Dealer Type:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlStatus" runat="server" Width="150px">
                                                                                <asp:ListItem Text="Active" Value="Active" Selected="True" />
                                                                                <asp:ListItem Text="Billable" Value="Billable" />
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 100%">
                                                                        <td colspan="3">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <table style="border-width: 0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <asp:Button ID="btnLeadReport" runat="server" Text="Lead Totals Report" OnClick="btnLeadReport_Click" CssClass="tivBtn"/>
                                                                                    </td>
                                                                                    <td align="left" valign="top">
                                                                                        &nbsp;
                                                                                        <asp:ImageButton ID="btnExportExcel1" Visible="false" runat="server" Height="24px"
                                                                                            Width="24px" OnClick="btnExportExcel_Click" ImageUrl="~/Images/Excelicon24x24.png"
                                                                                            ToolTip="Export to excel" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 3px">
                                                                        <td colspan="3">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td valign="middle" style="padding-right: 5px">
                                                                &nbsp;
                                                                <img id="Img1" runat="server" alt="" src="../Images/questionmark.png" onmouseover="Tip('&nbsp;This report will display the total number of leads for either <br>a single dealer or all dealers depending on selection criteria, <br>you can click on a dealername to display an indepth listing of the leads')"
                                                                    onmouseout="UnTip()" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                </div>

                                            </td>
                                            <td width="280px">
                                            </td>
                                            <td valign="top" align="left"  runat="server" id="tdLeadDetailsReport">
                                                <div class="reportBlock">
                                                    <asp:Label ID="lblDetailsLeadReportHeader" runat="server" Text="Lead details report" CssClass="reportBlockTitle"></asp:Label><br />
                                                    <div id="divDetailsLeadreport" runat="server" >
                                                    <table style="height: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="rblLastWeek" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="Last Mon-Sun" Enabled="true" Selected="False" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="Last 7 Days" Enabled="true" Selected="False" Value="1"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left">
                                                                <table width="450px" style="height: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            From:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="upStartDate" runat="server">
                                                                                <ContentTemplate>
                                                                                    <GAABackOfficeControls:DatePicker ID="dpStartDate" runat="server" Width="187px" ImageUrl="~/Images/Calendar/cal.gif" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            To:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="upEndDate" runat="server">
                                                                                <ContentTemplate>
                                                                                    <GAABackOfficeControls:DatePicker ID="dpEndDate" runat="server" Width="187px" ImageUrl="~/Images/Calendar/cal.gif" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            Dealership:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlDealerShipDeatails" runat="server" Width="250px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="CellLabel">
                                                                            Display:
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <table border="0" cellpadding="0" cellspacing="2">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButtonList ID="rblLeadsCountPerPage" runat="server" RepeatDirection="Horizontal"
                                                                                            Font-Bold="true">
                                                                                            <asp:ListItem Value="25" Selected="True">25</asp:ListItem>
                                                                                            <asp:ListItem Value="50">50</asp:ListItem>
                                                                                            <asp:ListItem Value="100">100</asp:ListItem>
                                                                                            <asp:ListItem Value="150">150</asp:ListItem>
                                                                                            <asp:ListItem Value="200">200</asp:ListItem>
                                                                                            <asp:ListItem Value="250">250</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                    <td class="CellLabel">
                                                                                        &nbsp;&nbsp;leads per page
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <asp:RadioButtonList ID="rbtnLeadtype" runat="server" RepeatDirection="Horizontal">
                                                                                <asp:ListItem Text="All Leads" Value="2" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Text="Facebook Leads" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="Dealer website Leads" Value="0"></asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <table style="border-width: 0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" CssClass="tivBtn" />
                                                                                    </td>
                                                                                    <td align="left" valign="top">
                                                                                        &nbsp;
                                                                                        <asp:Button ID="btnLeadDetailsReport" runat="server" Text="Lead Details Report" OnClick="btnLeadDetailsReport_Click" CssClass="tivBtn" />
                                                                                    </td>
                                                                                    <td align="left" valign="top">
                                                                                        &nbsp;
                                                                                        <asp:ImageButton ID="btnExportExcel2" runat="server" Height="24px" 
                                                                                            ImageUrl="~/Images/Excelicon24x24.png" OnClick="btnExportExcel_Click" 
                                                                                            ToolTip="Export to excel" Visible="false" Width="24px" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 5px">
                                                                        <td colspan="3">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="padding-right: 5px">
                                                                <img id="CreditHelp" runat="server" alt="" src="../Images/questionmark.png" onmouseover="Tip('&nbsp;This report will display the detail for each Lead for <br>a specific dealership based on the date range')"
                                                                    onmouseout="UnTip()" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                </div>

                                            </td>
                                            <td width="280px">
                                                &nbsp;
                                            </td>

                                            <%-- reseller leads report options--%>
                                            <td valign="top" align="left" runat="server" id="tdResellerLeadsReport">
                                                <div class="reportBlock">
                                                    <asp:Label ID="Label1" runat="server" Text="Reseller Leads Report" CssClass="reportBlockTitle"></asp:Label><br />
                                                    <div id="div1" runat="server" >
                                                    <table style="height: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButtonList ID="rlrLastWeek" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="Last Mon-Sun" Enabled="true" Selected="False" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="Last 7 Days" Enabled="true" Selected="False" Value="1"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left">
                                                                <table width="450px" style="height: 100%">
                                                                    <tr>
                                                                        <td>
                                                                            From:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="rlrUdpatePanelDate1" runat="server">
                                                                                <ContentTemplate>
                                                                                    <GAABackOfficeControls:DatePicker ID="rlrDate1" runat="server" Width="187px" ImageUrl="~/Images/Calendar/cal.gif" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            To:
                                                                        </td>
                                                                        <td>
                                                                            &nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <asp:UpdatePanel ID="rlrUpdatePanelDate2" runat="server">
                                                                                <ContentTemplate>
                                                                                    <GAABackOfficeControls:DatePicker ID="rlrDate2" runat="server" Width="187px" ImageUrl="~/Images/Calendar/cal.gif" />
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="CellLabel">
                                                                            Display:
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <table border="0" cellpadding="0" cellspacing="2">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:RadioButtonList ID="rlrLeadsPerPage" runat="server" RepeatDirection="Horizontal"
                                                                                            Font-Bold="true">
                                                                                            <asp:ListItem Value="25" Selected="True">25</asp:ListItem>
                                                                                            <asp:ListItem Value="50">50</asp:ListItem>
                                                                                            <asp:ListItem Value="100">100</asp:ListItem>
                                                                                            <asp:ListItem Value="150">150</asp:ListItem>
                                                                                            <asp:ListItem Value="200">200</asp:ListItem>
                                                                                            <asp:ListItem Value="250">250</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                    <td class="CellLabel">
                                                                                        &nbsp;&nbsp;leads per page
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td colspan="2">
                                                                            <table style="border-width: 0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td align="left" valign="top">
                                                                                        <asp:Button ID="rlrBackButton" runat="server" Text="Back" OnClick="btnBack_Click" CssClass="tivBtn" />
                                                                                    </td>
                                                                                    <td align="left" valign="top">
                                                                                        &nbsp;
                                                                                        <asp:Button ID="rlrLeadDetailsReportButton" runat="server" Text="Lead Details Report" OnClick="btnResellerLeadsReport_Click" CssClass="tivBtn" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="height: 5px">
                                                                        <td colspan="3">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                </div>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <table width="100%">
                                                    <tr>
                                                        <td id="tdCharts" runat="server" style="display: none;" align="center">
                                                            <GAABackOfficeControls:LeadReportCharts ID="lrCharts" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table id="DashboardTable" runat="server" width="100%">
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="red"></asp:Label>
                                    <asp:Label ID="lblHeader" runat="server" Text="" Font-Size="16px" Font-Bold="True"></asp:Label><p>
                                        &nbsp;
                                        <asp:Panel ID="pGrids" runat="server" CssClass="LeadsReportGrid">
                                            <asp:GridView ID="gvReportGrid" runat="server" CellPadding="3" EnableViewState="True" BackColor="#CCCCCC"
                                                ForeColor="Black" Width="100%" BorderStyle="Solid" BorderWidth="1px" BorderColor="White"
                                                CssClass="text" CaptionAlign="Top" OnDataBound="gvReportGrid_DataBound" OnRowDataBound="gvReportGrid_RowDataBound"
                                                OnRowCreated="gvReportGrid_RowCreated">
                                                <RowStyle Wrap="False" HorizontalAlign="Center" BackColor="White" />
                                                
                                                <AlternatingRowStyle BackColor="#F0F0F0" />
                                                <EmptyDataTemplate>
                                                    <asp:Label ID="lblEmptyGridText" Text="There is no data for the selected Dealer"
                                                        ForeColor="Red" runat="server"></asp:Label>
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                            <GAABackOfficeControls:DetailsReportGrid ID="drgDetailsReport" runat="server" />
                                            <GAABackOfficeControls:ResellerDetailsReportGrid ID="drgResellerDetailsReport" runat="server" />
                                        </asp:Panel>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportExcel2" />
            <asp:PostBackTrigger ControlID="btnExportExcel1" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
