﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="LeadReportDeatail.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesReport.LeadReportDeatail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
    <table width="100%">
        <tr>
            <td>
            <table width="800px" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center">
                        <span style="color: gray;" id="message" runat="server">&nbsp;</span>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <br />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <GAABackOfficeControls:LeadReportDetailMainGrid ID="lrdmgMaiReportGrid" runat="server" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <p><p>
                        <asp:Button ID="btnBack" runat="server" Text="Back" onclick="btnBack_Click"/>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
</asp:Content>
