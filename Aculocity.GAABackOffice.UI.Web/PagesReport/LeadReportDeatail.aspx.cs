﻿using System;
using Aculocity.GAABackOffice.BLL;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesReport
{
    public partial class LeadReportDeatail : BasePage {

        protected void Page_Load(object sender, EventArgs e) {
            try
            {
                lrdmgMaiReportGrid.DealerKey = Request.QueryString["dealer_key"];
                lrdmgMaiReportGrid.PeriodFrom = GetDateTimeFromRequestVarriable(Request.QueryString["startdate"]);
                lrdmgMaiReportGrid.PeriodTill = GetDateTimeFromRequestVarriable(Request.QueryString["enddate"]);
                lrdmgMaiReportGrid.FirstName = string.Empty;
                lrdmgMaiReportGrid.LastName = string.Empty;
                lrdmgMaiReportGrid.UserMail = string.Empty;
                lrdmgMaiReportGrid.Phone = string.Empty;

                Session["SummaryDealerKey"] = Request.QueryString["dealer_key"];
                Session["SummaryPeriodFrom"] = GetDateTimeFromRequestVarriable(Request.QueryString["startdate"]);
                Session["SummaryPeriodTill"] = GetDateTimeFromRequestVarriable(Request.QueryString["enddate"]);
                Session["SummaryFirstName"] = string.Empty;
                Session["SummaryLastName"] = string.Empty;
                Session["SummaryUserMail"] = string.Empty;
                Session["SummaryPhone"] = string.Empty;
                Session["SummarySortExpression"] = "DateCreated";
                Session["SummarySortDirection"] = SortDirection.Descending; 
                Session["SummaryLeadsPerPage"] = 25;

                if (!IsPostBack)
                {
                    if (!string.IsNullOrEmpty(lrdmgMaiReportGrid.DealerKey)) lrdmgMaiReportGrid.GenerateReport();
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' Controls 's events '

        protected void btnBack_Click(object sender, EventArgs e) {
            try 
            {
                Response.Redirect("~/PagesReport/LeadReport.aspx", false);
            } 
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        DateTime GetDateTimeFromRequestVarriable(string varriable) {
            string[] elements = null;
            return string.IsNullOrEmpty(varriable) || (elements = varriable.Split(new char[] { '/' })).Length < 3 ? DateTime.MinValue : new DateTime(int.Parse(elements[0]), int.Parse(elements[1]), int.Parse(elements[2]));
        }

    }
        
}