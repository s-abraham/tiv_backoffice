﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Xml;
using System.IO;
using System.Text;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;


namespace Aculocity.GAABackOffice.UI.Web.PagesReport
{
    public partial class BillableDealers : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                try
                {
                    //DataBind();
                    gvBillableDealers.Visible = true;
                    gvAcuDealers.Visible = true;
                    //btnShowHide.Text =  "Hide Details";

                    //Load the years
                    LoadYears();
                    //Load the months
                    LoadMonths(DateTime.Today.Year);
                    ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(DateTime.Today.Month.ToString()));
                    //Load the dealer list and the totals
                    LoadBillableDealers(Convert.ToInt32(ddlYear.SelectedValue),
                                        Convert.ToInt32(ddlMonth.SelectedValue),
                                        (int)Security.GetGroupLoggedUser());

                    //Set xml data for chart
                    SetDataForChart(Convert.ToInt32(ddlYear.SelectedValue),
                                    Convert.ToInt32(ddlMonth.SelectedValue),
                                    (int)Security.GetGroupLoggedUser());

                    //Load activation and deactivation data
                    LoadActiveandDeactiveData();

                    //Set Visible for BillableDeales Grid
                    trBillableDealers.Visible = true;
                    trNewAddedDealers.Visible = false;
                    trDeactivatedDealers.Visible = false;
                }
                catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            }
        }

        #region ' Controls 's Events '
        protected void lnkNewDealers_Click(object sender, EventArgs e)
        {
            try
            {
                gvNewAddedDealers.DataSource = ReportBLL.GetNewDealersForGrid((int) Security.GetGroupLoggedUser());
                gvNewAddedDealers.DataBind();
                trBillableDealers.Visible = false;
                trDeactivatedDealers.Visible = false;
                trNewAddedDealers.Visible = true;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void lnkDeactivatedDealers_Click(object sender, EventArgs e)
        {
            try
            {
                gvDeactivatedDealers.DataSource =
                    ReportBLL.GetDeactivatedDealersForGrid((int) Security.GetGroupLoggedUser());
                gvDeactivatedDealers.DataBind();
                trBillableDealers.Visible = false;
                trNewAddedDealers.Visible = false;
                trDeactivatedDealers.Visible = true;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void ddlYear_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                int year;
                if (int.TryParse(ddlYear.SelectedValue.ToString(), out year))
                {
                    LoadMonths(year);
                }

                //Set xml data for chart
                SetDataForChart(Convert.ToInt32(ddlYear.SelectedValue),
                                Convert.ToInt32(ddlMonth.SelectedValue),
                                (int)Security.GetGroupLoggedUser());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                //Load the dealer list and the totals
                LoadBillableDealers(Convert.ToInt32(ddlYear.SelectedValue),
                                    Convert.ToInt32(ddlMonth.SelectedValue),
                                    (int)Security.GetGroupLoggedUser());
                //Set xml data for chart
                SetDataForChart(Convert.ToInt32(ddlYear.SelectedValue),
                                Convert.ToInt32(ddlMonth.SelectedValue),
                                (int)Security.GetGroupLoggedUser());

                //Set Visible for BillableDeales Grid
                trBillableDealers.Visible = true;
                trNewAddedDealers.Visible = false;
                trDeactivatedDealers.Visible = false;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnShowReport_Click(object sender, EventArgs e)
        {
            try
            {
                //Load the dealer list and the totals
                LoadBillableDealers(Convert.ToInt32(ddlYear.SelectedValue),
                                    Convert.ToInt32(ddlMonth.SelectedValue),
                                    (int)Security.GetGroupLoggedUser());
                //Set xml data for chart
                SetDataForChart(Convert.ToInt32(ddlYear.SelectedValue),
                                Convert.ToInt32(ddlMonth.SelectedValue),
                                (int)Security.GetGroupLoggedUser());

                //Set Visible for BillableDeales Grid
                trBillableDealers.Visible = true;
                trNewAddedDealers.Visible = false;
                trDeactivatedDealers.Visible = false;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnShowHide_Click(object sender, EventArgs e)
        {
            try
            {
                //bool isShow = btnShowHide.Text.Contains("View");
                //gvBillableDealers.Visible = isShow;
                //gvAcuDealers.Visible = isShow;
                //btnShowHide.Text = btnShowHide.Text.Contains("View") ? "Hide Details" : "View Details";
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                var file = new StringBuilder();
                DateTime dtStart = DateTime.Parse(string.Format("{0}/{1}/1", ddlYear.SelectedValue, ddlMonth.SelectedValue));
                file.AppendLine(string.Format(@"""Billing Date Range"", {0}, {1}", dtStart.ToShortDateString(), dtStart.AddMonths(1).AddDays(-1).ToShortDateString()));
                var ds = ReportBLL.GetBillableDealers(Convert.ToInt32(ddlYear.SelectedValue),
                                                      Convert.ToInt32(ddlMonth.SelectedValue),
                                                      (int)Security.GetGroupLoggedUser());
                string acuRate = "0";
                string reducedRate = "0";
                
                foreach(DataRow dr in ds.Tables[ds.Tables.Count - 1].Rows)
                {
                    double rate;    
                    if(Double.TryParse(dr["Rate"].ToString(), out rate))
                    {
                        if (rate == 25)
                        {
                            acuRate = dr["DealerCount"].ToString();
                        }
                        if (rate == 12.5)
                        {
                            reducedRate = dr["DealerCount"].ToString();
                        }
                    }
                }

                int tbltotal = ds.Tables.Count == 4 ? 2 : 1;

                file.AppendLine(string.Format(@"""Number of Billable Dealers"",""{0}""", ds.Tables[tbltotal].Rows[0]["TotalCount"]));
                file.AppendLine(string.Format(@"""Aculocity Dealer Fee"",""{0}""",
                                              Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.HomeNetSuperUser
                                                  ? (25).ToString("c", CultureInfo.CurrentCulture)
                                                  : (25).ToString("c", CultureInfo.CurrentCulture)));
                double amt;
                if (Double.TryParse(ds.Tables[ds.Tables.Count - 2].Rows[0]["TotalRate"].ToString(), out amt))
                {
                    file.AppendLine(string.Format(@"""Projected Invoice Amount"",""{0}""", amt.ToString("c", CultureInfo.CurrentCulture)));
                }

                file.AppendLine(@"""AutoAppraise Dealers""");
                file.AppendLine(String.Format(@"""{0}"",""{1}"",""{2}"",""{3}"",""{4}"",""{5}"",""{6}"",""{7}"",""{8}"",""{9}"",""{10}"",""{11}"",""{12}"""
                                              , "Dealer Name"
                                              , "Date Created"
                                              , "System Billable Start Date"
                                              , "Admin Billable Start Date"
                                              , ds.Tables[0].Columns[4].ColumnName
                                              , ds.Tables[0].Columns[5].ColumnName
                                              , ds.Tables[0].Columns[6].ColumnName
                                              , ds.Tables[0].Columns[7].ColumnName
                                              , ds.Tables[0].Columns[8].ColumnName
                                              , ds.Tables[0].Columns[9].ColumnName
                                              , ds.Tables[0].Columns[10].ColumnName
                                              , ds.Tables[0].Columns[11].ColumnName
                                              , "Comments"));
                foreach(DataRow dr in ds.Tables[0].Rows)
                {
                    DateTime dtDateCreated;
                    DateTime dtSystemBillable;
                    DateTime dtAdminBillable;
                    if (DateTime.TryParse(dr["DateCreated"].ToString(), out dtDateCreated) &&
                        DateTime.TryParse(dr["SystemBillableStartDate"].ToString(), out dtSystemBillable) &&
                        DateTime.TryParse(dr["AdminBillableStartDate"].ToString(), out dtAdminBillable)) 
                    {
                        file.AppendLine(string.Format(@"""{0}"",""{1}"",""{2}"",""{3}"",""{4}"",""{5}"",""{6}"",""{7}"",""{8}"",""{9}"",""{10}"",""{11}"",""{12}"""
                                                      , dr["DealerName"]
                                                      , dtDateCreated.ToShortDateString()
                                                      , dtSystemBillable.ToShortDateString()
                                                      , dtAdminBillable.ToShortDateString()
                                                      , Convert.ToDateTime(dr[4]).ToShortDateString()
                                                      , dr[5]
                                                      , dr[6]
                                                      , dr[7]
                                                      , dr[8]
                                                      , dr[9]
                                                      , dr[10]
                                                      , dr[11]
                                                      , dr["Comments"]));
                    }
                }
                byte[] btFile = StrToByteArray(file.ToString());
                Response.AddHeader("Content-disposition", string.Format("attachment; filename={0}-GAA.csv", DateTime.Today.ToString("yyyy-MM-dd")));
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(btFile);
                Response.End();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        #region ' Methods '

        private void LoadActiveandDeactiveData()
        {
            try
            {
                int NewDealers = ReportBLL.GetNewDealers((int)Security.GetGroupLoggedUser());
                int DeactivatedDealers = ReportBLL.GetDeactivatedDealers((int)Security.GetGroupLoggedUser());

                lblDealersAdded.Text = NewDealers.ToString();
                lblDealersDeactivated.Text = DeactivatedDealers.ToString();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void LoadYears()
        {
            try
            {
                DataSet ds = ReportBLL.GetBillableYears();
                ddlYear.DataSource = ds.Tables[0];
                ddlYear.DataTextField = "ArchiveYear";
                ddlYear.DataValueField = "ArchiveYear";
                ddlYear.DataBind();
                
                ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue(DateTime.Today.Year.ToString()));
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        private void LoadMonths(int year)
        {
            try
            {
                DataSet ds = ReportBLL.GetBillableMonths(year);
                ddlMonth.DataSource = ds.Tables[0];
                ddlMonth.DataTextField = "NameMonth";
                ddlMonth.DataValueField = "ArchiveMonth";
                ddlMonth.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void LoadBillableDealers(int year, int month, int dealerTypeID)
        {
            try
            {
                var ds = ReportBLL.GetBillableDealers(year, month, dealerTypeID);
                if (ds.Tables.Count > 0)
                {
                    int tbltotal; int tblRate;
                    //Set the date lables
                    DateTime dtStart = DateTime.Parse(string.Format("{0}/{1}/1", ddlYear.SelectedValue, ddlMonth.SelectedValue));
                    lblStartDate.Text = dtStart.ToShortDateString();
                    lblEndDate.Text = dtStart.AddMonths(1).AddDays(-1).ToShortDateString();

                    //set months values from last (6 months back) to selected month
                    for (int i = 5; i < 12;i++ )
                    {
                        var field = (BoundField)gvBillableDealers.Columns[i];
                        field.DataField = ds.Tables[0].Columns[i].ColumnName;
                        field.HeaderText = ds.Tables[0].Columns[i].ColumnName;
                    }

                    gvBillableDealers.DataSource = ds.Tables[0];
                    gvBillableDealers.DataBind();

                    if (ds.Tables.Count == 4)
                    {
                        gvAcuDealers.DataSource = ds.Tables[1];
                        gvAcuDealers.DataBind();
                        tbltotal = 2;
                        tblRate = 3;
                    }
                    else
                    {
                        tbltotal = 1;
                        tblRate = 2;
                    }

                    lblBillableDealers.Text = ds.Tables[tbltotal].Rows[0]["TotalCount"].ToString();

                    Double amt;
                    if(Double.TryParse(ds.Tables[tbltotal].Rows[0]["TotalRate"].ToString(),out amt))
                    {
                        lblAmtDue.Text = amt.ToString("c", CultureInfo.CurrentCulture);
                    }

                    string fee = string.Empty;
                    foreach(DataRow dr in ds.Tables[tblRate].Rows)
                    {
                        Double rate;
                        if (Double.TryParse(dr["Rate"].ToString(), out rate))
                        {
                            fee += rate.ToString("c", CultureInfo.CurrentCulture) + " (" + dr["DealerCount"] + " Dealers) <br />";
                        }
                    }
                    if (fee.Length > 2)
                    {
                        fee = fee.Substring(0, fee.Length - 7);
                    }
                    lblDealerFee.Text = string.IsNullOrEmpty(fee) ? "0.00" : fee;
                }
            }
            catch (Exception ex)
            {
                gvBillableDealers.Visible = false;
                gvAcuDealers.Visible = false;
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        private string GetDataForChart(int year, int month, int dealerTypeID)
        {
            var sb = new StringBuilder();
            try
            {
                Dictionary<string, string> data = ReportBLL.GetBillableDealersXML(year, month, dealerTypeID);
                sb.AppendLine("<graph  caption='Comparison' showValues = '0' numVDivLines = '10' ");
                sb.AppendLine(" numberPrefix = '' showAnchors = '0' divlinecolor = 'D7D8D3' divLineAlpha = '80' ");
                sb.AppendLine(" showAlternateHGridColor = '1' alternateHGridColor = 'D7D8D3' alternateHGridAlpha = '20' ");
                sb.AppendLine(" bgColor = 'E3E6D9' bgAlpha = '50' baseFontColor = '5D633F' canvasBorderThickness = '1' ");
                sb.AppendLine(" decimalPrecision='0' yAxisMinValue='0'> ");
                sb.AppendLine("<categories>");
                sb.AppendLine(data["CatigoriesDealer"].Replace(@"""", "'"));
                sb.AppendLine("</categories>");
                sb.AppendLine("<dataset seriesName='Billable Dealers' color='A66EDD'>");
                sb.AppendLine(data["SetDealers"].Replace(@"""", "'"));
                sb.AppendLine("</dataset>");
                sb.AppendLine("</graph>");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return sb.ToString();
        }

        private void SetDataForChart(int year, int month, int dealerTypeID)
        {
            try
            {
                //divXMLData.InnerText = GetDataForChart(year, month, dealerTypeID);
                litBillableDealersChart.Text = String.Empty;
                DotNet.Highcharts.Highcharts chart = GetBillableDealersChart(year, month, dealerTypeID);
                chart.InFunction("CreateBillableChart");
                litBillableDealersChart.Text = chart.ToHtmlString();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

            
        }
        private void log(Object obj)
        {
            Debugger.Log(0, "Test", "Message: " + obj.ToString() + "\n");
        }
        private DotNet.Highcharts.Highcharts GetBillableDealersChart(int year, int month, int dealerTypeID)
        {
            Dictionary<string,string> data = ReportBLL.GetBillableDealersXML(year, month, dealerTypeID);
            log(data["SetDealers"]);
            log(data["CatigoriesDealer"]);

            List<string> categories = new List<string>();
            
            XmlReader categoriesxml = XmlReader.Create(new StringReader("<data>" + data["CatigoriesDealer"] + "</data>"));
            while (categoriesxml.Read())
            {
                categoriesxml.MoveToFirstAttribute();
                if (categoriesxml.Value != "")
                {
                    categories.Add(categoriesxml.Value);
                }
            }

            List<string> values = new List<string>();
            XmlReader reader = XmlReader.Create(new StringReader("<data>" + data["SetDealers"] + "</data>"));
            
            while (reader.Read())
            {
                reader.MoveToFirstAttribute();
                if (reader.Value != "")
                {
                    values.Add(reader.Value);
                }
            }

            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("top_stats")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Line,
                    MarginRight = 30,
                    MarginBottom = 25,
                    ClassName = "top_stats",
                    Height = 200
                })
                .SetPlotOptions(new PlotOptions
                {
                    Line = new PlotOptionsLine
                    {
                        Color = ColorTranslator.FromHtml("#FBB003")
                        //LineColor = ColorTranslator.FromHtml("#666666")
                    }

                })
                .SetLegend(new Legend
                {
                    Enabled = false,
                    VerticalAlign = VerticalAligns.Top,
                    Align = HorizontalAligns.Right,
                    Shadow = false,
                    Layout = Layouts.Vertical,
                    ItemStyle = "font: 'normal 10px Arial, Verdana, sans-serif', color: '#666', fontWeight: 'bold'"
                })
                .SetTitle(new Title
                {
                    Text = ""
                })
                .SetXAxis(new XAxis
                {
                    Categories = categories.ToArray(),
                    Min = 0
                })
                .SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "" },
                    Min = 0
                })
                .SetSeries(new Series
                {
                    Data = new Data(values.ToArray()),
                    Name = "Dealers"
                });


            return chart;
        }

        private static byte[] StrToByteArray(string str)
        {
            try
            {
                var encoding = new ASCIIEncoding();
                return encoding.GetBytes(str);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return new byte[0];
        }
        #endregion
    }
}