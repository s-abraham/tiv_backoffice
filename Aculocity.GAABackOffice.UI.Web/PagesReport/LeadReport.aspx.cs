﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesReport
{
    public partial class LeadReport : BasePage
    {
        protected int totalLeads;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.ResellerAdmin)
                {
                    tdResellerLeadsReport.Visible = true;
                    tdLeadDetailsReport.Visible = false;
                    tdLeadTotalsReport.Visible = false;
                }
                else
                {
                    tdResellerLeadsReport.Visible = false;
                    tdLeadDetailsReport.Visible = true;
                    tdLeadTotalsReport.Visible = true;
                }

                if (Security.GetLoggedInUser().Trim().ToLower().StartsWith("adminbsb"))
                {
                    tdResellerLeadsReport.Visible = true;
                }

                if (!IsPostBack)
                {
                    var dealers = GetDealerList();
                    //fill ddlDealerShip
                    ddlDealerShip.DataSource = dealers;
                    ddlDealerShip.DataTextField = "DealerName";
                    ddlDealerShip.DataValueField = "DealerKey";
                    ddlDealerShip.DataBind();
                    ddlDealerShip.Items.Insert(0, new ListItem() { Text = "All Dealers", Value = "" });

                    //fill lstDealerShip
                    lstDealerShip.DataSource = dealers;
                    lstDealerShip.DataTextField = "DealerName";
                    lstDealerShip.DataValueField = "DealerKey";
                    lstDealerShip.DataBind();

                    //fill ddlDealerShipDeatails
                    ddlDealerShipDeatails.DataSource = dealers;
                    ddlDealerShipDeatails.DataTextField = "DealerName";
                    ddlDealerShipDeatails.DataValueField = "DealerKey";
                    ddlDealerShipDeatails.DataBind();


                    //fill ddlDateRange
                    var dateRange = new DataTable();
                    //dateRange.Clear();
                    dateRange.Columns.Add("Name");
                    dateRange.Columns.Add("Value");
                    int year = DateTime.Today.Year;
                    int month = DateTime.Today.Month;
                    dateRange.Rows.Add("Month to date", String.Format("{0}-{1}-01", year, month));
                    while (year > 2006)
                    {
                        month -= 1;
                        if (month == 0)
                        {
                            month = 12;
                            year--;
                        }
                        DateTime T = new DateTime(year, month, 1);
                        String sname = year == DateTime.Today.Year ? T.ToString("MMMM") : String.Format("{0} {1}", T.ToString("MMMM"), year);
                        dateRange.Rows.Add(sname, T.ToString());
                    }
                    dateRange.Rows.Add("Complete History", "CompleteHistory");
                    ddlDateRange.DataSource = dateRange;
                    ddlDateRange.DataTextField = "Name";
                    ddlDateRange.DataValueField = "Value";
                    ddlDateRange.DataBind();


                    //fill ddlYearRange
                    var yearRange = new DataTable();
                    yearRange.Columns.Add("Name");
                    yearRange.Columns.Add("Value");
                    year = DateTime.Today.Year;
                    yearRange.Rows.Add("Year to date", String.Format("{0}-{1}-01", year, month));
                    while (year > 2006)
                    {
                        year--;
                        DateTime T = new DateTime(year, 1, 1);
                        String sname = year.ToString();
                        yearRange.Rows.Add(sname, T.ToString());
                    }
                    yearRange.Rows.Add("Complete History", "CompleteHistory");
                    ddlYearRange.DataSource = yearRange;
                    ddlYearRange.DataTextField = "Name";
                    ddlYearRange.DataValueField = "Value";
                    ddlYearRange.DataBind();

                    //set date values by defaut
                    dpStartDate.Text = DateTime.Now.ToString("MM/01/yyyy").Replace(".", "/");
                    dpEndDate.Text = DateTime.Now.ToString("MM/dd/yyyy").Replace(".", "/");

                    ShowHideDealershipControls(false);

                    rblLastWeek.Attributes.Add("onclick", "CheckRadioButtons();");
                    rlrLastWeek.Attributes.Add("onclick", "CheckRadioButtons();");
                    rbScale.Attributes.Add("onclick", "CheckRadioButtons();");
                    ddlYearRange.Visible = false;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' Report Grid's Events '
        protected void gvReportGrid_DataBound(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ddlLayout.SelectedValue) == (int)Enumerations.Layout.Vertical)
                {
                    if (gvReportGrid.Rows.Count > 0)
                    {
                        gvReportGrid.FooterRow.Visible = true;
                        gvReportGrid.FooterRow.Cells[2].Text = totalLeads.ToString();
                        gvReportGrid.FooterRow.Cells[2].BackColor = System.Drawing.Color.Red;
                        gvReportGrid.FooterRow.Cells[2].ForeColor = System.Drawing.Color.White;
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void gvReportGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (Convert.ToInt32(ddlLayout.SelectedValue) == (int)Enumerations.Layout.Horizontal)
            {
                try
                {
                    e.Row.Cells[1].Text = Server.HtmlDecode(e.Row.Cells[1].Text);
                    if (e.Row.Cells[1].Text.Trim() != "Total Leads")
                    {
                        if (e.Row.Cells[e.Row.Cells.Count - 1].Controls.Count > 0)
                        {
                            if ((e.Row.Cells[e.Row.Cells.Count - 1].Controls[0]).GetType() == typeof(CheckBox))
                            {
                                if (!((CheckBox)(e.Row.Cells[e.Row.Cells.Count - 1].Controls[0])).Checked)
                                {
                                    e.Row.BackColor = System.Drawing.Color.Gray;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            }
            else if (Convert.ToInt32(ddlLayout.SelectedValue) == (int)Enumerations.Layout.Vertical)
            {
                try
                {
                    e.Row.Cells[0].Text = Server.HtmlDecode(e.Row.Cells[0].Text);
                    e.Row.Cells[1].Text = DateTime.Parse(e.Row.Cells[1].Text).ToShortDateString();
                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;

                    totalLeads += int.Parse(e.Row.Cells[2].Text);
                }
                catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            }
        }

        protected void gvReportGrid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[1].Wrap = false;
                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;
                /*
                if (e.Row.RowIndex < 0)
                {
                    e.Row.Cells[0].BackColor = System.Drawing.Color.Black;
                    e.Row.Cells[0].BorderColor = System.Drawing.Color.Black;
                    e.Row.Cells[1].BackColor = System.Drawing.Color.Black;
                    e.Row.Cells[1].BorderColor = System.Drawing.Color.Black;
                }
                else
                {
                    e.Row.Cells[0].Style["border-left"] = "0";
                    e.Row.Cells[0].Style["border-top"] = "0";
                    e.Row.Cells[0].Style["border-right"] = "1px solid #F0F0F0";
                    e.Row.Cells[1].Style["border-left"] = "0";
                    e.Row.Cells[1].Style["border-top"] = "0";
                    e.Row.Cells[1].Style["border-right"] = "1px solid #F0F0F0";
                }

                e.Row.Cells[0].Style["position"] = "relative";
                e.Row.Cells[0].Style["left"] = string.Format("expression(document.getElementById('{0}').scrollLeft+1)", pGrids.ClientID);
                e.Row.Cells[1].Style["position"] = "relative";
                e.Row.Cells[1].Style["left"] = string.Format("expression(document.getElementById('{0}').scrollLeft+1)", pGrids.ClientID);
                 */
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        #region ' Controls 's events '
        protected void chbMultipleSelection_Clicked(Object sender, EventArgs e)
        {
            try
            {
                ShowHideDealershipControls(chbMultipleSelection.Checked);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void btnLeadReport_Click(object sender, EventArgs e)
        {
            try
            {

                ShowHideTop10DesiredChart(false);
                gvReportGrid.Visible = true;

                lblError.Text = string.Empty;
                Session.Remove("ReportGrid");

                DataSet ds;

                string dealerKeys = string.Empty;
                if (chbMultipleSelection.Checked)
                {
                    var sb = new StringBuilder();
                    foreach (ListItem li in lstDealerShip.Items)
                    {
                        if (li.Selected) sb.Append(string.Format("'{0}',", li.Value));
                    }
                    dealerKeys = sb.ToString().TrimEnd(',');
                }
                else
                {
                    dealerKeys = string.Format("'{0}'", ddlDealerShip.SelectedValue);
                }

                Enumerations.Layout layout = ddlLayout.SelectedIndex == 0 ? Enumerations.Layout.Horizontal : Enumerations.Layout.Vertical;
                Enumerations.ScaleType scale = rbScale.SelectedIndex == 0 ? Enumerations.ScaleType.Daily : Enumerations.ScaleType.Monthly;

                String dateRange = rbScale.SelectedIndex == 0 ?
                    ddlDateRange.SelectedValue :
                    ddlYearRange.SelectedValue;
                ds = ReportBLL.GetLeadTotalScoreboard(dealerKeys, dateRange, ddlStatus.SelectedValue, rbtnLeadtype.SelectedValue, layout, scale);
                if (layout == Enumerations.Layout.Horizontal)
                {
                    AddCount(ref ds);
                    AddTotals(ref ds);
                }

                gvReportGrid.DataSource = ds;
                if (ds.Tables.Count > 0)
                {
                    gvReportGrid.DataBind();

                    if (Convert.ToInt32(ddlLayout.SelectedValue) == (int)Enumerations.Layout.Horizontal)
                    {
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            var dt = ds.Tables[1];
                            for (int i = 0; i < dt.Rows[0].ItemArray.Count(); i++)
                            {
                                gvReportGrid.HeaderRow.Cells[i].Text = dt.Rows[0][i].ToString();
                                gvReportGrid.HeaderRow.Cells[i].VerticalAlign = VerticalAlign.Top;
                                if (i != 1) gvReportGrid.HeaderRow.Cells[i].Width = 50;
                                gvReportGrid.HeaderRow.Cells[i].HorizontalAlign = i > 1
                                                                                      ? HorizontalAlign.Center
                                                                                      : HorizontalAlign.Left;
                            }
                        }
                    }
                    else
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            gvReportGrid.Width = 500;
                            gvReportGrid.HeaderRow.Cells[0].Width = 350;
                            gvReportGrid.HeaderRow.Cells[1].Width = 100;
                            gvReportGrid.HeaderRow.Cells[2].Width = 50;
                        }
                    }
                    btnExportExcel1.Visible = true;
                    btnExportExcel2.Visible = false;
                    drgDetailsReport.Visible = false;

                    Session["ReportGrid"] = gvReportGrid;
                }
                String labelText = rbScale.SelectedIndex == 0 ?
                    ddlDateRange.SelectedItem.Text :
                    ddlYearRange.SelectedItem.Text;
                lblHeader.Text = string.Format("Lead totals report for '{0}' - {1}",
                                                ddlDealerShip.SelectedItem.Text,
                                                labelText);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnResellerLeadsReport_Click(object sender, EventArgs e)
        {
            try
            {
                drgDetailsReport.Visible = false;
                drgResellerDetailsReport.Visible = true;
                gvReportGrid.Visible = false;
                drgDetailsReport.DealerKey = ddlDealerShipDeatails.SelectedValue;

                lblError.Text = string.Empty;

                Session.Remove("ReportGrid");

                bool datesAreValid = true;

                if (!Functions.IsDate(rlrDate1.Text))
                {
                    datesAreValid = false;
                    lblError.Text += "<li>Please enter a valid start date";
                }
                if (!Functions.IsDate(rlrDate2.Text))
                {
                    datesAreValid = false;
                    lblError.Text += "<li>Please enter a valid end date";
                }

                if (datesAreValid)
                {
                    if (DateTime.Parse(rlrDate2.Text, CultureInfo.InvariantCulture) < DateTime.Parse(rlrDate1.Text, CultureInfo.InvariantCulture))
                    {
                        lblError.Text += "<li>The end date cannot be less than the start date";
                    }
                    else
                    {
                        ShowHideTop10DesiredChart(false);
                        //lrCharts.DealerKey = ddlDealerShipDeatails.SelectedValue;
                        //lrCharts.GenerateCharts(rbtnLeadtype.SelectedValue);
                    }
                }

                drgResellerDetailsReport.StartDate = DateTime.Parse(rlrDate1.Text.Trim());
                drgResellerDetailsReport.EndDate = DateTime.Parse(rlrDate2.Text.Trim());
                drgResellerDetailsReport.LeadsPerPage = rlrLeadsPerPage.SelectedValue;

                drgResellerDetailsReport.GenerateReport(true);

                btnExportExcel1.Visible = false;
                btnExportExcel2.Visible = false;

                lblHeader.Text = string.Format("Reseller Leads report for {0} to {1}", dpStartDate.Text.Trim(), dpEndDate.Text.Trim());

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        protected void btnLeadDetailsReport_Click(object sender, EventArgs e)
        {
            try
            {
                drgDetailsReport.Visible = true;
                drgResellerDetailsReport.Visible = false;
                gvReportGrid.Visible = false;

                drgDetailsReport.DealerKey = ddlDealerShipDeatails.SelectedValue;

                lblError.Text = string.Empty;

                Session.Remove("ReportGrid");

                bool datesAreValid = true;

                if (!Functions.IsDate(dpStartDate.Text))
                {
                    datesAreValid = false;
                    lblError.Text += "<li>Please enter a valid start date";
                }
                if (!Functions.IsDate(dpEndDate.Text))
                {
                    datesAreValid = false;
                    lblError.Text += "<li>Please enter a valid end date";
                }

                if (datesAreValid)
                {
                    if (DateTime.Parse(dpEndDate.Text, CultureInfo.InvariantCulture) < DateTime.Parse(dpStartDate.Text, CultureInfo.InvariantCulture))
                    {
                        lblError.Text += "<li>The end date cannot be less than the start date";
                    }
                    else
                    {
                        ShowHideTop10DesiredChart(true);
                        lrCharts.DealerKey = ddlDealerShipDeatails.SelectedValue;
                        lrCharts.GenerateCharts(rbtnLeadtype.SelectedValue);
                    }
                }

                drgDetailsReport.StartDate = DateTime.Parse(dpStartDate.Text.Trim());
                drgDetailsReport.EndDate = DateTime.Parse(dpEndDate.Text.Trim());
                drgDetailsReport.FistName = string.Empty;
                drgDetailsReport.LastName = string.Empty;
                drgDetailsReport.EMail = string.Empty;
                drgDetailsReport.Phone = string.Empty;
                drgDetailsReport.LeadResultType = Convert.ToInt32(rbtnLeadtype.SelectedValue);

                drgDetailsReport.LeadsPerPage = rblLeadsCountPerPage.SelectedValue;

                drgDetailsReport.GenerateReport(false);

                btnExportExcel1.Visible = false;
                btnExportExcel2.Visible = true;

                lblHeader.Text = string.Format("Lead details report for '{0}' - {1} to {2}", ddlDealerShipDeatails.SelectedItem.Text
                                              , dpStartDate.Text.Trim()
                                               , dpEndDate.Text.Trim());

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (((ImageButton)sender).ID == "btnExportExcel1")
                {
                    ExportGridToExcel(string.Format("{0}_{1}_LeadTotalsReport", ddlDateRange.SelectedItem.Text.Trim().Replace(" ", "")
                                                             , ddlDealerShip.SelectedItem.Text.Trim().Replace(" ", "")));
                }
                else
                {
                    ExportGridToExcel(string.Format("{0}_{1}_{2}_LeadDetailreport", dpStartDate.Text.Trim().Replace("/", "")
                                                                                 , dpEndDate.Text.Trim().Replace("/", "")
                                                                                 , ddlDealerShipDeatails.SelectedItem.Text.Trim().Replace(" ", "").Replace(".", ":")));
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            // Confirms that an HtmlForm control is rendered for the
            //specified ASP.NET server control at run time.
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region ' custom methods '
        protected Dealers GetDealerList()
        {
            var dealers = new Dealers();
            try
            {
                if (Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.GroupDealerAdmin)
                    dealers.GetDealersByUserID(Security.GetLoggedUserID());
                else dealers.GetDealersByDealerType((int)Security.GetGroupLoggedUser());

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return dealers;
        }

        protected enum DealerStatus
        {
            Active = 0,
            Billable = 1
        }
        protected void AddTotals(ref DataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1][1] = "Total Leads";

                    for (int i = 2; i < ds.Tables[0].Columns.Count; i++)
                    {
                        ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1][i] = 0;
                        for (int j = 0; j < ds.Tables[0].Rows.Count - 1; j++)
                        {
                            var val = (int)ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1][i];
                            ds.Tables[0].Rows[ds.Tables[0].Rows.Count - 1][i] = val + Convert.ToInt32(ds.Tables[0].Rows[j][i]);
                        }
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void AddCount(ref DataSet ds)
        {
            try
            {
                if (ds != null)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ds.Tables[0].Rows[i][0] = (i + 1).ToString();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void ExportGridToExcel(string fileName)
        {
            try
            {
                Response.Buffer = true;
                Response.Clear();

                Response.Charset = string.Empty;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.ms-excel";

                var stringWrite = new StringWriter();
                var htmlWrite = new HtmlTextWriter(stringWrite);
                var grid = (GridView)Session["ReportGrid"];

                //  Create a form to contain the grid
                var table = new Table { GridLines = grid.Rows.Count > 0 ? grid.GridLines : GridLines.None };
                //  Add the header row to the table
                if (grid.HeaderRow != null)
                {
                    PrepareControlForExport(grid.HeaderRow);
                    table.Rows.Add(grid.HeaderRow);
                }
                //  Add each of the data rows to the table
                foreach (GridViewRow row in grid.Rows)
                {
                    PrepareControlForExport(row);
                    table.Rows.Add(row);
                }
                //  Add the footer row to the table
                if (grid.FooterRow != null)
                {
                    PrepareControlForExport(grid.FooterRow);
                    table.Rows.Add(grid.FooterRow);
                }
                table.RenderControl(htmlWrite);

                Response.Write(stringWrite.ToString());
                byte[] buffer = Response.ContentEncoding.GetBytes(stringWrite.ToString());
                Response.AppendHeader("content-disposition", string.Format("attachment; filename={0}.xls; Content-Length={1}", fileName, buffer.Length));
                Response.ClearContent();
                Response.BinaryWrite(buffer);
                Response.End();
            }
            catch (ThreadAbortException)
            {
                //Thread.ResetAbort();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private static void PrepareControlForExport(Control control)
        {
            try
            {
                int i = 0;
                while ((i < control.Controls.Count))
                {
                    Control current = control.Controls[i];
                    if ((current is LinkButton))
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl(((LinkButton)current).Text));
                    }
                    else if ((current is ImageButton))
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl(((ImageButton)current).AlternateText));
                    }
                    else if ((current is HyperLink))
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl(((HyperLink)current).Text));
                    }
                    else if ((current is DropDownList))
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl(((DropDownList)current).SelectedItem.Text));
                    }
                    else if ((current is CheckBox))
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl(((CheckBox)current).Checked.ToString()));
                    }
                    if (current.HasControls())
                    {
                        PrepareControlForExport(current);
                    }
                    i = (i + 1);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void ShowHideDealershipControls(bool isMultiple)
        {
            try
            {
                ddlDealerShip.Visible = !isMultiple;
                lstDealerShip.Visible = isMultiple;

                rbScale.SelectedIndex = 0;

                //divLeadTotalsReport.Attributes.Add("style", string.Format("border: 1px solid orange;height:{0}px;", isMultiple ? 270 : 190));
                //divDetailsLeadreport.Attributes.Add("style", string.Format("border: 1px solid orange;height:{0}px;", isMultiple ? 290 : 224));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void ShowHideTop10DesiredChart(bool isDisplay)
        {
            try
            {
                tdCharts.Attributes.Add("style", string.Format("display:{0}", isDisplay ? "block" : "none"));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        protected void rbScale_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbScale.SelectedIndex == 0)
            {
                ddlDateRange.Visible = true;
                ddlYearRange.Visible = false;
            }
            else
            {
                ddlDateRange.Visible = false;
                ddlYearRange.Visible = true;
            }
        }
    }
}