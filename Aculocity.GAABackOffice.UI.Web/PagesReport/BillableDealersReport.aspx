﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true"
    CodeBehind="BillableDealersReport.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesReport.BillableDealers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    
    <script src="../JScripts/highcharts/js/highcharts.js" type="text/javascript"></script>
    
    <style type="text/css">
        .reportHeader {
            background: #CCCCCC;
            color: black;
        }
        .reportHeader a {
            color: black;
        }
        .contentCell {
            background: white;
            color: black;

        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">

    <script language="JavaScript">
    changeclass("ctl00_liBillableDealers");
    </script>

    <script type="text/javascript" src="../JScripts/FusionCharts.js"></script>

    <asp:UpdatePanel ID="upContent" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="left">
                        <table width="800px" style="border-color: #CCCCCC" cellspacing="0" cellpadding="2"
                            align="center" border="1">
                            <tr>
                                <td width="25%" class="reportHeader">
                                    Report Year
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" Width="100%" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td width="25%" class="reportHeader">
                                    Report Month
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlMonth" runat="server" AutoPostBack="True" Width="100%" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <table width="800px" cellspacing="0" cellpadding="2" align="center" border="0">
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnShowReport" runat="server" Text="Refresh Report" OnClick="btnShowReport_Click">
                                    </asp:Button>
                                </td>
                                <td align="left" valign="middle" style="width: 25px">
                                    <asp:ImageButton ID="btnXLSDownload" runat="server" OnClick="btnDownload_Click" Height="24"
                                        Width="24" ImageUrl="~/Images/Excelicon24x24.png" ToolTip="Export to Excel" />
                                </td>
                            </tr>
                        </table>
                        <table width="800px" style="border-color: #CCCCCC" cellspacing="0" cellpadding="2"
                            align="center" border="1">
                            <tr>
                                <td colspan="2" width="25%" class="reportHeader">
                                    Billing Date Range
                                </td>
                                <td colspan="2" width="75%" align="left" class="contentCell">
                                    <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                                    -
                                    <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="25%" class="reportHeader">
                                    Number of Billable Dealers
                                </td>
                                <td colspan="2" width="25%" class="contentCell">
                                    <asp:Label ID="lblBillableDealers" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" valign="top" width="25%" class="reportHeader">
                                    Aculocity Dealer Fee
                                </td>
                                <td colspan="2" width="25%" class="contentCell">
                                    <asp:Label ID="lblDealerFee" runat="server" Text="0.00"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="25%" class="reportHeader">
                                    Projected Invoice Amount
                                </td>
                                <td colspan="2" width="25%" class="contentCell">
                                    <asp:Label ID="lblAmtDue" runat="server" Text="0.00"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="25%" class="reportHeader">
                                    <a id="lnkNewDealers" runat="server" onserverclick="lnkNewDealers_Click" class="reportHeader"> 
                                        New Dealers added (Past 30 days)
                                    </a>
                                </td>
                                <td colspan="2" width="25%" class="contentCell">
                                    <asp:Label ID="lblDealersAdded" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="25%" class="reportHeader">
                                    <a id="lnkDeactivatedDealers" runat="server" onserverclick="lnkDeactivatedDealers_Click" class="reportHeader">
                                        Dealers Deactivated (Past 30 days)
                                    </a>
                                </td>
                                <td colspan="2" width="25%" class="contentCell">
                                    <asp:Label ID="lblDealersDeactivated" runat="server" Text="0"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table width="800px" cellspacing="0" cellpadding="2" align="center" border="0">
                            <tr>
                                <td colspan="4">
                                    <asp:UpdatePanel ID="upBillableDealersChart" runat="server">
                                        <ContentTemplate>
                                            <asp:Literal ID="litBillableDealersChart" runat="server"></asp:Literal>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnShowReport" />
                                            <asp:PostBackTrigger ControlID="ddlYear" />
                                            <asp:PostBackTrigger ControlID="ddlMonth" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <div id="divXMLData" runat="server" style="display: none">
                                    </div>
                                    <div id="divFCBillableDealers" runat="server" style="display: none">
                                        <div id="BillabbleDealersChartDiv" align="center">
                                        </div>
                                        <asp:TextBox ID="txtXMLData" runat="server" Style="display: none" />

                                        <script type="text/javascript">
                                    function CreateChart(xmlData) {
                                        var chart_BillabbleDealersChart = new FusionCharts("../FusionCharts/FCF_MSLine.swf", "BillabbleDealersChart", "800", "250", "0", "0");
                                        chart_BillabbleDealersChart.setDataXML(xmlData);
                                        chart_BillabbleDealersChart.setTransparent(true);
                                        chart_BillabbleDealersChart.render("BillabbleDealersChartDiv");
                                    }
                                        </script>

                                    </div>
                                </td>
                            </tr>
                            <%--<tr>
                        <td align="left" valign="middle" style="width:100px">
                            <asp:Button id="btnShowHide" runat="server" Text="Hide Details" OnClick="btnShowHide_Click" Visible="false"></asp:Button>&nbsp; 
                        </td>
                        <td align="left" valign="middle" colspan="2">
                            <asp:ImageButton ID="btnXLSDownload"  runat="server"  OnClick="btnDownload_Click" Height="24" Width="24"
                                             ImageUrl="~/Images/Excelicon24x24.png"  ToolTip="Export to Excel"/>
                        </td>
                    </tr>--%>
                            <tr id="trBillableDealers" runat="server">
                                <td colspan="4" width="100%">
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                    <asp:GridView ID="gvBillableDealers" runat="server" Width="100%" PageSize="500" BackColor="White"
                                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True"
                                        AllowSorting="False" AutoGenerateColumns="False" EmptyDataText="">
                                        <FooterStyle BackColor="White" ForeColor="#000066"></FooterStyle>
                                        <Columns>
                                            <asp:BoundField ReadOnly="True" DataField="Number" SortExpression="Number" HeaderText="Number">
                                                <HeaderStyle HorizontalAlign="Center" Wrap="false" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="DealerName" SortExpression="DealerName"
                                                HeaderText="Dealer Name">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="false"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" HtmlEncode="False" DataFormatString="{0:M/dd/yyyy}"
                                                DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date Created">
                                                <ItemStyle Width="140px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" Wrap="false"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" HtmlEncode="False" DataFormatString="{0:M/dd/yyyy}"
                                                DataField="SystemBillableStartDate" SortExpression="SystemBillableStartDate"
                                                HeaderText="System Billable Start Date">
                                                <ItemStyle Width="140px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" Wrap="false"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" HtmlEncode="False" DataFormatString="{0:M/dd/yyyy}"
                                                DataField="AdminBillableStartDate" SortExpression="AdminBillableStartDate" HeaderText="Admin Billable Start Date">
                                                <ItemStyle Width="140px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" Wrap="false"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="" HeaderText="">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="" HeaderText="">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="" HeaderText="">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="" HeaderText="">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="" HeaderText="">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="" HeaderText="">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="" HeaderText="">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" HeaderText="Comments" DataField="Comments" Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle ForeColor="Black"></RowStyle>
                                        <SelectedRowStyle BackColor="#669999" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left"></PagerStyle>
                                        <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="WhiteSmoke"></AlternatingRowStyle>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblEmptyGrid" runat="server" Text="No ADP Dealers found"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <asp:GridView ID="gvAcuDealers" runat="server" Width="100%" PageSize="500" BackColor="White"
                                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True"
                                        AllowSorting="False" AutoGenerateColumns="False" EmptyDataText="No Dealer(s) Found...">
                                        <FooterStyle BackColor="White" ForeColor="#000066"></FooterStyle>
                                        <Columns>
                                            <asp:BoundField ReadOnly="True" DataField="DealerName" SortExpression="DealerName"
                                                HeaderText="Dealer Name">
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" HtmlEncode="False" DataFormatString="{0:M/dd/yyyy}"
                                                DataField="BillableStartDate" SortExpression="BillableStartDate" HeaderText="Billable Start Date">
                                                <ItemStyle Width="140px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:CheckBoxField DataField="Active" SortExpression="Active" HeaderText="Active">
                                                <ItemStyle Width="80px" HorizontalAlign="Center"></ItemStyle>
                                            </asp:CheckBoxField>
                                            <asp:CheckBoxField DataField="Billable" SortExpression="Billable" HeaderText="Billable">
                                                <ItemStyle Width="80px" HorizontalAlign="Center"></ItemStyle>
                                            </asp:CheckBoxField>
                                            <asp:BoundField ReadOnly="True" DataFormatString="{0:c}" DataField="Rate" SortExpression="Rate"
                                                HeaderText="Rate">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="TotalLeads" SortExpression="TotalLeads"
                                                HeaderText="Total Leads">
                                                <ItemStyle Width="60px" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle ForeColor="Black"></RowStyle>
                                        <SelectedRowStyle BackColor="#669999" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left"></PagerStyle>
                                        <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="WhiteSmoke"></AlternatingRowStyle>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="trNewAddedDealers" runat="server">
                                <td colspan="4" width="100%">
                                    <asp:GridView ID="gvNewAddedDealers" runat="server" Width="100%" PageSize="500" BackColor="White"
                                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True"
                                        AllowSorting="False" AutoGenerateColumns="False" EmptyDataText="">
                                        <FooterStyle BackColor="White" ForeColor="#000066"></FooterStyle>
                                        <Columns>
                                            <asp:BoundField ReadOnly="True" DataField="DealerName" HeaderText="Dealer Name">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="false"/>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="DealerKey" HeaderText="Dealer Name">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="false"/>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="DateCreated" HtmlEncode="False" DataFormatString="{0:M/dd/yyyy}" HeaderText="Date Created">
                                                <ItemStyle Width="140px" HorizontalAlign="Center"/>
                                                <HeaderStyle HorizontalAlign="Center" Wrap="false"/>
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle ForeColor="Black"></RowStyle>
                                        <SelectedRowStyle BackColor="#669999" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left"></PagerStyle>
                                        <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="WhiteSmoke"></AlternatingRowStyle>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblEmptyGrid" runat="server" Text="No New Dealers found"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr id="trDeactivatedDealers" runat="server">
                                <td colspan="4" width="100%">
                                    <asp:GridView ID="gvDeactivatedDealers" runat="server" Width="100%" PageSize="500" BackColor="White"
                                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" AllowPaging="True"
                                        AllowSorting="False" AutoGenerateColumns="False" EmptyDataText="">
                                        <FooterStyle BackColor="White" ForeColor="#000066"></FooterStyle>
                                        <Columns>
                                            <asp:BoundField ReadOnly="True" DataField="DealerName" HeaderText="Dealer Name">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="false"/>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="DealerKey" HeaderText="Dealer Name">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="false"/>
                                            </asp:BoundField>
                                            <asp:BoundField ReadOnly="True" DataField="DeactivationDate" HtmlEncode="False" DataFormatString="{0:M/dd/yyyy}" HeaderText="Date Deactivated">
                                                <ItemStyle Width="140px" HorizontalAlign="Center"/>
                                                <HeaderStyle HorizontalAlign="Center" Wrap="false"/>
                                            </asp:BoundField>
                                        </Columns>
                                        <RowStyle ForeColor="Black"></RowStyle>
                                        <SelectedRowStyle BackColor="#669999" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left"></PagerStyle>
                                        <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>
                                        <AlternatingRowStyle BackColor="WhiteSmoke"></AlternatingRowStyle>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="lblEmptyGrid" runat="server" Text="No Deactivated Dealers found"></asp:Label>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnXLSDownload" />
        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(afterPageLoad);
        function afterPageLoad() {
            try {
                ///var xmlData = document.getElementById('<%=divXMLData.ClientID %>').innerHTML.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
                //CreateChart(xmlData);

                CreateBillableChart();
            } catch (err) { }
        }
    </script>

</asp:Content>
