﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.Entity;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web
{
    public partial class Login : System.Web.UI.Page
    {

        protected void Page_PreInit(object sener, EventArgs e)
        {
            Response.AddHeader("P3P", "CP=\"CAO PSA OUR\"");
        }

        protected override void OnInit(EventArgs e) {
            try
            {
                base.OnInit(e);
                var link = new HtmlLink { Href = "~/CSS/Master.css" };
                link.Attributes.Add("rel", "stylesheet");
                link.Attributes.Add("type", "text/css");
                head.Controls.Add(link);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString.ToString().ToUpper().Contains("SSOPORTAL.ASPX")) CheckHomeNetUser();
                txtUsr.Focus();

                if (!IsPostBack)
                {
                    //Check if user was logged in with saving data
                    if (Request.Browser.Cookies)
                    {
                        if (Request.Cookies["GAABOUSERNAME"] != null)
                        {
                            var user = new UserBLL();
                            // ReSharper disable PossibleNullReferenceException
                            user.GetUserByUserName(Request.Cookies["GAABOUSERNAME"].Value);
                            // ReSharper restore PossibleNullReferenceException
                            txtUsr.Text = user.UserName;
                            txtPwd.Attributes.Add("value", user.Password);
                            chbSaveCredentials.Checked = user.UserName != string.Empty;
                        }
                    }
                }
                
                //check current user's IP
                //if(!IsPostBack)
                //{
                //    HttpCookie cookieUserName = Request.Cookies["UserName"];
                //    UserBLL loggedUser = Security.GetSavedLoggedUser(GetUserIPAddress());
                //    if(loggedUser != null)
                //    {
                //        txtUsr.Text = loggedUser.UserName;
                //        txtPwd.Attributes.Add("value", loggedUser.Password);
                //        chbSaveCredentials.Checked = true;
                //    }
                //}
            }
            catch(Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        protected void LoginControl_Authenticate(object sender, EventArgs e)
        {
            try
            {
                //reset error controls:
                pnlLoginError.Visible = false;
                lblLoginError.Text = string.Empty;

                //perform login:
                UserBLL authenticatedUser = Security.PerformLogin(txtUsr.Text.Trim(), txtPwd.Text.Trim());
                if(authenticatedUser == null)
                {
                    ShowError("You have entered an incorrect Username or Password. Please try again.");
                    return;
                }
                if(!authenticatedUser.Active)
                {
                    ShowError("This user account is currently inactive. Please contact your system administrator.");
                    return;
                }
                //set up group data
                string customData = string.Format("Group:{0};SubGroup:{1};UserID:{2}",
                                                  authenticatedUser.AuthGroup,
                                                  authenticatedUser.SubGroupDealerAdmin,
                                                  authenticatedUser.UserID);
                Response.Cookies.Add(Security.CreateAuthCookie(authenticatedUser.UserName, customData));
                
                Enumerations.AuthUserGroup currentGroup = authenticatedUser.AuthGroup;

                //check chbSaveUserData to save data
                //authenticatedUser.UserIPAddress = GetUserIPAddress();
                //if (chbSaveCredentials.Checked) 
                //{
                //    authenticatedUser.InsertLoggedUserIPData(); 
                //}
                //else
                //{
                //    authenticatedUser.DeleteLoggedUserIPData(); 
                //}


                //Save user's name as cookie if necessary
                // ReSharper disable PossibleNullReferenceException
                if (chbSaveCredentials.Checked)
                {
                    if (Request.Browser.Cookies)
                    {
                        if (Request.Cookies["GAABOUSERNAME"] != null)
                        {
                            Response.Cookies["GAABOUSERNAME"].Value = txtUsr.Text;
                            Response.Cookies["GAABOUSERNAME"].Expires = DateTime.Now.AddDays(30);
                        }
                        else
                        {
                            var cookie = new HttpCookie("GAABOUSERNAME", txtUsr.Text){Expires = DateTime.Now.AddDays(30)};
                            Response.Cookies.Add(cookie);
                        }
                    }
                }
                else
                {
                    if (Request.Cookies["GAABOUSERNAME"] != null)
                    {
                        Response.Cookies["GAABOUSERNAME"].Expires = DateTime.Now.AddDays(-30);
                    }
                }
                // ReSharper restore PossibleNullReferenceException

                if (currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                    currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                    currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                    currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin ||
                    currentGroup == Enumerations.AuthUserGroup.ResellerAdmin)
                {
                    if (currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin || currentGroup == Enumerations.AuthUserGroup.HomeNetSuperUser)
                    {
                        Session["HomeNet"] = "true";
                        Session["BSB"] = "false";
                        Session["Galves"] = "false";
                    }
                    else
                    { 
                        if (currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin || currentGroup == Enumerations.AuthUserGroup.GalvesFullAdmin)
                        {
                            Session["HomeNet"] = "false";
                            Session["Galves"] = "true";
                            Session["BSB"] = "false";

                        }
                        else if (currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin || currentGroup == Enumerations.AuthUserGroup.BSBConsultingFullAdmin)
                        {
                            Session["HomeNet"] = "false";
                            Session["BSB"] = "true";
                            Session["Galves"] = "false";
                        }
                        else
                        {
                            Session["HomeNet"] = "false";
                            Session["BSB"] = "false";
                            Session["Galves"] = "false"; 
                        }
                    }
                    //Commented out by Freddy, this will force it to go to dashboard always
                    //Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                    Response.Redirect("~/PagesDealer/Dashboard.aspx", false);
                }
                else
                {
                    if (currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin || currentGroup == Enumerations.AuthUserGroup.HomeNetSuperUser)
                    {
                        Session["HomeNet"] = "true";
                        Session["BSB"] = "false";
                        Session["Galves"] = "false";
                    }
                    else
                    {
                        if (currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin || currentGroup == Enumerations.AuthUserGroup.GalvesFullAdmin)
                        {
                            Session["HomeNet"] = "false";
                            Session["Galves"] = "true";
                            Session["BSB"] = "false";

                        }
                        else if (currentGroup == Enumerations.AuthUserGroup.ResellerAdmin || currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin || currentGroup == Enumerations.AuthUserGroup.BSBConsultingFullAdmin)
                        {
                            Session["HomeNet"] = "false";
                            Session["BSB"] = "true";
                            Session["Galves"] = "false";
                        }
                        else
                        {
                            Session["HomeNet"] = "false";
                            Session["BSB"] = "false";
                            Session["Galves"] = "false";
                        }
                    }
                    //Commented out by Freddy, this will force it to go to dashboard always
                    //Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
                    Response.Redirect("~/PagesDealer/Dashboard.aspx", false);                    
                }
            }
            catch(Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        private void ShowError(string errorText)
        {
            try
            {
                pnlLoginError.Visible = true;
                lblLoginError.Text = errorText;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private static string GetUserIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;

            string sIpAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(sIpAddress))
            {
                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                string[] ipArray = sIpAddress.Split(new Char[] { ',' });
                return ipArray[0];
            }
        }

        #region ' SSO Portal functionality for HomeNet dealer admins '
        private void CheckHomeNetUser()
        {
            try
            {
                var sb = new StringBuilder();

                sb.Append(Request.QueryString["Token"]);
                sb.Append(Request.QueryString["DealerKey"]);
                sb.Append("HomeNet");
                //Our Salt Value

                //Encode the string
                var md5Obj = new MD5CryptoServiceProvider();

                byte[] bytesToHash = Encoding.ASCII.GetBytes(sb.ToString());
                byte[] hashedBytes = md5Obj.ComputeHash(bytesToHash);

                var compareSB = new StringBuilder();
                foreach (byte b in hashedBytes)
                {
                    compareSB.Append(b.ToString("x2"));
                }

                var calculatedMD5 = compareSB.ToString();
                //Compare the encoded string to the key in the query string
                if ((Request.QueryString["key"] != null))
                {
                    if (calculatedMD5.Equals(Request.QueryString["Key"]))
                    {
                        LoginHomenetUser(Request.QueryString["DealerKey"]);
                        Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                    }
                    else
                    {
                        GoToLogin();
                    }
                }
                else
                {
                    GoToLogin();
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void LoginHomenetUser(string dealerKey)
        {
            try
            {
                var users = new Users();
                users.GetUsersByDealerKey(dealerKey);
                if (users.Count == 1){SetUser(users[0]);}
                else{GoToLogin();}
            }
            catch (Exception ex)
            {
                GoToLogin();
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }
        private void SetUser(UserEntity user)
        {
            try
            {
                UserBLL authenticatedUser = Security.PerformLogin(user.UserName, user.Password);
                //set up group data
                string customData = string.Format("Group:{0};SubGroup:{1};UserID:{2}",
                                                  authenticatedUser.AuthGroup,
                                                  authenticatedUser.SubGroupDealerAdmin,
                                                  authenticatedUser.UserID);
                Response.Cookies.Add(Security.CreateAuthCookie(authenticatedUser.UserName, customData));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void GoToLogin()
        {
            try
            {
                Session.Abandon(); Security.PerformLogout(); Response.Redirect("~/Login.aspx", false);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion
    }
}