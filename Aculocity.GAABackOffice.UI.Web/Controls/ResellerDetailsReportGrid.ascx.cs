﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;

using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web
{

    public partial class ResellerDetailsReportGrid : System.Web.UI.UserControl
    {

        public string DealerKey {get;set;}
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LeadsPerPage { get; set; }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            gvResellerDetailReportGrid.PageIndexChanging += new GridViewPageEventHandler(gvResellerDetailReportGrid_PageIndexChanging);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (Page.IsPostBack)
                LoadSessionFromViewState();
        }

        protected void gvResellerDetailReportGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //gvDetailReportGrid.PageIndex = e.NewPageIndex;
            GenerateReport(_resellerLeads);
        }

        private bool _resellerLeads = false;

        public void GenerateReport(bool resellerLeads)
        {
            _resellerLeads = resellerLeads;

            try
            {

                Session.Remove("ResellerReportGrid");

                gvResellerDetailReportGrid.DataSource = null;
                gvResellerDetailReportGrid.DataBind();
                
                //TODO: send the actual user id
                DataSet ds = ReportBLL.GetResellerDetailsLeadData(DealerKey, StartDate, EndDate, Security.GetLoggedUserID());

                if (ds != null && ds.Tables.Count > 0)
                {
                    var deatailsReport = ds.Tables[0];
                    if (deatailsReport.Columns["RowNumber"] == null) deatailsReport.Columns.Add("RowNumber");
                    int x = 1;
                    foreach (DataRow row in deatailsReport.Rows)
                    {
                        row["RowNumber"] = x++;
                    }

                    //set page size
                    if (deatailsReport.Rows.Count > gvResellerDetailReportGrid.PageIndex * gvResellerDetailReportGrid.PageSize)
                    {
                        gvResellerDetailReportGrid.PageSize = Convert.ToInt32(string.IsNullOrEmpty(LeadsPerPage) ? "10" : LeadsPerPage);
                    }

                    gvResellerDetailReportGrid.DataSource = deatailsReport;
                    gvResellerDetailReportGrid.DataBind();
                }

                //// set colors for columns
                //if (gvDetailReportGrid.Rows.Count > 0)
                //{
                //    var bgColor = System.Drawing.Color.FromName("#003366");
                //    for (int i = 0; i < 18; i++)
                //    {
                //        switch (i)
                //        {
                //            case 6:
                //                bgColor = System.Drawing.Color.FromName("#0066CC");
                //                break;
                //            case 10:
                //                bgColor = System.Drawing.Color.FromName("#3399FF");
                //                break;
                //            case 16:
                //                bgColor = System.Drawing.Color.FromName("#99CCFF");
                //                break;
                //        }
                //        gvDetailReportGrid.HeaderRow.Cells[i].BackColor = bgColor;
                //    }
                //}

                Session["ResellerReportGrid"] = gvResellerDetailReportGrid;

                SaveSessionToViewState();

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }


        }

        void SaveSessionToViewState()
        {

            ViewState["ResellerReportDealerKey"] = DealerKey;
            ViewState["ResellerReportStartDate"] = StartDate;
            ViewState["ResellerReportEndDate"] = EndDate;
            ViewState["ResellerReportLeadsPerPage"] = LeadsPerPage;

        }

        void LoadSessionFromViewState()
        {

            object value = ViewState["ResellerReportDealerKey"];
            if (value != null) DealerKey = value.ToString();
            if ((value = ViewState["ResellerReportStartDate"]) != null) StartDate = (DateTime)value;
            if ((value = ViewState["ResellerReportEndDate"]) != null) EndDate = (DateTime)value;
            if ((value = ViewState["ResellerReportLeadsPerPage"]) != null) LeadsPerPage = value.ToString();
        }

        public void ExportToExcell()
        {


            //gvDetailReportGrid.AllowPaging = false;

            //GenerateReport(false);

            //ExportGridToExcel(string.Format("{0:MM/dd/yyyy}_{1:MM/dd/yyyy}.xls", StartDate, EndDate));

            //gvDetailReportGrid.AllowPaging = true;

            //SaveSessionToViewState();
        }


        private void ExportGridToExcel(string fileName)
        {
            //try {


            //    Response.ClearHeaders();
            //    Response.ClearContent();


            //    Response.AddHeader("content-disposition", string.Format("attachment; filename={0:MM/dd/yyyy}_{1:MM/dd/yyyy}.xls", StartDate, EndDate));
            //    Response.ContentType = "application/ms-excel";

            //    gvDetailReportGrid.RenderControl(new HtmlTextWriter(new StreamWriter(Response.OutputStream)));


            //    Response.End();


            //} catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }


    }
}