﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Top10DesiredChart.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Top10DesiredChart" %>
 <table>
    <tr>
        <td ID="tdTop10DesiredChart" runat="server" align="left" style="width: 700px;">
            <div id="divTop10DesiredXMLData" runat="server" style="display:none"></div>
            <div id="divTop10DesiredChart" runat="server">
                <div id="Top10DesiredChartDiv" style="text-align:center;"></div>
                <asp:TextBox ID="txtTop10DesiredXMLData" runat="server" style="display:none" />
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(afterPageLoad);
    function afterPageLoad() {
        try {CreateTop10DesiredChart();} catch (err) { }
    }
    function CreateTop10DesiredChart() {
        var xmlData = document.getElementById('<%=divTop10DesiredXMLData.ClientID %>').innerHTML.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
        var chart_Top10DesiredChart = new FusionCharts(desiredReplacementModelPath, "Top10DesiredChart", "700", "250", "0", "0");
        chart_Top10DesiredChart.setDataXML(xmlData);
        chart_Top10DesiredChart.setTransparent(true);
        chart_Top10DesiredChart.render("Top10DesiredChartDiv");
    }
</script>