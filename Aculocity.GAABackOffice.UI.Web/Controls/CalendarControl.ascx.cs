﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Aculocity.Controls.Web;

namespace Aculocity.GAABackOffice.UI.Web {

    public partial class CalendarControl : UserControl, IValue  {

        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public TextBox MainTextBoxControl {
            get {
                return txtDate;
            }
        }


        public object Value {
        
            get {
                if(string.IsNullOrEmpty(txtDate.Text))
                    return DateTime.Now;

                return DateTime.Parse(txtDate.Text);
            }
            set {

                if (DateTime.MinValue.Equals(value)) {
                    txtDate.Text = string.Empty;
                    return;
                }

                txtDate.Text = value.ToString();
                ceDate.SelectedDate = (DateTime)value;
              
            }
        
        }
        
    }
}