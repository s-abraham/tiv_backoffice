﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web {

    public partial class Slider : UserControl{

        Enumerations.AuthUserGroup _currentGroup;

        public int Value {
            get {
                return int.Parse(txtMainSlider.Text);
            }
            set {
                txtMainSlider.Text = value.ToString();
            }
        }

        public int Minimum {
            get {
                
                return Convert.ToInt32(seMain.Minimum);
            }
            set {
                seMain.Minimum = Convert.ToDouble(value);
            }
        }

        public int Maximum {
            get {
                return Convert.ToInt32(seMain.Maximum);
            }
            set {
                seMain.Maximum = Convert.ToDouble(value);
            }
        }

        public int Length {
            get {
                return seMain.Length;
            }
            set {
                seMain.Length = value;
            }
        }

        public string TooltipText {
            get {
                return seMain.TooltipText;
            }
            set {
                seMain.TooltipText = value;
            }
        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);
            seMain.BehaviorID = string.Format("{0}_{1}", ID, seMain.BehaviorID);

            _currentGroup = Security.GetCurrentUserGroup();

        }

        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);

            seMain.DataBind();

            if (_currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin || 
                _currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin || 
                _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin )
            {
                txtMainSlider.Enabled = false;
                txtMainSlider_Bound.Enabled = false;
            }

        }

    }
}