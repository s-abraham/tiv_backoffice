﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatePicker.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Controls.DatePicker" %>
<style type="text/css">
    .myCalendar 
    {
        border:1px solid #646464;
        background-color:#ffffff;
        color:#000000;
        font-family:tahoma,verdana,helvetica;
        font-size:11px;
        width: 200px;
        position:absolute;
        z-index:1000;
    }
    .myCalendar a {
        text-decoration: none;
    }
    .myCalendar .myCalendarTitle td:hover{
        font-weight: bold;
        color:#0066cc;
        cursor:pointer;
    }
    .myCalendar .myCalendarTitle {
        font-weight: bold;
        font-family:tahoma,verdana,helvetica;
        font-size:11px;
        cursor:pointer;
    }
    .myCalendar td.myCalendarDay {
        border-left: 0;
        border-top: 0;
        height:17px;width:18px;text-align:right;padding:0 2px;cursor:pointer;
    }
    .myCalendar .myCalendarNextPrev {
        text-align: center;
    }
    .myCalendar td.myCalendarSelector {
        
        background-color: #ffffff;
        
    }
    .myCalendar .myCalendarDay a,
    .myCalendar .myCalendarSelector a,
    .myCalendar .myCalendarNextPrev a {
        display: block;
        line-height: 18px;
    }
    .myCalendar .myCalendarDay a:hover,
    .myCalendar .myCalendarSelector a:hover {
        background-color:#daf2fc;
        border-color:#daf2fc;
        color:#0066cc;
    }
</style>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
            <asp:HiddenField ID="hidDateValue" runat="server" />
        </td>
        <td style="width:3px;"></td>
        <td>
            <a id="lnkDate" runat="server" onserverclick="lnkDade_Click">
                <img id="imgDate" runat="server" alt="Pick a date" src="../Images/Calendar/cal.gif" border="0"/>
            </a>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="left">
            <asp:Calendar ID="calCalendar" runat="server" Visible="false"
                            DayNameFormat="FirstLetter"
                            Font-Names="Arial"
                            Font-Size="11px"
                            NextMonthText="&raquo;"
                            PrevMonthText="&laquo;"
                            SelectMonthText="&raquo;"
                            SelectWeekText="&rsaquo;"
                            CssClass="myCalendar"
                            CellPadding="1"
                            OnDayRender="DayRender">
                    <OtherMonthDayStyle ForeColor="Gray" />
                    <DayStyle CssClass="myCalendarDay" />
                    <SelectedDayStyle BackColor="#edf9ff" ForeColor="#646464" BorderColor="#0066cc" BorderWidth="1px"/>
                    <SelectorStyle CssClass="myCalendarSelector" />
                    <NextPrevStyle CssClass="myCalendarNextPrev" />
                    <TitleStyle CssClass="myCalendarTitle" BackColor="White"/>
                    <TodayDayStyle BackColor="#edf9ff" ForeColor="#646464" BorderColor="Gray" BorderWidth="1px"/>
                    <DayHeaderStyle BackColor="AliceBlue" />
            </asp:Calendar>
            <asp:RequiredFieldValidator ID="rev" runat="server" ErrorMessage="" 
                ControlToValidate="txtDate" Display="None" ValidationGroup="None">
            </asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="2"><asp:Button ID="btnChangeDate" runat="server" Style="display:none" OnClick="ChangeDate" Text="Change Date"/></td>
    </tr>
</table>