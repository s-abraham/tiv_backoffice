﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web {

    public partial class Top10DesiredChart : UserControl
    {

        public string DealerKey { get; set; }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            RegisterClientScript();
        }
        
        public void GenerateChart(string LeadresultType)
        {
            SetDataForTop10DesiredChart(LeadresultType);
        }

        string GetDataForTop10DesiredChart(string LeadresultType)
        {
            var sb = new StringBuilder();
            try {
                string top10Desired = ReportBLL.GetTop10DesiredAsXML(DealerKey == string.Empty ? "%" : DealerKey, LeadresultType);
                sb.AppendLine("<graph caption='Top 10 Desired Replacement models (Last 12 months)' ");
                sb.AppendLine("       decimalPrecision='0' formatNumberScale='0' yAxisMaxValue = '50' yAxisyAxisMinValue = '0'");
                sb.AppendLine("       bgColor = 'E3E6D9' baseFontColor = '5D633F' BgAlpha = '50' BorderColor = 'D7D8D3' ");
                sb.AppendLine("       canvasBorderThickness = '1' divLineColor = 'D7D8D3' divLineAlpha = '80' numVDivlines = '0'");
                sb.AppendLine("       showAlternateHGridColor = '1' AlternateHGridColor='D7D8D3' alternateHGridAlpha = '20'> ");
                sb.AppendLine("     <set name='' value='' color=''/>");
                sb.AppendLine(top10Desired.Replace(@"""", "'").Replace("######", "A66EDD"));
                sb.AppendLine("     <set name='' value='' color=''/>");
                sb.AppendLine("</graph>");
            } catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return sb.ToString();
        }

        private void SetDataForTop10DesiredChart(string LeadResultType) {
            try {
                divTop10DesiredXMLData.InnerText = GetDataForTop10DesiredChart(LeadResultType);
            } catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ClientScript
        
        void RegisterClientScript() {
            if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MainDef")) {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "MainDef", string.Format(@"
                var desiredReplacementModelPath = '{0}'; ", new object[]{
                    Response.ApplyAppPathModifier("~//FusionCharts//FCF_Column2D.swf")
                 }), true);
            }
        }

        #endregion
    }

}