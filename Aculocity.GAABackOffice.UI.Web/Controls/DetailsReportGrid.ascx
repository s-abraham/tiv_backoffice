﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DetailsReportGrid.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.DetailsReportGrid" %>

<asp:GridView ID="gvDetailReportGrid" runat="server" CellPadding="3" EnableViewState="True"
        ForeColor="Black" BorderStyle="Solid" BorderWidth="1px" BorderColor="White"
        CssClass="text" CaptionAlign="Top" UseAccessibleHeader="False" 
        PageSize="10" AllowPaging="true" AutoGenerateColumns="False" 
        OnPageIndexChanging="gvDetailReportGrid_PageIndexChanging"> 
   <RowStyle Wrap="true" HorizontalAlign="Center" BackColor="White" 
         Font-Italic="True" Font-Names="Calibri" Font-Size="11pt" />
   <AlternatingRowStyle HorizontalAlign="Center" BackColor="LightBlue"  
         Font-Italic="True" Font-Names="Calibri" Font-Size="11pt" />
   <HeaderStyle ForeColor="White" BorderStyle="None" Font-Names="Calibri" 
        Font-Size="11pt" CssClass="Freezing" Wrap="False" Font-Bold="True" BackColor = "black" />
   <EmptyDataTemplate>
        <asp:Label ID="lblEmptyGridText" Text="There is no data for the selected criteria."  ForeColor="Red" runat="server"></asp:Label>
   </EmptyDataTemplate>
   <Columns>
        <asp:BoundField DataField="RowNumber" HeaderText="Number">
            <ItemStyle HorizontalAlign="Center"/>
        </asp:BoundField>
        <asp:BoundField DataField="Date" HeaderText="Date" />
        <asp:BoundField DataField="FirstName" HeaderText="First Name" />
        <asp:BoundField DataField="LastName" HeaderText="Last Name" />
        <asp:BoundField DataField="Phone" HeaderText="Phone" />
        <asp:BoundField DataField="Email" HeaderText="Email" />
        <asp:BoundField DataField="Type" HeaderText="Type" />
        <asp:BoundField DataField="Year" HeaderText="Year" />    
        <asp:BoundField DataField="Make" HeaderText="Make" />    
        <asp:BoundField DataField="Model" HeaderText="Model" />    
        <%--<asp:BoundField DataField="Color" HeaderText="Color" /> --%>   
        <asp:BoundField DataField="TradeYear" HeaderText="Trade Year" />    
        <asp:BoundField DataField="TradeMake" HeaderText="Trade Make" />    
        <asp:BoundField DataField="TradeModel" HeaderText="Trade Model" /> 
        <asp:BoundField DataField="TradeBody" HeaderText="Trade Trim" /> 
        <%--<asp:BoundField DataField="TradeEngine" HeaderText="Engine" /> --%>
        <asp:BoundField DataField="Mileage" HeaderText="Mileage" /> 
        <asp:BoundField DataField="Zip" HeaderText="Zip" /> 
        <asp:BoundField DataField="BaseValueRange" HeaderText="Base Value Range" /> 
        <asp:BoundField DataField="TotalValueRange" HeaderText="Total Value Range" />
   </Columns>
</asp:GridView>
