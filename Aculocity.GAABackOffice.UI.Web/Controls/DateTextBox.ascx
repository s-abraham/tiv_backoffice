﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTextBox.ascx.cs" 
            Inherits="Aculocity.GAABackOffice.UI.Web.Controls.DateTextBox" %>
<table>
    <tr id="trDateTextBox" runat="server">
        <td>
            <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
            <ajaxToolkit:CalendarExtender ID="calExtTxtDate" runat="server" 
                                            TargetControlID="txtDate"
                                            PopupButtonID="imdDate"
                                            >
            </ajaxToolkit:CalendarExtender>       
        </td>
        <td>
            <asp:ImageButton ID="imdDate" ImageUrl="../Images/Calendar/cal.gif" 
                 AlternateText="Pick a date"
                 runat="server"></asp:ImageButton>
        </td>
    </tr>
</table>

