﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Slider.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Slider" %>


<table border="0" cellpadding="0" cellspacing="5">    
    <tr>
        <td align="right">
            <asp:TextBox ID="txtMainSlider" runat="server" Text="0"></asp:TextBox>
        </td>
        <td align="left">
            <asp:TextBox ID="txtMainSlider_Bound" runat="server" Width="40px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
        <ajaxToolkit:SliderExtender ID="seMain" runat="server" BehaviorID="txtMainSlider" 
        TargetControlID="txtMainSlider" BoundControlID="txtMainSlider_Bound" Orientation="Horizontal"
        EnableHandleAnimation="true" Minimum="-100" Maximum="100" Length="250" TooltipText=""> 
        </ajaxToolkit:SliderExtender>
        </td>
    </tr>
</table>

