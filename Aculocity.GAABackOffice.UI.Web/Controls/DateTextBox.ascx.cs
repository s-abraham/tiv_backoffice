﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aculocity.GAABackOffice.UI.Web.Controls
{
    public partial class DateTextBox : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region ' Properties '
        public string DateString
        {
            get { return txtDate.Text.Trim(); }
            set { txtDate.Text = value.Trim(); }
        }
        public DateTime Date
        {
            get { return DateTime.Parse(txtDate.Text, CultureInfo.InvariantCulture); }
            set { txtDate.Text = value.ToString("MM/dd/yyyy"); }
        }
        public int MaxLength
        {
            get { return txtDate.MaxLength; }
            set { txtDate.MaxLength = value; }
        }
        public Unit Width
        {
            get { return txtDate.Width; }
            set { txtDate.Width = value;}
        }
        public bool IsVisible
        {
            get { return trDateTextBox.Visible; }
            set { trDateTextBox.Visible = value;}
        }
        #endregion
    }
}