﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeadReportDetailMainGrid.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Controls.LeadReportDetailMainGrid" %>
    <asp:GridView ID="gvDealers" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" BorderColor="White" BorderStyle="Dotted" BorderWidth="1px"
        CellPadding="3" CssClass="text" EmptyDataText="No Appraisals found for the selected date range."
        PageSize="10" Width="95%"
        OnPageIndexChanging="gvDealers_PageIndexChanging"
        OnSorting="gvDealers_Sorting">
        <RowStyle HorizontalAlign="Left" Wrap="False" />
        <HeaderStyle BackColor="Black" ForeColor="White" HorizontalAlign="Left" />
        <AlternatingRowStyle BackColor="#F0F0F0" />
        <Columns>
            <asp:TemplateField HeaderText="Number">
                <ItemTemplate>
                    <asp:Label ID="lblOne" runat="server" Text='<%# Bind("RowNumber") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AprId" SortExpression="AprId" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblTwo" runat="server" Text='<%# Bind("AprId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date" SortExpression="DateCreated">
                <ItemTemplate>
                    <asp:Label ID="lblThree" runat="server" Text='<%# Bind("DateCreated", "{0:MM/dd/yyyy HH:mm}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="120px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="First Name" SortExpression="FirstName">
                <ItemTemplate>
                    <asp:Label ID="lblFour" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Last Name" SortExpression="LastName">
                <ItemTemplate>
                    <asp:Label ID="lblFive" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                <ItemTemplate>
                    <asp:Label ID="lblSix" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="User Email" SortExpression="Email">
                <ItemTemplate>
                    <asp:Label ID="lblSeven" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead Email 1" SortExpression="LeadEmail1">
                <ItemTemplate>
                    <asp:Label ID="lblEight" runat="server" Text='<%# Bind("LeadEmail1") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead 1">
                <ItemTemplate>
                    <asp:HyperLink ID="hypLead" runat="server" NavigateUrl='<%# string.Format("~/Pages/GetEmail.aspx?AprId={0}&DealerKey={1}&type=lead&num=1", Eval("AprId"), Eval("DealerKey"))%>'
                        Text="Open"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead Email 2" SortExpression="LeadEmail2">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("LeadEmail2") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead 2">
                <ItemTemplate>
                    <asp:HyperLink ID="hypLead2" runat="server" NavigateUrl='<%# string.Format("~/Pages/GetEmail.aspx?AprId={0}&type=lead&num=2", Eval("AprId"))%>'
                        Text="Open"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead Email 3" SortExpression="LeadEmail3">
                <ItemTemplate>
                    <asp:Label ID="lblEmail3" runat="server" Text='<%# Bind("LeadEmail3") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead 3">
                <ItemTemplate>
                    <asp:HyperLink ID="hypLead3" runat="server" NavigateUrl='<%# string.Format("~/Pages/GetEmail.aspx?AprId={0}&type=lead&num=3", Eval("AprId")) %>'
                        Text="Open"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead Email 4" SortExpression="LeadEmail4">
                <ItemTemplate>
                    <asp:Label ID="lblEmail4" runat="server" Text='<%# Bind("LeadEmail4") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lead 4">
                <ItemTemplate>
                    <asp:HyperLink ID="hypLead4" runat="server" NavigateUrl='<%# string.Format("~/Pages/GetEmail.aspx?AprId={0}&type=lead&num=4", Eval("AprId")) %>'
                        Text="Open"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Summary">
                <ItemTemplate>
                    <asp:HyperLink ID="hypSummary" runat="server" NavigateUrl='<%# string.Format("~/Pages/GetEmail.aspx?AprId={0}&type=summary", Eval("AprId")) %>'
                        Text="Open" Target="_blank"></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
                <HeaderStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>