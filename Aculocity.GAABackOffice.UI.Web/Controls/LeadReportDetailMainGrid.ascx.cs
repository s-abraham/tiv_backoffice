﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using Aculocity.GAABackOffice.Global;
using Aculocity.GAABackOffice.BLL;

namespace Aculocity.GAABackOffice.UI.Web.Controls
{
    public partial class LeadReportDetailMainGrid : UserControl {

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Page.IsPostBack)
            {
                Session["SummarySortExpression"] = "DateCreated";
                Session["SummarySortDirection"] = SortDirection.Descending;
            }
        }

        public DateTime PeriodFrom {
            get;
            set;
        }

        public DateTime PeriodTill {
            get;
            set;
        }

        public string DealerKey {
            get;
            set;
        }

        public string FirstName {
            get;
            set;
        }

        public string LastName {
            get;
            set;
        }

        public string UserMail {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public string LeadsPerPage
        {
            get;
            set;
        }


        protected void gvDealers_PageIndexChanging(object sender, GridViewPageEventArgs e) 
        {

            gvDealers.PageIndex = e.NewPageIndex;
            GenerateReport();
        }


        public  void GenerateReport()
        {
            try
            {

                DataTable dataTable = ReportBLL.GetDealerAppraisalsData(Session["SummaryDealerKey"].ToString(),
                                                                         (DateTime)Session["SummaryPeriodFrom"],
                                                                         (DateTime)Session["SummaryPeriodTill"],
                                                                         Session["SummaryFirstName"].ToString(),
                                                                         Session["SummaryLastName"].ToString(),
                                                                         Session["SummaryUserMail"].ToString(),
                                                                         Session["SummaryPhone"].ToString()
                                                                    ).Tables[0];
                if (dataTable != null)
                {
                    if (string.IsNullOrEmpty(Session["SummarySortDirection"].ToString())) Session["SummarySortDirection"] = Session["SummarySortDirection"] = SortDirection.Descending;
                    var sortDirection = (SortDirection)Enum.Parse(typeof(SortDirection), Session["SummarySortDirection"].ToString());
                    var dataView = new DataView(dataTable)
                                       {
                                           Sort = string.Format("{0} {1}", Session["SummarySortExpression"],
                                                                           ConvertSortDirectionToSql(sortDirection))
                                        };
                    //set page size
                    if (dataView.Table.Rows.Count > gvDealers.PageIndex * gvDealers.PageSize)
                    {
                        gvDealers.PageSize = Convert.ToInt32(Session["SummaryLeadsPerPage"].ToString());
                    }
                    
                    //set data
                    gvDealers.DataSource = GetTableToBinding(dataView);
                    gvDealers.DataBind();

                    foreach (TableCell cell in gvDealers.HeaderRow.Cells) { cell.Wrap = false; }
                }
                
                Session["ReportGrid"] = gvDealers;
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        protected void gvDealers_Sorting(object sender, GridViewSortEventArgs e)
        {
            var dataView = new DataView(((GridView)Session["ReportGrid"]).DataSource as DataTable);
            var sortDirection = e.SortExpression == Session["SummarySortExpression"].ToString() ? ReverseSortDirection((SortDirection)Enum.Parse(typeof(SortDirection), Session["SummarySortDirection"].ToString())) : e.SortDirection;
            var sortValue = string.Format("{0} {1}", e.SortExpression, ConvertSortDirectionToSql(sortDirection));

            if (dataView.Table != null)
            {
                dataView.Sort = sortValue;
                gvDealers.DataSource = GetTableToBinding(dataView);
                gvDealers.DataBind();

                foreach (TableCell cell in gvDealers.HeaderRow.Cells) { cell.Wrap = false; }
            }

            Session["SummarySortDirection"] = sortDirection;
            Session["SummarySortExpression"] = e.SortExpression;
        }

        private static string ConvertSortDirectionToSql(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = "ASC";
                    break;

                case SortDirection.Descending:
                    newSortDirection = "DESC";
                    break;
            }

            return newSortDirection;
        }

        private static SortDirection ReverseSortDirection(SortDirection sortDirection)
        {
            SortDirection newSortDirection = SortDirection.Ascending;

            switch (sortDirection)
            {
                case SortDirection.Ascending:
                    newSortDirection = SortDirection.Descending;
                    break;

                case SortDirection.Descending:
                    newSortDirection = SortDirection.Ascending;
                    break;
            }

            return newSortDirection;
        }
        private static DataTable GetTableToBinding(DataView dv)
        {
            var summaryReport = dv.ToTable();
            if (summaryReport.Columns["RowNumber"] == null) summaryReport.Columns.Add("RowNumber");
            int x = 1;

            foreach (DataRow row in summaryReport.Rows)
            {
                row["RowNumber"] = x++;
            }

            return summaryReport;
        }
    }
}