﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aculocity.GAABackOffice.UI.Web {
    
    public partial class QuestionTip : UserControl {

        public string Text {
            get;
            set;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);

            imgHelp.ImageUrl = "~//Images//questionmark.png";

            imgHelp.Attributes["onmouseover"] = string.Format("Tip('{0}')", Text);
            imgHelp.Attributes["onmouseout"] = "UnTip()";

            
            if (!Page.ClientScript.IsClientScriptIncludeRegistered(GetType(), "MainDef")) 
                Page.ClientScript.RegisterClientScriptInclude(GetType(), "MainDef", Response.ApplyAppPathModifier("~//JScripts//wz_tooltip.js"));
            


        }
    
    }

}