﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web {

    public partial class LeadReportCharts : UserControl {

        public string DealerKey { get; set; }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);

            RegisterClientScript();

        }
       
        public void GenerateCharts(string LeadresultType) {
            SetDataForTotalLeadsChart(LeadresultType);
            SetDataForTop10DesiredChart(LeadresultType);
        }

        string GetDataForTotalLeadsChart(string LeadResultType) {
            var sb = new StringBuilder();
            try {
                var dicTotalLeads = ReportBLL.GetTotalLeadsAsXML(DealerKey == string.Empty ? "%" : DealerKey, LeadResultType);
                sb.AppendLine("<graph  caption='Total Leads Per Month' showValues = '1' numVDivLines = '10'");
                sb.AppendLine(string.Format(" formatNumberScale = '0' FormatNumber = '0' numberSuffix = '' decimalPrecision = '0' yAxisMaxValue = '{0}' ", dicTotalLeads.Count == 0 ? "12" : dicTotalLeads["MaxValue"]));
                sb.AppendLine("        bgColor = 'E3E6D9' baseFontColor = '5D633F' BgAlpha = '50'");
                sb.AppendLine("        BorderColor = 'D7D8D3' canvasBorderThickness = '1' divLineColor = 'D7D8D3'");
                sb.AppendLine("        divLineAlpha = '80' numdivlines = '5' lineColor = 'A66EDD' anchorRadius = '3'");
                sb.AppendLine("        anchorBgColor = 'FFFFFF' anchorBorderColor = 'A66EDD' anchorBorderThickness = '1' lineThickness='2' ");
                sb.AppendLine("        showAlternateHGridColor = '1' AlternateHGridColor='D7D8D3' alternateHGridAlpha='20'>");
                sb.AppendLine("     <set name='' value=''/>");
                if (dicTotalLeads.Count != 0)
                    sb.AppendLine(dicTotalLeads["TotalLeads"].Replace(@"""", "'"));
                sb.AppendLine("     <set name='' value=''/>");
                sb.AppendLine("</graph>");
            } catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return sb.ToString();
        }

        string GetDataForTop10DesiredChart(string LeadResultType)
        {
            var sb = new StringBuilder();
            try {
                string top10Desired = ReportBLL.GetTop10DesiredAsXML(DealerKey == string.Empty ? "%" : DealerKey, LeadResultType);
                sb.AppendLine("<graph caption='Top 10 Desired Replacement models (Last 12 months)' ");
                sb.AppendLine("       decimalPrecision='0' formatNumberScale='0' yAxisMaxValue = '50' yAxisyAxisMinValue = '0'");
                sb.AppendLine("       bgColor = 'E3E6D9' baseFontColor = '5D633F' BgAlpha = '50' BorderColor = 'D7D8D3' ");
                sb.AppendLine("       canvasBorderThickness = '1' divLineColor = 'D7D8D3' divLineAlpha = '80' numVDivlines = '0'");
                sb.AppendLine("       showAlternateHGridColor = '1' AlternateHGridColor='D7D8D3' alternateHGridAlpha = '20'> ");
                sb.AppendLine("     <set name='' value='' color=''/>");
                sb.AppendLine(top10Desired.Replace(@"""", "'").Replace("######", "A66EDD"));
                sb.AppendLine("     <set name='' value='' color=''/>");
                sb.AppendLine("</graph>");
            } catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return sb.ToString();
        }

        protected void SetDataForTotalLeadsChart(string LeadResultType) {
            try {
                divTotalLeadsXMLData.InnerText = GetDataForTotalLeadsChart(LeadResultType);
            } catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void SetDataForTop10DesiredChart(string LeadResultType)
        {
            try {
                divTop10DesiredXMLData.InnerText = GetDataForTop10DesiredChart(LeadResultType);
            } catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ClientScript
        
        void RegisterClientScript() {

            if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MainDef")) {
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "MainDef", string.Format(@"
                var totalLeadsChartPath = '{0}';
                var desiredReplacementModelPath = '{1}';
                ", new object[]{
                    Response.ApplyAppPathModifier("~//FusionCharts//FCF_Line.swf"),
                    Response.ApplyAppPathModifier("~//FusionCharts//FCF_Column2D.swf")
                 }), true);
            }
        }

        #endregion
    }

}