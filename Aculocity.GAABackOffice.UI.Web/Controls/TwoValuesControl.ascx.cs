﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

using Aculocity.Controls.Web;

namespace Aculocity.GAABackOffice.UI {
    
    public partial class TwoValuesControl : UserControl, IValue  {

        public string FirstCaption {
            get;
            set;
        }

        public string SecondCaption {
            get;
            set;
        }

        [Browsable(false)]
        public object ValueFirst {
            get;
            set;
        }

        [Browsable(false)]
        public object ValueSecond {
            get;
            set;
        }

        public bool FirstElemntCompared {
            get;
            set;
        }

        public bool Enabled {
            get {
                return rbFirst.Enabled;
            }
            set {
                rbFirst.Enabled = rbSecond.Enabled = value;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public RadioButton FirstControl {
            get {
                return rbFirst;
            }
        }

        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public RadioButton SecondControl {
            get {
                return rbSecond;
            }
        }

        public object Value {
            get {
                
                if (rbSecond.Checked)
                    return ValueSecond;

                return ValueFirst;
            
            }
            set {

                if (FirstElemntCompared) {

                    if (value != null && value.Equals(ValueFirst)) {
                        rbFirst.Checked = true;
                        rbSecond.Checked = false;
                    } else {
                        rbFirst.Checked = false;
                        rbSecond.Checked = true;
                    }
                    
                } else {
                    
                    if (value != null && value.Equals(ValueSecond)) {
                        rbFirst.Checked = false;
                        rbSecond.Checked = true;
                    } else {
                        rbFirst.Checked = true;
                        rbSecond.Checked = false;
                    }
                    
                }
            
            }
        }

        public bool AutoPostBack 
        {
            set 
            { 
                rbFirst.AutoPostBack = value;
                rbSecond.AutoPostBack = value; 
            }
        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);

            rbFirst.GroupName = ID;
            rbSecond.GroupName = ID;

        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
        
            rbFirst.Text = FirstCaption;
            rbSecond.Text = SecondCaption;

            //rbFirst.AutoPostBack = rbSecond.AutoPostBack = AutoPostback;

            rbFirst.Attributes["onchange"] = "TVC_OnChange();";
            rbSecond.Attributes["onchange"] = "TVC_OnChange();";
            
        }


        
    }
}