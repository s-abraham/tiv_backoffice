﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.Controls.Web;

namespace Aculocity.GAABackOffice.UI.Web.Controls
{
    public partial class DatePicker : UserControl, IValue
    {
        public event EventHandler DataChanged;

        protected void Page_Load(object sender, EventArgs e)
        {
            //set event
            if (FireDataChangedEvent) { txtDate.TextChanged += DataChanged; }
        }

        #region ' Properties '
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public TextBox MainTextBoxControl
        {
            get
            {
                return txtDate;
            }
        }

        public object Value
        {

            get
            {
                if (string.IsNullOrEmpty(txtDate.Text))
                    return DateTime.Now;

                return DateTime.Parse(txtDate.Text);
            }
            set
            {

                if (DateTime.MinValue.Equals(value))
                {
                    txtDate.Text = string.Empty;
                    return;
                }

                txtDate.Text = ((DateTime)value).ToString("MM/dd/yyyy");
                calCalendar.SelectedDate = (DateTime)value;

            }
        }
        public string Text
        {
            get { return txtDate.Text.Trim(); }
            set { txtDate.Text = value.Trim(); }
        }
        public DateTime Date
        {
            get { return DateTime.Parse(txtDate.Text, CultureInfo.InvariantCulture); }
            set { txtDate.Text = value.ToString("MM/dd/yyyy"); }
        }
        public Unit Width
        {
            get { return txtDate.Width; }
            set { txtDate.Width = value; }
        }
        public Color ForeColor
        {
            get { return txtDate.ForeColor; }
            set { txtDate.ForeColor = value; }
        }
        public bool Enable
        {
            get { return txtDate.Enabled; }
            set 
            { 
                txtDate.Enabled = value;
                lnkDate.Visible = value;
            }
        }
        public String ImageUrl
        {
            get { return imgDate.Attributes["src"]; }
            set { imgDate.Attributes.Add("src", value); }
        }
        public string ValidationGroup
        {
            get { return rev.ValidationGroup; }
            set { rev.ValidationGroup = value; }
        }
        public string ErrorMessage
        {
            get { return rev.ErrorMessage; }
            set { rev.ErrorMessage = value; }
        }
        public bool FireDataChangedEvent
        {
            get { return Convert.ToBoolean(ViewState["FireDataChangedEvent"] ?? 0); }
            set { ViewState["FireDataChangedEvent"] = value; }
        }
        #endregion

        protected void lnkDade_Click(object sender, EventArgs e)
        {
            ShowHideCalendar();
        }

        protected void ChangeDate(object sender, EventArgs e)
        {
            //txtDate.Text = calCalendar.SelectedDate.ToShortDateString();
            txtDate.Text = Convert.ToDateTime(hidDateValue.Value).ToShortDateString();
            ShowHideCalendar();
            if (FireDataChangedEvent) { DataChanged(sender, e); }
        }

        protected void DayRender(Object source, DayRenderEventArgs e)
        {
            e.Cell.Attributes.Add("onClick", string.Format("javascript:document.getElementById('{0}').value = '{1}';document.getElementById('{2}').click(); return false;", hidDateValue.ClientID, e.Day.Date, btnChangeDate.ClientID));
        }


        private void ShowHideCalendar()
        {
            if (!string.IsNullOrEmpty(txtDate.Text))
            {
                var selectedDate = DateTime.Parse(txtDate.Text, CultureInfo.InvariantCulture);
                calCalendar.SelectedDate = selectedDate;
            }
            calCalendar.Visible = !calCalendar.Visible;
            lnkDate.Title = calCalendar.Visible ? "Hide Calendar" : "Show Calendar";
        }
    }
}