﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResellerDetailsReportGrid.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.ResellerDetailsReportGrid" %>

<asp:GridView ID="gvResellerDetailReportGrid" runat="server" CellPadding="3" EnableViewState="True"
        ForeColor="Black" BorderStyle="Solid" BorderWidth="1px" BorderColor="White"
        CssClass="text" CaptionAlign="Top" UseAccessibleHeader="False" 
        PageSize="10" AllowPaging="true" AutoGenerateColumns="False" 
        OnPageIndexChanging="gvResellerDetailReportGrid_PageIndexChanging" Height="307px" Width="1046px"> 
   <RowStyle Wrap="true" HorizontalAlign="Center" BackColor="White" 
         Font-Italic="True" Font-Names="Calibri" Font-Size="11pt" />
   <AlternatingRowStyle HorizontalAlign="Center" BackColor="LightBlue"  
         Font-Italic="True" Font-Names="Calibri" Font-Size="11pt" />
   <HeaderStyle ForeColor="White" BorderStyle="None" Font-Names="Calibri" 
        Font-Size="11pt" CssClass="Freezing" Wrap="False" Font-Bold="True" BackColor = "black" />
   <EmptyDataTemplate>
        <asp:Label ID="lblEmptyGridText" Text="There is no data for the selected criteria."  ForeColor="Red" runat="server"></asp:Label>
   </EmptyDataTemplate>
   <Columns>
        <asp:BoundField DataField="RowNumber" HeaderText="Number">
            <ItemStyle HorizontalAlign="Center"/>
        </asp:BoundField>
        <asp:BoundField DataField="Date" HeaderText="Date" />
        <asp:BoundField DataField="Account" HeaderText="Account Name" />

        <asp:BoundField DataField="DealerID" HeaderText="Dealer ID" />
        <asp:BoundField DataField="DealerName" HeaderText="Dealer Name" />
        <asp:BoundField DataField="DealerCity" HeaderText="Dealer City" />
        <asp:BoundField DataField="DealerState" HeaderText="Dealer State" />
        <asp:BoundField DataField="DealerZip" HeaderText="Dealer Zip" />

        <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" />
        <asp:BoundField DataField="Email" HeaderText="Email" />
        <asp:BoundField DataField="Desired" HeaderText="Desired Vehicle" />
        <asp:BoundField DataField="TradeIn" HeaderText="TradeIn Vehicle" />
   </Columns>
</asp:GridView>
