﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeadReportCharts.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.LeadReportCharts" %>
 <table>
    <tr>
        <td align="left" style="width: 700px">
            <div id="divTotalLeadsXMLData" runat="server" style="display:none"></div>
            <div id="divTotalLeadsChart" runat="server">
                <div id="TotalLeadsPerMonthChartDiv" style="text-align:center"></div>
                <asp:TextBox ID="txtTotalLeadsXMLData" runat="server" style="display:none" />
            </div>
        </td>
        <td align="left" style="width: 700px">
            <div id="divTop10DesiredXMLData" runat="server" style="display:none"></div>
            <div id="divTop10Desired" runat="server">
                <div id="Top10DesiredChartDiv" style="text-align:center"></div>
                <asp:TextBox ID="txtTop10DesiredXMLData" runat="server" style="display:none" />
            </div>
        </td>
    </tr>
</table>
<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(afterPageLoad);
    window.onload = afterPageLoad;
    function afterPageLoad() {
    
        try {
            var xmlData = document.getElementById('<%=divTotalLeadsXMLData.ClientID %>').innerHTML.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
            CreateTotalLeadsChart(xmlData);
            
            xmlData = document.getElementById('<%=divTop10DesiredXMLData.ClientID %>').innerHTML.replace(/&lt;/gi, "<").replace(/&gt;/gi, ">");
            CreateTop10DesiredChart(xmlData);
            
        } catch (err) { }
    }
    function CreateTotalLeadsChart(xmlData) {

        var chart_TotalLeadsPerMonthChart = new FusionCharts(totalLeadsChartPath, "TotalLeadsPerMonthChart", "700", "250", "0", "0");
        chart_TotalLeadsPerMonthChart.setDataXML(xmlData);
        chart_TotalLeadsPerMonthChart.setTransparent(true);
        chart_TotalLeadsPerMonthChart.render("TotalLeadsPerMonthChartDiv");

    }
    function CreateTop10DesiredChart(xmlData) {
        var chart_Top10DesiredChart = new FusionCharts(desiredReplacementModelPath, "Top10DesiredChart", "700", "250", "0", "0");
        chart_Top10DesiredChart.setDataXML(xmlData);
        chart_Top10DesiredChart.setTransparent(true);
        chart_Top10DesiredChart.render("Top10DesiredChartDiv");
    }
</script>