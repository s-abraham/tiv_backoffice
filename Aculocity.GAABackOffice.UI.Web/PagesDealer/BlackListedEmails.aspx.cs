﻿using System;
using System.Drawing;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class BlackListedEmails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetBlacklistedEmails();
            //Set the Master Page element
            Label MasterLabel = (Label)Form.Parent.FindControl("lblTitle");
            MasterLabel.Text = "Dealer Administration - Blacklisted Emails";
        }

        private void GetBlacklistedEmails()
        {
            grdBlackListEmails.DataSource = DealerBLL.GetBlacklistedEmails();
            grdBlackListEmails.DataBind();
        }

        protected void grdBlackListEmails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow selectedRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                switch (e.CommandName)
                {

                    case "Remove":
                        String Email = ((Label)grdBlackListEmails.Rows[selectedRow.RowIndex].Cells[0].FindControl("lblEmail")).Text;
                        DealerBLL.UnBlackListEmail(Email);
                        GetBlacklistedEmails();
                        break;
                }


            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnAddEmail_Click(object sender, EventArgs e)
        {
            Int32 result = DealerBLL.BlackListEmail(txtEmail.Text);

            switch (result)
            {
                case 0:
                    lblError.Text = "there was a problem blacklisting the email, please check the exception log";
                    lblError.ForeColor = Color.Red;
                    break;

                case 1:
                    lblError.Text = "";
                    break;

                case 2:
                    lblError.Text = "That email address is already blacklisted";
                    lblError.ForeColor = Color.Red;
                    break;
            }

            GetBlacklistedEmails();
        }
    }

}
