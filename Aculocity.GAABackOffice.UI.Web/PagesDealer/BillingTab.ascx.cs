﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;

using Aculocity.SimpleData;
using Aculocity.Controls.Web;

using Aculocity.GAABackOffice.DAL;

namespace Aculocity.GAABackOffice.UI.Web {

    public partial class BillingTab : EntityEditorUserControlBase {

        private string _dealerkey = string.Empty;

        public override Pair[] ControlFieldMappings {
            get {
                return new Pair[] {
                    new Pair("Billable", chkBillable),
                    new Pair("AdminAutoAppraisalCost", txtMonthlyBillingAmount),
                    new Pair("BillingInfo", txtAdditionalInformationBilling),
                    new Pair("BillingLastUpdated", ccBillingStatusLastUpdated),
                    new Pair("DateCreated", ccDealerDateCreated), 
                    new Pair("SystemBillingDate", ccSystemDealerStartDate),
                    new Pair("AdminSetupFees", ntbSetupFee),
                    new Pair("BillableStartDate", ccAdminBillingStartDate),
                    new Pair("BillingExplanation", txtReason)
                };
            }
        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);

            chkBillable.CheckedChanged += new EventHandler(chkBillable_CheckedChanged);
            Page.Load += new EventHandler(Page_Load);

            _dealerkey = Request.QueryString["DealerKey"];
        }

        public override void OnAfterDataBind() {
            base.OnAfterDataBind();

            ChangeBillable();

        }

        void chkBillable_CheckedChanged(object sender, EventArgs e) {

            ChangeBillable();

            ccBillingStatusLastUpdated.MainTextBoxControl.Text = DateTime.Now.ToString("MM/dd/yyyy");
        
        }

        void Page_Load(object sender, EventArgs e)
        {
            LoadLeadsForThreeLastMonths();
            SetDefaultValueForSystemBillingStartDate();
            ChangeBillable();

            if (string.IsNullOrEmpty(_dealerkey))
            {
                (Page as MainEditorHolder).UpdateButton.Text = "AddDealer";
            }
        }

        
        void ChangeBillable() {
            if (!chkBillable.Checked) 
            {
                txtReason.Visible = true;
                tdMotivate.InnerText = "Motivate";
            } 
            else 
            {
                txtReason.Visible = false;
                txtReason.Text = string.Empty;
                tdMotivate.InnerText = string.Empty;
            }
        }

        void LoadLeadsForThreeLastMonths() {

                DataTable table = DealerCommands.GetTriMonthlyLeads(DataManager, (Entity as Dealer).DealerKey).ExecuteDataTable();

                tdFirstMonth.InnerText = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-3).Month);
                tdSecondMonth.InnerText = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-2).Month);
                tdThirdMonth.InnerText = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-1).Month);

                tdFirstMonthTotal.InnerText = "0";
                tdSecondMonthTotal.InnerText = "0";
                tdThirdMonthTotal.InnerText = "0";

           
                string value = string.Empty; 
                HtmlTableCell[] cells = new HtmlTableCell[] {tdFirstMonth, tdFirstMonthTotal, tdSecondMonth, tdSecondMonthTotal, tdThirdMonth, tdThirdMonthTotal};
                for (int i = 0; i < table.Rows.Count; i+=2) {
                    cells[i].InnerText = string.IsNullOrEmpty(value = table.Rows[i][0].ToString()) ? "N/A" : value;
                    cells[i + 1].InnerText = string.IsNullOrEmpty(value = table.Rows[i][1].ToString()) ? "N/A" : value;
                }
        }

        private void SetDefaultValueForSystemBillingStartDate()
        {
            if (string.IsNullOrEmpty(_dealerkey))
            {
                var dealerDateCreated = DateTime.Parse(ccDealerDateCreated.MainTextBoxControl.Text, CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(dealerDateCreated.ToString()))
                {
                    if (ccDealerDateCreated.MainTextBoxControl.Text.Trim() == string.Empty ||
                        dealerDateCreated.ToString("M/d/yyyy") == DateTime.Today.ToString("M/d/yyyy"))
                    {
                        ccSystemDealerStartDate.Value = dealerDateCreated.AddDays(60);
                    }
                }
            }
        }

    }

}