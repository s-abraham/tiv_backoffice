﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;


namespace Aculocity.GAABackOffice.UI.Web {
    
    public partial class DealerTabHolder : TabHolderEditorBase {

        HtmlGenericControl[] buttons = null;
        
        protected override string[] Tabs {
            get {
                return new string[] {
                       "DealerInfo", "LeadManagement", "Billing", "WebsiteProvider"
                };
            }
        }

        public override Control ControlHolder {
            get {
                return cellMainHolder;
            }
        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);

            Page.PreRender += Page_PreRender;

        }

        void Page_PreRender(object sender, EventArgs e) {
            if (IsPostBack && Page.Request.Params["__EVENTTARGET"].Equals(hidCurrentIndex.UniqueID))
                ShowTab(int.Parse(hidCurrentIndex.Value));

            ApplySecurity();
        }

        void ApplySecurity()
        {

            Enumerations.AuthUserGroup currentGroup = Security.GetCurrentUserGroup();

            switch (currentGroup)
            {
                case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                    SetSecurityForSpecificDealer();
                    break;

                case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                case Enumerations.AuthUserGroup.ResellerAdmin:
                    SetSecurityForSpecificDealer();
                    break;

                case Enumerations.AuthUserGroup.HomeNetDealerAdmin:
                    SetSecurityForSpecificDealer();
                    break;

                case Enumerations.AuthUserGroup.GroupDealerAdmin:
                    SetSecurityForGroupAdmin();
                    break;
            }

        }

        void SetSecurityForSpecificDealer()
        {
            spanBilling.Style.Add("Display", "none");
        }

        void SetSecurityForGroupAdmin()
        {

            spanBilling.Style.Add("Display", "none");
            spanWebsiteProvider.Style.Add("Display", "none");
        }

        
        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);

            
            //(Page as MainEditorHolder).UpdateButton.Text = AddMode ? "AddDealer" : "Update Changes";
            

            buttons = new HtmlGenericControl[] {
                spanDealershipInfo, spanLeadManagement, spanBilling, spanWebsiteProvider
            };

            RegisterClientScript();

            buttons[CurrentTabIndex].Style["text-decoration"] = "underline";

        }

        
        protected override void InitializeEntity(Aculocity.SimpleData.Entity entity) {
            base.InitializeEntity(entity);

            Dealer dealer = entity as Dealer;

            dealer.GenerateNewDealerKey();
            dealer.DealerWebsite = string.Format("{0}//{1}{2}", ConfigurationManager.AppSettings["GAABackOffice.AppURL"], ConfigurationManager.AppSettings["AppraisalInitialPage"], dealer.DealerKey);
            dealer.AverageValueRange.VUnderTypeSelection = "%";
            dealer.AverageValueRange.VOverTypeSelection = "%";
            dealer.AverageValueRange.VUnderPercent = -10;
            dealer.AverageValueRange.VOverPercent = 10;
            dealer.AppraisalExpiration = 14;
            dealer.Language = "eng";
            dealer.NADABranding = true;
            dealer.LeadFormat = "XML";
            dealer.LeadFormat2 = "XML";
            dealer.LeadFormat3 = "XML";
            dealer.LeadFormat4 = "XML";
            dealer.Billable = true;
            dealer.AdminAutoAppraisalCost = 175;
            dealer.BillingLastUpdated = DateTime.Now;
            dealer.DateCreated = DateTime.Now;
            dealer.BillableStartDate = DateTime.Now;
            dealer.AdminSetupFees = 0;
            dealer.SystemBillingDate = DateTime.Now;
            dealer.Active = true;
            dealer.NewSelection = true;

        }


        #region Client Script

        void RegisterClientScript() {

            if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(),"TabDealerHolder_MainDef")) {
                 Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "TabDealerHolder_MainDef", string.Format(@"
                    
                    TabDealerHolder_hidCurrentIndexID = '{0}';

                    function TabDealerHolder_ShowTab(tabIndex) {{
    
                        document.getElementById(TabDealerHolder_hidCurrentIndexID).value = tabIndex;

                        {1};
                    }}

                 ", 
                  hidCurrentIndex.ClientID,
                  Page.ClientScript.GetPostBackEventReference(hidCurrentIndex, string.Empty)), true);   
            }

            
        }

        #endregion

    }

}