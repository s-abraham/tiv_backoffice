﻿using System;
using System.Web.UI;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class ManageDealerForSpecificDealer : MainBasePage
    {
        private string dealerKey;

        protected override void OnPreInit(EventArgs e)
        {
            try
            {
                Boolean Homenet;
                Boolean Galves;
                Boolean BSB;

                //Set the HomeNet
                if (Session["HomeNet"] != null)
                {
                    Homenet = Convert.ToBoolean(Session["HomeNet"].ToString());
                }
                else
                {
                    Homenet = false;
                }

                //Set the Galves
                if (Session["Galves"] != null)
                {
                    Galves = Convert.ToBoolean(Session["Galves"].ToString());
                }
                else
                {
                    Galves = false;
                }

                //Set the BSB
                if (Session["BSB"] != null)
                {
                    BSB = Convert.ToBoolean(Session["BSB"].ToString());
                }
                else
                {
                    BSB = false;
                }

                //Get the correct Master Page
                if (Homenet == true)
                {
                    MasterPageFile = "~/HomeNet.Master";
                }
                else
                {
                    if (Galves == true)
                    {
                        MasterPageFile = "~/Galves.Master";
                    }
                    else if (BSB == true)
                    {
                        MasterPageFile = "~/GetAutoAppraise.Master";
                    }
                    else
                    {
                        MasterPageFile = "~/GlobalMaster.Master";
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                btnLeadSummaryReport.Click += new EventHandler(btnLeadSummaryReport_Click);
                btnLeadDetailsReport.Click += new EventHandler(btnLeadDetailsReport_Click);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                if (!IsPostBack)
                {
                    ViewState["DealerKey"] = dealerKey = DealerCommands.GetDealerKeyByUserId(DataManager, Security.GetLoggedUserID()).ExecuteScalar().ToString();
                    ccFrom.Date = ccTo.Date = DateTime.Today;
                }
                else
                {
                    dealerKey = ViewState["DealerKey"].ToString();
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void btnLeadDetailsReport_Click(object sender, EventArgs e)
        {
            try
            {
                leadSummaryReport.Visible = false;
                drgDetailsReport.Visible = true;

                Session.Remove("ReportGrid");

                drgDetailsReport.DealerKey = dealerKey;
                drgDetailsReport.StartDate = (DateTime)ccFrom.Value;
                drgDetailsReport.EndDate = (DateTime)ccTo.Value;

                drgDetailsReport.FistName = txtFirstName.Text;
                drgDetailsReport.LastName = txtLastName.Text;
                drgDetailsReport.EMail = txtUserEmail.Text;
                drgDetailsReport.Phone = txtPhone.Text;

                drgDetailsReport.LeadsPerPage = rblLeadsCountPerPage.SelectedValue;

                drgDetailsReport.GenerateReport(false);

                //Display chart
                ShowHideTop10DesiredChart(true);
                chartTop10DesiredChart.DealerKey = dealerKey;
                chartTop10DesiredChart.GenerateChart("2");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void btnLeadSummaryReport_Click(object sender, EventArgs e)
        {
            try
            {
                leadSummaryReport.Visible = true;
                drgDetailsReport.Visible = false;

                leadSummaryReport.DealerKey = dealerKey;

                leadSummaryReport.PeriodFrom = (DateTime)ccFrom.Value;
                leadSummaryReport.PeriodTill = (DateTime)ccTo.Value;
                leadSummaryReport.FirstName = txtFirstName.Text;
                leadSummaryReport.LastName = txtLastName.Text;
                leadSummaryReport.UserEmail = txtUserEmail.Text;
                leadSummaryReport.Phone = txtPhone.Text;

                leadSummaryReport.LeadsPerPage = rblLeadsCountPerPage.SelectedValue;

                leadSummaryReport.GenerateReport();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                drgDetailsReport.DealerKey = dealerKey;
                drgDetailsReport.StartDate = (DateTime)ccFrom.Value;
                drgDetailsReport.EndDate = (DateTime)ccTo.Value;

                drgDetailsReport.FistName = txtFirstName.Text;
                drgDetailsReport.LastName = txtLastName.Text;
                drgDetailsReport.EMail = txtUserEmail.Text;

                drgDetailsReport.ExportToExcell();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }
        private void ShowHideTop10DesiredChart(bool isDisplay)
        {
            try
            {
                tdTop10DesiredChart.Attributes.Add("style", string.Format("display:{0}", isDisplay ? "block" : "none"));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
    }
}