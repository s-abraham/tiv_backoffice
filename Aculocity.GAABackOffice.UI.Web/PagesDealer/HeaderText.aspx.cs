﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class HeaderText : BasePage
    {
        private string _dealerKey = string.Empty;

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                _currentGroup = Security.GetCurrentUserGroup();
                _dealerKey = Request.QueryString["dealerKey"];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lnkEnglish.Attributes.Add("class", "selected");
                    LoadDealerHeaders(_dealerKey);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' controls's events '
        protected void lnk_Click(object sender, EventArgs e)
        {
            try
            {
                lnkEnglish.Attributes.Add("class", "");
                lnkSpanish.Attributes.Add("class", "");
                ((HtmlAnchor)sender).Attributes.Add("class", "selected");
                if (((HtmlAnchor)sender).ID == "lnkEnglish")
                {
                    txtSpanish.Text = ftbHeaderText.Text;
                    ftbHeaderText.Text = txtEnglish.Text;

                    txtIsAciveEsp.Text = chkActive.Checked.ToString();
                    chkActive.Checked = Convert.ToBoolean(txtIsAciveEng.Text);

                    txtLang.Text = "eng";
                }
                else
                {
                    txtEnglish.Text = ftbHeaderText.Text;
                    ftbHeaderText.Text = txtSpanish.Text;

                    txtIsAciveEng.Text = chkActive.Checked.ToString();
                    chkActive.Checked = Convert.ToBoolean(txtIsAciveEsp.Text);

                    txtLang.Text = "esp";
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //update local headers's copy
                UpdateDealerHeaderTextInLocalCopy(GetCurrentPageNumber()
                                                  , "eng"
                                                  , txtLang.Text == "eng" ? ftbHeaderText.Text.Trim() : txtEnglish.Text.Trim()
                                                  , txtLang.Text == "eng" ? chkActive.Checked : Convert.ToBoolean(txtIsAciveEng.Text));
                UpdateDealerHeaderTextInLocalCopy(GetCurrentPageNumber()
                                                  , "esp"
                                                  , txtLang.Text == "esp" ? ftbHeaderText.Text.Trim() : txtSpanish.Text.Trim()
                                                  , txtLang.Text == "esp" ? chkActive.Checked : Convert.ToBoolean(txtIsAciveEsp.Text));
                //set header text for current page
                SetHeaderTextForCurrentPage(Convert.ToInt32(ddlPage.SelectedValue), txtLang.Text);
                ResetSessionCurrentPageNo(Convert.ToInt32(ddlPage.SelectedValue));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateDealerHeaderTextInLocalCopy(GetCurrentPageNumber()
                                              , "eng"
                                              , txtLang.Text == "eng" ? ftbHeaderText.Text.Trim() : txtEnglish.Text.Trim()
                                              , txtLang.Text == "eng" ? chkActive.Checked : Convert.ToBoolean(txtIsAciveEng.Text));
                UpdateDealerHeaderTextInLocalCopy(GetCurrentPageNumber()
                                                  , "esp"
                                                  , txtLang.Text == "esp" ? ftbHeaderText.Text.Trim() : txtSpanish.Text.Trim()
                                                  , txtLang.Text == "esp" ? chkActive.Checked : Convert.ToBoolean(txtIsAciveEsp.Text));
                
                lblFeedback.Text = string.Empty;
                if (StripHTMLTagsLength(txtEnglish.Text.Trim()) > 1000) {
                //    lblFeedback.ForeColor = Color.Red;
                //    lblFeedback.Text += "<li>Please limit your English text to 1000 characters or less.";
                }
                
                if (StripHTMLTagsLength(txtSpanish.Text.Trim()) > 1000) {
                //    lblFeedback.ForeColor = Color.Red;
                //    lblFeedback.Text += "<li>Please limit your Spanish text to 1000 characters or less.";
                }

                if(lblFeedback.Text.Trim() == string.Empty)
                {
                    //TODO update
                    var headers = GetDealerHeadersLocalCopy();
                    foreach(DealerHeaderTextBLL header in headers.Items)
                    {
                        header.Update();
                    }
                }
                lblFeedback.Text = "(Dealership updated successfully)";
                lblFeedback.ForeColor = Color.Gray;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (_currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                {
                    Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                }
                else
                {
                    Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion
        
        #region ' Custom Methods '
        private void LoadDealerHeaders(string dealerKey)
        {
            try
            {
                var dealerHeaders = new DealerHeaders();
                dealerHeaders.GetAllDealerHeaders(dealerKey);

                ResetSessionDealerHeaders(dealerHeaders);
                ResetSessionCurrentPageNo(1);
                
                SetHeaderTextForCurrentPage(1, "eng");
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }
        
        private static int StripHTMLTagsLength(string htmlToStrip)
        {
            string stripped;
            try
            {
                if (!string.IsNullOrEmpty(htmlToStrip))
                {
                    stripped = Regex.Replace(htmlToStrip, "<(.|\\n)+?>", string.Empty);
                    return stripped.Length;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return 0;
        }
        private void SetHeaderTextForCurrentPage(int pageNo, string language)
        {
            try
            {
                var headerText = GetDealerHeaderText(pageNo);

                if (language == "eng")
                {
                    ftbHeaderText.Text = headerText[0, 0];
                    chkActive.Checked = Convert.ToBoolean(headerText[0, 1]);
                }
                else
                {
                    ftbHeaderText.Text = headerText[1, 0];
                    chkActive.Checked = Convert.ToBoolean(headerText[1, 1]);
                }

                txtLang.Text = language;

                txtEnglish.Text = headerText[0, 0];
                txtSpanish.Text = headerText[1, 0];

                txtIsAciveEng.Text = headerText[0, 1];
                txtIsAciveEsp.Text = headerText[1, 1];
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private string[,] GetDealerHeaderText(int pageNo)
        {
            
            var headers = new string[2, 2] { { "", false.ToString() }, { "", false.ToString() } };
            try
            {
                var dealerHeaders = GetDealerHeadersLocalCopy();
                foreach (DealerHeaderTextBLL header in dealerHeaders.Items)
                {
                    if (header.PageNo == pageNo && header.TextLanguage == "eng")
                    {
                        headers[0, 0] = header.HeaderText;
                        headers[0, 1] = header.Active.ToString();
                    }
                    if (header.PageNo == pageNo && header.TextLanguage == "esp")
                    {
                        headers[1, 0] = header.HeaderText;
                        headers[1, 1] = header.Active.ToString();
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return headers;
        }
        private void UpdateDealerHeaderTextInLocalCopy(int pageNo, string lang, string headerText, bool isActive)
        {
            try
            {
                var dealerHeaders = GetDealerHeadersLocalCopy();
                if (DealerHeaderExist(pageNo, lang))
                {
                    foreach (DealerHeaderTextBLL header in dealerHeaders.Items)
                    {
                        if (header.PageNo == pageNo && header.TextLanguage == lang)
                        {
                            header.HeaderText = headerText;
                            header.Active = isActive;
                        }
                    }
                }
                else
                {
                    var newDealerHeader = new DealerHeaderTextBLL()
                    {
                        DealerKey = _dealerKey
                        ,PageNo = pageNo
                        ,TextLanguage = lang
                        ,HeaderText = headerText
                        ,Active = isActive
                    };
                    dealerHeaders.Add(newDealerHeader);
                }
                ResetSessionDealerHeaders(dealerHeaders);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private bool DealerHeaderExist(int pageNo, string lang)
        {
            bool exist = false;
            var dealerHeaders = GetDealerHeadersLocalCopy();
            foreach (DealerHeaderTextBLL header in dealerHeaders.Items)
            {
                if (header.PageNo == pageNo && header.TextLanguage == lang) exist = true;
            }
            return exist; 
        }
        private void ResetSessionDealerHeaders(DealerHeaders dealerHeaders)
        {
            Session.Remove("DealerHeaders");
            Session["DealerHeaders"] = dealerHeaders;
        }
        private void ResetSessionCurrentPageNo(int pageNo)
        {
            Session.Remove("CurrentPageNo");
            Session["CurrentPageNo"] = pageNo;
        }
        private int GetCurrentPageNumber()
        {
            return (int) Session["CurrentPageNo"];
        }
        private DealerHeaders GetDealerHeadersLocalCopy()
        {
            return (DealerHeaders)Session["DealerHeaders"];
        }
        #endregion
        
    }
}
