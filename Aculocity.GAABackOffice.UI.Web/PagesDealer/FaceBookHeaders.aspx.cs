﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class FaceBookHeaders : BasePage
    {
        #region ' Page's Events '
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DealerKey = Request.QueryString["dealerKey"];
                LoadDealerHTML(DealerKey);
                lblTitle.Text = string.Format("Facebook Header for - \"{0}\"", DealerBLL.GetDealerName(DealerKey));
            }
        }
        #endregion

        #region ' controls's events '
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                lblFeedback.Text = string.Empty;
                if (StripHTMLTagsLength(ftbfaceBookHeader.Text.Trim()) > 1000)
                {
                        lblFeedback.ForeColor = Color.Red;
                        lblFeedback.Text += "<li>Please limit your FaceBook Header to 1000 characters or less.";
                }

                //update header:
                if (lblFeedback.Text.Trim() == string.Empty)
                {
                    DealerBLL.UpdateFaceBookHeader(DealerKey, ftbfaceBookHeader.Text.Trim());
                }
                lblFeedback.Text = "(Dealership updated successfully.)";
                lblFeedback.ForeColor = Color.Gray;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (_currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                {
                    Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                }
                else
                {
                    Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region ' Custom Methods '
        private void LoadDealerHTML(string dealerKey)
        {
            try
            {
                ftbfaceBookHeader.Text = DealerBLL.GetFaceBookHeader(dealerKey);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private int StripHTMLTagsLength(string htmlToStrip)
        {
            string stripped;
            try
            {
                if (!string.IsNullOrEmpty(htmlToStrip))
                {
                    stripped = Regex.Replace(htmlToStrip, "<(.|\\n)+?>", string.Empty);
                    return stripped.Length;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return 0;
        }
        #endregion

        #region ' properties '
        private string DealerKey
        {
            get { return ViewState["DealerKey"] != null ? ViewState["DealerKey"] as string : string.Empty; }
            set { ViewState["DealerKey"] = value; }
        }
        #endregion
    }
}
