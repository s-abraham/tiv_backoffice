﻿<%@ Page Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="BlackListedEmails.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.BlackListedEmails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        table.text,
        table.text td,
        table.text th {
            border-color: #666;

        }
        table.text th {
            text-align: left;
        }
        table.text th,
        table.text td {
            text-align: left;
            padding: 6px 6px 5px 7px;
        }

        table.text td {
            font-weight: lighter;
            color: #333;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
    <table>
        <tr>
            <td colspan="3">
                <asp:GridView ID="grdBlackListEmails" runat="server" AutoGenerateColumns="False" Width="100%"
                    DataKeyNames="Email" AlternatingRowStyle-BackColor="LightGray" HeaderStyle-BackColor="Black"
                    HeaderStyle-ForeColor="White" OnRowCommand="grdBlackListEmails_RowCommand" CssClass="text">
                    <Columns>
                        <asp:TemplateField HeaderText="Email" SortExpression="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Blacklisted Date" SortExpression="ListingDate">
                            <ItemTemplate>
                                <asp:Label ID="lblListingDate" runat="server" Text='<%# Bind("ListingDate") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Activate">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkRemove" runat="server" CommandName="Remove" CommandArgument='<%# Bind("Email") %>'
                                    Text="Activate" CausesValidation="true" ForeColor="Blue" />&nbsp;
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblEmailAdd" runat="server" Text="Email Address:"></asp:Label>  
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" Width="300px"></asp:TextBox>    
            </td>
            <td>
                <asp:Button ID="btnAddEmail" runat="server" Text="BlackList Email" 
                    onclick="btnAddEmail_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
