<%@ Page Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true"
    CodeBehind="DealerAdmin.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.DealerAdmin" %>

<asp:Content ID="Head" ContentPlaceHolderID="Head" runat="server">

    <link href="../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .dealer-create .ajax__tab_header,
        .dealer-login.dealer-edit .ajax__tab_header {
            display: none;
        }

        .dealer-create #ctl00_PageContent_MainContainer_InfoPanel_rowDealershipKey {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="PageContent" runat="server">

    <script language="javascript" type="text/javascript">
        function ShowManagement() {
            //document.getElementById("ctl00_PageContent_MainContainer_LeadPanel_TxtProvider").style.display = "block";
            //document.getElementById("ctl00_PageContent_MainContainer_LeadPanel_btnContinueProvider").style.display = "block";
            $('[data-toggle="add-provider"]').show();
            return false;
        }
    </script>

    <script type="text/javascript">
        window.onunload = function () {
            CheckingToSaving();
        }
        window.onload = function () {
            ShowAppraisalCode(false);
        }
        function CheckingToSaving() {
            if (document.getElementById('<%=hidInfoPanelIsChanged.ClientID%>').value != "" ||
            document.getElementById('<%=hidDataPanelIsChanged.ClientID%>').value != "" ||
            document.getElementById('<%=hidCutomizationPanelIsChanged.ClientID%>').value != "" ||
                <%--ocument.getElementById('<%=hidLeadPanelIsChanged.ClientID%>').value != "" || --%>
            <%--document.getElementById('<%=hidBillingPanelIsChanged.ClientID%>').value != "" || --%>
            document.getElementById('<%=hidWebsitePanelIsChanged.ClientID%>').value != "") {
                if (confirm('Would you like to save your unsaved changes?')) { document.getElementById('<%=btnSaveSettingsTab1.ClientID%>').click(); }
            }
        }

        function ShowAppraisalURL() {
            var URL = document.getElementById("ctl00_PageContent_MainContainer_CutomizationPanel_txtAppraisalURL").value;
            window.open(URL, "AppraisalWindow");
        }
        function ShowAppraisalCode(copyToClip) {
            var URL = $("#ctl00_PageContent_MainContainer_CutomizationPanel_txtAppraisalURL").val();
            var SelectedSize = $("#ctl00_PageContent_MainContainer_CutomizationPanel_rbtIFrameOptions input:checked").val();
            var IframeLink = "<div align='center'><iframe src='" + URL + "' style='" + SelectedSize + "' frameborder='0' scrolling='no' seamless='seamless' ></iframe> </div>";
            var escapedIframeLink = IframeLink.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            $("#ctl00_PageContent_MainContainer_CutomizationPanel_lblHtmlCode").html(escapedIframeLink);
        }
        function SetChangedState(tabName) {
            try {
                $('#ctl00_PageContent_lblStatus').hide();
                switch (tabName) {
                    case "InfoPanel": document.getElementById('<%=hidInfoPanelIsChanged.ClientID%>').value = "changed"; break;
                    case "DataPanel": document.getElementById('<%=hidDataPanelIsChanged.ClientID%>').value = "changed"; break;
                    case "CutomizationPanel": document.getElementById('<%=hidCutomizationPanelIsChanged.ClientID%>').value = "changed"; break;
                        <%--case "LeadPanel": document.getElementById('<%=hidLeadPanelIsChanged.ClientID%>').value = "changed"; break;--%>
                    <%--case "BillingPanel": document.getElementById('<%=hidBillingPanelIsChanged.ClientID%>').value = "changed"; break;--%>
                    case "WebsitePanel": document.getElementById('<%=hidWebsitePanelIsChanged.ClientID%>').value = "changed"; break;
                }
            }
            catch (err) { }
        }
    </script>

    <asp:HiddenField ID="hidRedirectPath" runat="server" Value="" />
    <div id="MainDiv" runat="server">
        <div id="CreateDealerStart" runat="server">
            <h2 class="title">Add Dealer</h2>
            <table>
                <tr>
                    <td>Is this dealer part of a Group?
                    </td>
                    <td>
                        <asp:RadioButtonList ID="DealerInGroup" CssClass="dealer-in-group" runat="server" AutoPostBack="True"
                            OnSelectedIndexChanged="SetDealerInGroup_Change" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                            <asp:ListItem Text="No" Value="false"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <div id="ChooseDealerGroup" runat="server">
                <asp:UpdatePanel ID="dealerGroupError" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlDealerGroupError" runat="server" Visible="false" CssClass="pnlLoginError">
                            <asp:Label ID="lblDealerGroupError" runat="server" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DealerGroupContinueBtn" />
                    </Triggers>
                </asp:UpdatePanel>
                <table id="ChooseDealerGroupTable" runat="server">
                    <tr runat="server">
                        <td runat="server">
                            <asp:Label ID="lblDealerGroup1" runat="server">Choose Dealer Group</asp:Label>
                        </td>
                        <td runat="server">
                            <asp:DropDownList ID="ddlDealerGroup1" runat="server" Width="100%" OnSelectedIndexChanged="SetDealerGroup_Change">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="createGroupErrorRow" runat="server" visible="false">
                        <td colspan="2" runat="server">
                            <asp:UpdatePanel ID="createGroupError" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlCreateGroupError" runat="server" CssClass="pnlLoginError">
                                        <asp:Label ID="lblCreateGroupError" runat="server" />
                                    </asp:Panel>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="AddDealerGroupBtn" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr id="addDealerRow" runat="server">
                        <td runat="server">
                            <asp:Label ID="lblAddDealerGroup" runat="server">Add Dealer Group</asp:Label>
                        </td>
                        <td runat="server">
                            <asp:TextBox ID="txtNewDealerGroup" runat="server" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr id="groupAdminRow" runat="server">
                        <td runat="server">
                            <asp:Label ID="lblGroupAdmin" runat="server">Group Dealer Type</asp:Label>
                        </td>
                        <td runat="server">
                            <asp:DropDownList ID="ddlGroupAdmin" runat="server" Visible="false">
                                <asp:ListItem Text="HOMENET" Value='4'></asp:ListItem>
                                <asp:ListItem Text="GALVES" Value="5"></asp:ListItem>
                                <asp:ListItem Text="BSB" Value="6"></asp:ListItem>
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr id="addDealerRowBtn" runat="server">
                        <td></td>
                        <td style="text-align: right">
                            <asp:Button ID="AddDealerGroupBtn" runat="server" Text="Add Dealer Group" OnClick="AddDealerGroup_Click" CssClass="tiv-btn" />
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Button ID="DealerGroupContinueBtn" Visible="false" runat="server" Text="Continue" OnClick="ValidateDealerGroup_Click" CssClass="tiv-btn" />

        </div>
        <table width="100%">
            <tr>
                <td>
                    <div>
                        <asp:ValidationSummary CssClass="div_ValidationSummaryHolder" runat="server" ID="vsInfoPanel"
                            HeaderText="<b>Dealer Information:</b>" DisplayMode="List" ShowSummary="true"
                            ValidationGroup="vgInfoPanel" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <asp:ValidationSummary CssClass="div_ValidationSummaryHolder" runat="server" ID="vsCutomizationPanel"
                            HeaderText="<b>Tool Customization:</b>" DisplayMode="List" ShowSummary="true"
                            ValidationGroup="vgCutomizationPanel" />
                    </div>
                </td>
            </tr>
            <%--
            <tr>
                <td>
                    <div>
                        <asp:ValidationSummary CssClass="div_ValidationSummaryHolder" runat="server" ID="vsLeadPanel"
                            HeaderText="<b>Lead Management:</b>" DisplayMode="List" ShowSummary="true" ValidationGroup="vgInfoPanel" />
                    </div>
                </td>
            </tr>
            --%>
            <tr>
                <td>
                    <div>
                        <asp:ValidationSummary CssClass="div_ValidationSummaryHolder" runat="server" ID="vsBillingPanel"
                            HeaderText="<b>Billing:</b>" DisplayMode="List" ShowSummary="true" ValidationGroup="vgBillingPanel" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <asp:ValidationSummary CssClass="div_ValidationSummaryHolder" runat="server" ID="vsWebsitePanel"
                            HeaderText="<b>Website Provider:</b>" DisplayMode="List" ShowSummary="true" ValidationGroup="vgWebsitePanel" />
                    </div>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <b>
                        <asp:Label ID="lblStatus" runat="server"></asp:Label></b><br />
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        <asp:Label ID="lblCustomerExceeded" runat="server"></asp:Label></b><br />
                </td>
            </tr>
        </table>
        <ajaxToolkit:TabContainer ID="MainContainer" runat="server" CssClass="ajax__tab_yuitabview-theme"
            OnActiveTabChanged="MainContainer_ActiveTabChanged">
            <%--Tab 1--%>
            <ajaxToolkit:TabPanel ID="InfoPanel" runat="server" HeaderText="Dealer Information">
                <ContentTemplate>
                    <asp:HiddenField ID="hidInfoPanelIsChanged" runat="server" />
                    <h2 class="title">Dealer Information</h2>
                    <table id="tblDealerInfo" runat="server">
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblDealerName" runat="server" CssClass="MandatoryField">Dealer Name*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtDealerName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqDealerName" runat="server" ErrorMessage="Please enter a Dealer Name"
                                    ControlToValidate="txtDealerName" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblActive" runat="server">Active</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:CheckBox ID="chkActive" runat="server" OnCheckedChanged="chkActive_CheckedChanged" AutoPostBack="True" />
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblSalesPerson" runat="server" CssClass="MandatoryField">Sales Person*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtSalesPerson" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqSalesPerson" runat="server" ErrorMessage="Please enter a Sales Person"
                                    ControlToValidate="txtSalesPerson" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                            </td>
                            <td id="Td5" runat="server">
                                <asp:Label ID="Label10" runat="server">Active Widget</asp:Label>
                            </td>
                            <td id="Td6" runat="server">
                                <asp:CheckBox ID="chkEnableWidget" runat="server" />
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblOriginalSalesPerson" runat="server">Original Sales Person</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtOriginalSalesPerson" runat="server"></asp:TextBox>
                            </td>
                            <td id="Td4" colspan="2" runat="server">
                                <b>
                                    <asp:Label ID="lblActivationStatus" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr runat="server" id="trIsReseller">
                            <td id="Td7" runat="server">
                                <asp:Label ID="lblIsReseller" runat="server">Reseller Mode</asp:Label>
                            </td>
                            <td id="Td8" runat="server">
                                <asp:CheckBox ID="chkIsReseller" runat="server" />
                            </td>
                        </tr>
                        <tr runat="server" id="dealerGroupRow">
                            <td runat="server">
                                <asp:Label ID="lblDealerGroup" runat="server">Dealer Group</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="ddlDealerGroup" runat="server" Width="100%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr runat="server" style="display: none">
                            <td runat="server">
                                <asp:Label ID="lblAutogroup" runat="server">Auto Group</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtAutoGroup" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblWebsite" runat="server">Website</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtWebsite" runat="server"></asp:TextBox>
                            </td>
                            <td runat="server">
                                <GAABackOfficeControls:QuestionTip ID="qtPublicWebSite" runat="server" Text="Public website URL" />
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server"></td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblProductType" runat="server" CssClass="MandatoryField">TIV Package*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:RadioButtonList ID="rbProductType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Basic" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Pro" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Enterprise" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="reqProductType" runat="server" ErrorMessage="Please Choose a Package"
                                    ControlToValidate="rbProductType" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                            </td>
                            <td runat="server"></td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <p>
                                    <b>Dealer Address:</b>
                                </p>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblStreet" runat="server" CssClass="MandatoryField">Street*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtStreet" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqStreet" runat="server" ErrorMessage="Please enter a Street"
                                    ControlToValidate="txtStreet" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblCity" runat="server" CssClass="MandatoryField">City*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqCity" runat="server" ErrorMessage="Please enter a City"
                                    ControlToValidate="txtCity" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblState" runat="server" CssClass="MandatoryField">State*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="ddlStateID" runat="server">
                                    <asp:ListItem Value="AL">Alabama</asp:ListItem>
                                    <asp:ListItem Value="AK">Alaska</asp:ListItem>
                                    <asp:ListItem Value="AZ">Arizona</asp:ListItem>
                                    <asp:ListItem Value="AR">Arkansas</asp:ListItem>
                                    <asp:ListItem Value="CA">California</asp:ListItem>
                                    <asp:ListItem Value="CANADA">Canada</asp:ListItem>
                                    <asp:ListItem Value="CO">Colorado</asp:ListItem>
                                    <asp:ListItem Value="CT">Connecticut</asp:ListItem>
                                    <asp:ListItem Value="DE">Delaware</asp:ListItem>
                                    <asp:ListItem Value="DC">District of Columbia</asp:ListItem>
                                    <asp:ListItem Value="EUROPE">Europe</asp:ListItem>
                                    <asp:ListItem Value="FL">Florida</asp:ListItem>
                                    <asp:ListItem Value="GA">Georgia</asp:ListItem>
                                    <asp:ListItem Value="HI">Hawaii</asp:ListItem>
                                    <asp:ListItem Value="ID">Idaho</asp:ListItem>
                                    <asp:ListItem Value="IL">Illinois</asp:ListItem>
                                    <asp:ListItem Value="IN">Indiana</asp:ListItem>
                                    <asp:ListItem Value="IA">Iowa</asp:ListItem>
                                    <asp:ListItem Value="KS">Kansas</asp:ListItem>
                                    <asp:ListItem Value="KY">Kentucky</asp:ListItem>
                                    <asp:ListItem Value="LA">Louisiana</asp:ListItem>
                                    <asp:ListItem Value="ME">Maine</asp:ListItem>
                                    <asp:ListItem Value="MD">Maryland</asp:ListItem>
                                    <asp:ListItem Value="MA">Massachusetts</asp:ListItem>
                                    <asp:ListItem Value="MI">Michigan</asp:ListItem>
                                    <asp:ListItem Value="MN">Minnesota</asp:ListItem>
                                    <asp:ListItem Value="MS">Mississippi</asp:ListItem>
                                    <asp:ListItem Value="MO">Missouri</asp:ListItem>
                                    <asp:ListItem Value="MT">Montana</asp:ListItem>
                                    <asp:ListItem Value="NE">Nebraska</asp:ListItem>
                                    <asp:ListItem Value="NV">Nevada</asp:ListItem>
                                    <asp:ListItem Value="NH">New Hampshire</asp:ListItem>
                                    <asp:ListItem Value="NJ">New Jersey</asp:ListItem>
                                    <asp:ListItem Value="NM">New Mexico</asp:ListItem>
                                    <asp:ListItem Value="NY">New York</asp:ListItem>
                                    <asp:ListItem Value="NC">North Carolina</asp:ListItem>
                                    <asp:ListItem Value="ND">North Dakota</asp:ListItem>
                                    <asp:ListItem Value="OH">Ohio</asp:ListItem>
                                    <asp:ListItem Value="OK">Oklahoma</asp:ListItem>
                                    <asp:ListItem Value="OR">Oregon</asp:ListItem>
                                    <asp:ListItem Value="PA">Pennsylvania</asp:ListItem>
                                    <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                                    <asp:ListItem Value="RI">Rhode Island</asp:ListItem>
                                    <asp:ListItem Value="SC">South Carolina</asp:ListItem>
                                    <asp:ListItem Value="SD">South Dakota</asp:ListItem>
                                    <asp:ListItem Value="TN">Tennessee</asp:ListItem>
                                    <asp:ListItem Value="TX">Texas</asp:ListItem>
                                    <asp:ListItem Value="UT">Utah</asp:ListItem>
                                    <asp:ListItem Value="VT">Vermont</asp:ListItem>
                                    <asp:ListItem Value="VA">Virginia</asp:ListItem>
                                    <asp:ListItem Value="WA">Washington</asp:ListItem>
                                    <asp:ListItem Value="WV">West Virginia</asp:ListItem>
                                    <asp:ListItem Value="WI">Wisconsin</asp:ListItem>
                                    <asp:ListItem Value="WY">Wyoming</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblZip" runat="server" CssClass="MandatoryField">Zip*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqZip" runat="server" ErrorMessage="Please enter a Zip Code"
                                    ControlToValidate="txtZip" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td colspan="3" runat="server">
                                <p>
                                    <b>Dealer Primary Contact:</b>
                                </p>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactName" runat="server" CssClass="MandatoryField">Name*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqContactName" runat="server" ErrorMessage="Please enter a contact name"
                                    ControlToValidate="txtContactName" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblEmail" runat="server" CssClass="MandatoryField">Email*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactEmail" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqContactEmail" runat="server" ErrorMessage="Please enter a contact email"
                                    ControlToValidate="txtContactemail" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Contact email address is in the incorrect format"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtContactemail" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactPhone" runat="server" CssClass="MandatoryField">Phone*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactPhone" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqContactPhone" runat="server" ErrorMessage="Please enter a contact phone number"
                                    ControlToValidate="txtContactPhone" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revContactPhone" runat="server" ErrorMessage="Incorrect formatting for the contact phone. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtContactPhone" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblContactAppointment" runat="server" CssClass="MandatoryField">Appointment Email*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactAppointment" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqContactAppointment" runat="server" ErrorMessage="Please enter an email for appointments"
                                    ControlToValidate="txtContactAppointment" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revContactAppointment" runat="server" ErrorMessage="Appointment email is in the incorrect format"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtContactAppointment" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactPhone2" runat="server">Phone 2</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactPhone2" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revContactPhone2" runat="server" ErrorMessage="Incorrect formatting for the contact phone 2. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtContactPhone2" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblFax" runat="server">Fax</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtFax" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revFax" runat="server" ErrorMessage="Incorrect formatting for the contact fax. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtFax" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactTitle" runat="server">Contact title</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactTitle" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td colspan="3" runat="server">
                                <p>
                                    <b>Internet manager (Optional):</b>
                                </p>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactName2" runat="server">Name</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactName2" runat="server"></asp:TextBox>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblEmail2" runat="server">Email</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactEmail2" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revContactEmail2" runat="server" ErrorMessage="Contact email is in the incorrect format(Dealer Secondary Contact)"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtContactEmail2" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactPhoneTwo" runat="server">Phone</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactPhoneTwo" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revContactPhoneTwo" runat="server" ErrorMessage="Incorrect formatting for the contact phone. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx(Dealer Secondary Contact)"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtContactPhoneTwo" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblContactAppointment2" runat="server">Appointment Email</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactAppointment2" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revContactAppointment2" runat="server" ErrorMessage="Appointment email is in the incorrect format(Dealer Secondary Contact)"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtContactAppointment2" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactPhonetwo2" runat="server">Phone 2</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactPhonetwo2" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revContactPhonetwo2" runat="server" ErrorMessage="Incorrect formatting for the contact phone 2. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx(Dealer Secondary Contact)"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtContactPhonetwo2" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:Label ID="lblFax2" runat="server">Fax</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtFax2" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revFax2" runat="server" ErrorMessage="Incorrect formatting for the contact fax. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx(Dealer Secondary Contact)"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtFax2" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="lblContactTitle2" runat="server">Contact title</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtContactTitle2" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="Tr2" runat="server">
                            <td id="Td2" colspan="3" runat="server">
                                <p>
                                    <b>Lead Management:</b>
                                </p>
                            </td>
                        </tr>
                        <tr id="rowProviderAndProduct" runat="server">
                            <td runat="server">CRM Provider
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="ddlProviderAndProduct" runat="server" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 150px;" runat="server"></td>
                        </tr>
                        <tr id="trProviderManagement" runat="server">
                            <td runat="server">Lead Provider Management
                            </td>
                            <td runat="server">
                                <asp:Button ID="btnContinueProv" runat="server" Text="Add CRM Provider" OnClientClick="ShowManagement();return false;" />
                            </td>
                            <td runat="server"></td>
                        </tr>
                        <tr data-toggle="add-provider" style="display: none" runat="server">
                            <td style="text-align: right; padding-right: 10px;" runat="server">CRM Provider: 
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="TxtProvider" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr data-toggle="add-provider" style="display: none" runat="server">
                            <td style="text-align: right; padding-right: 10px;" runat="server">Contact Name: 
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="TxtCRMContactName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr data-toggle="add-provider" style="display: none" runat="server">
                            <td runat="server"></td>
                            <td style="text-align: right;" runat="server">
                                <asp:Button ID="btnContinueProvider" runat="server" Text="Save CRM Provider"
                                    OnClick="btnContinueprovider_Click" />
                            </td>
                        </tr>
                        <tr runat="server">
                            <td colspan="3" align="left" runat="server">
                                <asp:Label ID="lblProvider" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">
                                <asp:Label ID="LeadLabel1" runat="server" CssClass="MandatoryField">Lead Email 1*</asp:Label>
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtEmailLeads1" runat="server" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqEmailLeads1" runat="server" ErrorMessage="Please enter at least 1 Lead email address"
                                    ControlToValidate="txtEmailLeads1" Display="None" ValidationGroup="vgInfoPanel"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmailLeads1" runat="server" ErrorMessage="Lead email format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads1" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:RadioButtonList ID="rblLeadFormat1" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>


                        </tr>
                        <tr runat="server">
                            <td runat="server">Lead Email 2
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtEmailLeads2" runat="server" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmailLeads2" runat="server" ErrorMessage="Lead email 2 format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads2" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:RadioButtonList ID="rblLeadFormat2" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>


                        </tr>
                        <tr runat="server">
                            <td runat="server">Lead Email 3
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtEmailLeads3" runat="server" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmailLeads3" runat="server" ErrorMessage="Lead email 3 format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads3" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:RadioButtonList ID="rblLeadFormat3" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server">Lead Email 4
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="txtEmailLeads4" runat="server" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revEmailLeads4" runat="server" ErrorMessage="Lead email 4 format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads4" Display="None" ValidationGroup="vgInfoPanel"></asp:RegularExpressionValidator>
                            </td>
                            <td runat="server">
                                <asp:RadioButtonList ID="rblLeadFormat4" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>

                        <tr id="rowDealershipKey" runat="server">
                            <td runat="server">Dealership Key
                            </td>
                            <td colspan="3" style="padding-right: 5px;" runat="server">
                                <asp:TextBox ID="txtDealershipKey" runat="server" Width="100%" Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="rowDealertrackID" runat="server">
                            <td id="Td3" runat="server">DealerTrack Dealer ID
                            </td>
                            <td colspan="3" style="padding-right: 5px;" runat="server">
                                <asp:Label ID="lblDealertrackID" runat="server" Width="100%" Enabled="False"></asp:Label>
                            </td>
                        </tr>
                        <tr id="Tr1" runat="server">
                            <td id="Td1" colspan="4" align="center" runat="server">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveSettingsTab1" runat="server" Class="tivBtn" Text="Save Settings" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnContinue1" runat="server" Text="Continue" OnClick="NextPage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--Tab 2--%>
            <ajaxToolkit:TabPanel ID="DataPanel" runat="server" HeaderText="Value Range">
                <ContentTemplate>
                    <asp:HiddenField ID="hidDataPanelIsChanged" runat="server" Value="" />
                    <h2 class="title">Value Range</h2>
                    <table id="tblDataPanel" runat="server">
                        <tr style="display: none">
                            <td>
                                <asp:Label ID="lblDataProvider" runat="server">Data Provider</asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblDataProvider" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="true">
                                    <asp:ListItem Text="GALVES" Value="GALVES"></asp:ListItem>
                                    <asp:ListItem Text="NADA" Value="NADA"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr id="trGalvesRangeCategory" runat="server">
                            <td>Average Range Category
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td runat="server" id="Cars">Cars
                                        </td>
                                        <td valign="top" runat="server">Trucks
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="rblGalvesRangeCategory" runat="server" RepeatDirection="Vertical"
                                                Font-Bold="true" CellSpacing="0" CellPadding="0">
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rblTruckGalvesRangeCategory" runat="server" RepeatDirection="vertical"
                                                Font-Bold="true" CellSpacing="0" CellPadding="0">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label9" runat="server" Text="Enable Value range"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkEnableValueRange" runat="server" Text="Enable Value Range" AutoPostBack="true" OnCheckedChanged="chkEnableValueRange_CheckedChanged" />
                            </td>
                        </tr>
                        <tr id="trAverageValueRange" runat="server">
                            <td colspan="2">
                                <table>

                                    <tr>
                                        <td colspan="2" align="left" style="height: 50px">
                                            <asp:Label ID="lblValueRange1" runat="server" Text="Vehicles below $10,000" Font-Underline="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td runat="server" id="tdNADARange1">Custom Value Range
                                        </td>
                                        <td>
                                            <table class="lowrange">
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Bottom" runat="server" Text="Lower:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnLowPercent1" runat="server" GroupName="lower1" Text="(%)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td></td>
                                                    <td colspan="2" align="center" width="100%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Top" runat="server" Text="Higher:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnHighPercent1" runat="server" GroupName="Higher1" Text="(%)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:TextBox ID="LowSlider1" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="LowSlider_Bound1" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="LowSliderPanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="LowLabel1" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="LowSlider1" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td width="30px">To:
                                                    </td>
                                                    <td align="right" id="tdHighSlider1" runat="server">
                                                        <asp:TextBox ID="HighSlider1" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left" id="tdHighSlider_Bound1" runat="server">
                                                        <asp:TextBox ID="HighSlider_Bound1" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td width="100%">
                                                        <asp:UpdatePanel ID="highSliderPanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="HighLabel1" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="HighSlider1" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="BottomDollar1" runat="server" Text="Lower:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <td>
                                                                        <asp:RadioButton ID="rbtnLowValue1" runat="server" GroupName="lower1" Text="($)" />
                                                                    </td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td></td>
                                                    <td colspan="2" align="center" width="100%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="TopDollar1" runat="server" Text="Higher:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnHighValue1" runat="server" GroupName="Higher1" Text="($)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:TextBox ID="LowSliderDollar1" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="LowSliderDollar_bound1" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="LowDollarPanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="LowLabelDollar1" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="LowSliderDollar1" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td width="30px">To:
                                                    </td>
                                                    <td align="right" id="tdhighSliderDollar1" runat="server">
                                                        <asp:TextBox ID="HighSliderDollar1" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left" id="tdHighSliderDollar_bound1" runat="server">
                                                        <asp:TextBox ID="HighSliderDollar_bound1" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td width="100%">
                                                        <asp:UpdatePanel ID="HighDollarPanel1" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="HighLabelDollar1" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="HighSliderDollar1" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:SliderExtender ID="LowSliderExtender1" runat="server" BehaviorID="LowSlider1"
                                                TargetControlID="LowSlider1" BoundControlID="LowSlider_Bound1" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-100" Maximum="100" Length="250" TooltipText="This is the lower value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="HighSliderExtender1" runat="server" BehaviorID="HighSlider1"
                                                TargetControlID="HighSlider1" BoundControlID="HighSlider_Bound1" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-100" Maximum="100" Length="250" TooltipText="This is the Higher value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="LowSliderExtenderDollar1" runat="server" BehaviorID="LowSliderDollar1"
                                                TargetControlID="LowSliderDollar1" BoundControlID="LowSliderDollar_Bound1" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-10000" Maximum="10000" Length="250" TooltipText="This is the lower value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="HighSliderExtenderDollar1" runat="server" BehaviorID="HighSliderDollar1"
                                                TargetControlID="HighSliderDollar1" BoundControlID="HighSliderDollar_Bound1"
                                                Orientation="Horizontal" EnableHandleAnimation="true" Minimum="-10000" Maximum="10000"
                                                Length="250" TooltipText="This is the Higher value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" style="height: 50px">
                                            <asp:Label ID="lblValueRange2" runat="server" Text="Vehicles between $10,000 and $20,000"
                                                Font-Underline="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td runat="server" id="tdNADARange2">Custom Value Range
                                        </td>
                                        <td>
                                            <table class="midrange">
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label1" runat="server" Text="Lower:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnLowPercent2" runat="server" GroupName="lower2" Text="(%)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td></td>
                                                    <td colspan="2" align="center" width="100%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label2" runat="server" Text="Higher:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnHighPercent2" runat="server" GroupName="Higher2" Text="(%)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:TextBox ID="LowSlider2" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="LowSlider_Bound2" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="LowSliderPanel2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="LowLabel2" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="LowSlider2" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td width="30px">To:
                                                    </td>
                                                    <td align="right" id="tdHighSlider2" runat="server">
                                                        <asp:TextBox ID="HighSlider2" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left" id="tdHighSlider_Bound2" runat="server">
                                                        <asp:TextBox ID="HighSlider_Bound2" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td width="100%">
                                                        <asp:UpdatePanel ID="highSliderPanel2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="HighLabel2" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="HighSlider2" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="BottomDollar2" runat="server" Text="Lower:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <td>
                                                                        <asp:RadioButton ID="rbtnLowValue2" runat="server" GroupName="lower2" Text="($)" />
                                                                    </td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td></td>
                                                    <td colspan="2" align="center" width="100%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="TopDollar2" runat="server" Text="Higher:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnHighValue2" runat="server" GroupName="Higher2" Text="($)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:TextBox ID="LowSliderDollar2" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="LowSliderDollar_bound2" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="LowDollarPanel2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="LowLabelDollar2" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="LowSliderDollar2" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td width="30px">To:
                                                    </td>
                                                    <td align="right" id="tdhighSliderDollar2" runat="server">
                                                        <asp:TextBox ID="HighSliderDollar2" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left" id="tdHighSliderDollar_bound2" runat="server">
                                                        <asp:TextBox ID="HighSliderDollar_bound2" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td width="100%">
                                                        <asp:UpdatePanel ID="HighDollarPanel2" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="HighLabelDollar2" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="HighSliderDollar2" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:SliderExtender ID="LowSliderExtender2" runat="server" BehaviorID="LowSlider2"
                                                TargetControlID="LowSlider2" BoundControlID="LowSlider_Bound2" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-100" Maximum="100" Length="250" TooltipText="This is the lower value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="HighSliderExtender2" runat="server" BehaviorID="HighSlider2"
                                                TargetControlID="HighSlider2" BoundControlID="HighSlider_Bound2" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-100" Maximum="100" Length="250" TooltipText="This is the Higher value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="LowSliderExtenderDollar2" runat="server" BehaviorID="LowSliderDollar2"
                                                TargetControlID="LowSliderDollar2" BoundControlID="LowSliderDollar_Bound2" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-10000" Maximum="10000" Length="250" TooltipText="This is the lower value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="HighSliderExtenderDollar2" runat="server" BehaviorID="HighSliderDollar2"
                                                TargetControlID="HighSliderDollar2" BoundControlID="HighSliderDollar_Bound2"
                                                Orientation="Horizontal" EnableHandleAnimation="true" Minimum="-10000" Maximum="10000"
                                                Length="250" TooltipText="This is the Higher value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left" style="height: 50px">
                                            <asp:Label ID="lblValueRange3" runat="server" Text="Vehicles above $20,000" Font-Underline="true"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td runat="server" id="tdNADARange3">Custom Value Range
                                        </td>
                                        <td>
                                            <table class="highrange">
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label3" runat="server" Text="Lower:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnLowPercent3" runat="server" GroupName="lower3" Text="(%)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td></td>
                                                    <td colspan="2" align="center" width="100%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label4" runat="server" Text="Higher:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnHighPercent3" runat="server" GroupName="Higher3" Text="(%)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:TextBox ID="LowSlider3" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="LowSlider_Bound3" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="LowSliderPanel3" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="LowLabel3" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="LowSlider3" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td width="30px">To:
                                                    </td>
                                                    <td align="right" id="tdHighSlider3" runat="server">
                                                        <asp:TextBox ID="HighSlider3" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left" id="tdHighSlider_Bound3" runat="server">
                                                        <asp:TextBox ID="HighSlider_Bound3" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td width="100%">
                                                        <asp:UpdatePanel ID="highSliderPanel3" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="HighLabel3" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="HighSlider3" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="BottomDollar3" runat="server" Text="Lower:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <td>
                                                                        <asp:RadioButton ID="rbtnLowValue3" runat="server" GroupName="lower3" Text="($)" />
                                                                    </td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td></td>
                                                    <td colspan="2" align="center" width="100%">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="TopDollar3" runat="server" Text="Higher:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="rbtnHighValue3" runat="server" GroupName="Higher3" Text="($)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <asp:TextBox ID="LowSliderDollar3" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="LowSliderDollar_bound3" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="LowDollarPanel3" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="LowLabelDollar3" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="LowSliderDollar3" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                    <td width="30px">To:
                                                    </td>
                                                    <td align="right" id="tdhighSliderDollar3" runat="server">
                                                        <asp:TextBox ID="HighSliderDollar3" runat="server" Text="0" AutoPostBack="true"></asp:TextBox>
                                                    </td>
                                                    <td align="left" id="tdHighSliderDollar_bound3" runat="server">
                                                        <asp:TextBox ID="HighSliderDollar_bound3" runat="server" Width="40px"></asp:TextBox>
                                                    </td>
                                                    <td width="100%">
                                                        <asp:UpdatePanel ID="HighDollarPanel3" runat="server">
                                                            <ContentTemplate>
                                                                <asp:Label ID="HighLabelDollar3" runat="server" Text="" Style="display: none"></asp:Label>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="HighSliderDollar3" EventName="TextChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                            <ajaxToolkit:SliderExtender ID="LowSliderExtender3" runat="server" BehaviorID="LowSlider3"
                                                TargetControlID="LowSlider3" BoundControlID="LowSlider_Bound3" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-100" Maximum="100" Length="250" TooltipText="This is the lower value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="HighSliderExtender3" runat="server" BehaviorID="HighSlider3"
                                                TargetControlID="HighSlider3" BoundControlID="HighSlider_Bound3" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-100" Maximum="100" Length="250" TooltipText="This is the Higher value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="LowSliderExtenderDollar3" runat="server" BehaviorID="LowSliderDollar3"
                                                TargetControlID="LowSliderDollar3" BoundControlID="LowSliderDollar_Bound3" Orientation="Horizontal"
                                                EnableHandleAnimation="true" Minimum="-10000" Maximum="10000" Length="250" TooltipText="This is the lower value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                            <ajaxToolkit:SliderExtender ID="HighSliderExtenderDollar3" runat="server" BehaviorID="HighSliderDollar3"
                                                TargetControlID="HighSliderDollar3" BoundControlID="HighSliderDollar_Bound3"
                                                Orientation="Horizontal" EnableHandleAnimation="true" Minimum="-10000" Maximum="10000"
                                                Length="250" TooltipText="This is the Higher value used for the range calculation">
                                            </ajaxToolkit:SliderExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveSettingsTab2" runat="server" Class="tivBtn" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnContinue2" runat="server" Text="Continue" OnClick="NextPage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%-- 
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveAllTab2" runat="server" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSaveDataTab" runat="server" Text="Save This Tab" ValidationGroup="vgDataPanel"
                                                OnClick="btnSaveDataTab_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAdd2" runat="server" Text="Add Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBackTab2" runat="server" Text="Back" OnClick="Back" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        --%>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--Tab 3--%>
            <ajaxToolkit:TabPanel ID="CutomizationPanel" runat="server" HeaderText="Tool Customization">
                <ContentTemplate>
                    <h2 class="title">Tool Customization</h2>
                    <asp:HiddenField ID="hidCutomizationPanelIsChanged" runat="server" Value="" />
                    <table id="tblToolCutomization" runat="server" style="white-space: nowrap">
                        <tr>
                            <td>
                                <asp:Label ID="lblAppraisal" runat="server" Width="170px">Appraisal URL</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtAppraisalURL" runat="server" Width="700px" Height="50px" TextMode="MultiLine"
                                                MaxLength="255"></asp:TextBox>
                                        </td>
                                        <td>
                                            <input type="button" id="btnTest" value="View" class="tivBtn" onclick="ShowAppraisalURL()">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" runat="server" Width="170px">HTML code</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:RadioButtonList ID="rbtIFrameOptions" runat="server" RepeatDirection="Horizontal" onclick="ShowAppraisalCode(false);">
                                                <asp:ListItem Text="Small (SmartPhones)" Value="width:400px; height:800px;"></asp:ListItem>
                                                <asp:ListItem Text="Medium (Tablets)" Value="width:600px; height:800px;"></asp:ListItem>
                                                <asp:ListItem Text="Large (Desktop / Laptop)" Value="width:800px; height:800px;" Selected></asp:ListItem>
                                            </asp:RadioButtonList></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblHtmlCode" Style="border-style: solid; border-width: 1px; text-indent: each-line; white-space: pre-wrap" Width="700px" Height="70px" runat="server"></asp:Label>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p>
                                    <b>Customer Display Customization:</b>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblInstantOption" runat="server">Optional Equipment Pricing</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtInstantOptionPricing" runat="server" Text="When checked the optional equipment pricing will be displayed on the first page." />
                                        </td>
                                        <td style="padding-right: 45px;">
                                            <asp:CheckBox ID="chkInstantOption" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRetailPricing" runat="server">Retail Pricing</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtRetailPricing" runat="server" Text="When checked the retail pricing model is employed for the trade vehicle as oppose to the trade pricing model." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkRetailPricing" runat="server" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblEmailAppraisal" runat="server">Email Appraisal Only</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtEmaiAppraisalOnly" runat="server" Text="When checked the vehicle appraisal amount will not be displayed on final page, just emailed to user." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkEmailAppraisal" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTradeImage" runat="server">Trade Image Upload</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtTradeImageUpload" runat="server" Text="When checked the user have ability to upload their trade vehicle images on the first page." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkTradeImage" runat="server" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <!--
                                        <td>
                                            <asp:Label ID="lblFourStep" runat="server">Four Step Process</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtFourStepProcess" runat="server" Text="When checked the vehicle optional equipment selection is moved onto a separate page <br> and act as the 2nd page thus making it 4 steps instead of 3. <br>In addition the ability to select replacement vehicle is also removed." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkFourStep" runat="server" />
                                        </td>
                                          -->

                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblNewSelection" runat="server">New Vehicle Selection</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtNewSelection" runat="server" Text="When unchecked the New replacement vehicle option is removed." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkNewSelection" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTradeVideoUpload" runat="server">Trade Video Upload</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtTradeVideoUpload" runat="server" Text="When checked the user have ability to upload their trade vehicle video on the first page." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkTradeVideoUpload" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblNewVehicleIncentives" runat="server">New Vehicle Incentives</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="QuestionTip1" runat="server" Text="When checked the new vehicle incentive data will be displayed on special offer page (30.aspx)." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkNewVehicleIncentives" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCurrentPayment" runat="server">Customer Monthly Payment</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtCurrentMonthlyPayment" runat="server" Text="When checked the user have the ability to enter their trade vehicle monthly payment amount." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkCurrentPayment" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblHideTradeIn" runat="server">Replacement Vehicle</asp:Label></td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="QuestionTip3" runat="server" Text="When checked, the selection of a replacement vehicle will be enabled" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkHideTradein" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBalanceOwed" runat="server">Customer Payoff</asp:Label></td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtBalanceOwned" runat="server" Text="When checked the user have the ability to enter their trade vehicle settlement amount." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkBalanceowed" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTaxIncentive" runat="server">Show Tax savings</asp:Label></td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="QuestionTip5" runat="server" Text="When checked, the state tax message will be displayed on the summary page" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkTaxIncentive" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblHyperLead" runat="server">Hyper-Leads</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="QuestionTip4" runat="server" Text="When checked, The normal lead email sent to the dealer will include additional hyperlead information from RONSMAP" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkHyperLead" runat="server" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblUnsure" runat="server">Unsure</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtUnsure" runat="server" Text="When unchecked the Ignore option for the replacement vehicle is removed, thus forcing a replacement vehicle selection." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkUnsure" runat="server" />
                                        </td>
                                        <!--
                                        <td>
                                            <asp:Label ID="lblShowSharing" runat="server">Display Social Media</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="QuestionTip7" runat="server" Text="When checked the user will have the ability to share the GAA URL within their social networks." />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkSocialSharing" runat="server" />
                                        </td>
                                        -->
                                    </tr>
                                </table>
                            </td>
                        </tr>






                        <!-- Default Makes -->
                        <tr>
                            <td>
                                <asp:Label ID="lblDefaultMakes" runat="server" CssClass="MandatoryField">Default New Car Makes*</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:ListBox ID="lstNewMakes" runat="server" SelectionMode="Multiple" Width="200px"
                                                            Height="150px"></asp:ListBox>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnContinueMake" runat="server" ImageUrl="~/Images/Buttons/right.png"
                                                                        OnClick="AddDefaultMake" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnRemoveMake" runat="server" ImageUrl="~/Images/Buttons/left.png"
                                                                        OnClick="RemoveDefaultMake" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lstMakes" runat="server" Width="200px" Height="150px" SelectionMode="Multiple" />
                                            <asp:CustomValidator ID="cvMakes" runat="server" ControlToValidate="lstMakes" Display="None"
                                                ErrorMessage="Please select at least one default make for the dealership" ValidationGroup="vgCutomizationPanel" />
                                            <asp:CustomValidator ID="cvModels" runat="server" Display="None" ErrorMessage=""
                                                ValidationGroup="vgCutomizationPanel" />
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>
                                            <asp:Label ID="lblGoogleUrchin" runat="server">Google Urchin</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtGoogleUrchin" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblAnalyticskey" runat="server">Google Analytics Key</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAnalyticsKey" runat="server" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblAppraisalExpiration" runat="server">Appraisal Expiration(Days)</asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAppraisalExpiration" runat="server" Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- HNI Dealers -->
                        <tr style="">
                            <td>
                                <asp:Label ID="lblHNIDelers" runat="server">HNI Dealer Feeds to Use</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:ListBox ID="lstFreeHNIDealers" runat="server" SelectionMode="Multiple" Width="200px"
                                                            Height="150px"></asp:ListBox>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnContinueHNIDeler" runat="server" CausesValidation="False" ImageUrl="~/Images/Buttons/right.png"
                                                                        OnClick="AddHNIDealer" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnRemoveHNIDealer" runat="server" CausesValidation="False"
                                                                        ImageUrl="~/Images/Buttons/left.png" OnClick="RemoveHNIDealer" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lstHNIDealers" runat="server" Width="200px" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                        </td>
                                    </tr>

                                </table>


                            </td>
                        </tr>

                        <!-- Exclude Makes Enable -->
                        <tr>
                            <td>&nbsp;</td>
                            <td id="enableExcludedMakesContainer" runat="server">
                                <asp:CheckBox ID="chkEnableExcludedMakes" runat="server" OnCheckedChanged="chkEnableExcludedMakes_CheckedChanged" AutoPostBack="true" />&nbsp; Enable Virtual Inventory - Select Excludes Makes</td>
                        </tr>

                        <!-- Exclude Makes -->
                        <tr id="trExcludeMakes" runat="server" style="">
                            <td>
                                <asp:Label ID="lblExcludeMakes" runat="server">Exclude  Used Car Makes</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:ListBox ID="lstExcludeMakesL" runat="server" SelectionMode="Multiple" Width="200px"
                                                            Height="150px"></asp:ListBox>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnAddExcMake" runat="server" CausesValidation="False" ImageUrl="~/Images/Buttons/right.png"
                                                                        OnClick="AddExcMake" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnRemoveExcMake" runat="server" CausesValidation="False"
                                                                        ImageUrl="~/Images/Buttons/left.png" OnClick="RemoveExcMake" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:ListBox ID="lstExcludeMakesR" runat="server" Width="200px" Height="150px" SelectionMode="Multiple"></asp:ListBox>
                                        </td>
                                    </tr>

                                </table>


                            </td>
                        </tr>








                        <tr>
                            <td colspan="2">
                                <p>
                                    <b>Credit Applications:</b>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblCreditApp" runat="server">Credit App URL</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCreditAppUrl" runat="server" Width="450px" MaxLength="500"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreditActive" runat="server">Active</asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkCreditActive" runat="server" />
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtCreditApplicationURL" runat="server" Text="Credit Application URL" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblCreditAppText" runat="server">Credit App Text</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlCreditAppText" runat="server" Width="450px" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p>
                                    <b>Inventory Applications:</b>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblUsedInv" runat="server">Used Inventory</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtUsedinv" runat="server" Width="450px" MaxLength="500"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblUsedActive" runat="server">Active</asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkUsedActive" runat="server" />
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtUsedCarInventory" runat="server" Text="Pre-owned inventory URL" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblNewInv" runat="server">New Inventory</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtNewInv" runat="server" Width="450px" MaxLength="500"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNewActive" runat="server">Active</asp:Label>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkNewActive" runat="server" />
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtNewCarInventory" runat="server" Text="New inventory URL" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p>
                                    <b>User Interface Display:</b>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTheme" runat="server" Width="170px">Theme</asp:Label>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlThemes" runat="server" Width="100%">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="QuestionTip2" runat="server" Text="Select a theme to update the look and feel of the appraisal application." />
                                        </td>
                                        <td>
                                            <span id="themeSample"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblBackground" runat="server" Width="107px">Default Background</asp:Label>
                            </td>
                            <td style="padding: 0">
                                <table>
                                    <tr style="padding: 0">
                                        <td style="padding: 0">
                                            <asp:RadioButtonList ID="rbtnDefaultBackground" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="Gray" Value="Gray"></asp:ListItem>
                                                <asp:ListItem Text="White" Value="White"></asp:ListItem>
                                                <asp:ListItem Text="Black" Value="Black"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="QuestionTip6" runat="server" Text="With these radio buttons you can select which color will be the default for the GetAutoAppraise application" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblLang" runat="server" Width="170px">Default Language</asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblLang" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="English" Value="eng"></asp:ListItem>
                                    <asp:ListItem Text="Spanish" Value="esp"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblNadaBranding" runat="server" Width="170px">NADA Branding</asp:Label>
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rblNADABranding" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Label ID="lblComments" runat="server" Width="170px">Comments</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtComments" runat="server" Height="150" Width="450" MaxLength="500"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td>Display New Replacement Vehicle Model/Year
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtDisplayNewReplacementVehicleModelYear" runat="server"
                                                Text="The model years you select here will be made available for the user to select when choosing a new desired replacement vehicle. At least 1 of the options need to be selected." />
                                        </td>
                                        <td>
                                            <asp:Repeater ID="repNewReplacementVehicleModelYear" runat="server">
                                                <ItemTemplate>
                                                    <td width="75px">
                                                        <asp:CheckBox ID="chkModelYear" runat="server" Text='<%# Eval("Text") %>' Value='<%# Eval("Value") %>' />
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveSettingsTab3" runat="server" Class="tivBtn" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnContinue3" runat="server" Text="Continue" OnClick="NextPage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--
                        <tr>
                            <td colspan="2" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveAlltab3" runat="server" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSaveCustomizationTab" runat="server" Text="Save This Tab" ValidationGroup="vgCutomizationPanel"
                                                OnClick="btnSaveCustomizationTab_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAdd3" runat="server" Text="Add Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBackTab3" runat="server" Text="Back" OnClick="Back" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        --%>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--Tab 4--%>
            <ajaxToolkit:TabPanel ID="LeadPanel" runat="server" HeaderText="Lead Management">
                <ContentTemplate>
                    <asp:HiddenField ID="hidLeadPanelIsChanged" runat="server" Value="" />
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr id="rowProviderAndProduct_old" runat="server">
                            <td>Provider And Product
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProviderAndProduct_old" runat="server" Width="150px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 150px;"></td>
                        </tr>
                        <tr id="trProviderManagement_old" runat="server">
                            <td>Lead Provider Management
                            </td>
                            <td colspan="3" align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnAddProv" runat="server" Text="Add Provider" OnClientClick="ShowManagement();return false;" />
                                        </td>
                                        <%--                                        <td>
                                            <asp:LinkButton ID="LnkAddProv" OnClientClick="ShowManagement();return false;" runat="server">Add Provider</asp:LinkButton> 
                                        </td>--%>
                                        <%--                                        <td id="LnkAddProvider" style="vertical-align: middle; font-family: Verdana; font-size: 11px;
                                            text-decoration: underline; cursor: hand;" onclick="ShowManagement();return false;">
                                            Add Provider
                                        </td>--%>
                                        <td>
                                            <asp:Button ID="btnDeleteProvider" runat="server" Text="Delete Selected provider"
                                                OnClick="lnkDeleteProvider_Click" />
                                            <%--<asp:LinkButton ID="LnkDeleteProvider" runat="server" OnClick="lnkDeleteProvider_Click">Delete Selected Provider</asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="TextBox1" runat="server" Style="display: none"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAddProvider" runat="server" Style="display: none" Text="Add provider"
                                                OnClick="btnContinueprovider_Click" />
                                            <%--<asp:ImageButton ImageUrl="Images/Buttons/addProvider.png" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" algn="left">
                                            <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Disable User HTML Email:
                            </td>
                            <td colspan="3">
                                <asp:CheckBox ID="chkDisableLeads" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" runat="server" CssClass="MandatoryField">Lead Email 1*</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox2" runat="server" Width="150px"></asp:TextBox>
                                <%--
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter at least 1 Lead email address"
                                    ControlToValidate="txtEmailLeads1" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Lead email format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads1" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RegularExpressionValidator>
                                --%>
                            </td>
                            <td>Lead Email 2
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox3" runat="server" Width="150px"></asp:TextBox>
                                <%--
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Lead email 2 format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads2" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RegularExpressionValidator>
                                --%>
                            </td>
                        </tr>
                        <tr>
                            <td>Format
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>Format
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>Lead Email 3
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox4" runat="server" Width="150px"></asp:TextBox>
                                <%--
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Lead email 3 format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads3" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RegularExpressionValidator>
                                --%>
                            </td>
                            <td>Lead Email 4
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox5" runat="server" Width="150px"></asp:TextBox>
                                <%-- 
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Lead email 4 format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtEmailLeads4" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RegularExpressionValidator>
                                --%>
                            </td>
                        </tr>
                        <tr>
                            <td>Format
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>Format
                            </td>
                            <td>
                                <asp:RadioButtonList ID="RadioButtonList4" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="XML" Value="XML"></asp:ListItem>
                                    <asp:ListItem Text="Text" Value="Text"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr id="Tr3" runat="server">
                            <td>Dealership Key!
                            </td>
                            <td colspan="3" style="padding-right: 5px;">
                                <asp:TextBox ID="TextBox6" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="rowContactNameContactEmail" runat="server">
                            <td>Contact Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtLeadContactName" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td>Contact Email
                            </td>
                            <td>
                                <asp:TextBox ID="txtLeadContactEmail" runat="server" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revLeadContactEmail" runat="server" ErrorMessage="Lead contact email format is incorrect"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*"
                                    ControlToValidate="txtLeadContactEmail" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr id="rowContactPhoneContactFax" runat="server">
                            <td>Contact Phone
                            </td>
                            <td>
                                <asp:TextBox ID="txtLeadContactPhone" runat="server" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revLeadContactPhone" runat="server" ErrorMessage="Incorrect formatting for the lead contact phone. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtLeadContactPhone" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RegularExpressionValidator>
                            </td>
                            <td>Contact Fax
                            </td>
                            <td>
                                <asp:TextBox ID="txtLeadContactFax" runat="server" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="revLeadContactFax" runat="server" ErrorMessage="Incorrect formatting for the lead contact fax. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtLeadContactFax" Display="None" ValidationGroup="vgLeadPanel">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveSettingsTab4" runat="server" Class="tivBtn" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnContinue4" runat="server" Text="Continue" OnClick="NextPage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%-- 
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveallTab4" runat="server" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSaveLeadTab" runat="server" Text="Save This Tab" ValidationGroup="vgLeadPanel"
                                                OnClick="btnSaveLeadTab_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAdd4" runat="server" Text="Add Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnBackTab4" runat="server" Text="Back" OnClick="Back" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        --%>
                    </table>

                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <%--Tab 5--%>
            <ajaxToolkit:TabPanel ID="BillingPanel" runat="server" HeaderText="Billing">
                <ContentTemplate>
                    <asp:HiddenField ID="hidBillingPanelIsChanged" runat="server" Value="" />
                    <h2 class="title">Billing</h2>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>Billable
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtBillable" runat="server" Text="Dealership billable status can be toggled between true and false." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBillable" runat="server" AutoPostBack="true" OnCheckedChanged="BillingChanged" />
                            </td>
                            <td style="padding-left: 30px;">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>Monthly Billing Amount
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtMonthlyBillingAmount" runat="server" Text="Recurring monthly billing fee." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMonthlyBilling" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdMotivate" runat="server">Motivate
                            </td>
                            <td>
                                <asp:TextBox ID="txtReason" runat="server" Width="150" TextMode="multiLine" Rows="4"></asp:TextBox>
                            </td>
                            <td style="padding-left: 30px;">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>Additional billing information
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtAdditionalBillingInformation" runat="server"
                                                Text="Additional information related to this dealerships billing activity may be entered here." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAdditionalInformationBilling" runat="server" Width="300px" Height="50px"
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>Billing Status Last Updated
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtBillingStatusLastUpdated" runat="server"
                                                Text="Last date that the billable status was updated." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="upBillingStatusLastUpdated" runat="server">
                                    <ContentTemplate>
                                        <GAABackOfficeControls:DatePicker ID="ccBillingStatusLastUpdated" runat="server"
                                            FireDataChangedEvent="true" Enable="false" Width="150px" ImageUrl="~/Images/Calendar/cal.gif" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td style="padding-left: 30px;">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>Dealer Date Created
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtDealerDateCreated" runat="server" Text="Date this dealership was first created." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="upDealerDateCreated" runat="server">
                                    <ContentTemplate>
                                        <GAABackOfficeControls:DatePicker ID="ccDealerDateCreated" runat="server" Enable="false"
                                            FireDataChangedEvent="true" Width="150px" ImageUrl="~/Images/Calendar/cal.gif" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style="padding-left: 30px;">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>System Billing Start Date
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtSystemBillingStartDate" runat="server" Text="Automated billing date set to default to 60 days following the date this dealership was created." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="upSystemDealerStartDate" runat="server">
                                    <ContentTemplate>
                                        <GAABackOfficeControls:DatePicker ID="ccSystemDealerStartDate" runat="server" Enable="false"
                                            FireDataChangedEvent="true" Width="150px" ImageUrl="~/Images/Calendar/cal.gif" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>Setup fee
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtSetupFee" runat="server" Text="Once off setup fee associated with setup of this dealership." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:TextBox ID="txtAdminSetupFee" runat="server" Width="150"></asp:TextBox>
                            </td>
                            <td style="padding-left: 30px;">
                                <table border="0" cellpadding="0" cellspacing="3">
                                    <tr>
                                        <td>
                                            <asp:Label ID="billinglabel" runat="server" CssClass="MandatoryField">Admin Billing Start Date*</asp:Label>
                                        </td>
                                        <td>
                                            <GAABackOfficeControls:QuestionTip ID="qtAdminBillingStartDate" runat="server" Text="Date the administrator determines this dealership to become billable." />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:UpdatePanel ID="upAdminBillingStartDate" runat="server">
                                    <ContentTemplate>
                                        <GAABackOfficeControls:DatePicker ID="ccAdminBillingStartDate" runat="server" Width="150px"
                                            FireDataChangedEvent="true" ImageUrl="~/Images/Calendar/cal.gif" ValidationGroup="vgBillingPanel"
                                            ErrorMessage="Select Admin Billing Start Date" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table border="0" cellpadding="0" cellspacing="3" width="50%">
                                    <tr>
                                        <td colspan="3" align="center">
                                            <b>Dealer leads for the past 3 months</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="tdFirstMonth" runat="server" align="center"></td>
                                        <td id="tdSecondMonth" runat="server" align="center"></td>
                                        <td id="tdThirdMonth" runat="server" align="center"></td>
                                    </tr>
                                    <tr>
                                        <td id="tdFirstMonthTotal" runat="server" align="center">0
                                        </td>
                                        <td id="tdSecondMonthTotal" runat="server" align="center">0
                                        </td>
                                        <td id="tdThirdMonthTotal" runat="server" align="center">0
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveSettingsTab5" runat="server" Class="tivBtn" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnContinue5" runat="server" Text="Continue" OnClick="NextPage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveAllTab5" runat="server" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSaveBillingtab" runat="server" Text="Save This Tab" ValidationGroup="vgBillingPanel"
                                                OnClick="btnSaveBillingtab_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnAdd5" runat="server" Text="Add Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnbackTab5" runat="server" Text="Back" OnClick="Back" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        --%>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--Tab 6--%>
            <ajaxToolkit:TabPanel ID="FacebookPanel" runat="server" HeaderText="Facebook App">
                <ContentTemplate>
                    <h2 class="title">Facebook App</h2>
                    <table border="0" cellpadding="0" cellspacing="3" width="40%">
                        <tr>
                            <td>App ID:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAppID" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>API Key:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAPIKey" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>App Secret:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAppSecret" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Wall Post URL:
                            </td>
                            <td>
                                <asp:TextBox ID="txtwallpost" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveSettingsTab6" runat="server" Class="tivBtn" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnContinue6" runat="server" Text="Continue" OnClick="NextPage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--Tab 7--%>
            <ajaxToolkit:TabPanel ID="WebsitePanel" runat="server" HeaderText="Website Provider">
                <ContentTemplate>
                    <asp:HiddenField ID="hidWebsitePanelIsChanged" runat="server" Value="" />
                    <h2 class="title">Website Provider</h2>
                    <table border="0" cellpadding="0" cellspacing="3">
                        <tr>
                            <td style="width: 150px">
                                <asp:Label ID="websiteLabel1" runat="server" CssClass="MandatoryField">
                                    Website Company*</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtWebsiteCompany" runat="server" Width="150px" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqWebsiteCompany" runat="server" ErrorMessage="Enter Website Company"
                                    ControlToValidate="txtWebsiteCompany" Display="None" ValidationGroup="vgWebsitePanel">
                                </asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 150px; padding-left: 30px">
                                <asp:Label ID="websiteLabel2" runat="server" CssClass="MandatoryField">
                                    Contact Phone*</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtWebsiteContactPhone" runat="server" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqWebsiteContactPhone" runat="server" ErrorMessage="Enter Contact Phone"
                                    ControlToValidate="txtWebsiteContactPhone" Display="None" ValidationGroup="vgWebsitePanel">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revWebsiteContactPhone" runat="server" ErrorMessage="Incorrect formatting for the website contact phone. Format is xxx-xxx-xxxx or (xxx)-xxx-xxxx or xxxxxxxxxx"
                                    ValidationExpression="^([0-9]( |-)?)?(\(?[0-9]{3}\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})$"
                                    ControlToValidate="txtWebsiteContactPhone" Display="None" ValidationGroup="vgWebsitePanel">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="websiteLabel3" runat="server">
                                    Contact Name</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtWebsiteContactName" runat="server" Width="150px"></asp:TextBox>
                            </td>
                            <td style="width: 150px; padding-left: 30px">
                                <asp:Label ID="websiteLabel4" runat="server" CssClass="MandatoryField">
                                    Contact Email*</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtWebsiteContacEmail" runat="server" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqWebsiteContacEmail" runat="server" ErrorMessage="Enter Contact Email"
                                    ControlToValidate="txtWebsiteContacEmail" Display="None" ValidationGroup="vgWebsitePanel">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revWebsiteContacEmail" runat="server" ErrorMessage="Incorrect Contact Email's format"
                                    ValidationExpression="^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z];?)+$"
                                    ControlToValidate="txtWebsiteContacEmail" Display="None" ValidationGroup="vgWebsitePanel">
                                </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server">
                                    Comments:</asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtWebsiteComments" runat="server" Rows="5" MaxLength="255" Width="100%"
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveSettingsTab7" runat="server" Class="tivBtn" Text="Save Dealer" OnClick="SaveAll" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnContinue7" runat="server" Text="Add Dealer" OnClick="SaveAll" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--Tab 8--%>
            <ajaxToolkit:TabPanel ID="AuditPanel" runat="server" HeaderText="Change Log">
                <ContentTemplate>
                    <asp:HiddenField ID="HiddenField1" runat="server" Value="" />
                    <h2 class="title">Change Log</h2>
                    <table border="0" cellpadding="0" cellspacing="3" width="100%">
                        <tr>
                            <td>
                                <%--                                <asp:GridView ID="grdEditLog" runat="server" AllowPaging="true" AllowSorting="true" AlternatingRowStyle-BackColor="LightGray" AutoGenerateColumns="true" BackColor="White"
                                 HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White">
                                    <Columns>
                                        <asp:BoundField DataField="User Name" />
                                        <asp:BoundField DataField="Edit Date" />
                                        <asp:BoundField DataField="Changes Made" HtmlEncode="false"/>
                                    </Columns>
                                </asp:GridView>--%>
                                <asp:DataGrid ID="dgEditLog" runat="server" AlternatingRowStyle-BackColor="LightGray"
                                    HeaderStyle-BackColor="#CCCCCC" HeaderStyle-ForeColor="#666666" BackColor="White"
                                    AlternatingItemStyle-BackColor="#F5F5F5" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>


    </div>
    <asp:HiddenField ID="lblIsActive" runat="server" />
</asp:Content>
