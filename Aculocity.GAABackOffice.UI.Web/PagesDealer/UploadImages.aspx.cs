﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;
using Aculocity.GAABackOffice.UI.Web.Controls;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class UploadImages : BasePage
    {

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string dealerKey = Request.QueryString["dealerKey"];
                    FillGrid(dealerKey, Enumerations.Language.Eng.ToString());

                    lnkEnglish.Attributes.Add("class", "selected");
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' controls's events '
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (_currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                {
                    Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                }
                else
                {
                    Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvImages_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string dealerKey = Request.QueryString["DealerKey"];
                switch (e.CommandName)
                {
                    case "DeleteImage":
                        try
                        {
                            DealerBLL.DeleteDealerImage(dealerKey, Enum.Parse(typeof(Enumerations.Language), txtLang.Text.Trim()).ToString(), e.CommandArgument.ToString());
                        }
                        catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
                        FillGrid(dealerKey, txtLang.Text.Trim());
                        break;
                    case "DateImage":
                        try
                        {
                            DealerBLL.EnableDealerImageDate(dealerKey, txtLang.Text.Trim(), Convert.ToInt32(e.CommandArgument));
                        }
                        catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
                        FillGrid(dealerKey, txtLang.Text.Trim());
                        break;
                    case "UpdateDate":
                        try
                        {
                            var row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                            var activeDateControl = (DateTextBox)row.FindControl("txtImgActiveDate");
                            var inactiveDateControl = (DateTextBox)row.FindControl("txtInactiveDate");
                            DealerBLL.UpdateDealerImageDate(dealerKey, txtLang.Text.Trim(), Convert.ToInt32(e.CommandArgument), activeDateControl.Date, inactiveDateControl.Date);
                        }
                        catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
                        FillGrid(dealerKey, txtLang.Text.Trim());
                        break;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvImages_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    var lblNoImage = (Label)e.Row.FindControl("lblNoImage");
                    var imgDealerImage = (Image)e.Row.FindControl("imgDealerImage");
                    if (lblNoImage != null && imgDealerImage != null)
                    {
                        imgDealerImage.Visible = !DataBinder.Eval(e.Row.DataItem, "ImageID").Equals("No Image");
                        lblNoImage.Visible = DataBinder.Eval(e.Row.DataItem, "ImageID").Equals("No Image");
                    }

                    var lnkDelete = (LinkButton)e.Row.FindControl("lnkDelete");
                    lnkDelete.Visible = !DataBinder.Eval(e.Row.DataItem, "ImageID").Equals("No Image");

                    if (DataBinder.Eval(e.Row.DataItem, "ImageID").Equals("No Image"))
                    {
                        var lnkDate = (LinkButton)e.Row.FindControl("lnkDate");
                        var lnkUpdateDate = (LinkButton)e.Row.FindControl("lnkUpdateDate");
                        if (lnkDate != null) lnkDate.Visible = false;
                        if (lnkUpdateDate != null) lnkUpdateDate.Visible = false;
                    }

                    if ((DataBinder.Eval(e.Row.DataItem, "DateEnabled").ToString() != "True"))
                    {
                        var txtImgActiveDateV = (DateTextBox)e.Row.FindControl("txtImgActiveDate");
                        var txtImgInActiveDateV = (DateTextBox)e.Row.FindControl("txtInactiveDate");
                        var lblStartDate = (Label)e.Row.FindControl("lblStartDate");
                        var lblEndDate = (Label)e.Row.FindControl("lblEndDate");
                        var lnkDate = (LinkButton)e.Row.FindControl("lnkDate");
                        var lnkUpdateDate = (LinkButton)e.Row.FindControl("lnkUpdateDate");

                        if (txtImgActiveDateV != null) txtImgActiveDateV.IsVisible = false;
                        if (txtImgInActiveDateV != null) txtImgInActiveDateV.IsVisible = false;
                        if (lblStartDate != null) lblStartDate.Visible = false;
                        if (lblEndDate != null) lblEndDate.Visible = false;
                        if (lnkUpdateDate != null) lnkUpdateDate.Visible = false;
                        if (lnkDate != null) lnkDate.Text = "Enable";
                    }
                    else
                    {
                        var lnkDate = (LinkButton)e.Row.FindControl("lnkDate");
                        if (lnkDate != null)
                        {
                            lnkDate.Text = "Disable";
                        }
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void RbActiveCheckedChanged(object sender, EventArgs e)
        {
            string dealerKey = Request.QueryString["DealerKey"];
            try
            {
                var row = (GridViewRow)((CheckBox)sender).NamingContainer;
                string filename = ((Label)row.FindControl("lblFilename")).Text;

                if (filename == "No Image") filename = "-1";
                DealerBLL.SetDealerImageActive(dealerKey, txtLang.Text.Trim(), filename);
                FillGrid(dealerKey, txtLang.Text.Trim());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            finally
            {
                FillGrid(dealerKey, txtLang.Text.Trim());
            }
        }

        protected void lnk_Click(object sender, EventArgs e)
        {
            try
            {
                string dealerKey = Request.QueryString["DealerKey"];
                lnkEnglish.Attributes.Add("class", "");
                lnkSpanish.Attributes.Add("class", "");
                ((HtmlAnchor)sender).Attributes.Add("class", "selected");
                if (((HtmlAnchor)sender).ID == "lnkEnglish" && txtLang.Text.Trim() == Enumerations.Language.Esp.ToString())
                {
                    FillGrid(dealerKey, Enumerations.Language.Eng.ToString());
                }
                else if (((HtmlAnchor)sender).ID == "lnkSpanish" && txtLang.Text.Trim() == Enumerations.Language.Eng.ToString())
                {
                    FillGrid(dealerKey, Enumerations.Language.Esp.ToString());
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(fuImage.FileName.Trim()))
            {
                lblError.Text += "<li>Please specify an image.";
                return;
            }
            if (fuImage.PostedFile.ContentType.ToLower() != "image/pjpeg" && 
                fuImage.PostedFile.ContentType.ToLower() != "image/jpeg" && 
                fuImage.PostedFile.ContentType.ToLower() != "image/gif")
            {
                lblError.Text += "<li>Only .JPG, .JPEG and .GIF files are allowed. Image not uploaded.";
                return;
            }

            try
            {
                System.Drawing.Image uploadedImage = System.Drawing.Image.FromStream(fuImage.PostedFile.InputStream);
                float uploadedImageWidth = uploadedImage.PhysicalDimension.Width;

                if (uploadedImageWidth < 400 | uploadedImageWidth > 600)
                {
                    lblError.Text += "<li>The image should have a width of between 400 and 600 pixels";
                    return;
                }
            }
            catch (Exception ex)
            {
                lblError.Text += "<li>The image dimensions could not be verified. Image not uploaded.";
                ExceptionHandler.HandleException(ref ex, 0);
                return;
            }

            if (fuImage.PostedFile.ContentLength > 512000)
            {
                lblError.Text += "<li>The image exceeds 500kb. Please reduce the size and try again.";
                return;
            }

            string dealerKey = Request.QueryString["DealerKey"];

            try
            {
                int len = fuImage.PostedFile.ContentLength;
                var pic = new byte[len + 1];
                fuImage.PostedFile.InputStream.Seek(0, SeekOrigin.Begin);
                fuImage.PostedFile.InputStream.Read(pic, 0, len);

                DealerBLL.InsertDealerImage(dealerKey, txtLang.Text.Trim(), pic);

                FillGrid(dealerKey, txtLang.Text.Trim());
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        #endregion

        #region 'Custom Methods'
        protected static string GetDateStart(object dateObj)
        {
            string functionReturnValue;
            try
            {
                functionReturnValue = DateTime.Parse(dateObj.ToString()).ToString("MM/dd/yyyy");
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, 0);
                functionReturnValue = DateTime.Today.ToString("MM/dd/yyyy");
            }
            return functionReturnValue;
        }
        protected static string GetDateEnd(object dateObj)
        {
            string functionReturnValue;
            try
            {
                functionReturnValue = DateTime.Parse(dateObj.ToString()).ToString("MM/dd/yyyy");
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, 0);
                functionReturnValue = DateTime.Today.AddMonths(1).ToString("MM/dd/yyyy");
            }
            return functionReturnValue;
        }
        private void CheckUploadLimit()
        {
            try
            {
                if (gvImages.Rows.Count >= 21)
                {
                    tblUploadImage.Visible = false;
                    lblUploadLimited.Visible = true;
                }
                else
                {
                    tblUploadImage.Visible = true;
                    lblUploadLimited.Visible = false;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void FillGrid(string dealerKey, string lang)
        {
            try
            {
                gvImages.DataSource = DealerBLL.GetImages(dealerKey, lang);
                gvImages.DataBind();
                CheckUploadLimit();
                txtLang.Text = lang.Trim();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion
    }
}
