﻿<%@ Page Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true"
    CodeBehind="DraftDealers.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.DraftDealers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
    <table>
        <tr>
            <td>
                <asp:GridView ID="grdDraftDealers" runat="server" AutoGenerateColumns="False" Width="100%"
                    DataKeyNames="DealerID" AlternatingRowStyle-BackColor="LightGray" HeaderStyle-BackColor="Black"
                    HeaderStyle-ForeColor="White" OnRowCommand="grdDraftDealers_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="Dealer ID" InsertVisible="False" SortExpression="DealerID"
                            Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblDealerID" runat="server" Text='<%# Bind("DealerID") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <FooterStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dealer Name" SortExpression="DealerName">
                            <ItemTemplate>
                                <asp:Label ID="lblDealerName" runat="server" Text='<%# Bind("DealerName") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Street" SortExpression="Street">
                            <ItemTemplate>
                                <asp:Label ID="lblStreet" runat="server" Text='<%# Bind("Street") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="City" SortExpression="City">
                            <ItemTemplate>
                                <asp:Label ID="lblCity" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State" SortExpression="State">
                            <ItemTemplate>
                                <asp:Label ID="lblState" runat="server" Text='<%# Bind("State") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ZipCode" SortExpression="ZipCode">
                            <ItemTemplate>
                                <asp:Label ID="lblZipCode" runat="server" Text='<%# Bind("ZipCode") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contact Name" SortExpression="ContactName">
                            <ItemTemplate>
                                <asp:Label ID="lblContact" runat="server" Text='<%# Bind("ContactName") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
                            <ItemTemplate>
                                <asp:Label ID="lblPhone" runat="server" Text='<%# Bind("Phone") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Contact Email" SortExpression="Email">
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lead Email" SortExpression="LeadEmail">
                            <ItemTemplate>
                                <asp:Label ID="lblLeadEmail" runat="server" Text='<%# Bind("LeadEmail") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Created" SortExpression="DateCreated">
                            <ItemTemplate>
                                <asp:Label ID="lbDateCreated" runat="server" Text='<%# Bind("DateCreated") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Website Company" SortExpression="WebsiteCompany">
                            <ItemTemplate>
                                <asp:Label ID="lblWebsiteCompany" runat="server" Text='<%# Bind("WebsiteCompany") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Website Company Email" SortExpression="WebsiteEmail">
                            <ItemTemplate>
                                <asp:Label ID="lblWebsiteEmail" runat="server" Text='<%# Bind("WebsiteEmail") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Website Company Phone" SortExpression="WebsitePhone">
                            <ItemTemplate>
                                <asp:Label ID="lblWebsitePhone" runat="server" Text='<%# Bind("WebsitePhone") %>'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IsActive" InsertVisible="False" SortExpression="IsActive"
                            Visible="False">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsActive" runat="server" Checked='<%# Bind("IsActive") %>' />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <FooterStyle VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Activate">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkActivate" runat="server" CommandName="Activate" CommandArgument='<%# Bind("DealerID") %>'
                                    Text="Activate" CausesValidation="true" ForeColor="Blue" />&nbsp;
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remove">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkRemove" runat="server" CommandName="Remove" CommandArgument='<%# Bind("DealerID") %>'
                                    Text="Remove" CausesValidation="true" ForeColor="Blue" />&nbsp;
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
