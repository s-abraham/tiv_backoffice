﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class DraftDealers : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getDraftDealers();
        }

        private void getDraftDealers()
        {
            DataTable DraftDealers = DealerBLL.GetDraftAllDealers();

            grdDraftDealers.DataSource = DraftDealers;
            grdDraftDealers.DataBind();
        }

        protected void grdDraftDealers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                GridViewRow selectedRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                switch (e.CommandName)
                {
                    case "Activate":
                        Session["DraftDealerID"] = Convert.ToInt32(((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[0].FindControl("lblDealerID")).Text);
                        Session["DealerName"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[1].FindControl("lblDealerName")).Text;
                        Session["Street"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[2].FindControl("lblStreet")).Text;
                        Session["City"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[3].FindControl("lblCity")).Text;
                        Session["State"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[4].FindControl("lblState")).Text;
                        Session["ZipCode"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[5].FindControl("lblZipCode")).Text;
                        Session["Contact"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[6].FindControl("lblContact")).Text;
                        Session["Phone"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[7].FindControl("lblPhone")).Text;
                        //take out the emailto
                        String email = ((Label) grdDraftDealers.Rows[selectedRow.RowIndex].Cells[8].FindControl("lblEmail")).Text;
                        email = email.Substring(email.IndexOf(">") + 1);
                        email = email.Substring(0, email.IndexOf("<"));
                        Session["Email"] = email;
                        Session["LeadEmail"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[9].FindControl("lblLeadEmail")).Text;
                        Session["WebsiteCompany"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[11].FindControl("lblWebsiteCompany")).Text;
                        //take out the emailto
                        String WebsiteEmail = ((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[12].FindControl("lblWebsiteEmail")).Text;
                        WebsiteEmail = WebsiteEmail.Substring(WebsiteEmail.IndexOf(">") + 1);
                        WebsiteEmail = WebsiteEmail.Substring(0, WebsiteEmail.IndexOf("<"));
                        Session["WebsiteEmail"] = WebsiteEmail;
                        Session["WebsitePhone"] =((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[13].FindControl("lblWebsitePhone")).Text;
                        Session["IsDraft"] = true;
                        Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx"), false);
                        break;
                    case "Remove":
                        Int32 DealerID = Convert.ToInt32(((Label)grdDraftDealers.Rows[selectedRow.RowIndex].Cells[0].FindControl("lblDealerID")).Text);
                        DealerBLL.DeleteDraftDealer(DealerID);
                        getDraftDealers();
                        break;   
                }


            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
    }
}
