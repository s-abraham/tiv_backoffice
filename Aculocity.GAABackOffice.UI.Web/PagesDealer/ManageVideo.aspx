﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="ManageVideo.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.AddVideo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
<script language="JavaScript">
    changeclass("ctl00_liAddVideo");
</script>
    <asp:UpdatePanel ID="MainPanel" runat="server">
        <ContentTemplate>
            <table cellpadding="5">
                <tr>
                    <td colspan = "3" align="center" valign="middle">
                        <asp:Label ID="lblFeedback" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td>
                        <p><h2>Non Video Dealers:</h2></p>
                        <asp:ListBox ID="lstDealers" runat="server" Height="400px" Width="400px"></asp:ListBox>
                    </td>
                    <td align="center" valign="middle">
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/Images/Buttons/right.png" OnClick="btnAdd_Click"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/Images/Buttons/left.png" OnClick="btnDelete_Click" />
                                </td>
                            </tr>
                        </table>      
                    </td>
                    <td>
                        <p><h2>Allowed Video Dealers:</h2></p>
                        <asp:ListBox ID="lstSelectedDealers" runat="server" Height="400px" Width="400px"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td colspan = "3" align="center" valign="middle">
                    <asp:Button ID="btnBack" runat="server"  Text="Back" OnClick="btnBack_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
