﻿using System;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.DAL;
using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.Global;
using Aculocity.GAABackOffice.BLL;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;

namespace Aculocity.GAABackOffice.UI.Web
{
    public partial class Dashboard : MainBasePage
    {
        
        #region ' Dashboard Page Load'

        protected Enumerations.AuthUserGroup _currentGroup;

        protected CultureInfo provider = CultureInfo.InvariantCulture;

        protected string[] validDateFormats = new string[] { "MM/dd/yyyy", "MM/d/yyyy", "M/dd/yyyy", "M/d/yyyy" };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Code when initial loading 
                DateTime end = DateTime.Today;
                //DateTime start = new DateTime(end.Year, end.Month - 3, end.Day);
                DateTime start = end.AddMonths(-3);

                if (Request.QueryString.ToString() != "")
                {
                    try
                    {
                        start = DateTime.ParseExact(Request.QueryString.GetValues("start")[0], validDateFormats, provider, DateTimeStyles.None);
                        end = DateTime.ParseExact(Request.QueryString.GetValues("end")[0], validDateFormats, provider, DateTimeStyles.None);
                    }
                    catch (Exception ex)
                    {
                    }

                }

                if (inputStart.Text.Trim() == "")
                {
                    inputStart.Text = start.ToShortDateString();
                    inputEnd.Text = end.ToShortDateString();
                }
                InitDashboard(start, end);
            }
        }

        protected void LoadReportDates(object sender, EventArgs e)
        {
            DateTime start;
            DateTime end;


            start = DateTime.ParseExact(inputStart.Text.Trim(), validDateFormats, provider, DateTimeStyles.None);
            end = DateTime.ParseExact(inputEnd.Text.Trim(), validDateFormats, provider, DateTimeStyles.None);
            string url = Request.Url.GetLeftPart(UriPartial.Path);
            url += "?start=" + start.ToShortDateString() + "&end=" + end.ToShortDateString();

            Response.Redirect(url);
        }

        private Boolean UserHasDealerKey()
        {
            //Set Dealer Key
            var user = new UserBLL();
            user.GetUserByUserName(Security.GetLoggedInUser().Trim());
            return user.DealerKey != null;
        }

        private void InitDashboard(DateTime start, DateTime end)
        {
            try
            {
                _currentGroup = Security.GetCurrentUserGroup();
                log("Current Group:");
                log(_currentGroup);
                switch (_currentGroup)
                {
                    case Enumerations.AuthUserGroup.AllDealerAdmin:
                        DrawAdminDashboard(start, end);
                        break;
                    case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                    case Enumerations.AuthUserGroup.ResellerAdmin:
                        //Given that in the software the only options to add users are this and dealer group admin, check for dealerkey check here
                        if (UserHasDealerKey())
                        {
                            DrawDealerDashboard(start, end);
                        }
                        else
                        {
                            DrawAdminDashboard(start, end);
                        }
                        break;
                    case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                        DrawAdminDashboard(start, end);
                        break;
                    case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                        DrawDealerDashboard(start, end);
                        break;
                    case Enumerations.AuthUserGroup.GalvesFullAdmin:
                        DrawDealerDashboard(start, end);
                        break;
                    case Enumerations.AuthUserGroup.HomeNetDealerAdmin:
                        DrawDealerDashboard(start, end);
                        break;
                    case Enumerations.AuthUserGroup.HomeNetSuperUser:
                        DrawDealerDashboard(start, end);
                        break;
                    case Enumerations.AuthUserGroup.GroupDealerAdmin:
                        if (Convert.ToBoolean(Session["HomeNet"].ToString()) == true)
                        {
                            DrawDealerDashboard(start, end);
                        }
                        else
                        {
                            if (Convert.ToBoolean(Session["Galves"].ToString()) == true)
                            {
                                DrawDealerDashboard(start, end);
                            }
                            else 
                            {
                                DrawGroupAdminDashboard(start, end);
                            }
                        }
                        break;
                }
            }
            catch (Exception ex) {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); 
            }
        }

        #endregion
       
        #region ' Draw Dashboard '
        
        private void DrawAdminDashboard(DateTime start, DateTime end)
        {
            int userID = Security.GetLoggedUserID();

            lblSideTitle.Text = "Monthly Signup Data";
            lblTitleStats.Text = "Leads";
            lblStat1.Text = "Active";
            lblStat2.Text = "Nonactive";
            lblStat3.Text = "Leads";
            lblStat4.Text = "Facebook";

            lblSideStat1.Text = "Last";
            lblSideStat2.Text = "Current";
            lblSideStat3.Text = "Potential";

            chart1.Text = GetLeadsOnlyChart(start, end).ToHtmlString();
            chart2.Text = GetProductGrowthChart(start, end).ToHtmlString();
            chart3.Text = GetSalesChart(start, end).ToHtmlString();

            



            DataSet stats = ReportDAL.GetDashboardStats(start, end);

            ltrTitleStats.Text = FormatNumber(stats.Tables[2].Rows[0][0]);
            ltrStat1.Text = FormatNumber(stats.Tables[0].Rows[0][0]);
            ltrStat2.Text = FormatNumber(stats.Tables[1].Rows[0][0]);
            ltrStat3.Text = FormatNumber(stats.Tables[2].Rows[0][0]);
            ltrStat4.Text = FormatNumber(stats.Tables[5].Rows[0][0]);


        }

        private void DrawGroupAdminDashboard(DateTime start, DateTime end)
        {
            log(start);
            log(end);
            int userID = Security.GetLoggedUserID();

            lblSideTitle.Text = "Last 90 Days";
            lblTitleStats.Text = "Leads";
            lblStat1.Text = "Leads";
            lblStat2.Text = "Facebook";
            lblStat3.Text = "Leads with Photos";
            lblStat4.Text = "Leads with Video";

            lblSideStat1.Text = "Last";
            lblSideStat2.Text = "Current";
            lblSideStat3.Text = "Potential";

            chart1.Text = GetLeadStatsChart(start, end, userID).ToHtmlString();
            chart2.Text = GetTopReplacementChart(start, end, userID).ToHtmlString();
            chart3.Text = GetDesiredVehicleChart(start, end, userID).ToHtmlString();

            DataSet stats2 = ReportDAL.GetSignupStats();

            ltrSideStats1.Text = FormatNumber(stats2.Tables[0].Rows[0][0]);
            ltrSideStats2.Text = FormatNumber(stats2.Tables[1].Rows[0][0]);
            ltrSideStats3.Text = FormatNumber(stats2.Tables[2].Rows[0][0]);


            DataSet stats = ReportDAL.GetDashboardStats(start, end, userID);

            ltrTitleStats.Text = FormatNumber(stats.Tables[2].Rows[0][0]);
            ltrStat1.Text = FormatNumber(stats.Tables[2].Rows[0][0]);
            ltrStat2.Text = FormatNumber(stats.Tables[5].Rows[0][0]);
            ltrStat3.Text = FormatNumber(stats.Tables[3].Rows[0][0]);
            ltrStat4.Text = FormatNumber(stats.Tables[4].Rows[0][0]);


        }

        private void DrawDealerDashboard(DateTime start, DateTime end)
        {
            int userID = Security.GetLoggedUserID();

            lblSideTitle.Text = "Last 90 Days";
            lblTitleStats.Text = "Leads";
            lblStat1.Text = "Leads";
            lblStat2.Text = "Facebook";
            lblStat3.Text = "Leads with Photos";
            lblStat4.Text = "Leads with Video";

            lblSideStat1.Text = "Last";
            lblSideStat2.Text = "Current";
            lblSideStat3.Text = "Potential";

            chart1.Text = GetLeadStatsChart(start, end, userID).ToHtmlString();
            chart2.Text = GetTopReplacementChart(start, end, userID).ToHtmlString();
            chart3.Text = GetDesiredVehicleChart(start, end, userID).ToHtmlString();

            DataSet stats = ReportDAL.GetDashboardStats(start, end, userID);

            ltrTitleStats.Text = FormatNumber(stats.Tables[2].Rows[0][0]);
            ltrStat1.Text = FormatNumber(stats.Tables[2].Rows[0][0]);
            ltrStat2.Text = FormatNumber(stats.Tables[5].Rows[0][0]);
            ltrStat3.Text = FormatNumber(stats.Tables[3].Rows[0][0]);
            ltrStat4.Text = FormatNumber(stats.Tables[4].Rows[0][0]);

            
            DataSet stats2 = ReportDAL.GetSignupStats();

            ltrSideStats1.Text = FormatNumber(stats2.Tables[0].Rows[0][0]);
            ltrSideStats2.Text = FormatNumber(stats2.Tables[1].Rows[0][0]);
            ltrSideStats3.Text = FormatNumber(stats2.Tables[2].Rows[0][0]);


        }

        #endregion

        #region ' Utility '

        private void log(Object obj)
        {
            Debugger.Log(0, "Test", "Message: " + obj.ToString() + "\n");
        }

        private IEnumerable<DateTime> EachDay(DateTime start, DateTime end)
        {
            for (var day = start.Date; day.Date <= end.Date; day = day.AddDays(1))
                yield return day;
        }

        private Dictionary<DateTime, object> ListDays(DateTime start, DateTime end)
        {
            Dictionary<DateTime, object> days = new Dictionary<DateTime, object>();
            foreach (DateTime d in EachDay(start, end))
            {
                days[d] = 0;
            }
            return days;
        }

        private object[] GetFullDateRangeValues(DateTime start, DateTime end, DataRowCollection data)
        {
            //TimeSpan span = end - start;
            Dictionary<DateTime, object> dates = ListDays(start, end);
            DateTime key;
            foreach (DataRow row in data)
            {
                key = DateTime.ParseExact((string)row[0], "yyyyMMdd", provider);
                if (dates.ContainsKey(key))
                {
                    dates[key] = row[1];
                }
            }
            object[] values = new object[dates.Count];
            dates.Values.CopyTo(values, 0);
            return values;
        }

        protected string FormatNumber(object number)
        {
            try
            {
                int n = int.Parse(number.ToString(), NumberStyles.None, provider);
                return n.ToString("0,0", CultureInfo.InvariantCulture);
            }
            catch(Exception ex)
            {
                log(ex.ToString());
                return number.ToString();
            }
            
        }

        #endregion

        #region ' Admin Charts '

        private DotNet.Highcharts.Highcharts GetLeadsOnlyChart(DateTime start, DateTime end, int userID = -1)
        {
            //Table - Leads per days
            //Table1 - Leads with Photo
            //Table2 - Leads with Video
            //Table3 - Facebook Leads
            
            DataSet stats = ReportDAL.GetLeadStats(start, end, userID);
            object[] values = GetFullDateRangeValues(start, end, stats.Tables[0].Rows);

            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("top_stats")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Line,
                    MarginRight = 30,
                    MarginBottom = 25,
                    ClassName = "top_stats",
                    Height = 200
                })
                .SetPlotOptions(new PlotOptions
                {
                    Line = new PlotOptionsLine
                    {
                        Color = ColorTranslator.FromHtml("#FBB003")
                        //LineColor = ColorTranslator.FromHtml("#666666")
                    }
                })
                .SetLegend(new Legend
                {
                    Enabled = false
                })
                .SetTitle(new Title
                {
                    Text = ""
                })
                .SetXAxis(new XAxis
                {
                    Type = AxisTypes.Datetime
                })
                .SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "" },
                    Min = 0
                })
                .SetSeries(new Series
                {
                    Data = new Data(values),
                    Name = "Leads",
                    PlotOptionsSeries = new PlotOptionsSeries
                    {
                        PointStart = new PointStart(start),
                        PointInterval = 24 * 3600 * 1000 // one day
                    }
                });
                

            return chart;
        }

        private DotNet.Highcharts.Highcharts GetProductGrowthChart(DateTime start, DateTime end)
        {
            DataSet stats = ReportDAL.GetDealerSignupLevels(start, end);
            object[] values = new object[stats.Tables[0].Columns.Count];
            for (int index = 0; index < stats.Tables[0].Columns.Count; index++)
            {
                values[index] = new object[] { stats.Tables[0].Columns[index].ColumnName, stats.Tables[0].Rows[0].ItemArray[index] };
            }

            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("product_growth")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Column,
                    MarginRight = 20,
                    MarginBottom = 25,
                    ClassName = "product_growth",
                    Height = 220,
                    Width = 400
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Color = ColorTranslator.FromHtml("#F5831F")
                    }
                })
                .SetLegend(new Legend
                {
                    Enabled = false
                })
                .SetTitle(new Title
                {
                    Text = "Product Growth",
                    Style = "font: 'normal 14px Arial, Verdana, sans-serif', color: '#666', fontWeight: 'bold'"
                })
                .SetXAxis(new XAxis
                {
                    Categories = new[] { "Basic", "Pro", "Enterprise" }
                })
                .SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "" }
                })
                .SetSeries(new Series
                {
                    Data = new Data(values)
                });

            return chart;
        }

        private DotNet.Highcharts.Highcharts GetSalesChart(DateTime start, DateTime end)
        {
            DataSet stats = ReportDAL.GetSalesmanStats(start, end);
            object[] values = new object[stats.Tables[0].Rows.Count];
            int count = stats.Tables[0].Rows.Count;
            if (count > 20) count = 20;

            for (int index = 0; index < count; index++)
            { 
                values[index] = stats.Tables[0].Rows[index].ItemArray;
            }
            
            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("sales")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Pie,
                    MarginRight = 10,
                    MarginBottom = 25,
                    ClassName = "sales",
                    Height = 220
                })
                .SetTitle(new Title
                {
                    Text = "Sales",
                    Style = "font: 'normal 14px Arial, Verdana, sans-serif', color: '#666', fontWeight: 'bold'"
                })
                .SetSeries(new Series
                {
                    Data = new Data(values),
                    Name = "Sales"
                });

            return chart;
        }

        #endregion

        #region ' Dealer Charts '

        private DotNet.Highcharts.Highcharts GetLeadStatsChart(DateTime start, DateTime end, int userID = -1)
        {
            //Table - Leads per days
            //Table1 - Leads with Photo
            //Table2 - Leads with Video
            //Table3 - Facebook Leads
            DataSet stats = ReportDAL.GetLeadStats(start, end, userID);
            Series[] plotData = new Series[4];
            string[] plotTitles = new string[] { "Leads", "Facebook", "Leads with Photo", "Leads with Video" };
            int[] plotOrder = new int[] {0, 3, 1, 2};
            string[] colors = new string[] { "#F5831F", "#FBB033", "#ffd081", "#FBF583" };
            

            for(int idx = 0; idx < plotTitles.Length; idx++)
            {
                plotData[idx] = new Series {
                    Name = plotTitles[idx],
                    Data = new Data(GetFullDateRangeValues(start, end, stats.Tables[plotOrder[idx]].Rows)),
                    PlotOptionsSeries = new PlotOptionsSeries
                    {
                        PointStart = new PointStart(start),
                        PointInterval = 24 * 3600 * 1000 // one day
                    },
                    Color = ColorTranslator.FromHtml(colors[idx])
                };
            }
            
            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("top_stats")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Line,
                    MarginRight = 30,
                    MarginBottom = 25,
                    ClassName = "top_stats",
                    Height = 200
                })
                .SetPlotOptions(new PlotOptions
                {
                    Line = new PlotOptionsLine
                    {
                        Color = ColorTranslator.FromHtml("#FBB003")
                        //LineColor = ColorTranslator.FromHtml("#666666")
                    }

                })
                .SetLegend(new Legend
                {
                    VerticalAlign = VerticalAligns.Top,
                    Align = HorizontalAligns.Right,
                    Shadow = false,
                    Layout = Layouts.Vertical,
                    ItemStyle = "font: 'normal 10px Arial, Verdana, sans-serif', color: '#666', fontWeight: 'bold'"
                })
                .SetTitle(new Title
                {
                    Text = ""
                })
                .SetXAxis(new XAxis
                {
                    Type = AxisTypes.Datetime
                    
                })
                .SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "" },
                    Min = 0
                })
                .SetSeries(plotData);


            return chart;
        }

        private DotNet.Highcharts.Highcharts GetTopReplacementChart(DateTime start, DateTime end, int userID = -1)
        {
            DataSet stats = ReportDAL.GetTopReplacementVehicles(start, end, userID);
            object[] values = new object[stats.Tables[0].Rows.Count];
            string[] keys = new string[stats.Tables[0].Rows.Count];
            string model;
            for (int index = 0; index < stats.Tables[0].Rows.Count; index++)
            {
                //Object of Model / Count
                model = stats.Tables[0].Rows[index][2].ToString();

                values[index] = new object[] { model, stats.Tables[0].Rows[index][3] };
                keys[index] = model;
            }

            
            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("top_tradeins")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Column,
                    ClassName = "top_tradeins",
                    MarginRight = 30,
                    MarginBottom = 95,
                    Height = 330,
                    Width = 550
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Color = ColorTranslator.FromHtml("#F5831F")
                    }
                })
                .SetLegend(new Legend
                {
                    Enabled = false
                })

                .SetTitle(new Title
                {
                    Text = "Top 10 Repacement Vehicles",
                    Style = "font: 'normal 14px Arial, Verdana, sans-serif', color: '#666', fontWeight: 'bold'"
                })
                .SetXAxis(new XAxis
                {
                    Categories = keys,
                    Labels = new XAxisLabels
                    {
                        Rotation = -70,
                        Align = HorizontalAligns.Right,
                        Style = "font: 'normal 10px Arial, Verdana, sans-serif'"

                    }
                })
                .SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "" }
                })
                .SetSeries(new Series
                {
                    Data = new Data(values)
                });

            return chart;
        }

        private DotNet.Highcharts.Highcharts GetDesiredVehicleChart(DateTime start, DateTime end, int userID = -1)
        {
            DataSet stats = ReportDAL.GetDesiredVehicleNewUSedUnsure(start, end, userID);
            object[] values = new object[stats.Tables[0].Rows.Count];
            int count = stats.Tables[0].Rows.Count;
            if (count > 20) count = 20;
            string[] colors = new string[] { "#F5831F", "#FBB033", "#ffd081" };
            DotNet.Highcharts.Options.Point tmpPoint;
            for (int index = 0; index < count; index++)
            {
                tmpPoint = new DotNet.Highcharts.Options.Point();

                tmpPoint.Y = Number.GetNumber(stats.Tables[0].Rows[index][1]);
                tmpPoint.Name = stats.Tables[0].Rows[index][0].ToString();
                tmpPoint.Color = ColorTranslator.FromHtml(colors[index]);
                
                values[index] = tmpPoint;
            }

            DotNet.Highcharts.Highcharts chart = new DotNet.Highcharts.Highcharts("desired_vehicles")
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Pie,
                    PlotShadow = true,
                    ClassName = "desired_vehicles",
                    MarginRight = 30,
                    MarginBottom = 15,
                    Height = 330
                })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels { Enabled = false },
                        ShowInLegend = true
                    }
                })
                .SetLegend(new Legend {
                    VerticalAlign = VerticalAligns.Middle,
                    Align = HorizontalAligns.Right,
                    Shadow = false,
                    Layout = Layouts.Vertical
                })
                .SetTitle(new Title
                {
                    Text = "Desired Vehicle Selection",
                    Style = "font: 'normal 14px Arial, Verdana, sans-serif', color: '#666', fontWeight: 'bold'"
                })
                .SetSeries(new Series
                {
                    Data = new Data(values),
                    Name = "Total"
                });

            return chart;
        }
        #endregion
    }
}