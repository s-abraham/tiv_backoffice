﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewReplacementVehicleMode.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.NewReplacementVehicleMode" %>

<table border="0" cellpadding="0" cellspacing="3">
    <tr>
        <td>
                    
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td class="CellLabel">
                        Display New Replacement Vehicle ModelYear      
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtDisplayNewReplacementVehicleModelYear" runat="server" Text="The model years you select here will be made available for the user to select when choosing a new desired replacement vehicle. At least 1 of the options need to be selected." />                                                                                                        
                    </td>
                </tr>
            </table>
                    
        </td>
        <td>
            <table>
                <tr>
                    <asp:Repeater ID="repNewReplacementVehicleModelYear" runat="server">
                        <ItemTemplate>
                            <td width="75px">
                                <asp:CheckBox ID="chkModelYear" runat="server" Text='<%# Eval("Text") %>' Value='<%# Eval("Value") %>' />
                            </td>
                        </ItemTemplate>
                     </asp:Repeater>
                </tr>
            </table>
        </td>
    </tr>
</table>