﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using System.ComponentModel;

using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.DAL;


namespace Aculocity.GAABackOffice.UI.Web {
   
    public partial class NewReplacementVehicleMode : UserControl, IValue {

        public bool Enabled {
            get;
            set;
        }

        public bool AddMode {
            get;
            set;
        }

        [Browsable(false)]
        public object Value {
            
            get {
                return GetValues();
            }

            set {
                if (value == null)
                    return;

                SetValues(value.ToString());
            }

        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);

            Page.PreRender += new EventHandler(Page_PreRender);

            LoadData();
        
        }

      

      
        void Page_PreRender(object sender, EventArgs e) {
            foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    (item.FindControl("chkModelYear") as CheckBox).Enabled = Enabled;

            if (AddMode) {
                if (repNewReplacementVehicleModelYear.Items.Count > 0) {
                    (repNewReplacementVehicleModelYear.Items[repNewReplacementVehicleModelYear.Items.Count - 1].FindControl("chkModelYear") as CheckBox).Checked = true;
                }

                if (repNewReplacementVehicleModelYear.Items.Count > 1) {
                    (repNewReplacementVehicleModelYear.Items[repNewReplacementVehicleModelYear.Items.Count - 2].FindControl("chkModelYear") as CheckBox).Checked = true;
                }
            }

        }  

        void LoadData() {
            if (repNewReplacementVehicleModelYear.DataSource != null)
                return;

            repNewReplacementVehicleModelYear.DataSource = DealerCommands.GetAllNewReplacementVehicleForModelYear((Page as PageBase).DataManager).ExecuteDataTable();
            repNewReplacementVehicleModelYear.DataBind();
        }

        string GetValues() {
            
            StringBuilder result = new StringBuilder();

            CheckBox chkModelYear = null; 
            foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items) 
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    if ((chkModelYear = item.FindControl("chkModelYear") as CheckBox).Checked) 
                        result.AppendFormat("{0},", chkModelYear.Text);
                        
           
            return result.ToString();
        
        }

        void SetValues(string values) {
            string[] currentValues = values.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);            
            CheckBox chkModelYear = null;
            foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem) {
                    (chkModelYear = item.FindControl("chkModelYear") as CheckBox).Checked = Array.Exists<string>(currentValues, currentItem => currentItem.Equals(chkModelYear.Text));
                }
        }

        public NewReplacementVehicleMode() {
            Enabled = true;
        }
        
    }
}