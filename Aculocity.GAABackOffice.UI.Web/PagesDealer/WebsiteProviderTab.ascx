﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WebsiteProviderTab.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.WebsiteProviderTab" %>

<table border="0" cellpadding="0" cellspacing="3">
    <tr>
        <td class="CellLabel" style="width:150px" >
            Website Company
        </td>
        <td>
            <asp:TextBox ID="txtWebsiteCompany" runat="server" Width="150px" MaxLength="100"></asp:TextBox>
        </td>
        <td class="CellLabel" style="width:150px;padding-left:30px">
            Contact Phone
           <%-- <asp:RegularExpressionValidator ID="valRegWebsiteContactPhone" runat="server" ControlToValidate="txtContactPhone"
                                                Display="Dynamic" ErrorMessage="Please enter a valid Website Company Contact Phone number. e.g. XXX-XXX-XXXX"
                                                ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$"
                                                ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="valReqWebsiteContactPhone"
                                                    runat="server" ControlToValidate="txtWebContactPhone" Display="Dynamic" ErrorMessage="Please enter a Website Company Contact Phone number."
                                                    ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtContactPhone" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="CellLabel">
            Contact Name
        </td>
        <td>
            <asp:TextBox ID="txtContactName" runat="server" Width="150px"></asp:TextBox>
        </td>
        <td class="CellLabel" style="width:150px;padding-left:30px">
            Contact Email
<%--            <asp:RegularExpressionValidator ID="valRegWebsiteContactEmail" runat="server" ControlToValidate="txtContactEmail"
                                                Display="Dynamic" ErrorMessage="Please enter a valid Website Company Company Contact E-mail address."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="valReqWebsiteContactEmail" runat="server" ControlToValidate="txtWebContactEmail"
                                                    Display="Dynamic" ErrorMessage="Please enter a Website Contact E-mail address."
                                                    Enabled="False" ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtContactEmail" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
</table>