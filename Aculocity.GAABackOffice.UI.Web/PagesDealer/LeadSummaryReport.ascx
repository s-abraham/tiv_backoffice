﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeadSummaryReport.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.LeadSummaryReport" %>

<script type="text/javascript" src="../JScripts/wz_tooltip.js"></script>
<script type="text/javascript" src="../JScripts/FusionCharts.js"></script>

<table border="0" cellpadding="0" cellspacing="3">
    <tr>
        <td>
            <GAABackOfficeControls:LeadReportCharts ID="lrcCharts" runat="server" />
        </td>
   </tr>
   <tr>
        <td>
            <GAABackOfficeControls:LeadReportDetailMainGrid ID="lrdmgGrid" runat="server" />
        </td>
    </tr>
</table>