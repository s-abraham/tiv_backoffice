﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;


namespace Aculocity.GAABackOffice.UI.Web
{

    public partial class DealerInfoTab : EntityEditorUserControlBase
    {

        bool dealerGroupHasValue = false;
        bool isSpecificDealer = false;
        bool isGroupDealerAdmin = false;

        private string _dealerkey = string.Empty;
        Enumerations.AuthUserGroup _currentGroup;

        public override Pair[] ControlFieldMappings
        {
            get
            {

                return new Pair[] { 
          
                    new Pair("DealerName", txtDealerName),
                    new Pair("Active", cbxActive),
                    new Pair("Salesperson", txtSalesPerson),
                    new Pair("OriginalSalesPerson", txtOriginalSalesPerson),
                    new Pair("DealerGroup_Id", ddlDealerGroup),
                    new Pair("DealerStreet", txtStreet),
                    new Pair("DealerCity", txtCity),
                    new Pair("DealerState", txtState),
                    new Pair("DealerZip", txtZip),
                    new Pair("DealerAutoGroup", txtAutomotiveGroup),  
                    new Pair("DealerContactName", txtContactName),
                    new Pair("DealerContactEmail", txtContactEmail),
                    new Pair("DealerContactPhone", txtContactPhone),
                    new Pair("AppointmentEmail", txtAppointmentEmail),
                    new Pair("DealerContactPhone2", txtContactPhone2),
                    new Pair("DealerContactFax", txtContactFax),
                    new Pair("DealerTitle", txtContactTitle),
                    new Pair("SecondDealerContactName", txtSecondContactName),
                    new Pair("SecondDealerContactEmail", txtSecondContactEMail),
                    new Pair("SecondDealerContactPhone", txtSecondContactPhone),
                    new Pair("SecondDealerContactPhone2", txtSecondContactPhone2),
                    new Pair("SecondDealerContactFax", txtSecondContactFax),
                    new Pair("SecondDealerTitle", txtSecondDealerTitle), 
                    new Pair("DealerWebsite", txtAppraisalURL),  
                    new Pair("CreditApp", txtCreditApplicationUrl),
                    new Pair("CreditEnabled", chkCreditApplicationActive),
                    new Pair("CreditAppText_Id", ddlCreditApplicationText),
                    new Pair("InvUsed", txtUsedCarInventory),
                    new Pair("InvUsedEnabled", chkUsedCarInventory),
                    new Pair("InvNew", txtNewCarInventory),
                    new Pair("InvNewEnabled", chkNewCarInventory),
                    new Pair("DealerPublicSite", txtPublicWebSite),
                    new Pair("InstantOptionPricing", chkInstantOptionPricing),
                    new Pair("EmailAppraisal", chkEmailAppraisalOnly),
                    new Pair("FourStep", chkFourStepProcess),
                    new Pair("NewSelection", chkNewSelection),
                    new Pair("ShowAmountOwed", chkBalanceOwned),
                    new Pair("ShowIncentives", chkNewVehicleIncentives),
                    new Pair("ShowMonthlyPayment", chkCurrentMonthlyPayment),
                    new Pair("UnsureOption", chkUnsure),
                    new Pair("RetailPricing", chkRetailPricing),
                    new Pair("TradeImageUpload", chkTradeImageUpload),
                    new Pair("GoogleUrchin", txtUrchin),
                    new Pair("AppraisalExpiration", ddlAppraisalExpiration),
                    new Pair("Comments", txtComments),
                    new Pair("GalvesRangeCategory_Id", rblGalvesRangeCategory),
                    new Pair("TruckGalvesRangeCategory_Id", rblTruckGalvesRangeCategory),
                    new Pair("Language", tvcLanguages),
                    new Pair("NADABranding", tvcNADABRanding),
                    new Pair("DataProvider", tvcDataProvider),
                    new Pair("DealerModelYears_NewModelYears", nrvmReplacementVehicleMode),
                    new Pair("Theme", ddlThemes),
                    new Pair("OperatingHours",txtOperatingHours),
                    new Pair("HideCondition", chkHideVehicleCondition),
                    new Pair("HideEmailTradeIn", chkHideEmailTradeIn),
                    new Pair("EmailTitle", txtEmailTitle),
                    new Pair("HideSpecialOffer", chkHideSpecialOffer),
                    new Pair("HideAddress", chkHideAddress)
                    
                };
            }
        }

        /*        public override ControlExtender[] AdditionalControlFieldExtenders {
                    get {
                        return new ControlExtender[] { 
                            new SelectedListBoxSimpleStringCE(slbDealershipDefaultMakes, "DealerDefaultMakes"),
                            new SelectedListBoxSimpleStringCE(slbDealershipHNIMappings, "HNIDealers")
                        };

                    }
                }
        */
        public override string[] AdditionalFields
        {
            get
            {
                return new string[]{
                    "DealerType_Id",
                    "AverageValueRange_VUnderPercent", "AverageValueRange_VUnderAmount",
                    "AverageValueRange_VOverPercent", "AverageValueRange_VOverAmount", "AverageValueRange_VUnderTypeSelection",
                    "AverageValueRange_VOverTypeSelection"
                };
            }
        }

        public override EntityValidator[] EntityValidators
        {
            get
            {
                return new EntityValidator[]{
                   
                    new EntityNonEmptyFieldValidator("DealerName", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Dealer Name."},
                    new EntityNonEmptyFieldValidator("Salesperson", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Salesperson."}, 
                    new EntityNonEmptyFieldValidator("OriginalSalesPerson" , AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Original Salesperson."},
                    new EntityNonEmptyFieldValidator("DealerState", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Dealer State."},
                    new EntityNonEmptyFieldValidator("DealerContactEmail", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Dealer Contact E-Mail."},
                    new EntityMailAddressValidator("DealerContactEmail", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a valid Dealer Contact E-Mail."},
                    new EntityNonEmptyFieldValidator("DealerContactPhone", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Dealer Contact Phone number."},
                    new EntityPhoneValidator("DealerContactPhone", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a valid Dealer Contact Phone number. e.g. XXX-XXX-XXXX"},
                    new EntityNonEmptyFieldValidator("AppointmentEmail", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Appointment E-Mail."},
                    new EntityMailAddressValidator("AppointmentEmail", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a valid Appointment E-Mail."},
                    new EntityPhoneValidator("DealerContactFax", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a valid Dealer Contact Fax number. e.g. XXX-XXX-XXXX"},
                    new EntityNonEmptyFieldValidator("DealerZip", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a Dealer Zip Code."},
                    new EntityZipCodeValidator("DealerZip", AdditionalConditionIsSpecDealer) {ErrorMessage = "Please enter a valid Dealer Zip Code."},
                    new EntityMailAddressValidator("SecondDealerContactEmail") {ErrorMessage = "Please enter a valid Second Dealer Contact E-Mail."},
                    new EntityPhoneValidator("SecondDealerContactPhone") {ErrorMessage = "Please enter a valid Second Dealer Contact Phone number. e.g. XXX-XXX-XXXX"},
                    new EntityPhoneValidator("SecondDealerContactFax")  {ErrorMessage = "Please enter a valid Second Dealer Contact Fax number. e.g. XXX-XXX-XXXX"},
                    new GenericEntityValidator(ValidateCreditApplicationNonEmpty){ErrorMessage = "Credit Application URL required if active."},
                    new GenericEntityValidator(InvUsedNonEmpty) {ErrorMessage = "Used Car Inventory URL required if active."},
                    new GenericEntityValidator(InvNewNonEmpty) {ErrorMessage = "New Car Inventory URL required if active."},
                    new GenericEntityValidator(ValidateCustomerMakes) {ErrorMessage = "Please select at least one default make."},
                    new GenericEntityValidator(ValidateDealershipHNIMappings) { ErrorMessage="Please select at least one HNI Dealer."},
                    new GenericEntityValidator(ValidateModelYear) {ErrorMessage = "Select 1 model year at least"}
                
                };
            }
        }

        #region Validators

        bool AdditionalConditionIsSpecDealer(EntityValidator validator, SimpleData.Entity entity)
        {
            GetCurrentGroup();
            return !isSpecificDealer;
        }

        bool ValidateCreditApplicationNonEmpty(EntityValidator validator, SimpleData.Entity entity)
        {
            Dealer dealer = Entity as Dealer;

            GetCurrentGroup();

            return isSpecificDealer || !dealer.CreditEnabled || !string.IsNullOrEmpty(dealer.CreditApp);
        }

        bool InvUsedNonEmpty(EntityValidator validator, SimpleData.Entity entity)
        {
            Dealer dealer = Entity as Dealer;

            GetCurrentGroup();

            return (isSpecificDealer && !isGroupDealerAdmin) || !dealer.InvUsedEnabled || !string.IsNullOrEmpty(dealer.InvUsed);
        }

        bool InvNewNonEmpty(EntityValidator validator, SimpleData.Entity entity)
        {
            Dealer dealer = Entity as Dealer;

            GetCurrentGroup();

            return (isSpecificDealer && !isGroupDealerAdmin) || !dealer.InvNewEnabled || !string.IsNullOrEmpty(dealer.InvNew);
        }

        bool ValidateCustomerMakes(EntityValidator validator, Aculocity.SimpleData.Entity entity)
        {
            return !string.IsNullOrEmpty((entity as Dealer).DealerDefaultMakes);
        }

        bool ValidateDealershipHNIMappings(EntityValidator validator, Aculocity.SimpleData.Entity entity)
        {

            GetCurrentGroup();

            return (_currentGroup == Enumerations.AuthUserGroup.BSBConsultingFullAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.GalvesFullAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.AllDealerAdmin) || !string.IsNullOrEmpty((entity as Dealer).HNIDealers);

        }

        bool ValidateModelYear(EntityValidator validator, Aculocity.SimpleData.Entity entity)
        {
            return !string.IsNullOrEmpty((entity as Dealer).DealerModelYears.NewModelYears);
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Page.PreRender += Page_PreRender;
            Page.Load += Page_Load;
            Page.InitComplete += Page_InitComplete;
            Page.LoadComplete += Page_LoadComplete;
            lnkShowSecondContact.Click += lnkShowSecondContact_Click;

            tvcNADABRanding.ValueFirst = true;
            tvcNADABRanding.ValueSecond = false;

            tvcDataProvider.ValueFirst = "GALVES";
            tvcDataProvider.ValueSecond = "NADA";

            tvcLanguages.ValueFirst = "eng";
            tvcLanguages.ValueSecond = "esp";

            _dealerkey = Request.QueryString["DealerKey"];
            _currentGroup = Security.GetCurrentUserGroup();
        }

        void Page_LoadComplete(object sender, EventArgs e)
        {
        }

        void Page_InitComplete(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadDealershipDefaultMakes();
                LoadDealershipHNIMappings();
            }

            LoadDealerGroups();
            LoadCreditAppTexts();
            LoadAppraisalExpirationInDays();
            LoadThemes();


            LoadCarGalvesRangeCategories();
            LoadTruckGalvesRangeCategories();

        }


        void lnkShowSecondContact_Click(object sender, EventArgs e)
        {
            lnkShowSecondContact.Text = (rowSecondContact.Visible = !rowSecondContact.Visible) ? "Hide Second Contact" : "Show Second Contact";
        }

        void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                rowSecondContact.Visible = false;

                if (string.IsNullOrEmpty(_dealerkey))
                {
                    if (_currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                        _currentGroup == Enumerations.AuthUserGroup.GalvesFullAdmin)
                    {
                        tvcDataProvider.Value = Enumerations.DataProvider.Galves.ToString().ToUpper();
                    }
                    else if (_currentGroup == Enumerations.AuthUserGroup.AllDealerAdmin ||
                            _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                            _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                            _currentGroup == Enumerations.AuthUserGroup.BSBConsultingFullAdmin ||
                            _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin ||
                            _currentGroup == Enumerations.AuthUserGroup.HomeNetSuperUser)
                    {
                        tvcDataProvider.Value = Enumerations.DataProvider.Nada.ToString().ToUpper();
                    }

                    ((MainEditorHolder)Page).UpdateButton.Text = "Add Dealer";
                }
            }

            RegisterClientScript();

            nrvmReplacementVehicleMode.AddMode = TabHolderEditorBase.AddMode;

            Dealer dealer = Entity as Dealer;


            if ("NADA".Equals(tvcDataProvider.Value))
            {

                //rowCustomValueRange.Style["display"] = "inline";
                //rowAverageRangeCategoryLabel.Style["display"] = "none";
                //rowAverageRangeCategory.Style["display"] = "none";

                rowCustomValueRange.Visible = true;
                rowAverageRangeCategoryLabel.Visible = false;
                rowAverageRangeCategory.Visible = false;

                dealer.DealerType.Id = _currentGroup != Enumerations.AuthUserGroup.HomeNetSuperUser &&
                                       _currentGroup != Enumerations.AuthUserGroup.HomeNetDealerAdmin ? 6 : 4;

            }
            else
            {

                //rowCustomValueRange.Style["display"] = "none";
                //rowAverageRangeCategoryLabel.Style["display"] = "inline";
                //rowAverageRangeCategory.Style["display"] = "inline";

                rowCustomValueRange.Visible = false;
                rowAverageRangeCategoryLabel.Visible = true;
                rowAverageRangeCategory.Visible = true;

                dealer.DealerType.Id = _currentGroup != Enumerations.AuthUserGroup.HomeNetSuperUser &&
                                       _currentGroup != Enumerations.AuthUserGroup.HomeNetDealerAdmin ? 5 : 4;

            }
        }


        void Page_PreRender(object sender, EventArgs e)
        {
            ApplySecurity();
        }

        public override void OnAfterDataBind()
        {
            base.OnAfterDataBind();
            BindCustomRangeData();
        }

        public override void OnAfterCollectData()
        {
            base.OnAfterCollectData();
            CollectCustomerRangeData();

            Dealer dealer = Entity as Dealer;

            dealer.DealerWebsite = ConvertStringToHttpSuffix(dealer.DealerWebsite);
            dealer.CreditApp = ConvertStringToHttpSuffix(dealer.CreditApp);
            dealer.InvUsed = ConvertStringToHttpSuffix(dealer.InvUsed);
            dealer.InvNew = ConvertStringToHttpSuffix(dealer.InvNew);
            dealer.DealerPublicSite = ConvertStringToHttpSuffix(dealer.DealerPublicSite);
        }

        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            BindCustomRangeData();
        }

        void LoadDealerGroups()
        {

            //ddlDealerGroup.DataSource = DealerCommands.GetAllDealerGroups(DataManager, Security.UserIsInGroup(Enumerations.AuthUserGroup.HomeNetDealerAdmin) || Security.UserIsInGroup(Enumerations.AuthUserGroup.HomeNetSuperUser)).ExecuteDataTable();
            if (_currentGroup != Enumerations.AuthUserGroup.GroupDealerAdmin)
            {
                ddlDealerGroup.DataSource = DealerCommands.GetAllDealerGroups(DataManager, _currentGroup.ToString()).ExecuteDataTable();
                ddlDealerGroup.DataValueField = "Id";
                ddlDealerGroup.DataTextField = "Description";
                ddlDealerGroup.DataBind();
                ddlDealerGroup.Items.Insert(0, new ListItem("--GROUP--", "0"));
            }
            else
            {
                var dealerGroup = new DealerGroupBLL();
                dealerGroup.GetDealerGroupByID(Security.GetCurrentUserDealerGroupID());
                ddlDealerGroup.Items.Insert(0, new ListItem(dealerGroup.Description, dealerGroup.ID.ToString()));
            }

        }

        void LoadCreditAppTexts()
        {

            ddlCreditApplicationText.DataSource = DealerCommands.GetAllCreditAppTexts(DataManager).ExecuteDataTable();
            ddlCreditApplicationText.DataValueField = "MsgID";
            ddlCreditApplicationText.DataTextField = "Description";
            ddlCreditApplicationText.DataBind();

        }

        void LoadDealershipDefaultMakes()
        {

            Dealer dealer = Entity as Dealer;

            slbDealershipDefaultMakes.CommonDataSource = DealerCommands.GetAllDefaultMakesForDealer(DataManager, dealer.DealerKey).ExecuteDataTable();
            slbDealershipDefaultMakes.CommonDataSourceValueField = "Make";
            slbDealershipDefaultMakes.CommonDataSourceTextField = "Make";

        }

        void LoadDealershipHNIMappings()
        {

            Dealer dealer = Entity as Dealer;

            slbDealershipHNIMappings.CommonDataSource = DealerCommands.GetAllDealershipHNIMappingsForDealer(DataManager, dealer.DealerKey).ExecuteDataTable();
            slbDealershipHNIMappings.CommonDataSourceValueField = "Dealer_ID";
            slbDealershipHNIMappings.CommonDataSourceTextField = "Dealer_ID";
        }

        void LoadAppraisalExpirationInDays()
        {
            for (int i = 1; i < 31; i++)
                ddlAppraisalExpiration.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        void LoadThemes()
        {

            ddlThemes.Items.Add(new ListItem("Default", "Default"));
            ddlThemes.Items.Add(new ListItem("BlueGray", "BlueGray"));
            ddlThemes.Items.Add(new ListItem("YellowGray", "YellowGray"));
            ddlThemes.Items.Add(new ListItem("YellowGrayBlackBg", "YellowGrayBlackBg"));
            ddlThemes.Items.Add(new ListItem("YellowGrayGrayBg", "YellowGrayGrayBg"));
            ddlThemes.Items.Add(new ListItem("GreenGray", "GreenGray"));
            ddlThemes.Items.Add(new ListItem("DarkGray", "DarkGray"));

        }

        void LoadCarGalvesRangeCategories()
        {
            rblGalvesRangeCategory.DataSource = DealerCommands.GetCarsRangeCategories(DataManager).ExecuteDataTable();
            rblGalvesRangeCategory.DataValueField = "Category";
            rblGalvesRangeCategory.DataTextField = "Description";
            rblGalvesRangeCategory.DataBind();
        }

        void LoadTruckGalvesRangeCategories()
        {
            rblTruckGalvesRangeCategory.DataSource = DealerCommands.GetTrucksRangeCategories(DataManager).ExecuteDataTable();
            rblTruckGalvesRangeCategory.DataValueField = "Category";
            rblTruckGalvesRangeCategory.DataTextField = "Description";
            rblTruckGalvesRangeCategory.DataBind();
        }

        void ApplySecurity()
        {

            Dealer dealer = Entity as Dealer;

            bool isAllowedCompanyDealer = UtilityFunctions.CheckIsAllowedCompanyDealer(DataManager.Clone());

            tdHideConditionLabel.Visible = tdHideCondition.Visible = isAllowedCompanyDealer;
            trEmailTitle.Visible = isAllowedCompanyDealer;
            trOperatingHours.Visible = isAllowedCompanyDealer;
            tdHideEmailTradeInLabel.Visible = tdHideEmailTradeIn.Visible = isAllowedCompanyDealer;
            tdHideSpecialOfferLabel.Visible = tdHideSpecialOffer.Visible = isAllowedCompanyDealer;
            tdHideAddressLabel.Visible = tdHideAddress.Visible = isAllowedCompanyDealer;

            GetCurrentGroup();

            switch (_currentGroup)
            {
                case Enumerations.AuthUserGroup.GalvesFullAdmin:
                    tvcDataProvider.SecondControl.Visible = false;
                    if (dealer != null) dealer.DataProvider = "GALVES";
                    break;

                case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                    tvcDataProvider.FirstControl.Visible = false;
                    if (dealer != null) dealer.DataProvider = "NADA";
                    break;

                case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                    tvcDataProvider.SecondControl.Visible = false;
                    if (dealer != null) dealer.DataProvider = string.Empty;
                    break;

                case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                case Enumerations.AuthUserGroup.ResellerAdmin:
                    tvcDataProvider.FirstControl.Visible = true;
                    if (dealer != null) dealer.DataProvider = string.Empty;
                    break;

                case Enumerations.AuthUserGroup.GroupDealerAdmin:
                    if (Security.UserIsInSubGroupDealerAdmin(Enumerations.GroupDealerAdmin.Galves))
                    {
                        tvcDataProvider.SecondControl.Visible = false;
                        if (dealer != null) dealer.DataProvider = string.Empty;
                    }
                    else if (Security.UserIsInSubGroupDealerAdmin(Enumerations.GroupDealerAdmin.BSB))
                    {
                        tvcDataProvider.FirstControl.Visible = false;
                        if (dealer != null) dealer.DataProvider = string.Empty;
                    }
                    break;
            }

            tvcDataProvider.Enabled = trGoodleUrchin.Visible = (
                (_currentGroup != Enumerations.AuthUserGroup.BSBConsultingDealerAdmin)
                && (_currentGroup != Enumerations.AuthUserGroup.ResellerAdmin)
            );


            if (!(_currentGroup == Enumerations.AuthUserGroup.GalvesFullAdmin ||
                  _currentGroup == Enumerations.AuthUserGroup.BSBConsultingFullAdmin ||
                  _currentGroup == Enumerations.AuthUserGroup.HomeNetSuperUser ||
                  _currentGroup == Enumerations.AuthUserGroup.AllDealerAdmin
                ))
            {

                rowCommentsLabel.Visible = rowComments.Visible = false;
                rowDealerGroupLabel.InnerHtml = string.Empty;
                ddlDealerGroup.Visible = false;

            }

            //display the HNI list box
            switch (dealer.DataProvider)
            {
                case "NADA":
                    cellDealerHNIMappingsLabel.InnerText = "Dealership HNI Mappings";
                    slbDealershipHNIMappings.Visible = true;
                    break;

                case "GALVES":
                    cellDealerHNIMappingsLabel.InnerText = string.Empty;
                    slbDealershipHNIMappings.Visible = false;
                    break;
            }

            //if (_currentGroup == Enumerations.AuthUserGroup.GalvesFullAdmin || _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin)
            //{
            //    cellDealerHNIMappingsLabel.InnerText = string.Empty;
            //    slbDealershipHNIMappings.Visible = false;
            //}
            //else if (_currentGroup == Enumerations.AuthUserGroup.BSBConsultingFullAdmin || _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin)
            //{
            //    cellDealerHNIMappingsLabel.InnerText = "Dealership HNI Mappings";
            //    slbDealershipHNIMappings.Visible = true;
            //}

            //Trade image eupload
            if (_currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.GalvesFullAdmin)
            {
                cellTradeImageUploadLabel.Visible = false;
                cellTradeImageUpload.Visible = false;
                rowNADABranding.Visible = false;
            }
            //else if (_currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin) {
            //    slbDealershipHNIMappings.Enabled = false;
            //}

            if (isSpecificDealer)
            {
                SetSecurityForSpecificDealer();
            }

            if (isGroupDealerAdmin)
            {
                SetSecurityForGroupAdmin();
            }


        }

        void BindCustomRangeData()
        {

            var dealer = Entity as Dealer;
            if (dealer != null)
            {
                sliderLowerPercent.Value = dealer.AverageValueRange.VUnderPercent;
                sliderHigherPercent.Value = dealer.AverageValueRange.VOverPercent;

                sliderLowerAmount.Maximum = 10000;
                sliderLowerAmount.Minimum = -10000;
                sliderLowerAmount.Value = Convert.ToInt32(dealer.AverageValueRange.VUnderAmount);

                sliderHigherAmount.Maximum = 10000;
                sliderHigherAmount.Minimum = -10000;
                sliderHigherAmount.Value = Convert.ToInt32(dealer.AverageValueRange.VOverAmount);

                rbLowerAmount.Checked = !(rbLowerPercent.Checked = dealer.AverageValueRange.VUnderTypeSelection.Equals("%"));
                rbHigherAmount.Checked = !(rbHigherPercent.Checked = dealer.AverageValueRange.VOverTypeSelection.Equals("%"));
            }
        }

        void CollectCustomerRangeData()
        {

            Dealer dealer = Entity as Dealer;

            dealer.AverageValueRange.VUnderPercent = sliderLowerPercent.Value;
            dealer.AverageValueRange.VOverPercent = sliderHigherPercent.Value;
            dealer.AverageValueRange.VUnderAmount = Convert.ToDecimal(sliderLowerAmount.Value);
            dealer.AverageValueRange.VOverAmount = Convert.ToDecimal(sliderHigherAmount.Value);

            dealer.AverageValueRange.VUnderTypeSelection = rbLowerPercent.Checked ? "%" : "$";
            dealer.AverageValueRange.VOverTypeSelection = rbHigherPercent.Checked ? "%" : "$";
        }

        void SetSecurityForSpecificDealer()
        {

            //disable all fields
            txtDealerName.Enabled = false;
            txtAppointmentEmail.Enabled = false;
            txtAppraisalURL.Enabled = false;
            txtAutomotiveGroup.Enabled = false;
            txtCity.Enabled = false;
            txtComments.Enabled = false;
            txtContactEmail.Enabled = false;
            txtContactFax.Enabled = false;
            txtContactName.Enabled = false;
            txtContactPhone.Enabled = false;
            txtContactPhone2.Enabled = false;
            txtContactTitle.Enabled = false;
            txtNewCarInventory.Enabled = false;
            txtOperatingHours.Enabled = false;
            txtOriginalSalesPerson.Enabled = false;
            txtPublicWebSite.Enabled = false;
            txtSalesPerson.Enabled = false;
            txtSecondContactEMail.Enabled = false;
            txtSecondContactFax.Enabled = false;
            txtSecondContactName.Enabled = false;
            txtSecondContactPhone.Enabled = false;
            txtSecondContactPhone2.Enabled = false;
            txtSecondDealerTitle.Enabled = false;
            txtState.Enabled = false;
            txtStreet.Enabled = false;
            txtUrchin.Enabled = false;
            txtUsedCarInventory.Enabled = false;
            txtZip.Enabled = false;
            txtCreditApplicationUrl.Enabled = false;
            ddlAppraisalExpiration.Enabled = false;
            ddlCreditApplicationText.Enabled = false;
            ddlDealerGroup.Enabled = false;
            ddlThemes.Enabled = false;
            chkBalanceOwned.Enabled = false;
            chkCreditApplicationActive.Enabled = false;
            chkCurrentMonthlyPayment.Enabled = false;
            chkEmailAppraisalOnly.Enabled = false;
            chkFourStepProcess.Enabled = false;
            chkHideAddress.Enabled = false;
            chkHideEmailTradeIn.Enabled = false;
            chkHideSpecialOffer.Enabled = false;
            chkHideVehicleCondition.Enabled = false;
            chkInstantOptionPricing.Enabled = false;
            chkNewCarInventory.Enabled = false;
            chkNewSelection.Enabled = false;
            chkNewVehicleIncentives.Enabled = false;
            chkRetailPricing.Enabled = false;
            chkTradeImageUpload.Enabled = false;
            chkUnsure.Enabled = false;
            chkUsedCarInventory.Enabled = false;
            rblGalvesRangeCategory.Enabled = false;
            rbLowerAmount.Enabled = false;
            rbLowerPercent.Enabled = false;
            rblTruckGalvesRangeCategory.Enabled = false;
            slbDealershipDefaultMakes.Enabled = false;
            slbDealershipHNIMappings.Enabled = false;
            rbHigherAmount.Enabled = false;
            rbHigherPercent.Enabled = false;
            tvcLanguages.Enabled = false;
            tvcDataProvider.Enabled = false;
            tvcNADABRanding.Enabled = false;

        }

        void SetSecurityForGroupAdmin()
        {
            //enabled fields
            txtOriginalSalesPerson.Enabled = true;
            txtCreditApplicationUrl.Enabled = true;
            txtUsedCarInventory.Enabled = true;
            txtNewCarInventory.Enabled = true;
            chkCreditApplicationActive.Enabled = true;
            chkNewCarInventory.Enabled = true;
            chkUsedCarInventory.Enabled = true;
            chkNewVehicleIncentives.Enabled = true;
            chkBalanceOwned.Enabled = true;
            chkCurrentMonthlyPayment.Enabled = true;
            ddlAppraisalExpiration.Enabled = true;
            slbDealershipDefaultMakes.Enabled = true;
            slbDealershipHNIMappings.Enabled = true;
            ddlThemes.Enabled = true;
            tvcLanguages.Enabled = true;
            tvcDataProvider.Enabled = true;
            tvcNADABRanding.Enabled = true;
            rblGalvesRangeCategory.Enabled = true;
            rbLowerAmount.Enabled = true;
            rbLowerPercent.Enabled = true;
            rblTruckGalvesRangeCategory.Enabled = true;
            rbHigherAmount.Enabled = true;
            rbHigherPercent.Enabled = true;
            txtPublicWebSite.Enabled = true;


            //disabled fields
            txtDealerName.Enabled = false;
            txtAppointmentEmail.Enabled = false;
            txtAutomotiveGroup.Enabled = false;
            txtCity.Enabled = false;
            txtComments.Enabled = false;
            txtContactEmail.Enabled = false;
            txtContactFax.Enabled = false;
            txtContactName.Enabled = false;
            txtContactPhone.Enabled = false;
            txtContactPhone2.Enabled = false;
            txtContactTitle.Enabled = false;
            txtOperatingHours.Enabled = false;
            txtSalesPerson.Enabled = false;
            txtSecondContactEMail.Enabled = false;
            txtSecondContactFax.Enabled = false;
            txtSecondContactName.Enabled = false;
            txtSecondContactPhone.Enabled = false;
            txtSecondContactPhone2.Enabled = false;
            txtSecondDealerTitle.Enabled = false;
            txtState.Enabled = false;
            txtStreet.Enabled = false;
            txtUrchin.Enabled = false;
            txtZip.Enabled = false;
            ddlCreditApplicationText.Enabled = false;
            ddlDealerGroup.Enabled = false;
            chkEmailAppraisalOnly.Enabled = false;
            chkFourStepProcess.Enabled = false;
            chkHideAddress.Enabled = false;
            chkHideEmailTradeIn.Enabled = false;
            chkHideSpecialOffer.Enabled = false;
            chkHideVehicleCondition.Enabled = false;
            chkInstantOptionPricing.Enabled = false;
            chkNewSelection.Enabled = false;
            chkRetailPricing.Enabled = false;
            chkTradeImageUpload.Enabled = false;
            chkUnsure.Enabled = false;
            txtAppraisalURL.Enabled = false;

            ////make required fields hidden
            ////rowDealerNameAutomotiveGroup.Visible = false;
            //lblAutoGroup.Visible = false;
            //txtAutomotiveGroup.Visible = false;
            //rowAppraisalURL.Visible = true;
            //trSalesPersonContactNameContactEmail.Visible = false;
            //rowCreditAppticationText.Visible = false;
            //rowStreetCityContactTitle.Visible = false;
            //rowCommentsLabel.Visible = false;
            //rowComments.Visible = false;
            //rowPublicWebSite.Visible = false;
            //rowStateZip.Visible = false;

            ////cellContactPhone.Visible = cellContactPhoneLabel.Visible = false;
            //cellAppointmentEmail.Visible = cellAppointmentEmailLabel.Visible = false;

            //rowInstantOptionPricingUnsure.Visible = true;
            //rowDealerGroupContactPhoneContactFax.Visible = false;

            //rowAverageRangeCategoryLabel.Visible = false;
            //rowAverageRangeCategory.Visible = false;

            //rowUsedCarInventory.Visible = isGroupDealerAdmin;
            //rowNewCarInventory.Visible = isGroupDealerAdmin;

            //rowCreditApplicationURL.Visible = isGroupDealerAdmin;

        }

        void GetCurrentGroup()
        {
            if (!dealerGroupHasValue)
            {
                _currentGroup = Security.GetCurrentUserGroup();
                isSpecificDealer = (_currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                    _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin);
                isGroupDealerAdmin = _currentGroup == Enumerations.AuthUserGroup.GroupDealerAdmin;
                dealerGroupHasValue = true;
            }
        }

        #region Utilities
        string ConvertStringToHttpSuffix(string url)
        {
            if (string.IsNullOrEmpty(url))
                return url;
            return url.StartsWith("http://") || url.StartsWith("https://") ? url : string.Format("http://{0}", url);
        }
        #endregion

        #region Client Script

        void RegisterClientScript()
        {

            if (!Page.ClientScript.IsClientScriptBlockRegistered(Page.GetType(), "DealerInfoTab_MainDef"))
            {
                string script = string.Format(@"
                    var DealerInfoTab_txtAppraisalURLId = '{0}';
                ", txtAppraisalURL.ClientID);
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "DealerInfoTab_MainDef", script, true);


            }


            string resourceName = "IE".Equals(Request.Browser.Browser) ? "Aculocity.UtilityFunctions.Script.ModalDialogParentIE.js" : "Aculocity.UtilityFunctions.Script.ModalDialogParentFF.js";
            Page.ClientScript.RegisterClientScriptResource(typeof(Aculocity.UtilityFunctions.UtilityFunctions), resourceName);
        }

        #endregion

    }
}