﻿using System;
using System.Web.UI;
using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.DAL;

using Aculocity.GAABackOffice.Global;
using Aculocity.GAABackOffice.BLL;


namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class LeadManagementTab : EntityEditorUserControlBase {

        private string _dealerkey = string.Empty;

        public override Pair[] ControlFieldMappings 
        {
            get {
                return new[] {
                                 new Pair("LeadProvider", ddlProviderAndProduct),
                                 new Pair("LeadEmail1", txtEmailLeads1),
                                 new Pair("LeadEmail2", txtEmailLeads2),
                                 new Pair("LeadFormat", tvcFormatOfLead),
                                 new Pair("LeadFormat2", tvcFormatOfLead2),
                                 new Pair("LeadEmail3", txtEmailLeads3),
                                 new Pair("LeadEmail4", txtEmailLeads4),
                                 new Pair("LeadFormat3", tvcFormatOfLead3),
                                 new Pair("LeadFormat4", tvcFormatOfLead4),
                                 new Pair("LeadContactName", txtContactName),
                                 new Pair("LeadContactEmail", txtContactEmail),
                                 new Pair("LeadContactPhone", txtContactPhone),
                                 new Pair("LeadContactFax", txtContactFax)
                    
                             };
            }
        }

        public override EntityValidator[] EntityValidators
        {
            get {
                return new EntityValidator[] {
                                                 new EntityNonEmptyFieldValidator("LeadEmail1") {ErrorMessage = "Please enter the first Lead Email address."},
                                                 new EntityMailAddressValidator("LeadEmail1") {ErrorMessage = "Please enter a valid first Lead Email address."},
                                                 new EntityMailAddressValidator("LeadEmail2") {ErrorMessage = "Please enter a valid first Lead Email 2 address."},
                                                 new EntityMailAddressValidator("LeadEmail3") {ErrorMessage = "Please enter a valid first Lead Email 3 address."},
                                                 new EntityMailAddressValidator("LeadEmail4") {ErrorMessage = "Please enter a valid first Lead Email 4 address."},
                                                 new EntityMailAddressValidator("LeadContactEmail") {ErrorMessage = "Please enter a valid Lead Contact E-mail address."},
                                                 new EntityPhoneValidator("LeadContactPhone") {ErrorMessage = "Please enter a Lead Contact Phone number. e.g. XXX-XXX-XXXX"},
                                                 new EntityPhoneValidator("LeadContactFax") {ErrorMessage = "Please enter a Lead Contact Fax number. e.g. XXX-XXX-XXXX"}
                                             };
            }
        }

        protected override void OnInit(EventArgs e) 
        {
            base.OnInit(e);

            Page.InitComplete += Page_InitComplete;

            tvcFormatOfLead.ValueFirst = "XML";
            tvcFormatOfLead.ValueSecond = "Text";

            tvcFormatOfLead2.ValueFirst = "XML";
            tvcFormatOfLead2.ValueSecond = "Text";

            tvcFormatOfLead3.ValueFirst = "XML";
            tvcFormatOfLead3.ValueSecond = "Text";

            tvcFormatOfLead4.ValueFirst = "XML";
            tvcFormatOfLead4.ValueSecond = "Text";

            _dealerkey = Request.QueryString["DealerKey"];
        
        }

        void Page_InitComplete(object sender, EventArgs e)
        {
            LoadProviderAndProduct();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (string.IsNullOrEmpty(_dealerkey))
            {
                ((MainEditorHolder)Page).UpdateButton.Text = "AddDealer";
            }
        }

        void Page_PreRender(object sender, EventArgs e)
        {
            ApplySecurity();
        }

        public override void OnBeforeDataBind() 
        {
            base.OnBeforeDataBind();

            txtDealershipKey.Text = ((Dealer)Entity).DealerKey;
     
        }

        void LoadProviderAndProduct() {

            ddlProviderAndProduct.DataSource = DealerCommands.GetLeadProviders(DataManager).ExecuteDataTable();
            ddlProviderAndProduct.DataValueField = "LeadProvider";
            ddlProviderAndProduct.DataTextField = "LeadProvider";
            ddlProviderAndProduct.DataBind();

        }

        void ApplySecurity() 
        {

            Enumerations.AuthUserGroup currentGroup = Security.GetCurrentUserGroup();

            switch (currentGroup)
            {
                case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                    SetSecurityForSpecificDealer();
                    break;

                case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                case Enumerations.AuthUserGroup.ResellerAdmin:
                    SetSecurityForSpecificDealer();
                    break;

                case Enumerations.AuthUserGroup.HomeNetDealerAdmin:
                    SetSecurityForSpecificDealer();
                    break;

                case Enumerations.AuthUserGroup.GroupDealerAdmin:
                    SetSecurityForGroupAdmin();
                    break;

            }
            
        }

        void SetSecurityForSpecificDealer() 
        {

            ddlProviderAndProduct.Enabled = false;
            txtContactEmail.Enabled = false;
            txtContactFax.Enabled = false;
            txtContactName.Enabled = false;
            txtContactPhone.Enabled = false;
            txtDealershipKey.Enabled = false;

            txtEmailLeads1.Enabled = true;
            txtEmailLeads2.Enabled = true;
            txtEmailLeads3.Enabled = true;
            txtEmailLeads4.Enabled = true;

            tvcFormatOfLead.Enabled = true;
            tvcFormatOfLead2.Enabled = true;
            tvcFormatOfLead3.Enabled = true;
            tvcFormatOfLead4.Enabled = true;
            
        }

        void SetSecurityForGroupAdmin()
        {

            ddlProviderAndProduct.Enabled = false;
            txtContactEmail.Enabled = false;
            txtContactFax.Enabled = false;
            txtContactName.Enabled = false;
            txtContactPhone.Enabled = false;
            txtDealershipKey.Enabled = false;

            txtEmailLeads1.Enabled = true;
            txtEmailLeads2.Enabled = true;
            txtEmailLeads3.Enabled = true;
            txtEmailLeads4.Enabled = true;

            tvcFormatOfLead.Enabled = true;
            tvcFormatOfLead2.Enabled = true;
            tvcFormatOfLead3.Enabled = true;
            tvcFormatOfLead4.Enabled = true;


        }
        
    }
}