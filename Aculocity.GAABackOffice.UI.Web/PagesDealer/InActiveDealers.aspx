﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="InActiveDealers.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.InActiveDealers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
    <table width="100%">
        <tr>
            <td>
                <asp:GridView ID="gvInActiveDealers" runat="server" AutoGenerateColumns="False" Width="100%" DataKeyNames="DealerKey" 
                                AlternatingRowStyle-BackColor="LightGray" HeaderStyle-BackColor="Black"
                                HeaderStyle-ForeColor="White" OnRowCommand="gvInActiveDealers_RowCommand">
                    <EmptyDataTemplate>
                          <asp:Label ID="lblNoData" runat="server" Text="Currently no admin deactivated dealers" ForeColor="Red" style="text-align:center;" Width="100%"></asp:Label>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Dealer Name">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="lblDealerName" runat="server" Text='<%# Bind("DealerName") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="170px"/>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Deactivation Date">
                            <ItemTemplate>
                                <asp:Label ID="lblDeactivationDate" runat="server" Text='<%# Bind("DeactivationDate") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" Width="100px"/>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LeadEmail 1">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="lblLeadEmail1" runat="server" Text='<%# Bind("LeadEmail1") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="110px"/>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LeadEmail 2">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="lblLeadEmail2" runat="server" Text='<%# Bind("LeadEmail2") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="110px"/>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LeadEmail 3">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="lblLeadEmail3" runat="server" Text='<%# Bind("LeadEmail3") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="110px"/>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LeadEmail 4">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="lblLeadEmail4" runat="server" Text='<%# Bind("LeadEmail4") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="110px"/>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Activate">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkActivate" runat="server" CommandName="Activate" CommandArgument='<%# Bind("DealerKey") %>'
                                                Text="Activate" CausesValidation="true" ForeColor="Blue" />
                            </ItemTemplate>
                            <ItemStyle VerticalAlign="Top" HorizontalAlign="Center" Width="50px"/>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
