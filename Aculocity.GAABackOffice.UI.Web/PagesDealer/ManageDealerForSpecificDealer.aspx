﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="ManageDealerForSpecificDealer.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.ManageDealerForSpecificDealer" Theme="BackOfficeMainTheme" %>
<%@ Register Src="~/PagesDealer/LeadSummaryReport.ascx" TagPrefix="GAABackOfficeControl" TagName="LeadSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
<script type="text/javascript" src="../JScripts/FusionCharts.js"></script>
<asp:UpdatePanel ID="upContent" runat="server">
<ContentTemplate>
<table border="0" cellpadding="0"  cellspacing="3" width="100%">
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="3">
                            <tr>
                                <td class="CellLabel">
                                    From:
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="upFrom" runat="server">
                                        <ContentTemplate>
                                            <GAABackOfficeControls:DatePicker ID="ccFrom" runat="server" Width="150px" ImageUrl="~/Images/Calendar/cal.gif"/>    
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="CellLabel">
                                    First Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
                                </td>
                                <td class="CellLabel">
                                    User Email:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserEmail" runat="server" Width="150px" MaxLength="100"></asp:TextBox>                                                
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                    To:
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="upTo" runat="server">
                                        <ContentTemplate>
                                            <GAABackOfficeControls:DatePicker ID="ccTo" runat="server" Width="150px" ImageUrl="~/Images/Calendar/cal.gif"/>    
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td class="CellLabel">
                                    Last Name
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" Width="150px" MaxLength="50"></asp:TextBox>                                                                                    
                                </td>
                                <td class="CellLabel">
                                    User Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhone" runat="server" Width="150px" MaxLength="20"></asp:TextBox>                                                
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="2">
                <tr>
                    <td class="CellLabel">
                        Display
                    </td>
                    <td>
                        <asp:RadioButtonList id="rblLeadsCountPerPage" runat="server" RepeatDirection="Horizontal" Font-Bold="true">
                            <asp:ListItem Value="10" Selected="True">10</asp:ListItem>
                            <asp:ListItem Value="25">25</asp:ListItem>
                            <asp:ListItem Value="50">50</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td class="CellLabel">
                        &nbsp;leads per page
                    </td>
                </tr>
            </table> 
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellpadding="0" cellspacing="2">
                <tr>
                    <td>
                        <asp:Button ID="btnLeadSummaryReport" runat="server" Text="Lead Summary Report"  />
                    </td>
                    <td>
                        <asp:ImageButton ID="btnExportExcel1"  runat="server" Height="24px" Width="24px" OnClick="btnExportExcel_Click"
                                                                                     ImageUrl="~/Images/Excelicon24x24.png" ToolTip="Export to excel" />
                    </td>
                    <td>
                        <asp:Button ID="btnLeadDetailsReport" runat="server" Text="Lead Details Rerport" />
                    </td>
                </tr>
           </table>
        </td>
    </tr>
    <tr>
        <td id="tdTop10DesiredChart" runat="server" style="display:none;" align="left">
            <asp:UpdatePanel ID="upTop10DesiredChart" runat="server" >
                <ContentTemplate>
                    <GAABackOfficeControls:Top10DesiredChart ID="chartTop10DesiredChart" runat="server"/>        
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger  ControlID="btnLeadDetailsReport" />
                </Triggers>
            </asp:UpdatePanel>
        </td> 
    </tr>
    <tr>
        <td ID="cellContent" runat="server">
            <GAABackOfficeControl:LeadSummaryReport ID="leadSummaryReport" runat="server" Visible="false"/>
            <GAABackOfficeControls:DetailsReportGrid ID="drgDetailsReport" runat="server" Visible="false" />
            <%--<GAABackOfficeControls:ResellerDetailsReportGrid ID="drgResellerDetailsReport" runat="server" Visible="false" />--%>
        </td>
    </tr>
</table>
</ContentTemplate>
<Triggers>
    <asp:PostBackTrigger ControlID="btnExportExcel1" />
</Triggers>
</asp:UpdatePanel>
</asp:Content>
