﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master"  
         AutoEventWireup="true" CodeBehind="FaceBookHeaders.aspx.cs" 
         ValidateRequest="false" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.FaceBookHeaders" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
<div id="title"><asp:Label class="title" ID="lblTitle" runat="server"></asp:Label></div>
    <table width="100%">
        <tr>
            <td align="center">
                <br/>
                <div align="left">
                    <asp:UpdatePanel ID="upFTB" runat="server">
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblFeedback" runat="server" 
                                                   EnableViewState="False"
                                                   ForeColor="Gray">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:5px" colspan="2">
                                        <p>
                                        <FTB:FreeTextBox id="ftbfaceBookHeader" runat="Server"
                                                     EnableToolbars="true"
                                                     EnableHtmlMode="true" Height="300px" Width="100%"
                                                     SupportFolder="aspnet_client/FreeTextBox/"
                                                     ButtonSet="Office2003"
                                                     BackColor="158, 190, 245"
                                                     GutterBackColor="169, 226, 226">
                                        </FTB:FreeTextBox>    
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblInfo" runat="server" Font-Size="11px" 
                                                Text="<b>Maximum text length:</b> 1000 characters">
                                        </asp:Label>
                                    </td>
                                    <td align="right" >
                                        <asp:Button ID="btnBack" runat="server" Text="Back" onclick="btnBack_Click"/>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update Changes" onclick="btnUpdate_Click"/>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click"/>
                            <asp:PostBackTrigger ControlID="btnUpdate" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
