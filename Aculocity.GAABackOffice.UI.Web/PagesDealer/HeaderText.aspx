﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" 
        AutoEventWireup="true" 
        CodeBehind="HeaderText.aspx.cs" 
        ValidateRequest="false"
        Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.HeaderText" 
        Theme="BackOfficeMainTheme"
        %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
<script language="JavaScript">
    changeclass("ctl00_liHeaderText");
</script>
    <table width="100%">
        <tr>
            <td align="center">
                <br/>
                <div align="left">
                    <asp:UpdatePanel ID="upFTB" runat="server">
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:Label ID="lblFeedback" runat="server" 
                                                   EnableViewState="False"
                                                   ForeColor="Gray">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%"> 
                                            <tr>
                                                <td align="left">
                                                    <asp:DropDownList ID="ddlPage" runat="server" AutoPostBack="True" 
                                                        onselectedindexchanged="ddlPage_SelectedIndexChanged">
                                                        <asp:ListItem Selected="True" Value="1">Getting Started</asp:ListItem>
                                                        <asp:ListItem Value="2">Optional Equipment (If four step)</asp:ListItem>
                                                        <asp:ListItem Value="3">Special Offer</asp:ListItem>
                                                        <asp:ListItem Value="4">Summary</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="right">
                                                    <asp:CheckBox ID="chkActive" runat="server" Text="Active" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" align="left">
                                        <div id="tabs">
                                            <p>
                                                <ul>
                                                    <li id="liEnglish" runat="server"><a id="lnkEnglish" runat="server" OnServerClick="lnk_Click">English Text</a></li>
                                                    <li id="liSpanish" runat="server"><a id="lnkSpanish" runat="server" OnServerClick="lnk_Click">Spanish Text</a></li>
                                                </ul>
                                                <p>
                                                </p>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:5px">
                                        <p>
                                        <FTB:FreeTextBox id="ftbHeaderText" runat="Server"
                                                     EnableHtmlMode="true" Height="300px" Width="100%"
                                                     SupportFolder="aspnet_client/FreeTextBox/"
                                                     ButtonSet="Office2003"
                                                     BackColor="158, 190, 245"
                                                     GutterBackColor="169, 226, 226">
                                        </FTB:FreeTextBox>    
                                        </p>
                                        <div style="display: none;">
                                            <asp:TextBox ID="txtEnglish" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            <asp:TextBox ID="txtSpanish" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            <asp:TextBox ID="txtIsAciveEng" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtIsAciveEsp" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtLang" runat="server"></asp:TextBox>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblEng" runat="server" Font-Size="11px" 
                                                Text="<b>Maximum text length:</b> 1000 characters">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnBack" runat="server" Text="Back" onclick="btnBack_Click"/>
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update Changes" onclick="btnUpdate_Click"/>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlPage" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
