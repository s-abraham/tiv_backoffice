﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true"
    CodeBehind="UploadImages.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.UploadImages"
    ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">

    <script language="JavaScript">
        changeclass("ctl00_liManageImages");
        function CheckFileExistence() {

            var filePath = document.getElementById('<%= this.fuImage.ClientID %>').value;

            if (filePath.length < 1) {

                alert("File Name Can not be empty"); return false;
            }

            var validExtensions = new Array(); var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            validExtensions[0] = 'jpg';
            validExtensions[1] = 'jpeg';                  
            validExtensions[2] = 'gif';

            for (var i = 0; i < validExtensions.length; i++) {

                if (ext == validExtensions[i]) return true;
            }

            alert('Only .JPG, .JPEG and .GIF files are allowed. Selected file cannot be uploaded.');

            return false;
        }
    </script>

    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <div align="center">
                    <asp:UpdatePanel ID="upMain" runat="server">
                        <ContentTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" width="800px">
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Label ID="lblFeedback" runat="server" EnableViewState="False" ForeColor="Gray">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="bottom" align="left">
                                        <div id="tabs">
                                            <ul>
                                                <li id="liEnglish" runat="server"><a id="lnkEnglish" runat="server" onserverclick="lnk_Click">
                                                    English Images</a></li>
                                                <li id="liSpanish" runat="server"><a id="lnkSpanish" runat="server" onserverclick="lnk_Click">
                                                    Spanish Images</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" align="left" style="padding: 5px">
                                        <div>
                                            <p>
                                                <asp:TextBox ID="txtLang" runat="server" Style="display: none"></asp:TextBox>
                                                <asp:GridView ID="gvImages" runat="server" AutoGenerateColumns="False" DataKeyNames="DealerKey,ImageID"
                                                    EmptyDataText="No images have been uploaded yet." OnRowCommand="gvImages_RowCommand"
                                                    OnRowDataBound="gvImages_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Active" SortExpression="Active">
                                                            <HeaderStyle Width="60px" />
                                                            <ItemTemplate>
                                                                <asp:RadioButton ID="rbActive" runat="server" Checked='<%# Bind("Active") %>' OnCheckedChanged="RbActiveCheckedChanged"
                                                                    AutoPostBack="true" GroupName="rblImages" />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ImageID" SortExpression="ImageID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFilename" runat="server" Text='<%# Bind("ImageID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblNoImage" runat="server" Text="No Image"></asp:Label>
                                                                <asp:Image ID="imgDealerImage" runat="server" ImageUrl='<%# string.Format("~/Pages/RenderImage.aspx?DealerKey={0}&FileName={1}", Eval("DealerKey"), Eval("ImageID")) %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Tools">
                                                            <HeaderStyle Width="60px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkDelete" runat="server" CommandName="DeleteImage" CommandArgument='<%# Eval("ImageID") %>'
                                                                    ForeColor="Black" OnClientClick="return confirm('Are you sure you want to delete this image?');">Delete</asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Data Range">
                                                            <HeaderStyle Width="60px" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="panelActiveDate" runat="server">
                                                                    <ContentTemplate>
                                                                        <table>
                                                                            <tr>
                                                                                <td align="center">
                                                                                    <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                                                                </td>
                                                                                <td align="center">
                                                                                    <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                                                                </td>
                                                                                <td rowspan="2" valign="bottom">
                                                                                    <asp:LinkButton ID="lnkDate" ForeColor="Black" runat="server" CommandName="DateImage"
                                                                                        CommandArgument='<%# Eval("ImageID")%>'>Enable</asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <GAABackOfficeControls:DateTextBox ID="txtImgActiveDate" runat="server" DateString='<%# GetDateStart(Eval("StartDate")) %>'
                                                                                        MaxLength="100" Width="100px" />
                                                                                </td>
                                                                                <td>
                                                                                    <GAABackOfficeControls:DateTextBox ID="txtInactiveDate" runat="server" DateString='<%# GetDateStart(Eval("EndDate")) %>'
                                                                                        MaxLength="100" Width="100px" />
                                                                                </td>
                                                                                <td valign="bottom">
                                                                                    <asp:LinkButton ID="lnkUpdateDate" ForeColor="Black" runat="server" CommandName="UpdateDate"
                                                                                        Style="display: inline" CommandArgument='<%# Eval("ImageID")%>'>Update</asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="3" align="left">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="Black" ForeColor="White" />
                                                </asp:GridView>
                                            </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblUploadLimited" runat="server" Text="You have reached your upload limit of 5 images.  Please delete an image before trying to upload another image."
                                            Visible="False" ForeColor="Red">
                                        </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblError" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div align="center">
                        <table border="0" cellpadding="2" cellspacing="0" id="tblUploadImage" runat="server"
                            width="800px">
                            <tr>
                                <td align="left">
                                    <asp:UpdatePanel ID="upUploadImage" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblUpload" runat="server" Text="New Image:" Font-Bold="True"></asp:Label>&nbsp;
                                            <asp:FileUpload ID="fuImage" runat="server" Height="22px" />&nbsp;
                                            <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" OnClientClick="return CheckFileExistence()"/>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnUpload" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblInfo" runat="server" Font-Size="10px" Text="<b>Image Type:</b> .jpg, .jpeg, .gif | <b>Image Width:</b> 400-600px | <b>Image Size:</b> 500KB or less"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
