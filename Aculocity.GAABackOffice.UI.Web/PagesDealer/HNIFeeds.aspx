﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HNIFeeds.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.HNIFeeds" MasterPageFile="~/GlobalMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">

    <script language="JavaScript">
        changeclass("ctl00_liHNIFeeds");

        function doHNIChange(id, hniid, dkey) {
            var sel = $('#' + id + '_sel');
            var can = $('#' + id + '_can');
            var ch = $('#' + id + '_ch');
            var did = $('#' + id + '_did');

            ch.hide();

            if (!sel.hasClass('bound')) {
                $.ajax({
                    type: "post",
                    data: { type: 'dlist' },
                    success: function (retval) {
                        sel.addClass('bound');
                        $.each(retval, function () {
                            var opt = $('<option>').attr('value', this.DealerKey).html(this.DealerName);
                            if (dkey == this.DealerKey)
                                opt.attr('selected', '');
                            sel.append(opt);
                        });

                        sel.change(function () {
                            //console.log(hniid + '->' + sel.val());
                            $.ajax({
                                type: "post",
                                data: { type: 'upd', hnidealerid: hniid, dealerkey: sel.val() },
                                success: function () {
                                    if (sel.val() == '')
                                        did.html('Not Assigned');
                                    else
                                        did.html(sel.val() + ' - ' + sel.children("option[value='" + sel.val() + "']").text());
                                }
                            });
                        });

                        sel.show();
                        can.show();
                    }
                });
            } else {
                sel.show();
                can.show();
            }


        }

        function doHNICancel(id) {
            $('#' + id + '_sel').hide();
            $('#' + id + '_can').hide();
            $('#' + id + '_ch').show();
        }

    </script>

    <table cellspacing="0" cellpadding="2" border="1" style="border-color: Black; width: 100%; border-collapse: collapse;" rules="all" class="GridBackOffice">
        <tbody>
            <tr style="color: White; background-color: Black;">
                <th scope="col">HNI Dealer ID</th>
                <th scope="col" colspan="2">Assigned to Dealer</th>
            </tr>
            <%
                if (_dealerList != null)
                {
                    foreach (System.Data.DataRow row in _dealerList.Rows)
                    {
                        string key = (string)row["HNIDealerID"];
                        key = key.Replace(" ", "").Replace(".", "").Replace(",", "").Replace("'", "").Trim().ToLower();

                        string didKey = string.Format("{0}_did", key);
                        string actKey = string.Format("{0}_act", key);
                        string changeKey = string.Format("{0}_ch", key);
                        string selKey = string.Format("{0}_sel", key);
                        string cancelKey = string.Format("{0}_can", key);
            %>
            <tr align="left">
                <td><%=row["HNIDealerID"] %></td>
                <% if (row.IsNull("DealerKey"))
                   { %>
                <td id="<%=didKey %>">Not Assigned</td>
                <% }
                   else
                   { %>

                <td id="<%=didKey %>"><%=row["DealerKey"] %> - <%=row["DealerName"] %></td>
                <%} %>
                <td id="<%=actKey %>" style="text-align: center; width: 360px;">
                    <a id="<%=changeKey %>" href="javascript:void(0);" onclick="doHNIChange('<%=key %>', '<%=row["HNIDealerID"] %>', '<%=row["DealerKey"] %>' );">change</a>
                    <select id="<%=selKey %>" style="width: 220px; display: none;" class="hnifeedsel">
                        <option value="">&lt;Select Dealer To Assign&gt;</option>
                    </select>
                    <a id="<%=cancelKey %>" style="display: none; margin-left: 15px;" href="javascript:void(0);" onclick="doHNICancel('<%=key %>');">close</a>
                </td>
            </tr>

            <%
                    }
                }
            %>
        </tbody>
    </table>

</asp:Content>
