﻿<%@ Page Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Dashboard" Theme="BackOfficeMainTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <link href="../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    
    <script src="../JScripts/highcharts/js/highcharts.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JScripts/jquery-ui/jquery-ui-datepicker.js"></script>
    <link rel="stylesheet" href="../JScripts/jquery-ui/jquery-ui-datepicker.css" type="text/css" />
    <style type="text/css">
        table .label {
            width:85px;
            text-align: left;
        }
        .chart_container {
            width: 700px;
            margin-right: 50px;
        }

        .chart_container#chart {
            
        }

        .chart_container#stats {
            height: 100px;
        }
        .chart_container table.stats th {
            background: none;
            font-size: 40px;
            color: #666;
            font-weight: normal;
            padding-left: 30px;
        }
        .chart_container table.stats td {
            background: none;
            font-size: 14px;
            color: #666;
            font-weight: normal;
            padding-left: 7px;
            padding-right: 5px;
        }
        .chart_container#title-stat table.stats th {
            font-size: 60px;
        }
        .side_container {
            width: 270px; 
            border: 1px #CCC solid;
            margin-bottom: 20px;
            overflow: auto;
        }

        .side_container#monthly-stats {
            height: 18  0px;
        }


        .side_container .side_content {
            margin: 15px;
        }

        .side_container table.stats {
            width: 100%;
        }
        .side_container table.stats th {
            background: none;
            font-size: 40px;
            color: #666;
            font-weight: normal;
            text-align: left;
            padding-bottom: 15px;
            width: 33%;
            
        }

        .side_container table.stats th#stat1 {
            color: #FBB11A;
        }
        .side_container table.stats th#stat2 {
            color: #f5831f;
        }
        .side_container table.stats th#stat3 {
            color: #666666;
        }



    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
    <script type="text/javascript" language="JavaScript">
        changeclass("ctl00_liDashboard");
    </script>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="chart_container" id="title-stat">
                    <table class="stats" cellpadding="0" cellspacing="0">
                        <tr>
                            <th name="leads"><asp:Literal ID="ltrTitleStats" runat="server"></asp:Literal></th>
                            <td><asp:Label ID="lblTitleStats" runat="server">Leads</asp:Label></td>
                        </tr>
                    </table>
                </div>
                <div class="chart_container" id="chart">
                    <asp:Literal ID="chart1" runat="server"></asp:Literal>
                </div>
                <div class="chart_container" id="stats">
                    
                    <table class="stats" cellpadding="0" cellspacing="0">
                        <tr>
                            <th><asp:Literal ID="ltrStat1" runat="server"></asp:Literal></th>
                            <td><asp:Label ID="lblStat1" runat="server">Active</asp:Label></td>
                            <th><asp:Literal ID="ltrStat2" runat="server"></asp:Literal></th>
                            <td><asp:Label ID="lblStat2" runat="server">Nonactive</asp:Label></td>
                            <th><asp:Literal ID="ltrStat3" runat="server"></asp:Literal></th>
                            <td><asp:Label ID="lblStat3" runat="server">Leads</asp:Label></td>
                            <th><asp:Literal ID="ltrStat4" runat="server"></asp:Literal></th>
                            <td><asp:Label ID="lblStat4" runat="server">Facebook</asp:Label></td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top">
                <div class="side_container" id="date-range">
                    <div class="side_content">
                        <h3 class="title">Custom Date Range</h3>
                        
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="label">From:</td>
                                <td><asp:TextBox ID="inputStart" runat="server" Width="125px" data-toggle="datepicker" MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="label">To:</td>
                                <td><asp:TextBox ID="inputEnd" runat="server" Width="125px" data-toggle="datepicker" MaxLength="10"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: right;">
                                    <!--<input type="submit" id="btnReportView" class="viewButton" value="View"/>-->
                                    <asp:Button ID="btnReportView" OnClick="LoadReportDates" CssClass="viewButton" Text="View" runat="server" UseSubmitBehavior="False" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="side_container" id="monthly-stats">
                    <div class="side_content">
                        <h3><asp:Label ID="lblSideTitle" runat="server">Monthly Sign Up Data</asp:Label></h3>
                        <table class="stats" cellpadding="0" cellspacing="0">
                            <tr>
                                <th id="stat1"><asp:Literal ID="ltrSideStats1" runat="server"></asp:Literal></th>
                                <th id="stat2"><asp:Literal ID="ltrSideStats2" runat="server"></asp:Literal></th>
                                <th id="stat3"><asp:Literal ID="ltrSideStats3" runat="server"></asp:Literal></th>
                            </tr>
                            <tr>
                                <td><asp:Label ID="lblSideStat1" runat="server">Last</asp:Label></td>
                                <td><asp:Label ID="lblSideStat2" runat="server">Current</asp:Label></td>
                                <td><asp:Label ID="lblSideStat3" runat="server">Potential</asp:Label></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <hr />
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="width:50%;">
                <asp:Literal ID="chart2" runat="server"></asp:Literal>
            </td>
            <td style="width:50%;">
                <asp:Literal ID="chart3" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        $(function () {
            $(':input[data-toggle="datepicker"]').datepicker({
                dateFormat: 'm/d/yy',
                buttonImage: '../Images/calender-black.jpg',
                buttonImageOnly: true,
                showOn: "button"
            })
        });
    </script>
</asp:Content>