﻿using System;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Globalization;
using Aculocity.GAABackOffice.DAL;
using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.Global;
using Aculocity.GAABackOffice.BLL;

namespace Aculocity.GAABackOffice.UI.Web {
    
    public partial class ManageDealer : MainBasePage 
    {

        bool isAllowedCompanyDealer;
        bool simpleUser;

        bool showOnlyError;

        int currentUserId;

        Enumerations.AuthUserGroup currentGroup;

        protected void BtnAddDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx"), false);
        }
        

        protected string FormatNumber(object number)
        {
            try
            {
                int n = int.Parse(number.ToString(), NumberStyles.None, CultureInfo.InvariantCulture);
                return n.ToString("0,0", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return number.ToString();
            }
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            Boolean Homenet;
            Boolean Galves;
            Boolean BSB;

            //Set the HomeNet
            if (Session["HomeNet"] != null)
            {
                Homenet = Convert.ToBoolean(Session["HomeNet"].ToString());
            }
            else
            {
                Homenet = false;
            }

            //Set the Galves
            if (Session["Galves"] != null)
            {
                Galves = Convert.ToBoolean(Session["Galves"].ToString());
            }
            else
            {
                Galves = false;
            }

            //Set the BSB
            if (Session["BSB"] != null)
            {
                BSB = Convert.ToBoolean(Session["BSB"].ToString());
            }
            else
            {
                BSB = false;
            }

            //Get the correct Master Page
            if (Homenet == true)
            {
                MasterPageFile = "~/HomeNet.Master";
            }
            else
            {
                if (Galves == true)
                {
                    MasterPageFile = "~/Galves.Master";
                }
                else if (BSB == true)
                {
                    MasterPageFile = "~/GetAutoAppraise.Master";
                }
                else
                {
                    MasterPageFile = "~/GlobalMaster.Master";
                }
            }
        }

        protected override void OnInit(EventArgs e) {
            try
            {
                base.OnInit(e);

                currentUserId = Security.GetLoggedUserID();
                currentGroup = Security.GetCurrentUserGroup();

                sgDealer.Grid.RowDataBound += new GridViewRowEventHandler(Grid_RowDataBound);
                lnkErrorDealers.Click += new EventHandler(lnkErrorDealers_Click);

               
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void lnkErrorDealers_Click(object sender, EventArgs e) {
            try
            {
                showOnlyError = !showOnlyError;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void Grid_RowDataBound(object sender, GridViewRowEventArgs e) {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if ((!Convert.ToBoolean((e.Row.DataItem as DataRowView)["HasOfferText"])) && (!Convert.ToBoolean((e.Row.DataItem as DataRowView)["HasOfferImage"])))
                        e.Row.BackColor = Color.White;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        
        protected override void OnLoad(EventArgs e) 
        {
            try
            {
                base.OnLoad(e);

                ApplySecurity();
                RegisterClientScript();

                if (!IsPostBack)
                {
                    DataSet stats = ReportDAL.GetDashboardStats(DateTime.Today, DateTime.Today);
                    DataSet stats2 = ReportDAL.GetSignupStats();

                    lblTotalDealers.Text = FormatNumber(stats.Tables[0].Rows[0][0]);
                    lblNewDealersCount.Text = FormatNumber(stats2.Tables[0].Rows[0][0]);
                    

                    ddlFields.DataSource = simpleUser ? DealerCommands.GetDealershipFilterDataTableOnlyDealerKeyDealerName() : DealerCommands.GetDealershipFilterDataTable();
                    ddlFields.DataValueField = "ID";
                    ddlFields.DataTextField = "Description";
                    ddlFields.DataBind();

                    object value = Session["ManageDealer_FilterValue"];
                    if (value == null)
                    {
                        ddlFields.SelectedValue = "DealerName";
                        cbxShowDealersWOSpecialOfferAndImages.Checked = true;
                        cbxShowOnlyActive.Checked = false;
                    }
                    else
                    {
                        txtValue.Text = Session["ManageDealer_FilterValue"].ToString();
                        ddlFields.SelectedValue = Session["ManageDealer_CurrentField"].ToString();
                        cbxShowDealersWOSpecialOfferAndImages.Checked = Convert.ToBoolean(Session["ManageDealer_ShowWOSO"]);
                        cbxShowOnlyActive.Checked = Convert.ToBoolean(Session["ManageDealer_ShowOnlyActive"]);
                        sgDealer.Grid.PageIndex = Convert.ToInt32(Session["ManageDealer_GridPageIndex"]);
                    }
                }
                else
                {
                    showOnlyError = (bool)ViewState["ShowOnlyError"];
                }

                isAllowedCompanyDealer = UtilityFunctions.CheckIsAllowedCompanyDealer(DataManager.Clone());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected override void OnPreRender(EventArgs e) 
        {
            try
            {
                base.OnPreRender(e);

                ViewState["ShowOnlyError"] = showOnlyError;

                DataSet data = DealerCommands.GetDealersByFilterCriteria(DataManager, ddlFields.SelectedItem.Value, txtValue.Text, cbxShowOnlyActive.Checked, cbxShowDealersWOSpecialOfferAndImages.Checked, currentGroup, currentUserId, showOnlyError).ExecuteDataSet();
                sgDealer.Grid.DataSource = data.Tables[0];
                sgDealer.Grid.DataBind();


                Session["ManageDealer_FilterValue"] = txtValue.Text;
                Session["ManageDealer_CurrentField"] = ddlFields.SelectedValue;
                Session["ManageDealer_ShowWOSO"] = cbxShowDealersWOSpecialOfferAndImages.Checked;
                Session["ManageDealer_ShowOnlyActive"] = cbxShowOnlyActive.Checked;
                Session["ManageDealer_GridPageIndex"] = sgDealer.Grid.PageIndex;
                
                //spanErrorDealers.InnerText = string.Format("There are {0} dealers without special offer text or image.", data.Tables[1].Rows[0]["ErrorsCount"]);
                lnkErrorDealers.Text = string.Format("There are {0} dealers without special offer text or image.", data.Tables[1].Rows[0]["ErrorsCount"]);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void ApplySecurity() 
        {
            try
            {
                simpleUser = currentGroup == Enumerations.AuthUserGroup.GroupDealerAdmin || currentGroup == Enumerations.AuthUserGroup.HomeNetSuperUser;
                cellErrorDealers.Visible = !simpleUser;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected override void OnPreRenderComplete(EventArgs e) 
        {
            try
            {
                base.OnPreRenderComplete(e);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
       
        #region ClientScript
        void RegisterClientScript() 
        {
            try
            {
                ClientScript.RegisterClientScriptResource(typeof(SingleKeyEntityEditableSimpleGrid), "Aculocity.Controls.Web.GridEditorElements.Scripts.SingleKeyEntityEditableSimpleGrid.js");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

    }

}
