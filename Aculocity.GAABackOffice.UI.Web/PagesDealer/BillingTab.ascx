﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillingTab.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.BillingTab" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI" %>
<table border="0" cellpadding="0" cellspacing="3">
    <tr>
        <td class="CellLabel">
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        Billable       
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtBilable" runat="server" Text="Dealership billable status can be toggled between true and false." />                                                                                                        
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <asp:CheckBox ID="chkBillable" runat="server" AutoPostBack="true" />
        </td>
        <td class="CellLabel" style="padding-left:30px;">
             <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        Monthly Billing Amount       
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtMonthlyBillingAmount" runat="server" Text="Recurring monthly billing fee." />                                                                                                        
                    </td>
                </tr>
            </table>
            
        </td>
        <td>
            <%--<acw:NumericTextBox ID="txtMonthlyBillingAmount" runat="server" Width="150"></acw:NumericTextBox>--%>
            <ew:NumericBox ID="txtMonthlyBillingAmount" runat="server" Width="150" />
        </td>
    </tr>   
    <tr>
        <td ID="tdMotivate" runat="server" class="CellLabel">Motivate</td>
        <td>
            <asp:TextBox ID="txtReason" runat="server" Width="150" TextMode="multiLine" Rows="4"></asp:TextBox>
            <%--<asp:RequiredFieldValidator ID="Motivationvalidator" runat="server" ControlToValidate="txtReason"
                                                        Display="Dynamic" ErrorMessage="Please enter a motivation for the dealer being non billable."
                                                        ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td class="CellLabel" style="padding-left:30px;">
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        Additional billing information       
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtAdditionalBillingInformation" runat="server" Text="Additional information related to this dealerships billing activity may be entered here." />                                                                                                        
                    </td>
                </tr>
            </table>
            
        </td>
        <td>
            <asp:TextBox ID="txtAdditionalInformationBilling" runat="server" Width="300px" Height="50px" TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="CellLabel">
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        Billing Status Last Updated      
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtBillingStatusLastUpdated" runat="server" Text="Last date that the billable status was updated." />                                                                                                        
                    </td>
                </tr>
            </table>
            
        </td>
        <td>
            <%--<GAABackOfficeControlsCC:CalendarControl ID="ccBillingStatusLastUpdated" runat="server" >
                <MainTextBoxControl Width="150px" Enabled="false">
                </MainTextBoxControl>
            </GAABackOfficeControlsCC:CalendarControl>--%>
            <asp:UpdatePanel ID="upBillingStatusLastUpdated" runat="server">
                <ContentTemplate>
                    <GAABackOfficeControls:DatePicker ID="ccBillingStatusLastUpdated" runat="server" Enable="false" Width="150px" 
                                                        ImageUrl="~/Images/Calendar/cal.gif"/>    
                </ContentTemplate>
            </asp:UpdatePanel>    
        </td>
        <td class="CellLabel" style="padding-left:30px;">
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        Dealer Date Created      
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtDealerDateCreated" runat="server" Text="Date this dealership was first created." />                                                                                                        
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <%--<GAABackOfficeControlsCC:CalendarControl ID="ccDealerDateCreated" runat="server" >
                <MainTextBoxControl Width="150px" Enabled="false">
                </MainTextBoxControl>
            </GAABackOfficeControlsCC:CalendarControl>--%>
            <asp:UpdatePanel ID="upDealerDateCreated" runat="server">
                <ContentTemplate>
                    <GAABackOfficeControls:DatePicker ID="ccDealerDateCreated" runat="server" Enable="false" Width="150px" 
                                                        ImageUrl="~/Images/Calendar/cal.gif"/>    
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td class="CellLabel" style="padding-left:30px;">
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        System Billing Start Date      
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtSystemBillingStartDate" runat="server" Text="Automated billing date set to default to 60 days following the date this dealership was created." />                                                                                                        
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <%--<GAABackOfficeControlsCC:CalendarControl ID="ccSystemDealerStartDate" runat="server" >
                <MainTextBoxControl Width="150px" Enabled="false">
                </MainTextBoxControl>
            </GAABackOfficeControlsCC:CalendarControl>--%>
            <asp:UpdatePanel ID="upSystemDealerStartDate" runat="server">
                <ContentTemplate>
                    <GAABackOfficeControls:DatePicker ID="ccSystemDealerStartDate" runat="server" Enable="false" Width="150px" 
                                                        ImageUrl="~/Images/Calendar/cal.gif"/>    
                </ContentTemplate>
            </asp:UpdatePanel>    
        </td>
    </tr>
    <tr>
        <td class="CellLabel">
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        Setup fee      
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtSetupFee" runat="server" Text="Once off setup fee associated with setup of this dealership." />                                                                                                        
                    </td>
                </tr>
            </table>
            
        </td>
        <td>
            <%--<acw:NumericTextBox ID="ntbSetupFee" runat="server" Width="150"></acw:NumericTextBox>--%>
            <ew:NumericBox ID="ntbSetupFee" runat="server" Width="150" />
        </td>
        <td class="CellLabel" style="padding-left:30px;">
            
            <table border="0" cellpadding="0" cellspacing="3">
                <tr>
                    <td>
                        Admin Billing Start Date      
                    </td>
                    <td>
                        <GAABackOfficeControls:QuestionTip ID="qtAdminBillingStartDate" runat="server" Text="Date the administrator determines this dealership to become billable." />                                                                                                        
                    </td>
                </tr>
            </table>
        </td>
        <td>
            <%--<GAABackOfficeControlsCC:CalendarControl ID="ccAdminBillingStartDate" runat="server" >
                <MainTextBoxControl Width="150px">
                </MainTextBoxControl>
            </GAABackOfficeControlsCC:CalendarControl> --%>
            <asp:UpdatePanel ID="upAdminBillingStartDate" runat="server">
                <ContentTemplate>
                    <GAABackOfficeControls:DatePicker ID="ccAdminBillingStartDate" runat="server" Width="150px" 
                                                        ImageUrl="~/Images/Calendar/cal.gif"/>    
                </ContentTemplate>
            </asp:UpdatePanel>            
        </td>
    </tr>
    <tr>
        <td colspan="4" align="center">
             <table border="0" cellpadding="0" cellspacing="3" width="50%">
                <tr>
                    <td colspan="3" align="center">
                        <b>Dealer leads for the past 3 months</b>
                    </td>
                </tr>
                <tr>
                    <td ID="tdFirstMonth" runat="server" align="center"></td>
                    <td ID="tdSecondMonth" runat="server" align="center"></td>
                    <td ID="tdThirdMonth" runat="server" align="center"></td>
                </tr>
                <tr>
                    <td ID="tdFirstMonthTotal" runat="server" align="center">
                        0   
                    </td>
                    <td ID="tdSecondMonthTotal" runat="server" align="center">
                        0
                    </td>
                    <td ID="tdThirdMonthTotal" runat="server" align="center">
                        0
                    </td>
                </tr>
             </table>       
        </td>
    </tr>
</table>