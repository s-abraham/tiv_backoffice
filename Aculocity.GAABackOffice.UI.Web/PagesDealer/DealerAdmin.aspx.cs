﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Net.Mail;
using System.Text;
using System.Diagnostics;
using System.Linq;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class DealerAdmin : BasePage
    {
        private void log(Object obj)
        {
            Debugger.Log(0, "Test", "Message: " + obj.ToString() + "\n");
        }

        #region Page startup

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            MainContainer.Visible = false;
            CreateDealerStart.Visible = false;
            ChooseDealerGroup.Visible = false;
            LeadPanel.Enabled = false;
            trExcludeMakes.Visible = false;
        }

        private Boolean UserHasDealerKey()
        {
            //Set Dealer Key
            var user = new UserBLL();
            user.GetUserByUserName(Security.GetLoggedInUser().Trim());
            return user.DealerKey != null;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    //Set the dealerkey from the QueryString
                    ViewState["DealerKey"] = Request.QueryString["DealerKey"];

                    //check and assign the Dealerkey
                    ValidateDealerKey();

                    //Clear redirect path
                    Session["DealerAdmin_RedirectPath"] = string.Empty;

                    //Do all startup stuff before loading the dealer
                    LoadDealerGroups();
                    LoadCreditAppTexts();
                    LoadVehicleMakes();
                    LoadExcludedMakes();
                    LoadHNIDealers();
                    LoadAppraisalExpirationInDays();
                    LoadThemes();
                    LoadCarGalvesRangeCategories();
                    LoadTruckGalvesRangeCategories();
                    LoadLeadproviders();
                    LoadRepeaterData();
                    LoadEditLog();
                    SetPageDefaultValues();

                    //and finally, lets load the dealer if we have a DealerKey
                    if (!string.IsNullOrEmpty(ViewState["DealerKey"].ToString()))
                    {
                        if (LoadDealer())
                        {
                            LoadLeadsForThreeLastMonths();
                            MainContainer.CssClass += " dealer-edit";
                            if (
                                ( Security.GetCurrentUserGroup() == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin
                                  || Security.GetCurrentUserGroup() == Enumerations.AuthUserGroup.ResellerAdmin )
                                && UserHasDealerKey())
                            {
                                MainContainer.CssClass += " dealer-login";
                            }

                            MainContainer.Visible = true;
                            CreateDealerStart.Visible = false;

                            try
                            {
                                if (Request.QueryString["acttab"] != null)
                                {
                                    MainContainer.ActiveTab = MainContainer.Tabs[Convert.ToInt32(Request.QueryString["acttab"])];
                                }
                            }
                            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
                        }
                        else
                        {
                            MainContainer.CssClass += " dealer-create";
                            MainContainer.Visible = false;
                            CreateDealerStart.Visible = true;
                            BillingPanel.Enabled = false;
                        }
                    }

                    //Check if the dealer is a draft dealer being Activated
                    if (Session["IsDraft"] != null)
                    {
                        switch ((Boolean)Session["IsDraft"])
                        {
                            case true:
                                txtDealerName.Text = Session["DealerName"].ToString();
                                txtStreet.Text = Session["Street"].ToString();
                                txtCity.Text = Session["City"].ToString();
                                ddlStateID.SelectedValue = Session["State"].ToString();
                                txtZip.Text = Session["ZipCode"].ToString();
                                txtContactName.Text = Session["Contact"].ToString();
                                txtContactEmail.Text = Session["Email"].ToString();
                                txtContactPhone.Text = Session["Phone"].ToString();
                                txtEmailLeads1.Text = Session["LeadEmail"].ToString();
                                txtContactAppointment.Text = Session["Email"].ToString();
                                txtWebsiteCompany.Text = Session["WebsiteCompany"].ToString();
                                txtWebsiteContacEmail.Text = Session["WebsiteEmail"].ToString();
                                txtWebsiteContactPhone.Text = Session["WebsitePhone"].ToString();
                                break;

                            case false:
                                break;
                        }
                    }

                    //Setup the page based on logged in user
                    ApplySecurity();

                    //Set validation groups
                    SetValidetionGroups(chkActive.Checked);

                    //Set OnChange Handler
                    SetOnChangeHandlerForAllControls();
                }

                //Set OnChange Handler
                ccAdminBillingStartDate.DataChanged += DateChangedOnBillingPanel;
                ccBillingStatusLastUpdated.DataChanged += DateChangedOnBillingPanel;
                ccDealerDateCreated.DataChanged += DateChangedOnBillingPanel;
                ccSystemDealerStartDate.DataChanged += DateChangedOnBillingPanel;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                /////Hide certain elements based on the dynamic selections
                //data provider
                if (rblDataProvider.SelectedValue.Equals("NADA"))
                {
                    trAverageValueRange.Visible = true;
                    trGalvesRangeCategory.Visible = false;

                    if (_currentGroup.Equals(Enumerations.AuthUserGroup.AllDealerAdmin) || _currentGroup.Equals(Enumerations.AuthUserGroup.BSBConsultingFullAdmin) || _currentGroup.Equals(Enumerations.AuthUserGroup.HomeNetSuperUser) || _currentGroup.Equals(Enumerations.AuthUserGroup.HomeNetDealerAdmin))
                    {
                        lblHNIDelers.Visible = true;
                        lstHNIDealers.Visible = true;
                        lstFreeHNIDealers.Visible = true;
                        btnContinueHNIDeler.Visible = true;
                        btnRemoveHNIDealer.Visible = true;
                    }
                    else
                    {
                        lblHNIDelers.Visible = false;
                        lstHNIDealers.Visible = false;
                        lstFreeHNIDealers.Visible = false;
                        btnContinueHNIDeler.Visible = false;
                        btnRemoveHNIDealer.Visible = false;
                    }
                }
                else
                {
                    trGalvesRangeCategory.Visible = true;
                    trAverageValueRange.Visible = false;
                    lblHNIDelers.Visible = false;
                    lstHNIDealers.Visible = false;
                    lstFreeHNIDealers.Visible = false;
                    btnContinueHNIDeler.Visible = false;
                    btnRemoveHNIDealer.Visible = false;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        #region Page Security

        void ValidateDealerKey()
        {
            //Check if the dealer is updating or adding
            if (ViewState["DealerKey"] != null)
            {
                //Viewstate is not null, so it must have recieved something, check the dealerkey for an empty string
                if (!string.IsNullOrEmpty(ViewState["DealerKey"].ToString()))
                {
                    //There is a dealerKey in viewstate so setup for Updating
                    SetupButtonsForEdit();
                }
                else
                {
                    //The dealerKey is empty but instatiated, so lets try getting it from the querystring
                    ViewState["DealerKey"] = Request.QueryString["DealerKey"];
                }
            }
            else
            {
                //the DealerKey was never instatiated so lets setup for adding
                ClearAllControls();
                SetupButtonsForAdd();
            }
        }

        void ApplySecurity()
        {
            try
            {
                switch (_currentGroup)
                {
                    case Enumerations.AuthUserGroup.AllDealerAdmin:
                        SetSecurityForMainAdmin();
                        break;
                    case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                    case Enumerations.AuthUserGroup.ResellerAdmin:
                        SetSecurityForSingleAdmin();
                        break;
                    case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                        SetSecurityForDealerAdmin();
                        break;
                    case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                        SetSecurityForSingleAdmin();
                        break;
                    case Enumerations.AuthUserGroup.GalvesFullAdmin:
                        SetSecurityForDealerAdmin();
                        break;
                    case Enumerations.AuthUserGroup.HomeNetDealerAdmin:
                        SetSecurityForSingleHomeNetAdmin();
                        break;
                    case Enumerations.AuthUserGroup.HomeNetSuperUser:
                        SetSecurityForDealerAdmin();
                        break;
                    case Enumerations.AuthUserGroup.GroupDealerAdmin:
                        SetSecurityForGroupAdmin();
                        break;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SetSecurityForMainAdmin()
        {
            try
            {
                //Everything is visible for the main Admin;
                rblDataProvider.SelectedValue = "NADA";
                rblNADABranding.SelectedValue = "True";
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SetSecurityForDealerAdmin()
        {
            try
            {
                if (rblDataProvider.SelectedValue.Equals("NADA"))
                {
                    rblDataProvider.Items[0].Enabled = false;
                    rblDataProvider.Items.RemoveAt(0);
                    chkHyperLead.Visible = true;
                    lblHyperLead.Visible = true;
                    QuestionTip4.Visible = true;
                }
                else
                {
                    rblDataProvider.Items[1].Enabled = false;
                    rblDataProvider.Items.RemoveAt(1);
                    rblNADABranding.Visible = false;
                    chkHyperLead.Visible = false;
                    lblHyperLead.Visible = false;
                    QuestionTip4.Visible = false;
                    chkHyperLead.Checked = false;
                }

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SetSecurityForSingleHomeNetAdmin()
        {
            try
            {
                txtSalesPerson.Enabled = false;
                txtOriginalSalesPerson.Enabled = false;

                chkInstantOption.Enabled = false;
                chkEmailAppraisal.Enabled = false;
                chkFourStep.Enabled = false;
                chkNewSelection.Enabled = false;
                chkBalanceowed.Enabled = false;
                chkNewVehicleIncentives.Enabled = false;
                chkCurrentPayment.Enabled = false;
                chkUnsure.Enabled = false;
                chkRetailPricing.Enabled = false;
                chkTradeImage.Enabled = false;
                chkTradeVideoUpload.Enabled = false;
                chkHideTradein.Enabled = false;
                AuditPanel.Visible = false;

                BillingPanel.Visible = false;

                if (rblDataProvider.SelectedValue.Equals("NADA"))
                {
                    rblDataProvider.Items[0].Enabled = false;
                    rblDataProvider.Items.RemoveAt(0);
                }
                else
                {
                    rblDataProvider.Items[1].Enabled = false;
                    rblDataProvider.Items.RemoveAt(1);
                    rblNADABranding.Visible = false;
                }

            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        private void SetSecurityForSingleAdmin()
        {
            try
            {
                //Disabled Elements
                txtDealerName.Enabled = false;
                chkActive.Enabled = false;
                txtSalesPerson.Enabled = false;
                txtOriginalSalesPerson.Enabled = false;
                ddlDealerGroup.Enabled = false;
                txtStreet.Enabled = false;
                txtCity.Enabled = false;
                ddlStateID.Enabled = false;
                txtZip.Enabled = false;
                txtAutoGroup.Enabled = false;
                txtContactEmail.Enabled = false;
                txtContactName.Enabled = false;
                txtContactPhone.Enabled = false;
                txtContactAppointment.Enabled = false;
                txtContactPhone2.Enabled = false;
                txtFax.Enabled = false;
                txtContactTitle.Enabled = false;
                txtContactName2.Enabled = false;
                txtContactEmail2.Enabled = false;
                txtContactPhoneTwo.Enabled = false;
                txtContactPhonetwo2.Enabled = false;
                txtFax2.Enabled = false;
                txtContactTitle2.Enabled = false;
                txtWebsite.Enabled = false;
                rbProductType.Enabled = false;
                txtCreditAppUrl.Enabled = false;
                chkCreditActive.Enabled = false;
                ddlCreditAppText.Enabled = false;
                txtUsedinv.Enabled = false;
                chkUsedActive.Enabled = false;
                txtNewInv.Enabled = false;
                chkNewActive.Enabled = false;
                txtWebsite.Enabled = false;
                chkInstantOption.Enabled = false;
                chkEmailAppraisal.Enabled = false;
                chkFourStep.Enabled = false;
                chkNewSelection.Enabled = false;
                chkBalanceowed.Enabled = false;
                chkNewVehicleIncentives.Enabled = false;
                chkCurrentPayment.Enabled = false;
                chkUnsure.Enabled = false;
                chkRetailPricing.Enabled = false;
                chkTradeImage.Enabled = false;
                chkTradeVideoUpload.Enabled = false;
                txtGoogleUrchin.Enabled = false;
                ddlAppraisalExpiration.Enabled = false;
                txtComments.Enabled = false;
                rblGalvesRangeCategory.Enabled = false;
                rblTruckGalvesRangeCategory.Enabled = false;
                rblLang.Enabled = false;
                rblNADABranding.Enabled = false;
                rblDataProvider.Enabled = false;
                rblDataProvider.Visible = false;
                lblDataProvider.Visible = false;
                ddlThemes.Enabled = false;
                txtWebsiteCompany.Enabled = false;
                txtWebsiteContactName.Enabled = false;
                txtWebsiteContactPhone.Enabled = false;
                txtWebsiteContacEmail.Enabled = false;
                ddlProviderAndProduct.Enabled = false;
                txtLeadContactName.Enabled = false;
                txtLeadContactEmail.Enabled = false;
                txtLeadContactPhone.Enabled = false;
                txtLeadContactFax.Enabled = false;
                chkBillable.Enabled = false;
                txtMonthlyBilling.Enabled = false;
                txtAdditionalInformationBilling.Enabled = false;
                ccBillingStatusLastUpdated.Enable = false;
                ccDealerDateCreated.Enable = false;
                ccSystemDealerStartDate.Enable = false;
                txtAdminSetupFee.Enabled = false;
                ccAdminBillingStartDate.Enable = false;
                txtReason.Enabled = false;
                lstMakes.Enabled = false;
                lstHNIDealers.Enabled = false;

                LowSlider1.Enabled = false;
                LowSliderDollar1.Enabled = false;
                HighSlider1.Enabled = false;
                HighSliderDollar1.Enabled = false;
                rbtnLowPercent1.Enabled = false;
                rbtnLowValue1.Enabled = false;
                rbtnHighPercent1.Enabled = false;
                rbtnHighValue1.Enabled = false;

                LowSlider2.Enabled = false;
                LowSliderDollar2.Enabled = false;
                HighSlider2.Enabled = false;
                HighSliderDollar2.Enabled = false;
                rbtnLowPercent2.Enabled = false;
                rbtnLowValue2.Enabled = false;
                rbtnHighPercent2.Enabled = false;
                rbtnHighValue2.Enabled = false;

                LowSlider3.Enabled = false;
                LowSliderDollar3.Enabled = false;
                HighSlider3.Enabled = false;
                HighSliderDollar3.Enabled = false;
                rbtnLowPercent3.Enabled = false;
                rbtnLowValue3.Enabled = false;
                rbtnHighPercent3.Enabled = false;
                rbtnHighValue3.Enabled = false;
                AuditPanel.Visible = false;

                chkHyperLead.Enabled = false;

                if (rblDataProvider.SelectedValue.Equals("NADA"))
                {
                    chkHyperLead.Visible = true;
                    lblHyperLead.Visible = true;
                    QuestionTip4.Visible = true;
                }
                else
                {
                    chkHyperLead.Visible = false;
                    lblHyperLead.Visible = false;
                    QuestionTip4.Visible = false;
                    chkHyperLead.Checked = false;
                }

                btnContinueHNIDeler.Enabled = false;
                btnContinueMake.Enabled = false;
                btnRemoveMake.Enabled = false;
                btnRemoveHNIDealer.Enabled = false;

                //disable the add buttons
                btnContinue1.Enabled = false;
                btnContinue2.Enabled = false;
                btnContinue3.Enabled = false;
                btnContinue4.Enabled = false;
                btnContinue5.Enabled = false;
                btnContinue6.Enabled = false;

                //Disable the save buttons
                btnSaveSettingsTab1.Enabled = true;
                btnSaveSettingsTab2.Enabled = true;
                btnSaveSettingsTab3.Enabled = true;
                btnSaveSettingsTab4.Enabled = false;
                btnSaveSettingsTab5.Enabled = false;
                btnSaveSettingsTab6.Enabled = true;
                btnSaveSettingsTab7.Enabled = false;

                try
                {
                    if (rblDataProvider.SelectedValue.Equals("NADA"))
                    {
                        chkHyperLead.Visible = true;
                        lblHyperLead.Visible = true;
                        QuestionTip4.Visible = true;
                    }
                    else
                    {
                        chkHyperLead.Visible = false;
                        lblHyperLead.Visible = false;
                        QuestionTip4.Visible = false;
                        chkHyperLead.Checked = false;
                    }

                }
                catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SetSecurityForGroupAdmin()
        {
            try
            {
                //Disable tabs that cannot be viewed
                BillingPanel.Enabled = false;
                WebsitePanel.Enabled = false;

                //Disabled Elements
                txtDealerName.Enabled = false;
                chkActive.Enabled = false;
                txtSalesPerson.Enabled = false;
                txtOriginalSalesPerson.Enabled = false;
                ddlDealerGroup.Enabled = false;
                txtStreet.Enabled = false;
                txtCity.Enabled = false;
                ddlStateID.Enabled = false;
                txtZip.Enabled = false;
                txtAutoGroup.Enabled = false;
                txtContactName2.Enabled = false;
                txtContactEmail2.Enabled = false;
                txtContactPhoneTwo.Enabled = false;
                txtContactPhonetwo2.Enabled = false;
                txtFax2.Enabled = false;
                txtContactTitle2.Enabled = false;
                txtWebsite.Enabled = false;
                rbProductType.Enabled = false;
                ddlCreditAppText.Enabled = false;
                chkInstantOption.Enabled = false;
                chkEmailAppraisal.Enabled = false;
                chkFourStep.Enabled = false;
                chkNewSelection.Enabled = false;
                chkBalanceowed.Enabled = false;
                chkNewVehicleIncentives.Enabled = false;
                chkCurrentPayment.Enabled = false;
                chkUnsure.Enabled = false;
                chkRetailPricing.Enabled = false;
                chkTradeImage.Enabled = false;
                chkTradeVideoUpload.Enabled = false;
                txtGoogleUrchin.Enabled = false;
                txtComments.Enabled = false;
                rblGalvesRangeCategory.Enabled = false;
                rblTruckGalvesRangeCategory.Enabled = false;
                rblDataProvider.Enabled = false;
                lblDataProvider.Visible = false;
                rblDataProvider.Visible = false;
                txtWebsiteCompany.Enabled = false;
                txtWebsiteContactName.Enabled = false;
                txtWebsiteContactPhone.Enabled = false;
                txtWebsiteContacEmail.Enabled = false;
                ddlProviderAndProduct.Enabled = false;
                txtLeadContactName.Enabled = false;
                txtLeadContactEmail.Enabled = false;
                txtLeadContactPhone.Enabled = false;
                txtLeadContactFax.Enabled = false;
                chkBillable.Enabled = false;
                txtMonthlyBilling.Enabled = false;
                txtAdditionalInformationBilling.Enabled = false;
                ccBillingStatusLastUpdated.Enable = false;
                ccDealerDateCreated.Enable = false;
                ccSystemDealerStartDate.Enable = false;
                txtAdminSetupFee.Enabled = false;
                ccAdminBillingStartDate.Enable = false;
                txtReason.Enabled = false;
                lblHNIDelers.Visible = false;
                lstHNIDealers.Visible = false;
                lstFreeHNIDealers.Visible = false;
                btnRemoveHNIDealer.Visible = false;
                btnContinueHNIDeler.Visible = false;
                rblDataProvider.Visible = false;
                lblDataProvider.Visible = false;
                AuditPanel.Visible = false;

                //RONSMAP hyper leads
                if (rblDataProvider.SelectedValue.Equals("NADA"))
                {
                    chkHyperLead.Visible = true;
                    lblHyperLead.Visible = true;
                    QuestionTip4.Visible = true;
                }
                else
                {
                    chkHyperLead.Visible = false;
                    lblHyperLead.Visible = false;
                    QuestionTip4.Visible = false;
                    chkHyperLead.Checked = false;
                }

                //disable the add buttons
                btnContinue1.Enabled = false;
                btnContinue2.Enabled = false;
                btnContinue3.Enabled = false;
                btnContinue4.Enabled = false;
                btnContinue5.Enabled = false;
                btnContinue6.Enabled = false;
                btnContinue7.Enabled = false;

                //Disable the save buttons
                btnSaveSettingsTab1.Enabled = true;
                btnSaveSettingsTab2.Enabled = true;
                btnSaveSettingsTab3.Enabled = true;

                btnSaveSettingsTab4.Enabled = false;
                btnSaveSettingsTab5.Enabled = true;
                btnSaveSettingsTab6.Enabled = false;
                btnSaveSettingsTab7.Enabled = false;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

            var dBLL = new DealerBLL();
            int DealerType = dBLL.GetDealerGroupByID(Convert.ToInt32(ddlDealerGroup.SelectedValue));
            if (DealerType.Equals(5))
            {
                rblNADABranding.SelectedValue = "0";
                rblNADABranding.SelectedValue = "False";
                rblNADABranding.Visible = false;
                lblNadaBranding.Visible = false;
            }
        }

        private void SetupButtonsForAdd()
        {
            try
            {

                //make the add buttons visible
                btnContinue1.Visible = true;
                btnContinue2.Visible = true;
                btnContinue3.Visible = true;
                btnContinue4.Visible = true;
                btnContinue5.Visible = true;
                btnContinue6.Visible = true;
                btnContinue7.Visible = true;

                //Hide the save buttons
                btnSaveSettingsTab1.Visible = false;
                btnSaveSettingsTab2.Visible = false;
                btnSaveSettingsTab3.Visible = false;
                btnSaveSettingsTab4.Visible = false;
                btnSaveSettingsTab5.Visible = false;
                btnSaveSettingsTab6.Visible = false;
                btnSaveSettingsTab7.Visible = false;

                //Generate a new GUID for the dealer
                if (ViewState["DealerKey"] == null)
                {
                    ViewState["DealerKey"] = Guid.NewGuid().ToString().Replace("-", String.Empty).ToUpper();
                    txtDealershipKey.Text = ViewState["DealerKey"].ToString();
                    //txtAppraisalURL.Text = SettingManager.AppURL + "/10.aspx?DealerKey=" + ViewState["DealerKey"];   ConfigurationManager.AppSettings["AppraisalInitialPage"]
                    txtAppraisalURL.Text = SettingManager.AppURL + SettingManager.AppInitialPage + ViewState["DealerKey"];
                    //txtAppraisalURL.Text = SettingManager.AppURL + ConfigurationManager.AppSettings["AppraisalInitialPage"] + ViewState["DealerKey"];
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SetupButtonsForEdit()
        {
            try
            {
                //hide the add buttons
                btnContinue1.Visible = false;
                btnContinue2.Visible = false;
                btnContinue3.Visible = false;
                btnContinue4.Visible = false;
                btnContinue5.Visible = false;
                btnContinue6.Visible = false;
                btnContinue7.Visible = false;

                //Show the save buttons
                btnSaveSettingsTab1.Visible = true;
                btnSaveSettingsTab2.Visible = true;
                btnSaveSettingsTab3.Visible = true;
                btnSaveSettingsTab4.Visible = true;
                btnSaveSettingsTab5.Visible = true;
                btnSaveSettingsTab6.Visible = true;
                btnSaveSettingsTab7.Visible = true;

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region Data Methods

        void LoadLeadsForThreeLastMonths()
        {
            try
            {
                var dBLL = new DealerBLL();

                DataTable table = dBLL.GetTriMonthlyLeads(ViewState["DealerKey"].ToString());

                tdFirstMonth.InnerText = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-3).Month);
                tdSecondMonth.InnerText = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-2).Month);
                tdThirdMonth.InnerText = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-1).Month);

                tdFirstMonthTotal.InnerText = "0";
                tdSecondMonthTotal.InnerText = "0";
                tdThirdMonthTotal.InnerText = "0";

                string value;
                tdFirstMonth.InnerText = string.IsNullOrEmpty(value = table.Rows[0][0].ToString()) ? CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-3).Month) : value;
                tdFirstMonthTotal.InnerText = string.IsNullOrEmpty(value = table.Rows[0][1].ToString()) ? "N/A" : value;

                tdSecondMonth.InnerText = string.IsNullOrEmpty(value = table.Rows[1][0].ToString()) ? CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-2).Month) : value;
                tdSecondMonthTotal.InnerText = string.IsNullOrEmpty(value = table.Rows[1][1].ToString()) ? "N/A" : value;

                tdThirdMonth.InnerText = string.IsNullOrEmpty(value = table.Rows[2][0].ToString()) ? CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.AddMonths(-1).Month) : value;
                tdThirdMonthTotal.InnerText = string.IsNullOrEmpty(value = table.Rows[2][1].ToString()) ? "N/A" : value;

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadDealerGroups()
        {
            try
            {
                var dealerGroups = new DealerGroups();
                dealerGroups.GetDealerGroupsByDealerAdmin(Security.GetGroupLoggedUser().ToString());

                ddlDealerGroup.Items.Clear();
                ddlDealerGroup.DataTextField = "Description";
                ddlDealerGroup.DataValueField = "ID";
                ddlDealerGroup.DataSource = dealerGroups;
                ddlDealerGroup.DataBind();
                ddlDealerGroup.Items.Insert(0, new ListItem { Text = "None", Value = "0" });

                ddlDealerGroup1.Items.Clear();
                ddlDealerGroup1.DataTextField = "Description";
                ddlDealerGroup1.DataValueField = "ID";
                ddlDealerGroup1.DataSource = dealerGroups;
                ddlDealerGroup1.DataBind();
                ddlDealerGroup1.Items.Insert(0, new ListItem { Text = "None", Value = "0" });
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadCreditAppTexts()
        {
            try
            {
                var dBLL = new DealerBLL();

                ddlCreditAppText.DataSource = dBLL.GetCreditAppTexts();
                ddlCreditAppText.DataValueField = "MsgID";
                ddlCreditAppText.DataTextField = "Description";
                ddlCreditAppText.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadVehicleMakes()
        {
            try
            {
                var dBLL = new DealerBLL();

                List<string> list = dBLL.GetAvailableDefaultMakes(ViewState["DealerKey"].ToString());
                lstNewMakes.Items.Clear();
                foreach (string item in list)
                {
                    lstNewMakes.Items.Add(item);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadHNIDealers()
        {
            try
            {
                var dBLL = new DealerBLL();

                List<string> list = dBLL.GetAvailableHNIFeeds(ViewState["DealerKey"].ToString());
                lstFreeHNIDealers.Items.Clear();
                foreach (string item in list)
                {
                    lstFreeHNIDealers.Items.Add(item);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadExcludedMakes()
        {
            try
            {
                var dBLL = new DealerBLL();

                lstExcludeMakesL.Items.Clear();
                lstExcludeMakesL.DataSource = dBLL.GetAvailableExcludedUsedCarMakes(string.IsNullOrEmpty(ViewState["DealerKey"].ToString()) ? "zzzzzzzzzzzzz" : ViewState["DealerKey"].ToString());
                lstExcludeMakesL.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadAppraisalExpirationInDays()
        {
            try
            {
                for (int i = 1; i < 31; i++)
                {
                    ddlAppraisalExpiration.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void LoadThemes()
        {
            try
            {
                ddlThemes.Items.Add(new ListItem("RedGray", "RedGray"));
                ddlThemes.Items.Add(new ListItem("BlueGray", "BlueGray"));
                ddlThemes.Items.Add(new ListItem("GreenGray", "GreenGray"));
                ddlThemes.Items.Add(new ListItem("DarkGray", "DarkGray"));
                ddlThemes.Items.Add(new ListItem("YellowGray", "YellowGray"));
                ddlThemes.Items.Add(new ListItem("YellowGrayBlackBg", "YellowGrayBlackBg"));
                ddlThemes.Items.Add(new ListItem("YellowGrayGrayBg", "YellowGrayGrayBg"));
                ddlThemes.Items.Add(new ListItem("RedGrayBlackBG", "RedGrayBlackBG"));
                ddlThemes.Items.Add(new ListItem("BMW", "BMW"));
                ddlThemes.Items.Add(new ListItem("Purple", "Purple"));
                ddlThemes.Items.Add(new ListItem("Carsoup", "Carsoup"));

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        void LoadCarGalvesRangeCategories()
        {
            try
            {
                var dBLL = new DealerBLL();
                rblGalvesRangeCategory.DataSource = dBLL.GetGalvesCarRanges();
                rblGalvesRangeCategory.DataValueField = "Category";
                rblGalvesRangeCategory.DataTextField = "Description";
                rblGalvesRangeCategory.DataBind();
                rblGalvesRangeCategory.SelectedValue = "1";
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadTruckGalvesRangeCategories()
        {
            try
            {
                var dBLL = new DealerBLL();
                rblTruckGalvesRangeCategory.DataSource = dBLL.GetGalvesTruckRanges();
                rblTruckGalvesRangeCategory.DataValueField = "Category";
                rblTruckGalvesRangeCategory.DataTextField = "Description";
                rblTruckGalvesRangeCategory.DataBind();
                rblTruckGalvesRangeCategory.SelectedValue = "1";
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadLeadproviders()
        {
            try
            {
                var dBLL = new DealerBLL();
                ddlProviderAndProduct.Items.Clear();

                List<string> providers = dBLL.GetAllProviders();

                ddlProviderAndProduct.DataSource = providers;
                ddlProviderAndProduct.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void SetPageDefaultValues()
        {
            try
            {
                bool isAdmin = (Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.BSBConsultingFullAdmin)
                    || (Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.AllDealerAdmin)
                ;
                trIsReseller.Visible = isAdmin;

                chkActive.Checked = true;
                chkEnableWidget.Checked = true;
                chkIsReseller.Checked = false;
                chkEnableValueRange.Checked = true;
                ddlAppraisalExpiration.SelectedValue = "14";
                rblLang.SelectedValue = "eng";
                rblLeadFormat1.SelectedValue = "XML";
                rblLeadFormat2.SelectedValue = "XML";
                rblLeadFormat3.SelectedValue = "XML";
                rblLeadFormat4.SelectedValue = "XML";
                chkBillable.Checked = true;
                txtReason.Visible = false;
                ccBillingStatusLastUpdated.Text = DateTime.Now.ToString();
                ccDealerDateCreated.Text = DateTime.Now.ToString();
                ccSystemDealerStartDate.Text = DateTime.Now.AddDays(60).ToString();
                txtMonthlyBilling.Text = "199";
                txtAdminSetupFee.Text = "0";
                chkActive.Checked = true;

                rbtnDefaultBackground.SelectedValue = "White";
                chkInstantOption.Checked = true;
                chkNewSelection.Checked = true;
                chkNewVehicleIncentives.Checked = true;
                chkHideTradein.Checked = true;
                chkTaxIncentive.Checked = true;
                chkTradeImage.Checked = true;
                chkTradeVideoUpload.Checked = true;
                ddlAppraisalExpiration.SelectedValue = "14";
                rblLang.SelectedValue = "eng";
                rblLeadFormat1.SelectedValue = "Text";
                rblLeadFormat2.SelectedValue = "XML";
                rblLeadFormat3.SelectedValue = "XML";
                rblLeadFormat4.SelectedValue = "XML";
                chkBillable.Checked = true;
                txtReason.Visible = false;
                ccBillingStatusLastUpdated.Text = DateTime.Now.ToString();
                ccDealerDateCreated.Text = DateTime.Now.ToString();
                ccSystemDealerStartDate.Text = DateTime.Now.AddDays(60).ToString();

                rbtnLowPercent1.Checked = true;
                rbtnLowValue1.Checked = false;
                rbtnHighPercent1.Checked = true;
                rbtnHighValue1.Checked = false;
                LowSlider1.Text = "-20";
                HighSlider1.Text = "0";
                LowSliderDollar1.Text = "-1000";
                HighSliderDollar1.Text = "1000";

                rbtnLowPercent2.Checked = true;
                rbtnLowValue2.Checked = false;
                rbtnHighPercent2.Checked = true;
                rbtnHighValue2.Checked = false;
                LowSlider2.Text = "-15";
                HighSlider2.Text = "0";
                LowSliderDollar2.Text = "-1000";
                HighSliderDollar2.Text = "1000";

                rbtnLowPercent3.Checked = true;
                rbtnLowValue3.Checked = false;
                rbtnHighPercent3.Checked = true;
                rbtnHighValue3.Checked = false;
                LowSlider3.Text = "-10";
                HighSlider3.Text = "5";
                LowSliderDollar3.Text = "-1000";
                HighSliderDollar3.Text = "1000";

                txtAdminSetupFee.Text = "0";

                foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        CheckBox chkModelYear = item.FindControl("chkModelYear") as CheckBox;
                        if (chkModelYear != null)
                        {
                            if (chkModelYear.Text.Equals("2011") || chkModelYear.Text.Equals("2010"))
                            {
                                // ReSharper disable PossibleNullReferenceException
                                (item.FindControl("chkModelYear") as CheckBox).Checked = true;
                                // ReSharper restore PossibleNullReferenceException
                            }
                        }
                    }

                switch (_currentGroup)
                {
                    case Enumerations.AuthUserGroup.AllDealerAdmin:
                        rblDataProvider.SelectedValue = "NADA";
                        trGalvesRangeCategory.Visible = false;
                        trAverageValueRange.Visible = true;
                        rblNADABranding.SelectedValue = "1";
                        rblNADABranding.SelectedValue = "True";
                        break;
                    case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                        rblDataProvider.SelectedValue = "NADA";
                        trGalvesRangeCategory.Visible = false;
                        trAverageValueRange.Visible = true;
                        rblNADABranding.SelectedValue = "1";
                        rblNADABranding.SelectedValue = "True";
                        break;
                    case Enumerations.AuthUserGroup.GalvesFullAdmin:
                        rblDataProvider.SelectedValue = "GALVES";
                        trGalvesRangeCategory.Visible = true;
                        trAverageValueRange.Visible = false;
                        rblNADABranding.SelectedValue = "0";
                        rblNADABranding.SelectedValue = "False";
                        lblNadaBranding.Visible = false;
                        break;
                    case Enumerations.AuthUserGroup.HomeNetSuperUser:
                        rblDataProvider.SelectedValue = "NADA";
                        trGalvesRangeCategory.Visible = false;
                        trAverageValueRange.Visible = true;
                        rblNADABranding.SelectedValue = "1";
                        rblNADABranding.SelectedValue = "True";
                        ccAdminBillingStartDate.Text = Convert.ToString(DateTime.Today.AddDays(45));
                        break;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        bool LoadDealer()
        {
            try
            {
                var dBLL = new DealerBLL();

                DataTable dealer = dBLL.LoadDealer(ViewState["DealerKey"].ToString());
                DataTable FacebookSettings = dBLL.LoadDealerFacebookSettings(ViewState["DealerKey"].ToString());
                //Added by Freddy K 12/06/2012
                //Do this only if no dealers has been found, means we are adding a new dealer
                if (dealer.Rows.Count == 0)
                {
                    return false;
                }

                foreach (DataRow row in dealer.Rows)
                {
                    DealerHolder OriginalDealer = new DealerHolder();

                    //Dealer Info
                    OriginalDealer.DealerName = txtDealerName.Text = row["DealerName"].ToString();

                    //Set the Master Page element
                    Label MasterLabel = (Label)Form.Parent.FindControl("lblTitle");
                    MasterLabel.Text = "Dealer Administration - " + row["DealerName"];

                    //Carry on populating
                    chkActive.Checked = Convert.ToBoolean(row["Active"].ToString());
                    OriginalDealer.Active = chkActive.Checked;


                    if (chkActive.Checked)
                    {
                        lblIsActive.Value = "yes";
                    }
                    else
                    {
                        lblIsActive.Value = "no";
                    }

                    //Enable Widget
                    OriginalDealer.EnableWidget = Convert.ToBoolean(row["EnableWidget"].ToString());
                    OriginalDealer.IsReseller = Convert.ToBoolean(row["IsReseller"].ToString());

                    chkEnableWidget.Checked = OriginalDealer.EnableWidget;
                    chkIsReseller.Checked = OriginalDealer.IsReseller;

                    //if (!row.IsNull("EnableWidget") && ((bool)row["EnableWidget"]) == true)
                    //{
                    //    chkEnableWidget.Checked = true;
                    //}

                    OriginalDealer.Salesperson = txtSalesPerson.Text = row["SalesPerson"].ToString();
                    OriginalDealer.OriginalSalesPerson = txtOriginalSalesPerson.Text = row["OriginalSalesPerson"].ToString();
                    ddlDealerGroup.SelectedValue = row["DealerGroup_Id"].ToString();
                    OriginalDealer.GroupID = Convert.ToInt32(ddlDealerGroup.SelectedValue);
                    OriginalDealer.DealerStreet = txtStreet.Text = row["Dealerstreet"].ToString();
                    OriginalDealer.DealerCity = txtCity.Text = row["DealerCity"].ToString();
                    OriginalDealer.DealerState = ddlStateID.SelectedValue = row["DealerState"].ToString();
                    OriginalDealer.DealerZip = txtZip.Text = row["DealerZip"].ToString();
                    OriginalDealer.DealerAutoGroup = txtAutoGroup.Text = row["DealerAutoGroup"].ToString();

                    //Mandatory contact
                    OriginalDealer.DealerContactName = txtContactName.Text = row["DealerContactName"].ToString();
                    OriginalDealer.DealerContactEmail = txtContactEmail.Text = row["DealerContactEmail"].ToString();
                    OriginalDealer.DealerContactPhone = txtContactPhone.Text = row["DealerContactPhone"].ToString();
                    OriginalDealer.AppointmentEmail = txtContactAppointment.Text = row["AppointmentEmail"].ToString();
                    OriginalDealer.DealerContactPhone2 = txtContactPhone2.Text = row["DealerContactPhone2"].ToString();
                    OriginalDealer.DealerContactFax = txtFax.Text = row["DealerContactfax"].ToString();
                    OriginalDealer.DealerTitle = txtContactTitle.Text = row["DealerTitle"].ToString();

                    //secondary contact
                    OriginalDealer.SecondDealerContactName = txtContactName2.Text = row["SecondDealerContactName"].ToString();
                    OriginalDealer.SecondDealerContactEmail = txtContactEmail2.Text = row["SecondDealerContactEmail"].ToString();
                    OriginalDealer.SecondDealerContactPhone = txtContactPhoneTwo.Text = row["SecondDealerContactPhone"].ToString();
                    OriginalDealer.SecondDealerContactPhone2 = txtContactPhonetwo2.Text = row["SecondDealerContactPhone2"].ToString();
                    OriginalDealer.SecondDealerContactFax = txtFax2.Text = row["SecondDealerContactfax"].ToString();
                    OriginalDealer.SecondDealerTitle = txtContactTitle2.Text = row["SecondDealerTitle"].ToString();
                    OriginalDealer.DealerWebSite = txtWebsite.Text = row["Dealerwebsite"].ToString();

                    rbProductType.SelectedValue = row["ProductType"].ToString();
                    OriginalDealer.DealerProductType = Convert.ToInt32(rbProductType.SelectedValue);


                    //customization
                    //txtAppraisalURL.Text = SettingManager.AppURL + "/10.aspx?dealer_Key=" + ViewState["DealerKey"];
                    txtAppraisalURL.Text = SettingManager.AppURL + SettingManager.AppInitialPage + ViewState["DealerKey"];
                    OriginalDealer.CreditApp = txtCreditAppUrl.Text = row["CreditApp"].ToString();

                    String CreditActive = row["CreditEnabled"].ToString();
                    if (CreditActive.Equals(""))
                    {
                        OriginalDealer.CreditEnabled = chkCreditActive.Checked = false;
                    }
                    else
                    {
                        OriginalDealer.CreditEnabled = chkCreditActive.Checked = Convert.ToBoolean(CreditActive);
                    }

                    ddlCreditAppText.SelectedValue = row["CreditAppText_Id"].ToString();
                    OriginalDealer.CreditMsg = Convert.ToInt32(ddlCreditAppText.SelectedValue);

                    OriginalDealer.InvUsed = txtUsedinv.Text = row["InvUsed"].ToString();

                    String UsedActive = row["InvUsedEnabled"].ToString();
                    if (UsedActive.Equals(""))
                    {
                        OriginalDealer.InvUsedEnabled = chkUsedActive.Checked = false;
                    }
                    else
                    {
                        OriginalDealer.InvUsedEnabled = chkUsedActive.Checked = Convert.ToBoolean(UsedActive);
                    }

                    OriginalDealer.InvNew = txtNewInv.Text = row["InvNew"].ToString();

                    String NewActive = row["InvNewEnabled"].ToString();
                    if (NewActive.Equals(""))
                    {
                        OriginalDealer.InvNewEnabled = chkNewActive.Checked = false;
                    }
                    else
                    {
                        OriginalDealer.InvNewEnabled = chkNewActive.Checked = Convert.ToBoolean(NewActive);
                    }

                    OriginalDealer.InstantOptionPricing = chkInstantOption.Checked = Convert.ToBoolean(row["InstantOptionPricing"].ToString());
                    OriginalDealer.EmailAppraisal = chkEmailAppraisal.Checked = Convert.ToBoolean(row["EmailAppraisal"].ToString());
                    OriginalDealer.FourStep = chkFourStep.Checked = Convert.ToBoolean(row["FourStep"].ToString());
                    OriginalDealer.NewSelection = chkNewSelection.Checked = Convert.ToBoolean(row["NewSelection"].ToString());
                    OriginalDealer.ShowAmountOwed = chkBalanceowed.Checked = Convert.ToBoolean(row["ShowAmountOwed"].ToString());
                    OriginalDealer.ShowIncentives = chkNewVehicleIncentives.Checked = Convert.ToBoolean(row["ShowIncentives"].ToString());
                    OriginalDealer.ShowMonthlyPayment = chkCurrentPayment.Checked = Convert.ToBoolean(row["ShowMonthlyPayment"].ToString());
                    OriginalDealer.UnsureOption = chkUnsure.Checked = Convert.ToBoolean(row["UnsureOption"].ToString());
                    OriginalDealer.RetailPricing = chkRetailPricing.Checked = Convert.ToBoolean(row["Retailpricing"].ToString());
                    OriginalDealer.TradeImageUpload = chkTradeImage.Checked = Convert.ToBoolean(row["TradeImageUpload"].ToString());
                    OriginalDealer.TradeVideoUpload = chkTradeVideoUpload.Checked = Convert.ToBoolean(row["TradeVideoUpload"].ToString());
                    OriginalDealer.GoogleUrchin = txtGoogleUrchin.Text = row["GoogleUrchin"].ToString();
                    ddlAppraisalExpiration.SelectedValue = row["AppraisalExpiration"].ToString();
                    OriginalDealer.AppraisalExpiration = Convert.ToInt32(ddlAppraisalExpiration.SelectedValue);
                    OriginalDealer.Comments = txtComments.Text = row["Comments"].ToString();
                    SetRepeaterValues(row["DealerModelYears_NewModelYears"].ToString());
                    OriginalDealer.NewModelYears = row["DealerModelYears_NewModelYears"].ToString();
                    OriginalDealer.HideTradeIn = chkHideTradein.Checked = Convert.ToBoolean(row["HideTradeIn"].ToString());
                    OriginalDealer.HyperLead = chkHyperLead.Checked = Convert.ToBoolean(row["HyperLead"].ToString());
                    OriginalDealer.TaxIncentive = chkTaxIncentive.Checked = Convert.ToBoolean(row["TaxIncentive"].ToString());
                    OriginalDealer.SocialMedia = chkSocialSharing.Checked = Convert.ToBoolean(row["Socialmedia"].ToString());

                    OriginalDealer.Defaultbackground = rbtnDefaultBackground.SelectedValue = row["Defaultbackground"].ToString();

                    //data provider and ranges
                    rblGalvesRangeCategory.SelectedValue = row["GalvesRangeCategory_Id"].ToString();

                    if (!String.IsNullOrEmpty(row["GalvesRangeCategory_Id"].ToString()))
                    {
                        OriginalDealer.GalvesRangeCategory = Convert.ToInt32(row["GalvesRangeCategory_Id"].ToString());
                    }
                    else
                    {
                        OriginalDealer.GalvesRangeCategory = 0;
                    }

                    rblTruckGalvesRangeCategory.SelectedValue = row["TruckGalvesRangeCategory_Id"].ToString();
                    if (!String.IsNullOrEmpty(row["TruckGalvesRangeCategory_Id"].ToString()))
                    {
                        OriginalDealer.TruckGalvesRangeCategory = Convert.ToInt32(row["TruckGalvesRangeCategory_Id"].ToString());
                    }
                    else
                    {
                        OriginalDealer.TruckGalvesRangeCategory = 0;
                    }
                    OriginalDealer.Language = rblLang.SelectedValue = row["Language"].ToString();
                    rblNADABranding.SelectedValue = row["NADABranding"].ToString();
                    OriginalDealer.NADABranding = Convert.ToBoolean(rblNADABranding.SelectedValue);
                    OriginalDealer.DataProvider = rblDataProvider.SelectedValue = row["Dataprovider"].ToString();

                    //ValueRange enabled

                    OriginalDealer.EnableValueRanges = Convert.ToBoolean(row["EnableValueRanges"].ToString());

                    if (!row.IsNull("EnableValueRanges") && ((bool)row["EnableValueRanges"]) == true)
                    {
                        chkEnableValueRange.Checked = true;
                    }
                    disableValueRanges(OriginalDealer.EnableValueRanges);
                    //Value range 1
                    OriginalDealer.AverageValueRange_VUnderPercent1 = LowSlider1.Text = string.IsNullOrEmpty(row["AverageValueRange_VUnderPercent1"].ToString()) ? LowSlider1.Text : row["AverageValueRange_VUnderPercent1"].ToString();
                    OriginalDealer.AverageValueRange_VUnderAmount1 = LowSliderDollar1.Text = string.IsNullOrEmpty(row["AverageValueRange_VUnderAmount1"].ToString()) ? LowSliderDollar1.Text : row["AverageValueRange_VUnderAmount1"].ToString();
                    OriginalDealer.AverageValueRange_VOverPercent1 = HighSlider1.Text = string.IsNullOrEmpty(row["AverageValueRange_VOverPercent1"].ToString()) ? HighSlider1.Text : row["AverageValueRange_VOverPercent1"].ToString();
                    OriginalDealer.AverageValueRange_VOverAmount1 = HighSliderDollar1.Text = string.IsNullOrEmpty(row["AverageValueRange_VOverAmount1"].ToString()) ? HighSliderDollar1.Text : row["AverageValueRange_VOverAmount1"].ToString();
                    switch (row["AverageValueRange_VUnderTypeSelection1"].ToString())
                    {
                        case "$":
                            rbtnLowValue1.Checked = true;
                            rbtnLowPercent1.Checked = false;
                            break;
                        case "%":
                            rbtnLowPercent1.Checked = true;
                            rbtnLowValue1.Checked = false;
                            break;
                    }
                    switch (row["AverageValueRange_VOverTypeSelection1"].ToString())
                    {
                        case "$":
                            rbtnHighValue1.Checked = true;
                            rbtnHighPercent1.Checked = false;
                            break;
                        case "%":
                            rbtnHighPercent1.Checked = true;
                            rbtnHighValue1.Checked = false;
                            break;
                    }
                    OriginalDealer.AverageValueRange_VOverTypeSelection1 = row["AverageValueRange_VOverTypeSelection1"].ToString();
                    OriginalDealer.AverageValueRange_VUnderTypeSelection1 = row["AverageValueRange_VUnderTypeSelection1"].ToString();

                    //Value range 2
                    OriginalDealer.AverageValueRange_VUnderPercent2 = LowSlider2.Text = string.IsNullOrEmpty(row["AverageValueRange_VUnderPercent2"].ToString()) ? LowSlider2.Text : row["AverageValueRange_VUnderPercent2"].ToString();
                    OriginalDealer.AverageValueRange_VUnderAmount2 = LowSliderDollar2.Text = string.IsNullOrEmpty(row["AverageValueRange_VUnderAmount2"].ToString()) ? LowSliderDollar2.Text : row["AverageValueRange_VUnderAmount2"].ToString();
                    OriginalDealer.AverageValueRange_VOverPercent2 = HighSlider2.Text = string.IsNullOrEmpty(row["AverageValueRange_VOverPercent2"].ToString()) ? HighSlider2.Text : row["AverageValueRange_VOverPercent2"].ToString();
                    OriginalDealer.AverageValueRange_VOverAmount2 = HighSliderDollar2.Text = string.IsNullOrEmpty(row["AverageValueRange_VOverAmount2"].ToString()) ? HighSliderDollar2.Text : row["AverageValueRange_VOverAmount2"].ToString();
                    switch (row["AverageValueRange_VUnderTypeSelection2"].ToString())
                    {
                        case "$":
                            rbtnLowValue2.Checked = true;
                            rbtnLowPercent2.Checked = false;
                            break;
                        case "%":
                            rbtnLowPercent2.Checked = true;
                            rbtnLowValue2.Checked = false;
                            break;
                    }
                    switch (row["AverageValueRange_VOverTypeSelection2"].ToString())
                    {
                        case "$":
                            rbtnHighValue2.Checked = true;
                            rbtnHighPercent2.Checked = false;
                            break;
                        case "%":
                            rbtnHighPercent2.Checked = true;
                            rbtnHighValue2.Checked = false;
                            break;
                    }

                    OriginalDealer.AverageValueRange_VOverTypeSelection2 = row["AverageValueRange_VOverTypeSelection2"].ToString();
                    OriginalDealer.AverageValueRange_VUnderTypeSelection2 = row["AverageValueRange_VUnderTypeSelection2"].ToString();

                    //Value range 3
                    OriginalDealer.AverageValueRange_VUnderPercent3 = LowSlider3.Text = string.IsNullOrEmpty(row["AverageValueRange_VUnderPercent3"].ToString()) ? LowSlider3.Text : row["AverageValueRange_VUnderPercent3"].ToString();
                    OriginalDealer.AverageValueRange_VUnderAmount3 = LowSliderDollar3.Text = string.IsNullOrEmpty(row["AverageValueRange_VUnderAmount3"].ToString()) ? LowSliderDollar3.Text : row["AverageValueRange_VUnderAmount3"].ToString();
                    OriginalDealer.AverageValueRange_VOverPercent3 = HighSlider3.Text = string.IsNullOrEmpty(row["AverageValueRange_VOverPercent3"].ToString()) ? HighSlider3.Text : row["AverageValueRange_VOverPercent3"].ToString();
                    OriginalDealer.AverageValueRange_VOverAmount3 = HighSliderDollar3.Text = string.IsNullOrEmpty(row["AverageValueRange_VOverAmount3"].ToString()) ? HighSliderDollar3.Text : row["AverageValueRange_VOverAmount3"].ToString();
                    switch (row["AverageValueRange_VUnderTypeSelection3"].ToString())
                    {
                        case "$":
                            rbtnLowValue3.Checked = true;
                            rbtnLowPercent3.Checked = false;
                            break;
                        case "%":
                            rbtnLowPercent3.Checked = true;
                            rbtnLowValue3.Checked = false;
                            break;
                    }
                    switch (row["AverageValueRange_VOverTypeSelection3"].ToString())
                    {
                        case "$":
                            rbtnHighValue3.Checked = true;
                            rbtnHighPercent3.Checked = false;
                            break;
                        case "%":
                            rbtnHighPercent3.Checked = true;
                            rbtnHighValue3.Checked = false;
                            break;
                    }

                    OriginalDealer.AverageValueRange_VOverTypeSelection3 = row["AverageValueRange_VOverTypeSelection3"].ToString();
                    OriginalDealer.AverageValueRange_VUnderTypeSelection3 = row["AverageValueRange_VUnderTypeSelection3"].ToString();

                    //website tab
                    OriginalDealer.Theme = ddlThemes.SelectedValue = row["Theme"].ToString();
                    OriginalDealer.WebsiteCompany = txtWebsiteCompany.Text = row["WebsiteCompany"].ToString();
                    OriginalDealer.WebsiteContactName = txtWebsiteContactName.Text = row["WebsiteContactName"].ToString();
                    OriginalDealer.WebsiteContactPhone = txtWebsiteContactPhone.Text = row["WebsiteContactPhone"].ToString();
                    OriginalDealer.WebsiteContactEmail = txtWebsiteContacEmail.Text = row["WebsiteContactEmail"].ToString();
                    OriginalDealer.WebsiteComments = txtWebsiteComments.Text = row["WebsiteComments"].ToString();

                    //lead management tab
                    //Commented out by Freddy K 12/06/12
                    OriginalDealer.DisableLeadEmails = chkDisableLeads.Checked = Convert.ToBoolean(row["DisableLeadEmails"].ToString());
                    OriginalDealer.LeadProvider = ddlProviderAndProduct.SelectedValue = row["LeadProvider"].ToString();
                    OriginalDealer.LeadEmail = txtEmailLeads1.Text = row["LeadEmail1"].ToString();
                    OriginalDealer.LeadEmail2 = txtEmailLeads2.Text = row["LeadEmail2"].ToString();
                    OriginalDealer.LeadEmail3 = txtEmailLeads3.Text = row["LeadEmail3"].ToString();
                    OriginalDealer.LeadEmail4 = txtEmailLeads4.Text = row["LeadEmail4"].ToString();
                    OriginalDealer.LeadFormat = rblLeadFormat1.SelectedValue = row["LeadFormat"].ToString();
                    OriginalDealer.LeadFormat2 = rblLeadFormat2.SelectedValue = row["LeadFormat2"].ToString();
                    OriginalDealer.LeadFormat3 = rblLeadFormat3.SelectedValue = row["LeadFormat3"].ToString();
                    OriginalDealer.LeadFormat4 = rblLeadFormat4.SelectedValue = row["LeadFormat4"].ToString();
                    OriginalDealer.LeadContactName = txtLeadContactName.Text = row["LeadContactName"].ToString();
                    OriginalDealer.LeadContactEmail = txtLeadContactEmail.Text = row["LeadContactEmail"].ToString();
                    OriginalDealer.LeadContactPhone = txtLeadContactPhone.Text = row["LeadContactPhone"].ToString();
                    OriginalDealer.LeadContactFax = txtLeadContactFax.Text = row["LeadContactFax"].ToString();
                    lblDealertrackID.Text = row["DealerIDHash"].ToString();
                    txtDealershipKey.Text = ViewState["DealerKey"].ToString();

                    //billing tab
                    OriginalDealer.Billable = chkBillable.Checked = Convert.ToBoolean(row["Billable"].ToString());
                    txtMonthlyBilling.Text = row["AdminAutoAppraisalCost"].ToString();
                    OriginalDealer.AdminAutoAppraisalCost = Convert.ToDecimal(txtMonthlyBilling.Text);
                    OriginalDealer.BillingExplanation = txtReason.Text = row["BillingExplanation"].ToString();

                    //billable reason
                    txtReason.Visible = !chkBillable.Checked;

                    ccBillingStatusLastUpdated.Text = row["BillingLastUpdated"].ToString();
                    OriginalDealer.BillingLastUpdated = Convert.ToDateTime(ccBillingStatusLastUpdated.Text);
                    ccDealerDateCreated.Text = row["DateCreated"].ToString();
                    OriginalDealer.DateCreated = Convert.ToDateTime(ccDealerDateCreated.Text);
                    ccSystemDealerStartDate.Text = row["SystemBillingDate"].ToString();
                    OriginalDealer.SystemBillingDate = Convert.ToDateTime(ccSystemDealerStartDate.Text);
                    txtAdminSetupFee.Text = row["AdminSetupFees"].ToString();
                    OriginalDealer.AdminSetupFees = Convert.ToDecimal(txtAdminSetupFee.Text);
                    ccAdminBillingStartDate.Text = row["BillableStartDate"].ToString();
                    //Added by freddy
                    //If adminbillingstartdate causes an exception processing the date, ignore for now
                    try
                    {
                        OriginalDealer.BillableStartDate = Convert.ToDateTime(ccAdminBillingStartDate.Text);
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                    }


                    OriginalDealer.BillingInfo = txtAdditionalInformationBilling.Text = row["BillingInfo"].ToString();

                    //dealer default makes
                    //lstMakes
                    List<string> defMakes = dBLL.GetDefaultMakes(ViewState["DealerKey"].ToString());
                    lstMakes.Items.Clear();
                    foreach (string defMake in defMakes)
                    {
                        lstMakes.Items.Add(defMake);
                    }

                    //HNI Dealers
                    int hniDealersAdded = 0;

                    //load the list of hni feeds
                    List<string> hniDealers = dBLL.GetHNIFeeds(ViewState["DealerKey"].ToString());
                    lstHNIDealers.Items.Clear();
                    foreach (string hniDealer in hniDealers)
                    {
                        lstHNIDealers.Items.Add(hniDealer);
                        hniDealersAdded++;
                    }

                    //load the list of excluded used car makes
                    List<string> excludedMakes = dBLL.GetExcludedUsedCarMakes(ViewState["DealerKey"].ToString());
                    lstExcludeMakesR.Items.Clear();
                    foreach (string make in excludedMakes)
                    {
                        lstExcludeMakesR.Items.Add(make);
                    }


                    if (hniDealersAdded == 0)
                    {
                        enableExcludedMakesContainer.Visible = true;
                        chkEnableExcludedMakes.Checked = true;
                        chkEnableExcludedMakes.Enabled = false;
                        trExcludeMakes.Visible = true;
                    }
                    else
                    {
                        chkEnableExcludedMakes.Checked = false;
                        enableExcludedMakesContainer.Visible = true;
                        trExcludeMakes.Visible = false;
                        if (!row.IsNull("EnableExcludedUsedCarMakes") && ((bool)row["EnableExcludedUsedCarMakes"]) == true)
                        {
                            chkEnableExcludedMakes.Checked = true;
                            trExcludeMakes.Visible = true;
                        }
                    }

                    if (_currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                    {
                        lstFreeHNIDealers.Items.Clear();
                        if (lstHNIDealers.Items.Count == 0)
                            lstFreeHNIDealers.Items.Add(new ListItem
                                                            {
                                                                Text = txtDealerName.Text.Trim(),
                                                                Value = txtDealerName.Text.Trim()
                                                            });
                    }
                    OriginalDealer.HNIDealers = row["HNIDealers"].ToString();

                    foreach (DataRow FBrow in FacebookSettings.Rows)
                    {
                        OriginalDealer.AppID = txtAppID.Text = FBrow["APPID"].ToString();
                        OriginalDealer.AppSecret = txtAppSecret.Text = FBrow["AppSecret"].ToString();
                        OriginalDealer.APIKey = txtAPIKey.Text = FBrow["APIKey"].ToString();
                        OriginalDealer.WallURL = txtwallpost.Text = FBrow["WallURL"].ToString();
                    }

                    ViewState["OriginalDealer"] = OriginalDealer;
                    ViewState["UpdatedDealer"] = OriginalDealer;
                }
                //disable the desired vehicle year if it has no models associated with it.
                //we will only do this if the dealer has selected a single make
                if (lstMakes.Items.Count == 1)
                {
                    List<int> Years = dBLL.GetNewMakeYears(lstMakes.Items[0].Text);

                    foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkModelYear = item.FindControl("chkModelYear") as CheckBox;
                            if (chkModelYear != null)
                            {
                                Boolean MatchingYear = false;

                                foreach (int listitem in Years)
                                {
                                    if (chkModelYear.Text.Equals(listitem.ToString()))
                                    {
                                        MatchingYear = true;
                                        // ReSharper disable PossibleNullReferenceException
                                        (item.FindControl("chkModelYear") as CheckBox).Enabled = true;
                                        // ReSharper restore PossibleNullReferenceException
                                    }
                                }

                                if (MatchingYear == false)
                                {
                                    // ReSharper disable PossibleNullReferenceException
                                    (item.FindControl("chkModelYear") as CheckBox).Checked = false;
                                    (item.FindControl("chkModelYear") as CheckBox).Enabled = false;
                                    // ReSharper restore PossibleNullReferenceException
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkModelYear = item.FindControl("chkModelYear") as CheckBox;
                            if (chkModelYear != null)
                            {
                                // ReSharper disable PossibleNullReferenceException
                                (item.FindControl("chkModelYear") as CheckBox).Enabled = true;
                                // ReSharper restore PossibleNullReferenceException

                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
            return false;
        }

        void LoadRepeaterData()
        {
            try
            {
                if (repNewReplacementVehicleModelYear.DataSource != null) return;

                var dBLL = new DealerBLL();
                repNewReplacementVehicleModelYear.DataSource = dBLL.GetAllReplacementYears();
                repNewReplacementVehicleModelYear.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void LoadEditLog()
        {
            try
            {
                DealerBLL dBLL = new DealerBLL();
                //grdEditLog.DataSource = dBLL.GetEditLog(ViewState["DealerKey"].ToString());
                //grdEditLog.DataBind();

                dgEditLog.DataSource = dBLL.GetEditLog(ViewState["DealerKey"].ToString());
                dgEditLog.DataBind();
                dgEditLog.Width = Unit.Percentage(100);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        string GetRepeaterValues()
        {
            var result = new StringBuilder();
            try
            {
                CheckBox chkModelYear;
                foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        // ReSharper disable PossibleNullReferenceException
                        if ((chkModelYear = item.FindControl("chkModelYear") as CheckBox).Checked)
                            // ReSharper restore PossibleNullReferenceException
                            if (chkModelYear != null) result.AppendFormat("{0},", chkModelYear.Text);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return result.ToString();
        }

        void SetRepeaterValues(string values)
        {
            try
            {
                string[] currentValues = values.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                CheckBox chkModelYear = null;
                foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        // ReSharper disable PossibleNullReferenceException
                        (chkModelYear = item.FindControl("chkModelYear") as CheckBox).Checked = Array.Exists(currentValues, currentItem => currentItem.Equals(chkModelYear.Text));
                        // ReSharper restore PossibleNullReferenceException
                    }
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region control events

        protected void MainContainer_ActiveTabChanged(object sender, EventArgs e)
        {
        }

        protected void SetDealerInGroup_Change(object sender, EventArgs e)
        {
            try
            {
                dealerGroupRow.Visible = DealerInGroup.SelectedValue == "true";
                ChooseDealerGroup.Visible = DealerInGroup.SelectedValue == "true";
                DealerGroupContinueBtn.Visible = true;
                if (ChooseDealerGroup.Visible)
                {
                    groupAdminRow.Visible = false;
                    switch (Security.GetGroupLoggedUser())
                    {
                        case Enumerations.AuthUserGroup.AllDealerAdmin:
                            groupAdminRow.Visible = true;
                            break;
                    }
                }

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void SetDealerGroup_Change(object sender, EventArgs e)
        {
            ddlDealerGroup.SelectedValue = ddlDealerGroup1.SelectedValue;
        }

        protected void AddDealerGroup_Click(object sender, EventArgs e)
        {
            string groupNameInsert = txtNewDealerGroup.Text;
            int dealerTypeIDInsert = 0;
            switch (Security.GetGroupLoggedUser())
            {
                case Enumerations.AuthUserGroup.AllDealerAdmin:
                    dealerTypeIDInsert = Convert.ToInt32(ddlGroupAdmin.SelectedValue);
                    break;
                case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                    dealerTypeIDInsert = (int)Enumerations.GroupDealerAdmin.BSB;
                    break;
                case Enumerations.AuthUserGroup.GalvesFullAdmin:
                    dealerTypeIDInsert = (int)Enumerations.GroupDealerAdmin.Galves;
                    break;
                case Enumerations.AuthUserGroup.HomeNetSuperUser:
                    dealerTypeIDInsert = (int)Enumerations.GroupDealerAdmin.HomeNet;
                    break;
            }

            if (groupNameInsert.Trim() == string.Empty) lblCreateGroupError.Text = "Please enter a group name.";
            else if (dealerTypeIDInsert == 0) lblCreateGroupError.Text = "Please select a group admin.";
            else
            {
                DealerGroupBLL dealerGroup = new DealerGroupBLL { Description = groupNameInsert, DealerTypeID = dealerTypeIDInsert };
                if (dealerGroup.Insert() < 0)
                {
                    lblCreateGroupError.Text = "The group entered already exists.";
                }
                else
                {
                    lblCreateGroupError.Text = string.Empty;
                    LoadDealerGroups();
                    ddlDealerGroup.Items.FindByText(groupNameInsert).Selected = true;
                    ddlDealerGroup1.Items.FindByText(groupNameInsert).Selected = true;
                    txtNewDealerGroup.Text = string.Empty;
                }
            }

            if (lblCreateGroupError.Text.Trim() != string.Empty)
            {
                createGroupErrorRow.Visible = true;
            }
            else
            {
                createGroupErrorRow.Visible = false;
            }

            if (ChooseDealerGroup.Visible == false)
            {
                SetDealerInGroup_Change(sender, e);
            }
        }

        protected void ValidateDealerGroup_Click(object sender, EventArgs e)
        {
            Boolean dealerInGroup = (DealerInGroup.SelectedValue == "true");
            Boolean showForm = !dealerInGroup;
            if (dealerInGroup)
            {
                if (ddlDealerGroup1.SelectedValue == "0")
                {
                    pnlDealerGroupError.Visible = true;
                    lblDealerGroupError.Text = "You must Choose a Dealer Group before you can continue.";
                    dealerGroupRow.Visible = true;
                    CreateDealerStart.Visible = true;
                    ChooseDealerGroup.Visible = true;
                    MainContainer.Visible = false;
                }
                else
                {
                    dealerGroupRow.Visible = true;
                    showForm = true;
                }
            }
            if (showForm)
            {
                CreateDealerStart.Visible = false;
                ChooseDealerGroup.Visible = false;
                MainContainer.Visible = true;
            }
        }

        protected void Back(object sender, EventArgs e)
        {
            try
            {
                if (_currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                {
                    Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                }
                else
                {
                    Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
                }
                Session["DealerAdmin_RedirectPath"] = Response.RedirectLocation;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void BillingChanged(object sender, EventArgs e)
        {
            try
            {
                txtReason.Visible = !chkBillable.Checked;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void AddDefaultMake(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem it in lstNewMakes.Items)
                {
                    if (it.Selected)
                    {
                        lstMakes.Items.Add(it.Value);
                    }
                }

                SortListBox(ref lstMakes);

                int cnt = lstNewMakes.Items.Count;
                for (int i = 0; i < cnt; i++)
                {
                    lstNewMakes.Items.Remove(lstNewMakes.SelectedItem);
                }

                //disable the desired vehicle year if it has no models associated with it.
                //we will only do this if the dealer has selected a single make
                if (lstMakes.Items.Count == 1)
                {
                    DealerBLL dBLL = new DealerBLL();
                    List<int> Years = dBLL.GetNewMakeYears(lstMakes.Items[0].Text);

                    foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkModelYear = item.FindControl("chkModelYear") as CheckBox;
                            if (chkModelYear != null)
                            {
                                Boolean MatchingYear = false;

                                foreach (int listitem in Years)
                                {
                                    if (chkModelYear.Text.Equals(listitem.ToString()))
                                    {
                                        MatchingYear = true;
                                        // ReSharper disable PossibleNullReferenceException
                                        (item.FindControl("chkModelYear") as CheckBox).Enabled = true;
                                        // ReSharper restore PossibleNullReferenceException
                                    }
                                }

                                if (MatchingYear == false)
                                {
                                    // ReSharper disable PossibleNullReferenceException
                                    (item.FindControl("chkModelYear") as CheckBox).Checked = false;
                                    (item.FindControl("chkModelYear") as CheckBox).Enabled = false;
                                    // ReSharper restore PossibleNullReferenceException
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkModelYear = item.FindControl("chkModelYear") as CheckBox;
                            if (chkModelYear != null)
                            {
                                // ReSharper disable PossibleNullReferenceException
                                (item.FindControl("chkModelYear") as CheckBox).Enabled = true;
                                // ReSharper restore PossibleNullReferenceException

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        protected void RemoveDefaultMake(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem it in lstMakes.Items)
                {
                    if (it.Selected) { lstNewMakes.Items.Add(it.Value); }
                }

                SortListBox(ref lstNewMakes);

                int cnt = lstMakes.Items.Count;
                for (int i = 0; i < cnt; i++)
                {
                    lstMakes.Items.Remove(lstMakes.SelectedItem);
                }

                //disable the desired vehicle year if it has no models associated with it.
                //we will only do this if the dealer has selected a single make
                if (lstMakes.Items.Count == 1)
                {
                    DealerBLL dBLL = new DealerBLL();

                    List<int> Years = dBLL.GetNewMakeYears(lstMakes.Items[0].Text);

                    foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkModelYear = item.FindControl("chkModelYear") as CheckBox;
                            if (chkModelYear != null)
                            {
                                Boolean MatchingYear = false;

                                foreach (int listitem in Years)
                                {
                                    if (chkModelYear.Text.Equals(listitem.ToString()))
                                    {
                                        MatchingYear = true;
                                        // ReSharper disable PossibleNullReferenceException
                                        (item.FindControl("chkModelYear") as CheckBox).Enabled = true;
                                        // ReSharper restore PossibleNullReferenceException
                                    }
                                }

                                if (MatchingYear == false)
                                {
                                    // ReSharper disable PossibleNullReferenceException
                                    (item.FindControl("chkModelYear") as CheckBox).Checked = false;
                                    (item.FindControl("chkModelYear") as CheckBox).Enabled = false;
                                    // ReSharper restore PossibleNullReferenceException
                                }
                            }
                        }
                    }
                }
                else if (lstMakes.Items.Count == 0)
                {
                    foreach (RepeaterItem item in repNewReplacementVehicleModelYear.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            CheckBox chkModelYear = item.FindControl("chkModelYear") as CheckBox;
                            if (chkModelYear != null)
                            {
                                // ReSharper disable PossibleNullReferenceException
                                (item.FindControl("chkModelYear") as CheckBox).Enabled = true;
                                // ReSharper restore PossibleNullReferenceException
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        protected void AddHNIDealer(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem it in lstFreeHNIDealers.Items)
                {
                    if (it.Selected) { lstHNIDealers.Items.Add(it.Value); }
                }
                SortListBox(ref lstHNIDealers);

                int cnt = lstFreeHNIDealers.Items.Count;
                for (int i = 0; i < cnt; i++)
                {
                    lstFreeHNIDealers.Items.Remove(lstFreeHNIDealers.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        protected void RemoveHNIDealer(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem it in lstHNIDealers.Items)
                {
                    if (it.Selected)
                    {
                        lstFreeHNIDealers.Items.Add(it.Value);
                    }
                }
                SortListBox(ref lstFreeHNIDealers);

                int cnt = lstHNIDealers.Items.Count;
                for (int i = 0; i < cnt; i++) { lstHNIDealers.Items.Remove(lstHNIDealers.SelectedItem); }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void AddExcMake(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem it in lstExcludeMakesL.Items)
                {
                    if (it.Selected)
                    {
                        lstExcludeMakesR.Items.Add(it.Value);
                    }
                }
                SortListBox(ref lstExcludeMakesR);

                int cnt = lstHNIDealers.Items.Count;
                for (int i = 0; i < cnt; i++) { lstExcludeMakesL.Items.Remove(lstExcludeMakesL.SelectedItem); }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void RemoveExcMake(object sender, EventArgs e)
        {
            try
            {
                foreach (ListItem it in lstExcludeMakesR.Items)
                {
                    if (it.Selected)
                    {
                        lstExcludeMakesL.Items.Add(it.Value);
                    }
                }
                SortListBox(ref lstExcludeMakesL);

                int cnt = lstHNIDealers.Items.Count;
                for (int i = 0; i < cnt; i++) { lstExcludeMakesR.Items.Remove(lstExcludeMakesR.SelectedItem); }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void GoBack(object sender, EventArgs e)
        {
            MainContainer.ActiveTab = MainContainer.Tabs[MainContainer.ActiveTabIndex - 1];
        }

        protected void NextPage(object sender, EventArgs e)
        {
            Boolean valid = false;
            int currentTab = MainContainer.ActiveTabIndex;

            try
            {
                if (validationPanels[currentTab] != "")
                {
                    Page.Validate(validationPanels[currentTab]);
                    valid = Page.IsValid;
                    if (validationPanels[currentTab] == "vgCutomizationPanel")
                    {
                        cvMakes.IsValid = lstMakes.Items.Count > 0;
                        valid = valid && cvMakes.IsValid;
                    }
                }
                else
                {
                    valid = true;
                }

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            if (valid)
            {
                int nextTab = currentTab + 1;
                while (MainContainer.Tabs[nextTab].Enabled == false)
                {
                    nextTab += 1;
                }
                MainContainer.ActiveTab = MainContainer.Tabs[nextTab];
            }


        }

        protected void SaveAll(object sender, EventArgs e)
        {
            if (chkActive.Checked) ValidateTabs();
            var dBLL = new DealerBLL();
            if (Page.IsValid)
            {
                var dealerToSave = new DealerHolder();

                if (!(ViewState["UpdatedDealer"] == null))
                    dealerToSave = (DealerHolder)ViewState["UpdatedDealer"];
                try
                {
                    dealerToSave.DealerName = txtDealerName.Text;

                    if (!_currentGroup.Equals(Enumerations.AuthUserGroup.AllDealerAdmin) && DealerBLL.IsDeactivated(txtDealershipKey.Text) && chkActive.Checked)
                    {
                        lblCustomerExceeded.Visible = true;
                        lblCustomerExceeded.Text = "(This customer exceeded their 60 day free trial, please contact your system administrator to proceed)";
                        lblCustomerExceeded.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        dealerToSave.Active = chkActive.Checked;
                        //check for deactivation
                        if (!lblIsActive.Value.Equals(""))
                        {
                            if (lblIsActive.Value.Equals("yes") && !chkActive.Checked)
                            {
                                //Dealer was deactivated
                                dealerToSave.DateDeactivated = Convert.ToString(DateTime.Now);
                            }
                            else
                            {
                                dealerToSave.DateDeactivated = "";
                            }
                        }
                        else
                        {
                            dealerToSave.DateDeactivated = "";
                        }
                    }

                    dealerToSave.EnableExcludedUsedCarMakes = chkEnableExcludedMakes.Checked;
                    dealerToSave.Salesperson = txtSalesPerson.Text;
                    dealerToSave.OriginalSalesPerson = txtOriginalSalesPerson.Text;
                    dealerToSave.GroupID = Convert.ToInt32(ddlDealerGroup.SelectedValue);
                    dealerToSave.DealerStreet = txtStreet.Text;
                    dealerToSave.DealerCity = txtCity.Text;
                    dealerToSave.DealerState = ddlStateID.SelectedValue;
                    dealerToSave.DealerZip = txtZip.Text;
                    dealerToSave.DealerAutoGroup = txtAutoGroup.Text;
                    dealerToSave.DealerContactEmail = txtContactEmail.Text;
                    dealerToSave.DealerContactName = txtContactName.Text;
                    dealerToSave.DealerContactPhone = txtContactPhone.Text;
                    dealerToSave.AppointmentEmail = txtContactAppointment.Text;
                    dealerToSave.DealerContactPhone2 = txtContactPhone2.Text;
                    dealerToSave.DealerContactFax = txtFax.Text;
                    dealerToSave.DealerTitle = txtContactTitle.Text;
                    dealerToSave.SecondDealerContactName = txtContactName2.Text;
                    dealerToSave.SecondDealerContactEmail = txtContactEmail2.Text;
                    dealerToSave.SecondDealerContactPhone = txtContactPhoneTwo.Text;
                    dealerToSave.SecondDealerContactPhone2 = txtContactPhonetwo2.Text;
                    dealerToSave.SecondDealerContactFax = txtFax2.Text;
                    dealerToSave.SecondDealerTitle = txtContactTitle2.Text;
                    dealerToSave.DealerWebSite = txtWebsite.Text;
                    dealerToSave.DealerProductType = Convert.ToInt32(rbProductType.SelectedValue);
                    dealerToSave.CreditApp = txtCreditAppUrl.Text;
                    dealerToSave.CreditEnabled = chkCreditActive.Checked;
                    dealerToSave.CreditMsg = Convert.ToInt32(ddlCreditAppText.SelectedValue);
                    dealerToSave.InvUsed = txtUsedinv.Text;
                    dealerToSave.InvUsedEnabled = chkUsedActive.Checked;
                    dealerToSave.InvNew = txtNewInv.Text;
                    dealerToSave.InvNewEnabled = chkNewActive.Checked;
                    dealerToSave.DealerPublicSite = txtWebsite.Text;
                    dealerToSave.InstantOptionPricing = chkInstantOption.Checked;
                    dealerToSave.EmailAppraisal = chkEmailAppraisal.Checked;
                    dealerToSave.FourStep = chkFourStep.Checked;
                    dealerToSave.NewSelection = chkNewSelection.Checked;
                    dealerToSave.ShowAmountOwed = chkBalanceowed.Checked;
                    dealerToSave.ShowIncentives = chkNewVehicleIncentives.Checked;
                    dealerToSave.ShowMonthlyPayment = chkCurrentPayment.Checked;
                    dealerToSave.UnsureOption = chkUnsure.Checked;
                    dealerToSave.RetailPricing = chkRetailPricing.Checked;
                    dealerToSave.TradeImageUpload = chkTradeImage.Checked;
                    dealerToSave.TradeVideoUpload = chkTradeVideoUpload.Checked;
                    dealerToSave.GoogleUrchin = txtGoogleUrchin.Text;
                    dealerToSave.AppraisalExpiration = Convert.ToInt32(ddlAppraisalExpiration.SelectedValue);
                    dealerToSave.Comments = txtComments.Text;
                    dealerToSave.GalvesRangeCategory = Convert.ToInt32(rblGalvesRangeCategory.SelectedValue);
                    dealerToSave.TruckGalvesRangeCategory = Convert.ToInt32(rblTruckGalvesRangeCategory.SelectedValue);
                    dealerToSave.Language = rblLang.SelectedValue;
                    dealerToSave.NADABranding = Convert.ToBoolean(rblNADABranding.SelectedValue);
                    dealerToSave.HideTradeIn = Convert.ToBoolean(chkHideTradein.Checked);
                    dealerToSave.DataProvider = rblDataProvider.SelectedValue;
                    dealerToSave.NewModelYears = GetRepeaterValues();
                    dealerToSave.Theme = ddlThemes.SelectedValue;
                    dealerToSave.OperatingHours = "";
                    dealerToSave.HideCondition = false;
                    dealerToSave.HideEmailTradeIn = false;
                    dealerToSave.EmailTitle = "";
                    dealerToSave.HideSpecialOffer = false;
                    dealerToSave.HideAddress = false;
                    dealerToSave.WebsiteCompany = txtWebsiteCompany.Text;
                    dealerToSave.WebsiteContactName = txtWebsiteContactName.Text;
                    dealerToSave.WebsiteContactPhone = txtWebsiteContactPhone.Text;
                    dealerToSave.WebsiteContactEmail = txtWebsiteContacEmail.Text;
                    dealerToSave.LeadProvider = ddlProviderAndProduct.SelectedValue;
                    dealerToSave.LeadEmail = txtEmailLeads1.Text;
                    dealerToSave.LeadEmail2 = txtEmailLeads2.Text;
                    dealerToSave.LeadEmail3 = txtEmailLeads3.Text;
                    dealerToSave.LeadEmail4 = txtEmailLeads4.Text;
                    dealerToSave.LeadFormat = rblLeadFormat1.SelectedValue;
                    dealerToSave.LeadFormat2 = rblLeadFormat2.SelectedValue;
                    dealerToSave.LeadFormat3 = rblLeadFormat3.SelectedValue;
                    dealerToSave.LeadFormat4 = rblLeadFormat4.SelectedValue;
                    dealerToSave.LeadContactName = txtLeadContactName.Text;
                    dealerToSave.LeadContactEmail = txtLeadContactEmail.Text;
                    dealerToSave.LeadContactPhone = txtLeadContactPhone.Text;
                    dealerToSave.LeadContactFax = txtLeadContactFax.Text;
                    dealerToSave.Billable = chkBillable.Checked;
                    dealerToSave.AdminAutoAppraisalCost = Convert.ToInt32(txtMonthlyBilling.Text);
                    dealerToSave.BillingInfo = txtAdditionalInformationBilling.Text;
                    dealerToSave.BillingLastUpdated = Convert.ToDateTime(ccBillingStatusLastUpdated.Text);
                    dealerToSave.DateCreated = Convert.ToDateTime(ccDealerDateCreated.Text);
                    dealerToSave.SystemBillingDate = Convert.ToDateTime(ccSystemDealerStartDate.Text);
                    dealerToSave.AdminSetupFees = Convert.ToInt32(txtAdminSetupFee.Text);
                    try
                    {
                        dealerToSave.BillableStartDate = Convert.ToDateTime(ccAdminBillingStartDate.Text);
                    }
                    catch (Exception ex)
                    {
                        ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                    }

                    dealerToSave.BillingExplanation = txtReason.Text;
                    dealerToSave.HyperLead = chkHyperLead.Checked;
                    dealerToSave.TaxIncentive = chkTaxIncentive.Checked;
                    dealerToSave.Defaultbackground = rbtnDefaultBackground.SelectedValue;
                    dealerToSave.WebsiteComments = txtWebsiteComments.Text;
                    dealerToSave.SocialMedia = chkSocialSharing.Checked;
                    //Commented out by Freddy K 12/06/12
                    //dealerToSave.DisableLeadEmails = chkDisableLeads.Checked;

                    var defaultmakes = new StringBuilder();
                    foreach (ListItem it in lstMakes.Items)
                    {
                        defaultmakes.Append(string.Format("{0};", it.Value));
                    }
                    dealerToSave.DealerDefaultMakes = defaultmakes.ToString();

                    var hniDealers = new StringBuilder();
                    foreach (ListItem it in lstHNIDealers.Items)
                    {
                        hniDealers.Append(string.Format("{0};", it.Value));
                    }
                    dealerToSave.HNIDealers = hniDealers.ToString();

                    if (lstHNIDealers.Items.Count == 0)
                    {
                        dealerToSave.EnableExcludedUsedCarMakes = true;
                        chkEnableExcludedMakes.Checked = true;
                    }

                    //determine the Dealer Group
                    if (_currentGroup == Enumerations.AuthUserGroup.GroupDealerAdmin)
                    {
                        int DealerType = dBLL.GetDealerGroupByID(Convert.ToInt32(ddlDealerGroup.SelectedValue));
                        dealerToSave.DealerType = DealerType;
                    }
                    else
                    {
                        if (!(_currentGroup == Enumerations.AuthUserGroup.HomeNetSuperUser ||
                              _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin
                             ))
                        {
                            if (rblDataProvider.SelectedValue.Equals("NADA"))
                            {
                                dealerToSave.DealerType = 6;
                            }
                            else if (rblDataProvider.SelectedValue.Equals("GALVES"))
                            {
                                dealerToSave.DealerType = 5;
                            }
                        }
                        else
                        {
                            dealerToSave.DealerType = 4;
                        }
                    }

                    // Value Range enabled
                    dealerToSave.EnableWidget = chkEnableWidget.Checked;
                    bool isAdmin = (Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.BSBConsultingFullAdmin)
                        || (Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.AllDealerAdmin);
                    if (isAdmin)
                    {
                        trIsReseller.Visible = isAdmin;
                    }
                    dealerToSave.IsReseller = chkIsReseller.Checked;
                    dealerToSave.EnableValueRanges = chkEnableValueRange.Checked;
                    //Value Range (1)
                    dealerToSave.AverageValueRange_VUnderPercent1 = LowSlider1.Text;
                    dealerToSave.AverageValueRange_VUnderAmount1 = LowSliderDollar1.Text;
                    dealerToSave.AverageValueRange_VOverPercent1 = HighSlider1.Text;
                    dealerToSave.AverageValueRange_VOverAmount1 = HighSliderDollar1.Text;
                    //dealerToSave.ValueRange
                    if (rbtnHighPercent1.Checked)
                    {
                        dealerToSave.AverageValueRange_VOverTypeSelection1 = "%";
                    }
                    else if (rbtnHighValue1.Checked)
                    {
                        dealerToSave.AverageValueRange_VOverTypeSelection1 = "$";
                    }

                    if (rbtnLowPercent1.Checked)
                    {
                        dealerToSave.AverageValueRange_VUnderTypeSelection1 = "%";
                    }
                    else if (rbtnLowValue1.Checked)
                    {
                        dealerToSave.AverageValueRange_VUnderTypeSelection1 = "$";
                    }

                    //Value Range (2)
                    dealerToSave.AverageValueRange_VUnderPercent2 = LowSlider2.Text;
                    dealerToSave.AverageValueRange_VUnderAmount2 = LowSliderDollar2.Text;
                    dealerToSave.AverageValueRange_VOverPercent2 = HighSlider2.Text;
                    dealerToSave.AverageValueRange_VOverAmount2 = HighSliderDollar2.Text;

                    if (rbtnHighPercent2.Checked)
                    {
                        dealerToSave.AverageValueRange_VOverTypeSelection2 = "%";
                    }
                    else if (rbtnHighValue2.Checked)
                    {
                        dealerToSave.AverageValueRange_VOverTypeSelection2 = "$";
                    }

                    if (rbtnLowPercent2.Checked)
                    {
                        dealerToSave.AverageValueRange_VUnderTypeSelection2 = "%";
                    }
                    else if (rbtnLowValue2.Checked)
                    {
                        dealerToSave.AverageValueRange_VUnderTypeSelection2 = "$";
                    }

                    //Value Range (3)
                    dealerToSave.AverageValueRange_VUnderPercent3 = LowSlider3.Text;
                    dealerToSave.AverageValueRange_VUnderAmount3 = LowSliderDollar3.Text;
                    dealerToSave.AverageValueRange_VOverPercent3 = HighSlider3.Text;
                    dealerToSave.AverageValueRange_VOverAmount3 = HighSliderDollar3.Text;

                    if (rbtnHighPercent3.Checked)
                    {
                        dealerToSave.AverageValueRange_VOverTypeSelection3 = "%";
                    }
                    else if (rbtnHighValue3.Checked)
                    {
                        dealerToSave.AverageValueRange_VOverTypeSelection3 = "$";
                    }

                    if (rbtnLowPercent3.Checked)
                    {
                        dealerToSave.AverageValueRange_VUnderTypeSelection3 = "%";
                    }
                    else if (rbtnLowValue3.Checked)
                    {
                        dealerToSave.AverageValueRange_VUnderTypeSelection3 = "$";
                    }

                    //facebook stuff
                    dealerToSave.AppID = txtAppID.Text;
                    dealerToSave.APIKey = txtAPIKey.Text;
                    dealerToSave.AppSecret = txtAppSecret.Text;
                    dealerToSave.WallURL = txtwallpost.Text;

                    dealerToSave.DealerKey = ViewState["DealerKey"].ToString();

                    //Check if that is new dealer
                    if (!DealerBLL.IsDealerExists(txtDealershipKey.Text))
                    {
                        Session["DealerAdmin_IsNewDealer"] = true;
                    }

                    dealerToSave.ExcludedUsedCarMakes = new List<string>();
                    foreach (ListItem item in lstExcludeMakesR.Items)
                    {
                        dealerToSave.ExcludedUsedCarMakes.Add(item.Text);
                    }

                    dealerToSave.HNIFeeds = new List<string>();
                    foreach (ListItem item in lstHNIDealers.Items)
                    {
                        dealerToSave.HNIFeeds.Add(item.Text);
                    }

                    dealerToSave.DefaultMakes = new List<string>();
                    foreach (ListItem item in lstMakes.Items)
                    {
                        dealerToSave.DefaultMakes.Add(item.Text);
                    }

                    DealerBLL.SaveDealer(dealerToSave);

                    //Save any updates to the Audit Table.
                    if (!(ViewState["OriginalDealer"] == null))
                    {
                        DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
                        ViewState["OriginalDealer"] = ViewState["UpdatedDealer"];
                    }

                    //Update data in DeactivatedDealers table as well
                    SaveChangesForDeactivatedDealer();

                    lblStatus.Visible = true;
                    lblStatus.Text = "(Dealer Saved Successfully)";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    SetSavedSateForTab(PagePanels.All);

                    //Redirect if necessary
                    RedirectIfNecessary();

                    //Clear redirect path
                    Session["DealerAdmin_RedirectPath"] = string.Empty;

                    //Check if the dealer is a draft dealer being Activated)
                    if (Session["IsDraft"] != null)
                    {
                        switch ((Boolean)Session["IsDraft"])
                        {
                            case true:
                                DealerBLL.ActivateDraftDealer(Convert.ToInt32(Session["DraftDealerID"].ToString()));
                                break;

                            case false:
                                break;
                        }
                    }

                    //Check if that is new dealer which matches the name of the deactivated dealer or their lead emails 
                    if ((bool)(Session["DealerAdmin_IsNewDealer"] ?? false) && !_currentGroup.Equals(Enumerations.AuthUserGroup.AllDealerAdmin))
                    {
                        CheckMatchesForDeactivatedDealers(txtDealerName.Text);
                    }
                }
                catch (Exception ex)
                {
                    lblStatus.Visible = true;
                    lblStatus.Text = "(Error saving Dealer! please check the exception log and notify the web admin!)";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    var st = new StackTrace(ex, true);
                    var sourceFrame = Enumerable.Range(0, st.FrameCount).FirstOrDefault(i => st.GetFrame(i).GetFileLineNumber() > 0);
                    log(sourceFrame);

                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                }
            }
        }

        protected void btnSaveDealerInfo_Click(object sender, EventArgs e)
        {
            if (chkActive.Checked) ValidateDelerInfoTab();

            var dBLL = new DealerBLL();
            var infoTab = new DealerHolder();
            DealerHolder dealerToSave = new DealerHolder();
            dealerToSave = (DealerHolder)ViewState["UpdatedDealer"];

            if (Page.IsValid)
            {
                try
                {
                    dealerToSave.DealerKey = infoTab.DealerKey = txtDealershipKey.Text;
                    dealerToSave.DealerName = infoTab.DealerName = txtDealerName.Text;
                    if (!_currentGroup.Equals(Enumerations.AuthUserGroup.AllDealerAdmin) && DealerBLL.IsDeactivated(txtDealershipKey.Text) && chkActive.Checked)
                    {
                        lblCustomerExceeded.Visible = true;
                        lblCustomerExceeded.Text = "(This customer exceeded their 60 day free trial, please contact your system administrator to proceed)";
                        lblCustomerExceeded.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        dealerToSave.Active = infoTab.Active = chkActive.Checked;
                        //check for deactivation
                        if (!lblIsActive.Value.Equals(""))
                        {
                            if (lblIsActive.Value.Equals("yes") && !chkActive.Checked)
                            {
                                //Dealer was deactivated
                                dealerToSave.DateDeactivated = infoTab.DateDeactivated = Convert.ToString(DateTime.Now);
                            }
                            else
                            {
                                infoTab.DateDeactivated = "";
                            }
                        }
                        else
                        {
                            infoTab.DateDeactivated = "";
                        }
                    }

                    dealerToSave.Salesperson = infoTab.Salesperson = txtSalesPerson.Text;
                    dealerToSave.OriginalSalesPerson = infoTab.OriginalSalesPerson = txtOriginalSalesPerson.Text;
                    infoTab.GroupID = Convert.ToInt32(ddlDealerGroup.SelectedValue);
                    dealerToSave.GroupID = infoTab.GroupID;
                    dealerToSave.DealerStreet = infoTab.DealerStreet = txtStreet.Text;
                    dealerToSave.DealerCity = infoTab.DealerCity = txtCity.Text;
                    dealerToSave.DealerState = infoTab.DealerState = ddlStateID.SelectedValue;
                    dealerToSave.DealerZip = infoTab.DealerZip = txtZip.Text;
                    dealerToSave.DealerAutoGroup = infoTab.DealerAutoGroup = txtAutoGroup.Text;
                    dealerToSave.DealerContactEmail = infoTab.DealerContactEmail = txtContactEmail.Text;
                    dealerToSave.DealerContactName = infoTab.DealerContactName = txtContactName.Text;
                    dealerToSave.DealerContactPhone = infoTab.DealerContactPhone = txtContactPhone.Text;
                    dealerToSave.AppointmentEmail = infoTab.AppointmentEmail = txtContactAppointment.Text;
                    dealerToSave.DealerContactPhone2 = infoTab.DealerContactPhone2 = txtContactPhone2.Text;
                    dealerToSave.DealerContactFax = infoTab.DealerContactFax = txtFax.Text;
                    dealerToSave.DealerTitle = infoTab.DealerTitle = txtContactTitle.Text;
                    dealerToSave.SecondDealerContactName = infoTab.SecondDealerContactName = txtContactName2.Text;
                    dealerToSave.SecondDealerContactEmail = infoTab.SecondDealerContactEmail = txtContactEmail2.Text;
                    dealerToSave.SecondDealerContactPhone = infoTab.SecondDealerContactPhone = txtContactPhoneTwo.Text;
                    dealerToSave.SecondDealerContactPhone2 = infoTab.SecondDealerContactPhone2 = txtContactPhonetwo2.Text;
                    dealerToSave.SecondDealerContactFax = infoTab.SecondDealerContactFax = txtFax2.Text;
                    dealerToSave.SecondDealerTitle = infoTab.SecondDealerTitle = txtContactTitle2.Text;
                    dealerToSave.DealerWebSite = infoTab.DealerWebSite = txtWebsite.Text;
                    dealerToSave.DealerProductType = infoTab.DealerProductType = Convert.ToInt32(rbProductType.SelectedValue);

                    dBLL.SaveDealerInfoTab(infoTab);

                    //Update data in DeactivatedDealers table as well
                    SaveChangesForDeactivatedDealer();

                    //Save any updates to the Audit Table.
                    if (!(ViewState["OriginalDealer"] == null))
                    {
                        DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
                        ViewState["OriginalDealer"] = ViewState["UpdatedDealer"];
                    }

                    lblStatus.Visible = true;
                    lblStatus.Text = "(Dealer Information tab Saved Successfully)";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    SetSavedSateForTab(PagePanels.InfoPanel);
                }
                catch (Exception ex)
                {
                    lblStatus.Visible = true;
                    lblStatus.Text = "(Error saving the dealer information tab! please check the exception log and notify the web admin!)";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                }
            }
        }

        protected void btnSaveDataTab_Click(object sender, EventArgs e)
        {
            var dBLL = new DealerBLL();
            try
            {
                var dataTab = GetDealerToSaveDataTab();
                dBLL.SaveDataTab(dataTab);

                ////Save any updates to the Audit Table.
                //if (!(ViewState["OriginalDealer"] == null))
                //{
                //    DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
                //}

                lblStatus.Visible = true;
                lblStatus.Text = "(Data tab Saved Successfully)";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                SetSavedSateForTab(PagePanels.DataPanel);
            }
            catch (Exception ex)
            {
                lblStatus.Visible = true;
                lblStatus.Text = "(Error saving the Data tab! please check the exception log and notify the web admin!)";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }
        }

        private DealerHolder GetDealerToSaveDataTab()
        {
            var dBLL = new DealerBLL();
            var dataTab = new DealerHolder();
            try
            {
                dataTab.DealerKey = txtDealershipKey.Text;
                dataTab.DataProvider = rblDataProvider.SelectedValue;
                dataTab.GalvesRangeCategory = Convert.ToInt32(rblGalvesRangeCategory.SelectedValue);
                dataTab.TruckGalvesRangeCategory = Convert.ToInt32(rblTruckGalvesRangeCategory.SelectedValue);

                //Value Range (1)
                dataTab.AverageValueRange_VUnderPercent1 = LowSlider1.Text;
                dataTab.AverageValueRange_VUnderAmount1 = LowSliderDollar1.Text;
                dataTab.AverageValueRange_VOverPercent1 = HighSlider1.Text;
                dataTab.AverageValueRange_VOverAmount1 = HighSliderDollar1.Text;

                //Value Range (2)
                dataTab.AverageValueRange_VUnderPercent2 = LowSlider2.Text;
                dataTab.AverageValueRange_VUnderAmount2 = LowSliderDollar2.Text;
                dataTab.AverageValueRange_VOverPercent2 = HighSlider2.Text;
                dataTab.AverageValueRange_VOverAmount2 = HighSliderDollar2.Text;

                //Value Range (3)
                dataTab.AverageValueRange_VUnderPercent3 = LowSlider3.Text;
                dataTab.AverageValueRange_VUnderAmount3 = LowSliderDollar3.Text;
                dataTab.AverageValueRange_VOverPercent3 = HighSlider3.Text;
                dataTab.AverageValueRange_VOverAmount3 = HighSliderDollar3.Text;

                //determine the Dealer Group
                if (_currentGroup == Enumerations.AuthUserGroup.GroupDealerAdmin)
                {
                    dataTab.DealerType = dBLL.GetDealerGroupByID(Convert.ToInt32(ddlDealerGroup.SelectedValue));
                }
                else
                {
                    if (!(_currentGroup == Enumerations.AuthUserGroup.HomeNetSuperUser ||
                          _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin
                         ))
                    {
                        if (rblDataProvider.SelectedValue.Equals("NADA"))
                        {
                            dataTab.DealerType = 6;
                        }
                        else if (rblDataProvider.SelectedValue.Equals("GALVES"))
                        {
                            dataTab.DealerType = 5;
                        }
                    }
                    else
                    {
                        dataTab.DealerType = 4;
                    }
                }

                //Value Range (1)
                if (rbtnHighPercent1.Checked)
                {
                    dataTab.AverageValueRange_VOverTypeSelection1 = "%";
                }
                else if (rbtnHighValue1.Checked)
                {
                    dataTab.AverageValueRange_VOverTypeSelection1 = "$";
                }

                if (rbtnLowPercent1.Checked)
                {
                    dataTab.AverageValueRange_VUnderTypeSelection1 = "%";
                }
                else if (rbtnLowValue1.Checked)
                {
                    dataTab.AverageValueRange_VUnderTypeSelection1 = "$";
                }

                //Value Range (2)
                if (rbtnHighPercent2.Checked)
                {
                    dataTab.AverageValueRange_VOverTypeSelection2 = "%";
                }
                else if (rbtnHighValue2.Checked)
                {
                    dataTab.AverageValueRange_VOverTypeSelection2 = "$";
                }

                if (rbtnLowPercent2.Checked)
                {
                    dataTab.AverageValueRange_VUnderTypeSelection2 = "%";
                }
                else if (rbtnLowValue2.Checked)
                {
                    dataTab.AverageValueRange_VUnderTypeSelection2 = "$";
                }

                //Value Range (3)
                if (rbtnHighPercent3.Checked)
                {
                    dataTab.AverageValueRange_VOverTypeSelection3 = "%";
                }
                else if (rbtnHighValue3.Checked)
                {
                    dataTab.AverageValueRange_VOverTypeSelection3 = "$";
                }

                if (rbtnLowPercent3.Checked)
                {
                    dataTab.AverageValueRange_VUnderTypeSelection3 = "%";
                }
                else if (rbtnLowValue3.Checked)
                {
                    dataTab.AverageValueRange_VUnderTypeSelection3 = "$";
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

            return dataTab;
        }

        //protected void btnSaveCustomizationTab_Click(object sender, EventArgs e)
        //{
        //    DealerBLL dBLL = new DealerBLL();
        //    DealerHolder customizationTab = new DealerHolder();
        //    DealerHolder dealerToSave = new DealerHolder();
        //    dealerToSave = (DealerHolder)ViewState["UpdatedDealer"];

        //    if (chkActive.Checked) ValidateCustomizeTab();

        //    if (Page.IsValid)
        //    {
        //        try
        //        {
        //            dealerToSave.DealerKey = customizationTab.DealerKey = txtDealershipKey.Text;
        //            dealerToSave.CreditApp = customizationTab.CreditApp = txtCreditAppUrl.Text;
        //            dealerToSave.CreditEnabled = customizationTab.CreditEnabled = chkCreditActive.Checked;
        //            dealerToSave.CreditMsg = customizationTab.CreditMsg = Convert.ToInt32(ddlCreditAppText.SelectedValue);
        //            dealerToSave.InvUsed = customizationTab.InvUsed = txtUsedinv.Text;
        //            dealerToSave.InvUsedEnabled = customizationTab.InvUsedEnabled = chkUsedActive.Checked;
        //            dealerToSave.InvNew = customizationTab.InvNew = txtNewInv.Text;
        //            dealerToSave.InvNewEnabled = customizationTab.InvNewEnabled = chkNewActive.Checked;
        //            dealerToSave.InstantOptionPricing = customizationTab.InstantOptionPricing = chkInstantOption.Checked;
        //            dealerToSave.EmailAppraisal = customizationTab.EmailAppraisal = chkEmailAppraisal.Checked;
        //            dealerToSave.FourStep = customizationTab.FourStep = chkFourStep.Checked;
        //            dealerToSave.NewSelection = customizationTab.NewSelection = chkNewSelection.Checked;
        //            dealerToSave.ShowAmountOwed = customizationTab.ShowAmountOwed = chkBalanceowed.Checked;
        //            dealerToSave.ShowIncentives = customizationTab.ShowIncentives = chkNewVehicleIncentives.Checked;
        //            dealerToSave.ShowMonthlyPayment = customizationTab.ShowMonthlyPayment = chkCurrentPayment.Checked;
        //            dealerToSave.UnsureOption = customizationTab.UnsureOption = chkUnsure.Checked;
        //            dealerToSave.RetailPricing = customizationTab.RetailPricing = chkRetailPricing.Checked;
        //            dealerToSave.TradeImageUpload = customizationTab.TradeImageUpload = chkTradeImage.Checked;
        //            dealerToSave.TradeVideoUpload = customizationTab.TradeVideoUpload = chkTradeVideoUpload.Checked;
        //            dealerToSave.GoogleUrchin = customizationTab.GoogleUrchin = txtGoogleUrchin.Text;
        //            dealerToSave.AppraisalExpiration = customizationTab.AppraisalExpiration = Convert.ToInt32(ddlAppraisalExpiration.SelectedValue);
        //            dealerToSave.Comments = customizationTab.Comments = txtComments.Text;
        //            dealerToSave.Language = customizationTab.Language = rblLang.SelectedValue;
        //            dealerToSave.NADABranding = customizationTab.NADABranding = Convert.ToBoolean(rblNADABranding.SelectedValue);
        //            dealerToSave.Theme = customizationTab.Theme = ddlThemes.SelectedValue;
        //            dealerToSave.HideTradeIn = customizationTab.HideTradeIn = Convert.ToBoolean(chkHideTradein.Checked);
        //            dealerToSave.NewModelYears = customizationTab.NewModelYears = GetRepeaterValues();
        //            dealerToSave.HyperLead = customizationTab.HyperLead = chkHyperLead.Checked;
        //            dealerToSave.TaxIncentive = customizationTab.TaxIncentive = chkTaxIncentive.Checked;
        //            dealerToSave.Defaultbackground = customizationTab.Defaultbackground = rbtnDefaultBackground.SelectedValue;
        //            dealerToSave.SocialMedia = customizationTab.SocialMedia = chkSocialSharing.Checked;

        //            var defaultmakes = new StringBuilder();
        //            foreach (ListItem it in lstMakes.Items)
        //            {
        //                defaultmakes.Append(string.Format("{0};", it.Value));
        //            }
        //            dealerToSave.DealerDefaultMakes = customizationTab.DealerDefaultMakes = defaultmakes.ToString();

        //            var hniDealers = new StringBuilder();
        //            foreach (ListItem it in lstHNIDealers.Items)
        //            {
        //                hniDealers.Append(string.Format("{0};", it.Value));
        //            }
        //            dealerToSave.HNIDealers = customizationTab.HNIDealers = hniDealers.ToString();

        //            //checking if models for selected makes are available for selected years:
        //            var years = GetRepeaterValues().Trim().TrimEnd(',').Split(',');
        //            cvModels.ErrorMessage = string.Empty;
        //            if (years.Length > 0)
        //            {
        //                for (int x = 0; x < years.Length; x++)
        //                {
        //                    if (DealerBLL.GetCountAvailableModelsForSpecifiedYear(years[x], defaultmakes.ToString()) == 0)
        //                    {
        //                        if (!string.IsNullOrEmpty(years[x]))
        //                        {
        //                            cvModels.ErrorMessage = string.Format("Models are not available for selected {0} year.", years[x]);
        //                        }
        //                    }
        //                }
        //            }
        //            if (!string.IsNullOrEmpty(cvModels.ErrorMessage) && chkActive.Checked)
        //            {
        //                cvModels.IsValid = false;
        //            }
        //            else
        //            {
        //                dBLL.SaveCustomizationTab(customizationTab);

        //                //Save any updates to the Audit Table.
        //                if (!(ViewState["OriginalDealer"] == null))
        //                {
        //                    DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
        //                    ViewState["OriginalDealer"] = ViewState["UpdatedDealer"];
        //                }

        //                lblStatus.Visible = true;
        //                lblStatus.Text = "(Customization tab Saved Successfully)";
        //                lblStatus.ForeColor = System.Drawing.Color.Green;

        //                SetSavedSateForTab(PagePanels.CutomizationPanel);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            lblStatus.Visible = true;
        //            lblStatus.Text = "(Error saving the Customization tab! please check the exception log and notify the web admin!)";
        //            lblStatus.ForeColor = System.Drawing.Color.Red;
        //            ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
        //        }
        //    }
        //}

        protected void btnSaveLeadTab_Click(object sender, EventArgs e)
        {
            DealerBLL dBLL = new DealerBLL();
            DealerHolder leadTab = new DealerHolder();
            DealerHolder dealerToSave = new DealerHolder();
            dealerToSave = (DealerHolder)ViewState["UpdatedDealer"];

            if (chkActive.Checked) ValidateLeadTab();

            if (Page.IsValid)
            {
                try
                {
                    dealerToSave.DealerKey = leadTab.DealerKey = txtDealershipKey.Text;
                    dealerToSave.LeadEmail = leadTab.LeadEmail = txtEmailLeads1.Text;
                    dealerToSave.LeadEmail2 = leadTab.LeadEmail2 = txtEmailLeads2.Text;
                    dealerToSave.LeadEmail3 = leadTab.LeadEmail3 = txtEmailLeads3.Text;
                    dealerToSave.LeadEmail4 = leadTab.LeadEmail4 = txtEmailLeads4.Text;
                    dealerToSave.LeadFormat = leadTab.LeadFormat = rblLeadFormat1.SelectedValue;
                    dealerToSave.LeadFormat2 = leadTab.LeadFormat2 = rblLeadFormat2.SelectedValue;
                    dealerToSave.LeadFormat3 = leadTab.LeadFormat3 = rblLeadFormat3.SelectedValue;
                    dealerToSave.LeadFormat4 = leadTab.LeadFormat4 = rblLeadFormat4.SelectedValue;
                    dealerToSave.LeadContactName = leadTab.LeadContactName = txtLeadContactName.Text;
                    dealerToSave.LeadContactEmail = leadTab.LeadContactEmail = txtLeadContactEmail.Text;
                    dealerToSave.LeadContactPhone = leadTab.LeadContactPhone = txtLeadContactPhone.Text;
                    dealerToSave.LeadContactFax = leadTab.LeadContactFax = txtLeadContactFax.Text;
                    dealerToSave.LeadProvider = leadTab.LeadProvider = ddlProviderAndProduct.SelectedValue;
                    dealerToSave.DisableLeadEmails = leadTab.DisableLeadEmails = chkDisableLeads.Checked;

                    dBLL.SaveLeadTab(leadTab);

                    //Update data in DeactivatedDealers table as well
                    SaveChangesForDeactivatedDealer();

                    //Save any updates to the Audit Table.
                    if (!(ViewState["OriginalDealer"] == null))
                    {
                        DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
                        ViewState["OriginalDealer"] = ViewState["UpdatedDealer"];
                    }

                    lblStatus.Visible = true;
                    lblStatus.Text = "(Lead tab Saved Successfully)";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    //Commented out by Freddy K 12/06/12
                    SetSavedSateForTab(PagePanels.LeadPanel);
                }
                catch (Exception ex)
                {
                    lblStatus.Visible = true;
                    lblStatus.Text = "(Error saving the Lead tab! please check the exception log and notify the web admin!)";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                }
            }
        }

        protected void btnSaveBillingtab_Click(object sender, EventArgs e)
        {
            DealerBLL dBLL = new DealerBLL();
            DealerHolder billingTab = new DealerHolder();
            DealerHolder dealerToSave = new DealerHolder();
            dealerToSave = (DealerHolder)ViewState["UpdatedDealer"];

            if (chkActive.Checked) ValidateBillingTab();

            if (Page.IsValid)
            {
                try
                {
                    dealerToSave.DealerKey = billingTab.DealerKey = txtDealershipKey.Text;
                    dealerToSave.Billable = billingTab.Billable = chkBillable.Checked;
                    dealerToSave.AdminAutoAppraisalCost = billingTab.AdminAutoAppraisalCost = Convert.ToInt32(txtMonthlyBilling.Text);
                    dealerToSave.BillingInfo = billingTab.BillingInfo = txtAdditionalInformationBilling.Text;
                    dealerToSave.BillingLastUpdated = billingTab.BillingLastUpdated = Convert.ToDateTime(ccBillingStatusLastUpdated.Text);
                    dealerToSave.DateCreated = billingTab.DateCreated = Convert.ToDateTime(ccDealerDateCreated.Text);
                    dealerToSave.SystemBillingDate = billingTab.SystemBillingDate = Convert.ToDateTime(ccSystemDealerStartDate.Text);
                    dealerToSave.AdminSetupFees = billingTab.AdminSetupFees = Convert.ToInt32(txtAdminSetupFee.Text);
                    dealerToSave.BillableStartDate = billingTab.BillableStartDate = Convert.ToDateTime(ccAdminBillingStartDate.Text);
                    dealerToSave.BillingExplanation = billingTab.BillingExplanation = txtReason.Text;

                    dBLL.SaveBillingTab(billingTab);

                    //Save any updates to the Audit Table.
                    if (!(ViewState["OriginalDealer"] == null))
                    {
                        DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
                        ViewState["OriginalDealer"] = ViewState["UpdatedDealer"];
                    }

                    lblStatus.Visible = true;
                    lblStatus.Text = "(Billing tab Saved Successfully)";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    //Commented out by Freddy K 12/06/12
                    //SetSavedSateForTab(PagePanels.BillingPanel);
                }
                catch (Exception ex)
                {
                    lblStatus.Visible = true;
                    lblStatus.Text = "(Error saving the Billing tab! please check the exception log and notify the web admin!)";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                }
            }
        }

        protected void btnSaveFacebooktab_Click(object sender, EventArgs e)
        {

            DealerBLL dBLL = new DealerBLL();
            DealerHolder FacebookTab = new DealerHolder();
            DealerHolder dealerToSave = new DealerHolder();
            dealerToSave = (DealerHolder)ViewState["UpdatedDealer"];

            if (Page.IsValid)
            {
                try
                {
                    dealerToSave.DealerKey = FacebookTab.DealerKey = txtDealershipKey.Text;
                    dealerToSave.APIKey = FacebookTab.APIKey = txtAPIKey.Text;
                    dealerToSave.AppSecret = FacebookTab.AppSecret = txtAppSecret.Text;
                    dealerToSave.AppID = FacebookTab.AppID = txtAppID.Text;
                    dealerToSave.WallURL = FacebookTab.WallURL = txtwallpost.Text;

                    dBLL.SaveFacebookTab(FacebookTab);

                    //Save any updates to the Audit Table.
                    if (!(ViewState["OriginalDealer"] == null))
                    {
                        DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
                        ViewState["OriginalDealer"] = ViewState["UpdatedDealer"];
                    }

                    lblStatus.Visible = true;
                    lblStatus.Text = "(Facebook tab Saved Successfully)";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    SetSavedSateForTab(PagePanels.FacebookPanel);
                }
                catch (Exception ex)
                {
                    lblStatus.Visible = true;
                    lblStatus.Text = "(Error saving the Facebook tab! please check the exception log and notify the web admin!)";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                }
            }
        }

        protected void btnSaveWebsiteTab_Click(object sender, EventArgs e)
        {
            DealerBLL dBLL = new DealerBLL();
            DealerHolder websiteTab = new DealerHolder();
            DealerHolder dealerToSave = new DealerHolder();
            dealerToSave = (DealerHolder)ViewState["UpdatedDealer"];

            if (chkActive.Checked) ValidateWebsiteTab();

            if (Page.IsValid)
            {
                try
                {
                    dealerToSave.DealerKey = websiteTab.DealerKey = txtDealershipKey.Text;
                    dealerToSave.WebsiteCompany = websiteTab.WebsiteCompany = txtWebsiteCompany.Text;
                    dealerToSave.WebsiteContactName = websiteTab.WebsiteContactName = txtWebsiteContactName.Text;
                    dealerToSave.WebsiteContactPhone = websiteTab.WebsiteContactPhone = txtWebsiteContactPhone.Text;
                    dealerToSave.WebsiteContactEmail = websiteTab.WebsiteContactEmail = txtWebsiteContacEmail.Text;
                    dealerToSave.WebsiteComments = websiteTab.WebsiteComments = txtWebsiteComments.Text;

                    dBLL.SaveWebsiteTab(websiteTab);

                    //Save any updates to the Audit Table.
                    if (!(ViewState["OriginalDealer"] == null))
                    {
                        DealerBLL.AuditDealerChanges(dealerToSave, (DealerHolder)ViewState["OriginalDealer"]);
                        ViewState["OriginalDealer"] = ViewState["UpdatedDealer"];
                    }

                    lblStatus.Visible = true;
                    lblStatus.Text = "(Website tab Saved Successfully)";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    SetSavedSateForTab(PagePanels.WebsitePanel);
                }
                catch (Exception ex)
                {
                    lblStatus.Visible = true;
                    lblStatus.Text = "(Error saving the Website tab! please check the exception log and notify the web admin!)";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                }
            }
        }

        protected void chkActive_CheckedChanged(object sender, EventArgs e)
        {
            this.chkEnableWidget.Checked = false; // Inactive dealer cannot have widget enabled.
            if (_currentGroup.Equals(Enumerations.AuthUserGroup.AllDealerAdmin))
            {
                if (chkActive.Checked)
                {
                    try
                    {
                        DealerBLL.ActivateDealerByMainAdmin(txtDealershipKey.Text);

                        lblActivationStatus.Visible = true;
                        lblActivationStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    catch (Exception ex)
                    {
                        lblActivationStatus.Visible = true;
                        lblActivationStatus.Text = "(Error activation current dealer! Please check the exception log and notify the web admin!)";
                        lblActivationStatus.ForeColor = System.Drawing.Color.Red;
                        ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                    }
                }
                else
                {

                    try
                    {
                        DealerBLL.DeactivateDealerByMainAdmin(txtDealershipKey.Text, txtDealerName.Text, txtEmailLeads1.Text,
                                                                txtEmailLeads2.Text, txtEmailLeads3.Text, txtEmailLeads4.Text);

                        lblActivationStatus.Visible = true;
                        lblActivationStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    catch (Exception ex)
                    {
                        lblActivationStatus.Visible = true;
                        lblActivationStatus.Text = "(Error deactivation current dealer! Please check the exception log and notify the web admin!)";
                        lblActivationStatus.ForeColor = System.Drawing.Color.Red;
                        ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                    }
                }
            }

            SetValidetionGroups(chkActive.Checked);
            lblStatus.Text = string.Empty;
        }

        protected void btnContinueprovider_Click(object sender, EventArgs e)
        {
            try
            {
                String provider = TxtProvider.Text;

                if (provider.Equals(""))
                {
                    lblProvider.Text = "Please enter a provider name";
                    lblProvider.ForeColor = System.Drawing.Color.Orange;
                }
                else
                {
                    DealerBLL.AddLeadProvider(provider);
                    lblProvider.Text = "Provider added successfully";
                    lblProvider.ForeColor = System.Drawing.Color.Green;
                    LoadLeadproviders();
                }
            }
            catch (Exception ex)
            {
                lblProvider.Text = "Error adding the provider, Please check the exception log";
                lblProvider.ForeColor = System.Drawing.Color.Red;
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

        }

        protected void lnkDeleteProvider_Click(object sender, EventArgs e)
        {
            try
            {
                String provider = ddlProviderAndProduct.SelectedValue;

                int DuplicateCount = DealerBLL.CheckLeadProviderUsage(provider);

                if (DuplicateCount < 1)
                {
                    DealerBLL.DeleteLeadProvider(provider);
                    LoadLeadproviders();
                    lblProvider.Text = "lead provider deleted successfully";
                    lblProvider.ForeColor = System.Drawing.Color.Green;
                }

            }
            catch (Exception ex)
            {
                lblProvider.Text = "Error deleting the provider, Please check the exception log";
                lblProvider.ForeColor = System.Drawing.Color.Red;
                ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
            }

        }

        #endregion

        #region validation's methods

        private void ValidateTabs()
        {
            try
            {
                Page.Validate("vgInfoPanel");
                Page.Validate("vgCutomizationPanel");
                //Page.Validate("vgLeadPanel");
                //Page.Validate("vgBillingPanel");
                Page.Validate("vgWebsitePanel");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SetValidetionGroups(bool validate)
        {
            SetMandatoryFieldCssClass(validate);
        }

        private void ValidateDelerInfoTab()
        {
            try
            {
                Page.Validate("vgInfoPanel");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void ValidateCustomizeTab()
        {
            try
            {
                Page.Validate("vgCutomizationPanel");
                cvMakes.IsValid = lstMakes.Items.Count > 0;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void ValidateLeadTab()
        {
            try
            {
                Page.Validate("vgLeadPanel");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void ValidateBillingTab()
        {
            try
            {
                Page.Validate("vgBillingPanel");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void ValidateWebsiteTab()
        {
            try
            {
                Page.Validate("vgWebsitePanel");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        #region Misc Methods

        void ClearAllControls()
        {
            //Disabled Elements
            txtDealerName.Text = "";
            txtSalesPerson.Text = "";
            txtOriginalSalesPerson.Text = "";
            txtStreet.Text = "";
            txtCity.Text = "";
            txtZip.Text = "";
            txtAutoGroup.Text = "";
            txtContactEmail.Text = "";
            txtContactName.Text = "";
            txtContactPhone.Text = "";
            txtContactAppointment.Text = "";
            txtContactPhone2.Text = "";
            txtFax.Text = "";
            txtContactTitle.Text = "";
            txtContactName2.Text = "";
            txtContactEmail2.Text = "";
            txtContactPhoneTwo.Text = "";
            txtContactPhonetwo2.Text = "";
            txtFax2.Text = "";
            txtContactTitle2.Text = "";
            txtWebsite.Text = "";
            txtCreditAppUrl.Text = "";
            txtUsedinv.Text = "";
            txtNewInv.Text = "";
            txtWebsite.Text = "";
            txtGoogleUrchin.Text = "";
            txtComments.Text = "";
            txtWebsiteCompany.Text = "";
            txtWebsiteContactName.Text = "";
            txtWebsiteContactPhone.Text = "";
            txtWebsiteContacEmail.Text = "";
            txtLeadContactName.Text = "";
            txtLeadContactEmail.Text = "";
            txtLeadContactPhone.Text = "";
            txtLeadContactFax.Text = "";
            txtMonthlyBilling.Text = "";
            txtAdditionalInformationBilling.Text = "";
            txtReason.Text = "";
        }

        private static void SortListBox(ref ListBox pList)
        {
            try
            {
                SortedList lListItems = new SortedList();
                foreach (ListItem lItem in pList.Items)
                {
                    lListItems.Add(lItem.Value, lItem);
                }
                pList.Items.Clear();
                for (int i = 0; i < lListItems.Count; i++)
                    pList.Items.Add((ListItem)lListItems[lListItems.GetKey(i)]);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SetMandatoryFieldCssClass(bool isMandatory)
        {
            string cssClass = isMandatory ? "MandatoryField" : "";

            //Dealer Information Tab
            lblDealerName.CssClass = cssClass;
            lblSalesPerson.CssClass = cssClass;
            lblStreet.CssClass = cssClass;
            lblCity.CssClass = cssClass;
            lblState.CssClass = cssClass;
            lblZip.CssClass = cssClass;
            lblContactName.CssClass = cssClass;
            lblContactPhone.CssClass = cssClass;
            lblEmail.CssClass = cssClass;
            lblContactAppointment.CssClass = cssClass;

            lblDealerName.Text = string.Format("{0}{1}", lblDealerName.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblSalesPerson.Text = string.Format("{0}{1}", lblSalesPerson.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblStreet.Text = string.Format("{0}{1}", lblStreet.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblCity.Text = string.Format("{0}{1}", lblCity.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblState.Text = string.Format("{0}{1}", lblState.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblZip.Text = string.Format("{0}{1}", lblZip.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblContactName.Text = string.Format("{0}{1}", lblContactName.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblContactPhone.Text = string.Format("{0}{1}", lblContactPhone.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblEmail.Text = string.Format("{0}{1}", lblEmail.Text.TrimEnd('*'), isMandatory ? "*" : "");
            lblContactAppointment.Text = string.Format("{0}{1}", lblContactAppointment.Text.TrimEnd('*'), isMandatory ? "*" : "");

            //Tool Customization Tab
            lblDefaultMakes.CssClass = cssClass;

            lblDefaultMakes.Text = string.Format("{0}{1}", lblDefaultMakes.Text.TrimEnd('*'), isMandatory ? "*" : "");

            //Lead Management Tab
            LeadLabel1.CssClass = cssClass;

            LeadLabel1.Text = string.Format("{0}{1}", LeadLabel1.Text.TrimEnd('*'), isMandatory ? "*" : "");

            //Billing Tab
            billinglabel.CssClass = cssClass;

            billinglabel.Text = string.Format("{0}{1}", billinglabel.Text.TrimEnd('*'), isMandatory ? "*" : "");

            //Website Provider Tab
            websiteLabel1.CssClass = cssClass;
            websiteLabel2.CssClass = cssClass;
            websiteLabel4.CssClass = cssClass;

            websiteLabel1.Text = string.Format("{0}{1}", websiteLabel1.Text.TrimEnd('*'), isMandatory ? "*" : "");
            websiteLabel2.Text = string.Format("{0}{1}", websiteLabel2.Text.TrimEnd('*'), isMandatory ? "*" : "");
            websiteLabel4.Text = string.Format("{0}{1}", websiteLabel4.Text.TrimEnd('*'), isMandatory ? "*" : "");
        }

        private void CheckMatchesForDeactivatedDealers(String DealerName)
        {
            try
            {
                var matchesDealer = DealerBLL.GetMatchesFromDeactivatedDealers(txtDealerName.Text,
                                                                                        txtEmailLeads1.Text,
                                                                                        txtEmailLeads2.Text,
                                                                                        txtEmailLeads3.Text,
                                                                                        txtEmailLeads4.Text);
                if (matchesDealer.Rows.Count > 0)
                {
                    string body = string.Empty;
                    var row = matchesDealer.Rows[0];
                    if (row["LeadEmail"].ToString() == string.Empty)
                    {
                        body = string.Format("A dealer with Dealername '{2}' has been added to GetAutoAppraise and matches the name of the deactivated dealer '{0}' which was deactivated on '{1}'", row["DealerName"], row["DeactivationDate"], DealerName);
                    }
                    else
                    {
                        body = string.Format("A dealer with Dealername '{3}' has been added to GetAutoAppraise and matches the lead email '{1}' of the deactivated dealer: - '{0}' which was deactivated on '{2}'", row["DealerName"], row["LeadEmail"], row["DeactivationDate"], DealerName);
                    }

                    var message = new MailMessage();
                    message.To.Add("jbeard565@gmail.com ");
                    message.Subject = string.Format("Potential Deactivated Dealer Duplication in GAA");
                    message.Body = body;
                    message.From = new MailAddress(SettingManager.MailFrom);

                    Email.SendEmail(message);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private void SaveChangesForDeactivatedDealer()
        {
            try
            {
                DealerBLL.UpdateDeactivatedDealerData(txtDealershipKey.Text, txtDealerName.Text, txtEmailLeads1.Text,
                                                      txtEmailLeads2.Text, txtEmailLeads3.Text, txtEmailLeads3.Text);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        void SetOnChangeHandlerForAllControls()
        {
            //InfoPanel
            var infoPanelControls = new WebControl[]
                                        {
                                            txtDealerName, chkActive, txtSalesPerson, txtOriginalSalesPerson,
                                            ddlDealerGroup, txtAutoGroup, txtWebsite, rbProductType, txtStreet, txtCity,
                                            ddlStateID, txtZip, txtContactName, txtContactEmail, txtContactPhone,
                                            txtContactAppointment, txtContactPhone2, txtFax, txtContactTitle,
                                            txtContactName2, txtContactEmail2, txtContactPhoneTwo,
                                            txtContactAppointment2, txtContactPhonetwo2, txtFax2, txtContactTitle2
                                        };
            foreach (WebControl item in infoPanelControls) { item.Attributes.Add("onchange", string.Format("SetChangedState('{0}');", PagePanels.InfoPanel)); }

            //DataPanel
            var dataPanelControls = new WebControl[]
                                        {
                                            rblDataProvider, rblGalvesRangeCategory, rblTruckGalvesRangeCategory,
                                            rbtnLowPercent1, rbtnHighPercent1, LowSlider1, HighSlider1, rbtnLowValue1, 
                                            rbtnHighValue1, LowSliderDollar1, HighSliderDollar1, rbtnLowPercent2, rbtnHighPercent2,
                                            LowSlider2, HighSlider2, rbtnLowValue2, rbtnHighValue2, LowSliderDollar2, HighSliderDollar2, 
                                            rbtnLowPercent3, rbtnHighPercent3, LowSlider3, HighSlider3, rbtnLowValue3, rbtnHighValue3, 
                                            LowSliderDollar3, HighSliderDollar3, 
                                            LowSlider_Bound1, LowSliderDollar_bound1, HighSlider_Bound1, HighSliderDollar_bound1,
                                            LowSlider_Bound2, LowSliderDollar_bound2, HighSlider_Bound2, HighSliderDollar_bound2,
                                            LowSlider_Bound3, LowSliderDollar_bound3, HighSlider_Bound3, HighSliderDollar_bound3
                                        };
            foreach (WebControl item in dataPanelControls) { item.Attributes.Add("onchange", string.Format("SetChangedState('{0}');", PagePanels.DataPanel)); }

            //CutomizationPanel
            var cutomizationPanelControls = new WebControl[]
                                                {
                                                    txtAppraisalURL, chkInstantOption, chkUnsure, chkEmailAppraisal, chkRetailPricing, 
                                                    chkFourStep, chkTradeImage, chkNewSelection, chkTradeVideoUpload, chkNewVehicleIncentives, 
                                                    chkCurrentPayment, chkHideTradein, chkHyperLead, chkTaxIncentive, chkBalanceowed, lstNewMakes, 
                                                    lstMakes, lstFreeHNIDealers, lstHNIDealers, txtGoogleUrchin, ddlAppraisalExpiration, txtCreditAppUrl, 
                                                    chkCreditActive, ddlCreditAppText, txtUsedinv, chkUsedActive, txtNewInv, chkNewActive, ddlThemes, 
                                                    rbtnDefaultBackground, rblLang, rblNADABranding, txtComments
                                                };
            foreach (WebControl item in cutomizationPanelControls) { item.Attributes.Add("onchange", string.Format("SetChangedState('{0}');", PagePanels.CutomizationPanel)); }

            //LeadPanel
            var leadPanelControls = new WebControl[]
                                        {
                                            ddlProviderAndProduct, TxtProvider, txtEmailLeads1, txtEmailLeads2, rblLeadFormat1, rblLeadFormat2, 
                                            txtEmailLeads3, txtEmailLeads4, rblLeadFormat3, rblLeadFormat4, txtDealershipKey,
                                            txtLeadContactName, txtLeadContactEmail, txtLeadContactPhone, txtLeadContactFax
                                        };
            //Commented out by Freddy K 12/06/12
            //foreach (WebControl item in leadPanelControls) { item.Attributes.Add("onchange", string.Format("SetChangedState('{0}');", PagePanels.LeadPanel)); }

            //BillingPanel
            var billingPanelControls = new WebControl[]
                                           {
                                               chkBillable, txtMonthlyBilling, txtReason, txtAdditionalInformationBilling, txtAdminSetupFee
                                           };
            //Commented out by Freddy K 12/06/12
            //foreach (WebControl item in billingPanelControls) { item.Attributes.Add("onchange", string.Format("SetChangedState('{0}');", PagePanels.BillingPanel)); }

            //WebsitePanel
            var websitePanelControls = new WebControl[]
                                           {
                                                txtWebsiteCompany, txtWebsiteContactPhone, txtWebsiteContactName, txtWebsiteContacEmail    
                                           };
            foreach (WebControl item in websitePanelControls) { item.Attributes.Add("onchange", string.Format("SetChangedState('{0}');", PagePanels.WebsitePanel)); }
        }

        private void SetSavedSateForTab(PagePanels tabName)
        {
            switch (tabName)
            {
                case PagePanels.InfoPanel: hidInfoPanelIsChanged.Value = string.Empty; break;
                case PagePanels.DataPanel: hidDataPanelIsChanged.Value = string.Empty; break;
                case PagePanels.CutomizationPanel: hidCutomizationPanelIsChanged.Value = string.Empty; break;
                //Commented out by Freddy K 12/06/12
                //case PagePanels.LeadPanel: hidLeadPanelIsChanged.Value = string.Empty; break;
                //case PagePanels.BillingPanel: hidBillingPanelIsChanged.Value = string.Empty; break;
                case PagePanels.WebsitePanel: hidWebsitePanelIsChanged.Value = string.Empty; break;
                case PagePanels.All:
                    var hiddens = new[] { hidInfoPanelIsChanged, hidDataPanelIsChanged, hidCutomizationPanelIsChanged, 
                        hidLeadPanelIsChanged, 
                        hidBillingPanelIsChanged, hidWebsitePanelIsChanged };
                    foreach (HiddenField hf in hiddens) { hf.Value = string.Empty; }
                    break;
            }
        }

        private void DateChangedOnBillingPanel(object sender, EventArgs e)
        {
            hidBillingPanelIsChanged.Value = "changed";
        }

        private void RedirectIfNecessary()
        {
            if (!string.IsNullOrEmpty(Session["DealerAdmin_RedirectPath"].ToString()))
            {
                Response.Redirect(Session["DealerAdmin_RedirectPath"].ToString(), false);
            }
            else
            {
                if (Request.Params["__EVENTTARGET"].Contains("lnkManageDealers")) { Response.Redirect("~/PagesDealer/ManageDealer.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkAddDealear")) { Response.Redirect("~/PagesDealer/DealerAdmin.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkLeadsReports")) { Response.Redirect("~/PagesReport/LeadReport.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkBillableDealers")) { Response.Redirect("~/PagesReport/BillableDealersReport.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkManageUsers")) { Response.Redirect("~/PagesUsers/ManageUsers.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkManageGroups")) { Response.Redirect("~/PagesGroups/ManageGroups.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkManageDealerGroups")) { Response.Redirect("~/PagesGroups/ManageDealerGroups.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkAddVideo")) { Response.Redirect("~/PagesDealer/ManageVideo.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkExceptionLog")) { Response.Redirect("~/Pages/ExceptionLog.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkDraftDealers")) { Response.Redirect("~/PagesDealer/DraftDealers.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkDeactivatedDealers")) { Response.Redirect("~/PagesDealer/InActiveDealers.aspx", false); }
                else if (Request.Params["__EVENTTARGET"].Contains("lnkLogout")) { Response.Redirect("~/Login.aspx", false); }
            }
        }

        #endregion

        private string[] validationPanels = new string[] {
            "vgInfoPanel",
            "",
            "vgCutomizationPanel",
            "",
            "",
            "",
            "vgWebsitePanel",
            ""
        };

        private enum PagePanels
        {
            InfoPanel,
            DataPanel,
            CutomizationPanel,
            LeadPanel,
            BillingPanel,
            FacebookPanel,
            All,
            WebsitePanel


        }

        protected void chkEnableExcludedMakes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEnableExcludedMakes.Checked)
            {
                trExcludeMakes.Visible = true;
            }
            else
            {
                trExcludeMakes.Visible = false;
            }
        }

        protected void chkEnableValueRange_CheckedChanged(object sender, EventArgs e)
        {
            disableValueRanges(chkEnableValueRange.Checked);

        }
        private void disableValueRanges(bool enable)
        {
            this.rbtnHighPercent1.Enabled = enable;
            this.rbtnHighPercent2.Enabled = enable;
            this.rbtnHighPercent3.Enabled = enable;
            this.rbtnLowPercent1.Enabled = enable;
            this.rbtnLowPercent2.Enabled = enable;
            this.rbtnLowPercent3.Enabled = enable;

            this.rbtnHighValue1.Enabled = enable;
            this.rbtnHighValue2.Enabled = enable;
            this.rbtnHighValue3.Enabled = enable;
            this.rbtnLowValue1.Enabled = enable;
            this.rbtnLowValue2.Enabled = enable;
            this.rbtnLowValue3.Enabled = enable;

            LowSlider_Bound1.Enabled = enable;
            LowSlider1.Enabled = enable;
            HighSlider1.Enabled = enable;
            HighSlider_Bound1.Enabled = enable;

            LowSlider_Bound2.Enabled = enable;
            LowSlider2.Enabled = enable;
            HighSlider2.Enabled = enable;
            HighSlider_Bound2.Enabled = enable;

            LowSlider_Bound3.Enabled = enable;
            LowSlider3.Enabled = enable;
            HighSlider3.Enabled = enable;
            HighSlider_Bound3.Enabled = enable;

            LowSliderDollar_bound1.Enabled = enable;
            LowSliderDollar1.Enabled = enable;
            HighSliderDollar1.Enabled = enable;
            HighSliderDollar_bound1.Enabled = enable;

            LowSliderDollar_bound2.Enabled = enable;
            LowSliderDollar2.Enabled = enable;
            HighSliderDollar2.Enabled = enable;
            HighSliderDollar_bound2.Enabled = enable;

            LowSliderDollar_bound3.Enabled = enable;
            LowSliderDollar3.Enabled = enable;
            HighSliderDollar3.Enabled = enable;
            HighSliderDollar_bound3.Enabled = enable;

            LowSliderExtender1.BoundControlID = enable ? "LowSlider_Bound1" : "";

            HighSliderExtender1.BoundControlID = enable ? "HighSlider_Bound1" : "";

            LowSliderExtenderDollar1.BoundControlID = enable ? "LowSliderDollar_bound1" : "";

            HighSliderExtenderDollar1.BoundControlID = enable ? "HighSliderDollar_bound1" : "";


            LowSliderExtender2.BoundControlID = enable ? "LowSlider_Bound2" : "";
            HighSliderExtender2.BoundControlID = enable ? "HighSlider_Bound2" : "";
            LowSliderExtenderDollar2.BoundControlID = enable ? "LowSliderDollar_bound2" : "";
            HighSliderExtenderDollar2.BoundControlID = enable ? "HighSliderDollar_bound2" : "";

            LowSliderExtender3.BoundControlID = enable ? "LowSlider_Bound3" : "";
            HighSliderExtender3.BoundControlID = enable ? "HighSlider_Bound3" : "";
            LowSliderExtenderDollar3.BoundControlID = enable ? "LowSliderDollar_bound3" : "";
            HighSliderExtenderDollar3.BoundControlID = enable ? "HighSliderDollar_bound3" : "";


        }

    }
}
