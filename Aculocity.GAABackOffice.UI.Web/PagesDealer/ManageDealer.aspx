﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="ManageDealer.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.ManageDealer" Theme="BackOfficeMainTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        .tivBtn {
            background: transparent; 
            background-image: url(../Images/Buttons/orangebutton2-small-short.png); 
            background-repeat: no-repeat; 
            width: 135px; 
            border: 0; 
            line-height: 30px;
            font-size: 16px; 
            font-weight: bold;
            color: #fff;
        }
        .statText {
            overflow: auto;
            color: #666;
        }

        .statText h1,
        .statText h2 {
            clear: none;
            float: left;
            margin: 0;
            margin-right: 10px;
            font-weight: lighter;
        }

        .statText h1 {
            color: #666;
            font-size: 40pt;
            font-weight: normal;
            font-family: Arial;
        }
        .statText h2 {
            color: #F5831F;
            font-size: 18pt;
            font-weight: normal;
            font-family: Arial;
        }

        .statText.totalDealers p {
            line-height: 45px;
            font-size: 14pt;
            font-weight: lighter;
        }
        
        .statText.newDealers p {
            line-height: 7pt;
            font-size: 10pt;
            font-weight: lighter;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
<script language="JavaScript">
    changeclass("ctl00_liManageDealers");

    function setDefault() {
        //ctl00_PageContent_btnSearch
       
    }
</script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <div class="statText totalDealers">
                <h1><asp:Label ID="lblTotalDealers" runat="server"></asp:Label></h1>
                <p> Dealers</p>
            </div>
            <br />
        </td>
        <td>

        </td>
    </tr>
    <tr style="display: none;">
        <td ID="cellErrorDealers" runat="server" align="center">
            <asp:LinkButton ID="lnkErrorDealers" runat="server" style="color:black; background-color:White; cursor:pointer;"></asp:LinkButton>          
            <%--<span ID="spanErrorDealers" runat="server" style="color:black; background-color:White; cursor:pointer;"></span>--%>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Button ID="btnAddDealer" runat="server" CssClass="tivBtn" Text="+ Add Dealer" OnClick="BtnAddDealer_Click" />
        </td>
        <td>
            <div class="statText newDealers">
                <h2><asp:Label ID="lblNewDealersCount" runat="server"></asp:Label></h2> 
                <p>New Dealers This Month</p>
            </div>
        </td>
        <td align="right" style="padding-right:10px">
            <table border="0" cellpadding="0" cellspacing="5" style="display: none;">
                <tr>
                    <td align="left">
                        <asp:CheckBox ID="cbxShowDealersWOSpecialOfferAndImages" Text="Show Dealer Without Sepcial Offer Text or Image" runat="server" AutoPostBack="true" />            
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <asp:CheckBox ID="cbxShowOnlyActive" Text="Show Only Active" runat="server" AutoPostBack="true" />                                    
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="5">
                <tr>
                    <td>
                        <asp:DropDownList ID="ddlFields" runat="server" Width="150px"></asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtValue" runat="server" Width="200px"></asp:TextBox>
                    </td>
                 
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="Search" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
             <acw:SimpleGrid ID="sgDealer" runat="server" >
                <Grid>
                    <Columns>
                        <asp:BoundField DataField="DealerName" HeaderText="Dealer Name" />
                        <asp:BoundField DataField="DealerKey" HeaderText="Dealer Web Key" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <span class="LinkButton" onclick='ManageDealer_ShowSpecialOfferText("<%#DataBinder.Eval(Container.DataItem, "DealerKey")%>")'>
                                    Special Offer Text
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <span class="LinkButton" onclick='ManageDealer_ShowFaceBookHeaders("<%#DataBinder.Eval(Container.DataItem, "DealerKey")%>")'>
                                    Facebook Header
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <span class="LinkButton" onclick='ManageDealer_EditDealer("<%#Eval("DealerKey")%>")'>
                                    Edit
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <span class="LinkButton" onclick='ManageDealer_ShowUploadImages("<%#Eval("DealerKey")%>")'>
                                    Special Offer Banner
                                </span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                </Grid>
            </acw:SimpleGrid>
        </td>
    </tr>
</table>

<script type="text/javascript">
    function pageLoad(sender, args)
    {
        if(!args.get_isPartialLoad())
        {
            //  add our handler to the document's
            //  keydown event
            $addHandler(document, "keydown", onKeyDown);
            $addHandler(document, "keyup", onKeyUp);
        }
    }
    var ctrlOn = false;
    var altOn = false;
    var zOn = false;
    
    function onKeyDown(e)
    {
        if(e && e.keyCode == 17){
            ctrlOn = true;
        }
        if(e && e.keyCode == 18){
            altOn = true;
        }
        if(e && e.keyCode == 90){
            zOn = true;
        }
        
        if (ctrlOn && altOn && zOn)
        {
            window.location.href = '<%=Response.ApplyAppPathModifier("~//Pages//ExceptionMaintenance.aspx")%>';
        }
     } 
     
     function onKeyUp(e)
     {
        if(e && e.keyCode == 17){
            ctrlOn = false;
        }
        if(e && e.keyCode == 18){
            altOn = false;
        }
        if(e && e.keyCode == 90){
            zOn = false;
        }
     }
         
    function ManageDealer_EditDealer(DealerKey) {
        //SingleKeyEntityEditableGrid_ShowEditor('<%=Response.ApplyAppPathModifier("~//PagesBase/MainEditorHolder.aspx?BindingName=DealerTabHolder")%>' + '&DealerKey=' + DealerKey + '&ParentPath=' + escape('~//PagesDealer//ManageDealer.aspx'));
        //window.location.href = '<%=Response.ApplyAppPathModifier("~//PagesBase/MainEditorHolder.aspx?BindingName=DealerTabHolder")%>' + '&DealerKey=' + DealerKey + '&ParentPath=' + escape('~//PagesDealer//ManageDealer.aspx');
        window.location.href = '<%=Response.ApplyAppPathModifier("~//PagesDealer/DealerAdmin.aspx?DealerKey=")%>' + DealerKey + '&ParentPath=' + escape('~//PagesDealer//ManageDealer.aspx');
    }
    function ManageDealer_ShowSpecialOfferText(DealerKey) {
        //SingleKeyEntityEditableGrid_ShowEditor('<%=Response.ApplyAppPathModifier("~//PagesDealer//SpecialOfferText.aspx")%>?DealerKey=' + DealerKey);
        window.location.href = '<%=Response.ApplyAppPathModifier("~//PagesDealer//SpecialOfferText.aspx")%>?DealerKey=' + DealerKey;
    }
    function ManageDealer_ShowUploadImages(DealerKey) {
        //SingleKeyEntityEditableGrid_ShowEditor('<%=Response.ApplyAppPathModifier("~//PagesDealer//UploadImages.aspx")%>?DealerKey=' + DealerKey);
        window.location.href = '<%=Response.ApplyAppPathModifier("~//PagesDealer//UploadImages.aspx")%>?DealerKey=' + DealerKey;
    }
    function ManageDealer_ShowFaceBookHeaders(DealerKey) {
        window.location.href = '<%=Response.ApplyAppPathModifier("~//PagesDealer//FaceBookHeaders.aspx")%>?DealerKey=' + DealerKey;
    }
</script>
</asp:Content>
