﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

using Aculocity.SimpleData;

using Aculocity.Controls.Web;
using Aculocity.GAABackOffice.DAL;

namespace Aculocity.GAABackOffice.UI.Web {

    public partial class WebsiteProviderTab : EntityEditorUserControlBase {

        private string _dealerkey = string.Empty;
       
        public override Pair[] ControlFieldMappings {
            get {
                return new Pair[] {
                    new Pair("WebsiteCompany", txtWebsiteCompany),
                    new Pair("WebsiteContactName", txtContactName),
                    new Pair("WebsiteContactPhone", txtContactPhone),
                    new Pair("WebsiteContactEmail", txtContactEmail)
                };
            }
        }

        public override EntityValidator[] EntityValidators {
            get {
                return new EntityValidator[] {
                    new EntityNonEmptyFieldValidator("WebsiteContactPhone") {ErrorMessage = "Please enter a Website Company Contact Phone number."},
                    new EntityPhoneValidator("WebsiteContactPhone") {ErrorMessage = "Please enter a valid Website Company Contact Phone number. e.g. XXX-XXX-XXXX"},
                    new EntityNonEmptyFieldValidator("WebsiteContactEmail") {ErrorMessage = "Please enter a Website Contact E-mail address."},
                    new EntityMailAddressValidator("WebsiteContactEmail") { ErrorMessage = "Please enter a valid Website Company Company Contact E-mail address."} 
                };

            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Page.Load += new EventHandler(Page_Load);

            _dealerkey = Request.QueryString["DealerKey"];
        }

        void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_dealerkey))
            {
                (Page as MainEditorHolder).UpdateButton.Text = "AddDealer";
            }
        }
    }
}