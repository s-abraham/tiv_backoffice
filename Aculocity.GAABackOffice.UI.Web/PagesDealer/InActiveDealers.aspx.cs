﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class InActiveDealers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                ResetGridView();
            }
        }

        #region ' Grid's Events'
        protected void gvInActiveDealers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if(e.CommandName == "Activate")
                {
                    DealerBLL.ActivateDealerByMainAdmin(e.CommandArgument.ToString());
                    ResetGridView();
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region ' Methods '
        private void ResetGridView()
        {
            var dealers = DealerBLL.GetAllDeactivatedDealers();
            gvInActiveDealers.DataSource = dealers;
            gvInActiveDealers.DataBind();
        }

        #endregion
    }
}
