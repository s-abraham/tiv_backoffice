﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DealerTabHolder.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.DealerTabHolder" %>

<table border="0" cellpadding="0" cellspacing="0" width="100%" >
    <tr>
        <td style="width:150px; line-height:24px" valign="top" >
            <span ID="spanDealershipInfo" class="subMenu" runat="server" onclick="TabDealerHolder_ShowTab(0);" >Dealership Info</span> <br />         
            <span ID="spanLeadManagement" class="subMenu" runat="server" onclick="TabDealerHolder_ShowTab(1);"  >Lead Management</span> <br />
            <span ID="spanBilling" runat="server" class="subMenu" onclick="TabDealerHolder_ShowTab(2);" >Billing</span> <br />
            <span ID="spanWebsiteProvider" class="subMenu" runat="server" onclick="TabDealerHolder_ShowTab(3);" >Website Provider</span>
        </td> 
        <td ID="cellMainHolderA" runat="server">
            <asp:PlaceHolder ID="cellMainHolder" runat="server">
            </asp:PlaceHolder>
        </td>
    </tr>
    <asp:HiddenField ID="hidCurrentIndex" runat="server" />
</table>