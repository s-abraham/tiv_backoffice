﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DealerInfoTab.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.DealerInfoTab" %>
<%@ Register Src="~/PagesDealer/CurrentControls/NewReplacementVehicleMode.ascx" TagPrefix="NRVM" TagName="replacementVehicleMode"%>

<table border="0" cellpadding="0" cellspacing="2" width="100%"> 
    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                <tr>
                    <td style="width:150px;" valign="middle">
                        <h3>Dealer Information</h3>
                    </td>
                    <td valign="middle">
                        <h3><hr class="BlueLine"/></h3>
                    </td>
                </tr>
            </table> 
        </td>
    </tr>
    <tr>
        <td colspan="2">
           <table border="0" cellpadding="0" cellspacing="3">
                <tr ID="rowDealerNameAutomotiveGroup" runat="server">
                   <td class="CellLabel" style="width:150px">
                        <asp:label runat="server" ID="lblDealerName">Dealer Name</asp:label>
                   </td>
                   <td style="width:200px">
                        <asp:TextBox ID="txtDealerName" runat="server" Width="200" MaxLength="255"></asp:TextBox>
                   </td>
                    <td>
                        <asp:CheckBox ID="cbxActive" runat="server" Text="Active"></asp:CheckBox>
                    </td>
                    <td class="CellLabel">
                        <asp:label runat="server" ID="lblAutoGroup">Auto Group</asp:label>
                    </td> 
                    <td colspan="3">
                        <asp:TextBox ID="txtAutomotiveGroup" runat="server" Width="95%"></asp:TextBox>
                    </td>
                </tr>
                <tr ID="trSalesPersonContactNameContactEmail" runat="server">
                    <td class="CellLabel">
                        Sales Person
                    </td>
                    <td>
                        <asp:TextBox ID="txtSalesPerson" runat="server" Width="200" MaxLength="100"></asp:TextBox>
                    </td>
                    <td></td>
                    <td class="CellLabel">
                        Name
                    </td>
                    <td>
                        <asp:TextBox ID="txtContactName" runat="server" Width="150" MaxLength="100"></asp:TextBox>
                    </td>
                    <td class="CellLabel">
                        Email
                       
                    </td>
                     <td>
                        <asp:TextBox ID="txtContactEmail" runat="server" Width="150" MaxLength="400"></asp:TextBox>
                    </td>
                </tr>
                <tr ID="trOperatingHours" runat="server">
                    <td class="CellLabel">
                        Operating Hours
                    </td>
                    <td>
                        <asp:TextBox ID="txtOperatingHours" runat="server" MaxLength="500"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="CellLabel">
                        Original Sales Person
                    </td>
                    <td>
                        <asp:TextBox ID="txtOriginalSalesPerson" runat="server" Width="200" MaxLength="150"></asp:TextBox>
                    </td>
                    <td></td>
                    <td ID="cellContactPhoneLabel" runat="server" class="CellLabel">
                        Phone
                    </td>
                    <td ID="cellContactPhone" runat="server">
                        <asp:TextBox ID="txtContactPhone" runat="server" Width="150" MaxLength="20"></asp:TextBox>
                    </td>
                    <td ID="cellAppointmentEmailLabel" runat="server" class="CellLabel" width="150px">
                        Appointment Email
                   
                    </td>
                    <td ID="cellAppointmentEmail" runat="server">
                        <asp:TextBox ID="txtAppointmentEmail" runat="server" Width="150" MaxLength="400"></asp:TextBox>
                    </td>
                </tr>
                <tr ID="rowDealerGroupContactPhoneContactFax" runat="server">
                    <td ID="rowDealerGroupLabel" runat="server" class="CellLabel" width="150px">
                        Dealer Group
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDealerGroup" runat="server" Width="200px"></asp:DropDownList>
                    </td>
                    <td></td>
                    <td class="CellLabel">
                        Phone 2
                    </td>
                    <td>
                        <asp:TextBox ID="txtContactPhone2" runat="server" Width="150" MaxLength="20"></asp:TextBox>
                    </td>
                    <td class="CellLabel">
                        Fax
                    </td>
                    <td>
                        <asp:TextBox ID="txtContactFax" runat="server" Width="150" MaxLength="400"></asp:TextBox>
                    </td>
                </tr>
                <tr ID="rowStreetCityContactTitle" runat="server">
                    <td class="CellLabel">
                        Street
                    </td>
                    <td colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-top:3px">
                                    <asp:TextBox ID="txtStreet" runat="server" Width="150" MaxLength="100"></asp:TextBox>
                                </td>
                                <td class="CellLabel" style="padding-top:3px; padding-left:3px; width:30px">
                                    City
                                </td>
                                <td style="padding-top:3px; padding-left:3px">
                                    <asp:TextBox ID="txtCity" runat="server" Width="150" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="CellLabel">
                        Contact Title
                    </td>
                    <td>
                        <asp:TextBox ID="txtContactTitle" runat="server" Width="150" MaxLength="25"></asp:TextBox>
                    </td>
                </tr>
                <tr ID="rowStateZip" runat="server">
                    <td class="CellLabel">
                        State
                    </td>
                    <td colspan="2">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="padding-top:3px">
                                    <asp:TextBox ID="txtState" runat="server" Width="150" MaxLength="25"></asp:TextBox>
                                </td>
                                <td class="CellLabel" style="padding-top:3px; padding-left:3px; width:30px">
                                    Zip
                                </td>
                                <td style="padding-top:3px; padding-left:3px">
                                    <asp:TextBox ID="txtZip" runat="server" Width="150" MaxLength="25"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td ID="cellShowSecondContact" runat="server" colspan="2">
                        <asp:LinkButton ID="lnkShowSecondContact" runat="server" Text="Show Second Contact"></asp:LinkButton>
                    </td>
                </tr>
                <tr ID="rowSecondContact" runat="server">
                    <td colspan="3">
                    
                    </td>
                    <td colspan="4">
                        <table border="0" cellpadding="0" cellspacing="3">
                            <tr>
                                <td class="CellLabel">
                                     Name
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSecondContactName" runat="server" Width="150" MaxLength="100"></asp:TextBox>
                                </td>
                                <td class="CellLabel">
                                     Email
                                 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSecondContactEMail" runat="server" Width="150" MaxLength="400"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                     Phone
                           
                                 </td>
                                <td>
                                     <asp:TextBox ID="txtSecondContactPhone" runat="server" Width="150" MaxLength="20"></asp:TextBox>
                                </td>
                                <td class="CellLabel">
                                    Dealer Title
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSecondDealerTitle" runat="server" Width="150" MaxLength="400"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                     Phone 2
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSecondContactPhone2" runat="server" Width="150" MaxLength="20"></asp:TextBox>
                                </td>
                                <td class="CellLabel">
                                    Fax
                             
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSecondContactFax" runat="server" Width="150" MaxLength="400"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
   
           </table> 
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                <tr>
                    <td style="width:150px;" valign="middle">
                        <h3>Tool Customization</h3>
                    </td>
                    <td valign="middle">
                        <h3><hr class="BlueLine"/></h3>
                    </td>
                </tr>
            </table> 
        </td>
    </tr>
    <tr>
        <td  colspan="2" valign="top">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="3">
                            <tr ID="rowAppraisalURL" runat="server">
                                <td class="CellLabel" valign="top" style="width:150px">
                                    Appraisal URL 
                                </td>
                                <td style="width:300px">
                                    <asp:TextBox ID="txtAppraisalURL" runat="server" Width="300px" Height="100px" TextMode="MultiLine" MaxLength="255"></asp:TextBox>
                                </td>
                                <td valign="top">
                                    <asp:Button ID="btnView" runat="server" Text="View" Width="100px" OnClientClick="DealerInfoTab_ViewDealerSite(); return false;" />
                                </td>
                                <td></td>
                            </tr>
                            <tr ID="rowCreditApplicationURL" runat="server"> 
                                <td class="CellLabel">
                                    Credit App URL
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCreditApplicationUrl" runat="server" Width="300" MaxLength="500"></asp:TextBox>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkCreditApplicationActive" runat="server" Text="Active" />        
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtCreditApplicationURL" runat="server" Text="Credit Application URL" />                                
                                            </td>
                                        </tr>
                                    </table>    
                                </td>
                            </tr>
                            <tr ID="rowCreditAppticationText" runat="server">
                                <td class="CellLabel" valign="top">
                                    Credit App Text 
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCreditApplicationText" runat="server" Width="300px"></asp:DropDownList> 
                                </td>
                            </tr>
                            <tr ID="rowUsedCarInventory" runat="server">
                                <td class="CellLabel">
                                    Used Inventory 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUsedCarInventory" runat="server" Width="300" MaxLength="500"></asp:TextBox>
                                </td>
                                <td>
                                     <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkUsedCarInventory" runat="server" Text="Active" />
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtUsedCarInventory" runat="server" Text="Pre-owned inventory URL" />                                                                    
                                            </td>
                                        </tr>
                                     </table>
                                </td>
                            </tr>
                            <tr ID="rowNewCarInventory" runat="server">
                                <td class="CellLabel">
                                    New Inventory 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNewCarInventory" runat="server" Width="300" MaxLength="500"></asp:TextBox>
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkNewCarInventory" runat="server" Text="Active" />
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtNewCarInventory" runat="server" Text="New inventory URL" />                                                                    
                                            </td>
                                        </tr>
                                     </table>
                                    
                                </td>
                            </tr>
                            <tr ID="rowPublicWebSite" runat="server">
                                <td class="CellLabel">
                                    WebSite 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPublicWebSite" runat="server" Width="300" MaxLength="500"></asp:TextBox>
                                    
                                </td>
                                <td>
                                    <GAABackOfficeControls:QuestionTip ID="qtPublicWebSite" runat="server" Text="Public website URL" />
                                </td>
                            </tr>
                            <tr ID="trGoodleUrchin" runat="server">
                                <td class="CellLabel">
                                    Google Analytics Urchin
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUrchin" runat="server"></asp:TextBox>
                                </td>
                             </tr>
                             <tr ID="trAppraisalExpiration" runat="server"  >
                                <td class="CellLabel">
                                    Appraisal Expiration(days)
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAppraisalExpiration" runat="server" Width="100px">
                                    </asp:DropDownList>
                                </td>
                             </tr>    
                             <tr>
                                <td>
                                     <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td class="CellLabel">
                                                 App Theme       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="QuestionTip2" runat="server" Text="Select a theme to update the look and feel of the appraisal application." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlThemes" runat="server" Width="150px">
                                    </asp:DropDownList>
                                </td>
                             </tr>
                             <tr ID="trEmailTitle" runat="server">
                                    <td class="CellLabel" style="width:150px">
                                        Email Title Text
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEmailTitle" runat="server" MaxLength="200"></asp:TextBox>    
                                    </td>
                               </tr>   
                               <tr>
                                    <td colspan="7">
                                        <NRVM:replacementVehicleMode ID="nrvmReplacementVehicleMode" runat="server" />
                                    </td>
                               </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="3">
                            <tr ID="rowInstantOptionPricingUnsure" runat="server">
                                <td class="CellLabel">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                Instant Option Pricing        
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtInstantOptionPricing" runat="server" Text="When checked the optional equipment pricing will be displayed on the first page." />                                                                    
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkInstantOptionPricing" runat="server" />
                                </td>
                                <td class="CellLabel" style="width:150px">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                Unsure        
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtUnsure" runat="server" Text="When unchecked the Ignore option for the replacement vehicle is removed, thus forcing a replacement vehicle selection." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </td>
                                <td>
                                     <asp:CheckBox ID="chkUnsure" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                Email Appraisal Only        
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtEmaiAppraisalOnly" runat="server" Text="When checked the vehicle appraisal amount will not be displayed on final page, just emailed to user." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkEmailAppraisalOnly" runat="server" />
                                </td>
                               
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                     <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                Four Step Process       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtFourStepProcess" runat="server" Text="When checked the vehicle optional equipment selection is moved onto a separate page <br> and act as the 2nd page thus making it 4 steps instead of 3. <br>In addition the ability to select replacement vehicle is also removed." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkFourStepProcess" runat="server" />
                                </td>
                                <td class="CellLabel" style="width:150px">
                                     <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                Retail Pricing       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtRetailPricing" runat="server" Text="When checked the retail pricing model is employed for the trade vehicle as oppose to the trade pricing model." />                                                                                                        
                                            </td>
                                        </tr>
                                     </table>
                                </td>
                                <td>
                                     <asp:CheckBox ID="chkRetailPricing" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                     <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                New Selection       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtNewSelection" runat="server" Text="When unchecked the New replacement vehicle option is removed." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkNewSelection" runat="server" />
                                </td>
                                <td ID="cellTradeImageUploadLabel" runat="server" class="CellLabel" style="width:150px">
                                      <table border="0" cellpadding="0" cellspacing="3">
                                         <tr>
                                            <td>
                                                Trade Image Upload       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtTradeImageUpload" runat="server" Text="When checked the user have ability to upload their trade vehicle images on the first page." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td ID="cellTradeImageUpload" runat="server">
                                     <asp:CheckBox ID="chkTradeImageUpload" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                     <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                Balance Owned       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtBalanceOwned" runat="server" Text="When checked the user have the ability to enter their trade vehicle settlement amount." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkBalanceOwned" runat="server" />
                                </td>
                                <td ID="tdHideConditionLabel" runat="server" class="CellLabel" style="width:150px">
                                    Hide vehicle Condition
                                </td>
                                <td ID="tdHideCondition" runat="server">
                                    <asp:CheckBox ID="chkHideVehicleCondition" runat="server" />
                                </td>
                            </tr>
                            <tr>
                             
                                <td ID="tdHideEmailTradeInLabel" runat="server" class="CellLabel">
                                    Hide Text Email Trade-in
                                </td>
                                <td ID="tdHideEmailTradeIn" runat="server">
                                    <asp:CheckBox ID="chkHideEmailTradeIn" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                New Vehicle Incentives       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="QuestionTip1" runat="server" Text="When checked the new vehicle incentive data will be displayed on special offer page (30.aspx)." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>           
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkNewVehicleIncentives" runat="server" />
                                </td>
                                <td ID="tdHideSpecialOfferLabel" runat="server" class="CellLabel">
                                    Hide Special offer on Special offer page
                                </td>
                                <td ID="tdHideSpecialOffer" runat="server">
                                    <asp:CheckBox ID="chkHideSpecialOffer" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="CellLabel">
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td>
                                                Current Monthly Payment       
                                            </td>
                                            <td>
                                                <GAABackOfficeControls:QuestionTip ID="qtCurrentMonthlyPayment" runat="server" Text="When checked the user have the ability to enter their trade vehicle monthly payment amount." />                                                                                                        
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkCurrentMonthlyPayment" runat="server" />
                                </td>
                                <td ID="tdHideAddressLabel" runat="server" class="CellLabel">
                                    Hide Physical Address
                                </td>
                                <td ID="tdHideAddress" runat="server">
                                    <asp:CheckBox ID="chkHideAddress" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:5px;">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="CellLabel" valign="top" style="width:150px;">
                                    Dealership Default Makes
                                </td>
                                <td>
                                  <acw:SelectorListBoxes ID="slbDealershipDefaultMakes" runat="server" Width="200px" Height="150px" AllowMultipleSelection="true"></acw:SelectorListBoxes>
                                </td>
                            </tr>
                            <tr><td colspan="2" style="height:10px"></td></tr>
                            <tr>
                               <td ID="cellDealerHNIMappingsLabel" runat="server" class="CellLabel" valign="top">
                                    Dealership HNI Mappings
                                </td>
                                <td>
                                   <acw:SelectorListBoxes ID="slbDealershipHNIMappings" runat="server" Width="200px" Height="150px" AllowMultipleSelection="true"></acw:SelectorListBoxes>    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                   <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                               <td class="CellLabel" style="width:150px">
                                     Default Language 
                                </td>
                                <td>
                                   <GAABackOfficeControls:TwoValuesControl ID="tvcLanguages" runat="server" FirstCaption="English" SecondCaption="Spanish"/> 
                                </td>
                            </tr>
                        </table>
                   </td>
                </tr>
                <tr id="rowNADABranding" runat="server">
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="CellLabel" class="CellLabel" style="width:150px">
                                    NADA Branding
                                </td>
                                <td>
                                    <GAABackOfficeControls:TwoValuesControl ID="tvcNADABRanding" runat="server"  FirstCaption="On" SecondCaption="Off" />
                               </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td ID="rowCommentsLabel" runat="server" class="CellLabel" valign="top" style="width:150px">
                                    Comments
                               </td>
                               <td ID="rowComments" runat="server" rowspan="2" colspan="3" style="padding-left:5px">
                                    <asp:TextBox ID="txtComments" runat="server" Width="400px" Height="50px" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                               </td>
                            </tr>
                        </table>
                   </td>
                </tr>
            </table>
        </td>
   </tr>
    <tr>
        <td colspan="2">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                <tr>
                    <td style="width:240px;" valign="middle">
                        <h3>Data Provider and Value Range</h3>
                    </td>
                    <td valign="middle">
                        <h3><hr class="BlueLine"/></h3>
                    </td>
                </tr>
            </table> 
        </td>
    </tr>
    <tr>
        <td colspan="2">
           <%--<asp:UpdatePanel ID="updAverageRC" runat="server" UpdateMode="Always">
                <ContentTemplate>--%>
                    <table border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="CellLabel" style="width:150px">
                        Data Provider
                    </td>
                    <td colspan="2">
                        <GAABackOfficeControls:TwoValuesControl ID="tvcDataProvider" runat="server" FirstCaption="GALVES" SecondCaption="NADA" AutoPostBack="true"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr ID="rowAverageRangeCategoryLabel" runat="server">
                                <td class="CellLabel" style="width:150px">
                                    Average Range Category
                                </td>
                                <td style="padding-left:5px">
                                    <b>Cars</b>
                                </td>
                                <td>
                                    <b>Trucks</b>
                                </td>
                            </tr>
                            <tr ID="rowAverageRangeCategory" runat="server">
                                <td style="width:150px">
                                </td>
                                <td style="vertical-align: top; padding-left:3px;">
                                    <asp:RadioButtonList ID="rblGalvesRangeCategory" runat="server" RepeatDirection="Vertical"
                                        Font-Bold="true" CellSpacing="0" CellPadding="0">
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rblTruckGalvesRangeCategory" runat="server" RepeatDirection="vertical"
                                        Font-Bold="true" CellSpacing="0" CellPadding="0">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr ID="rowCustomValueRange" runat="server">
                                <td class="CellLabel">
                                    Custom Value Range 
                                </td>
                                <td colspan="4" style="vertical-align: top;">
                                
                                    <table border="0" cellpadding="0" cellspacing="3">
                                        <tr>
                                            <td align="center" class="SimpleLabel">
                                                Lower 
                                                <asp:RadioButton ID="rbLowerPercent" runat="server" Text="(%)" GroupName="Lower" />
                                            </td>
                                            <td class="SimpleLabel">
                                                To
                                            </td>
                                            <td align="center" class="SimpleLabel">
                                                Higher
                                                <asp:RadioButton ID="rbHigherPercent" runat="server" Text="(%)" GroupName="Higher"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                               
                                                <GAABackOfficeControlsSlider:Slider ID="sliderLowerPercent" runat="server"/> 
                                               
                                            </td>
                                            <td></td>
                                            <td>
                                                <GAABackOfficeControlsSlider:Slider ID="sliderHigherPercent" runat="server"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="SimpleLabel">
                                                Lower 
                                                <asp:RadioButton ID="rbLowerAmount" runat="server" Text="($)" GroupName="Lower"/>
                                            </td>
                                            <td class="SimpleLabel">
                                                To
                                            </td>
                                            <td align="center" class="SimpleLabel">
                                                Higher
                                                <asp:RadioButton ID="rbHigherAmount" runat="server" Text="($)" GroupName="Higher"/>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <GAABackOfficeControlsSlider:Slider ID="sliderLowerAmount" runat="server"/> 
                                            </td>
                                            <td></td>
                                            <td>
                                                <GAABackOfficeControlsSlider:Slider ID="sliderHigherAmount" runat="server"/>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
         <%--</ContentTemplate>
         </asp:UpdatePanel>--%>
        </td>
    </tr>
</table>
<script type="text/javascript">

    function DealerInfoTab_ViewDealerSite() {
        ModalDialog_ShowModalWindow(document.getElementById(DealerInfoTab_txtAppraisalURLId).value, '1000px', '1000px', null, null, '1', '1');
    }
    
    
    

</script>

