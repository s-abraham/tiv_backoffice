﻿using System;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class SpecialOfferText : BasePage
    {

        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    string dealerKey = Request.QueryString["dealerKey"];
                    lnkEnglish.Attributes.Add("class", "selected");
                    ShowHideDateControls(false);
                    LoadDealerHTML(dealerKey);

                    string startDate = string.Empty, stopDate = string.Empty;
                    ShowHideDateControls(DealerBLL.GetOfferTextDateRange(dealerKey, ref startDate, ref stopDate));
                    txtActiveDate.Text = startDate;
                    txtInactiveDate.Text = stopDate;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' controls's events '
        protected void lnkbtnEnable_Click(object sender, EventArgs e)
        {
            try
            {
                ShowHideDateControls(lnkbtnEnable.Text == "Enable");
                if (string.IsNullOrEmpty(txtActiveDate.Text.Trim()) || Convert.ToDateTime(txtActiveDate.Text) < DateTime.Now) txtActiveDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                if (string.IsNullOrEmpty(txtInactiveDate.Text.Trim()) || Convert.ToDateTime(txtInactiveDate.Text) < DateTime.Now) txtInactiveDate.Text = DateTime.Now.AddMonths(1).ToString("MM/dd/yyyy");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void ShowHideDateControls(bool isEnable)
        {
            try
            {
                lblEndDate.Visible = isEnable;
                lblStartDate.Visible = isEnable;
                txtActiveDate.Visible = isEnable;
                imgbtnActiveDate.Visible = isEnable;
                imgbtnInactiveDate.Visible = isEnable;
                txtInactiveDate.Visible = isEnable;
                lnkbtnEnable.Text = isEnable ? "Disable" : "Enable";
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void lnk_Click(object sender, EventArgs e)
        {
            try
            {
                lnkEnglish.Attributes.Add("class", "");
                lnkSpanish.Attributes.Add("class", "");
                ((HtmlAnchor)sender).Attributes.Add("class", "selected");

                if (((HtmlAnchor)sender).ID == "lnkEnglish")
                {
                    txtSpanish.Text = ftbOfferText.Text;
                    ftbOfferText.Text = txtEnglish.Text;
                    txtLang.Text = "eng";
                }
                else
                {
                    txtEnglish.Text = ftbOfferText.Text;
                    ftbOfferText.Text = txtSpanish.Text;
                    txtLang.Text = "esp";
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                lblFeedback.Text = "";
                if (StripHTMLTagsLength(txtEnglish.Text.Trim()) > 1000) {
                //    lblFeedback.ForeColor = Color.Red;
                //    lblFeedback.Text += "<li>Please limit your English text to 1000 characters or less.";
                }
                
                if (StripHTMLTagsLength(txtSpanish.Text.Trim()) > 1000) {
                //    lblFeedback.ForeColor = Color.Red;
                //    lblFeedback.Text += "<li>Please limit your Spanish text to 1000 characters or less.";
                }

                if(lblFeedback.Text.Trim() == string.Empty)
                {
                    string htmlEng = txtLang.Text == "eng" ? ftbOfferText.Text.Trim() : txtEnglish.Text.Trim();
                    string htmlEsp = txtLang.Text == "esp" ? ftbOfferText.Text.Trim() : txtSpanish.Text.Trim();
                    DealerBLL.UpdateOfferText(Request.QueryString["dealerKey"].Trim(), "eng", htmlEng);
                    DealerBLL.UpdateOfferText(Request.QueryString["dealerKey"].Trim(), "esp", htmlEsp);

                    if (string.IsNullOrEmpty(txtActiveDate.Text.Trim()) || Convert.ToDateTime(txtActiveDate.Text) < DateTime.Now) txtActiveDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    if (string.IsNullOrEmpty(txtInactiveDate.Text.Trim()) || Convert.ToDateTime(txtInactiveDate.Text) < DateTime.Now) txtInactiveDate.Text = DateTime.Now.AddMonths(1).ToString("MM/dd/yyyy");
                    
                    DealerBLL.UpdateOfferTextDate(Request.QueryString["dealerKey"].Trim(),
                                                    Convert.ToDateTime(txtActiveDate.Text, CultureInfo.InvariantCulture),
                                                    Convert.ToDateTime(txtInactiveDate.Text, CultureInfo.InvariantCulture),
                                                    txtActiveDate.Visible);
                }
                lblFeedback.Text = "(Dealership updated successfully.)";
                lblFeedback.ForeColor = Color.Gray;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (_currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin)
                {
                    Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
                }
                else
                {
                    Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion
        
        #region ' Custom Methods '
        private void LoadDealerHTML(string dealerKey)
        {
            try
            {
                var offerText = DealerBLL.GetOfferText(dealerKey);
                ftbOfferText.Text = offerText[0];
                txtLang.Text = "eng";

                txtEnglish.Text = offerText[0];
                txtSpanish.Text = offerText[1];
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        private int StripHTMLTagsLength(string htmlToStrip)
        {
            string stripped;
            try
            {
                if (!string.IsNullOrEmpty(htmlToStrip))
                {
                    stripped = Regex.Replace(htmlToStrip, "<(.|\\n)+?>", string.Empty);
                    return stripped.Length;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return 0;
        }
        #endregion

    }
}
