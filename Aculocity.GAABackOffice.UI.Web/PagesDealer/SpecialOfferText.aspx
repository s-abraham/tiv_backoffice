﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true"
    CodeBehind="SpecialOfferText.aspx.cs" ValidateRequest="false" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.SpecialOfferText"
    Theme="BackOfficeMainTheme" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">

    <script language="JavaScript">
        changeclass("ctl00_liSpecialOfferText");
    </script>

    <table width="100%">
        <tr>
            <td align="center">
                <br />
                <div align="left">
                    <%--                    <asp:UpdatePanel ID="upFTB" runat="server">
                        <ContentTemplate>--%>
                    <table width="100%">
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblFeedback" runat="server" EnableViewState="False" ForeColor="Gray">
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="bottom" align="left" style="width: 200px">
                                <div id="tabs">
                                    <p>
                                        <ul>
                                            <li id="liEnglish" runat="server"><a id="lnkEnglish" runat="server" onserverclick="lnk_Click">
                                                English Text</a></li>
                                            <li id="liSpanish" runat="server"><a id="lnkSpanish" runat="server" onserverclick="lnk_Click">
                                                Spanish Text</a></li>
                                        </ul>
                                    </p>
                                </div>
                            </td>
                            <td align="center">
                                <%--                                        <asp:UpdatePanel ID="upDateRange" runat="server">
                                                <ContentTemplate>--%>
                                <table>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                        </td>
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Date Range:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtActiveDate" runat="server" MaxLength="100" Width="100px" Enabled="false"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="calExtActiveDate" runat="server" TargetControlID="txtActiveDate"
                                                PopupButtonID="imgbtnActiveDate" Format="MM/dd/yyyy">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgbtnActiveDate" ImageUrl="../Images/Calendar/cal.gif" AlternateText="Pick a date"
                                                runat="server"></asp:ImageButton>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtInactiveDate" runat="server" MaxLength="100" Width="100px" Enabled="false"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="calExtInactiveDate" runat="server" TargetControlID="txtInactiveDate"
                                                PopupButtonID="imgbtnInactiveDate" Format="MM/dd/yyyy">
                                            </ajaxToolkit:CalendarExtender>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="imgbtnInactiveDate" ImageUrl="../Images/Calendar/cal.gif" AlternateText="Pick a date"
                                                runat="server"></asp:ImageButton>
                                        </td>
                                        <td valign="middle">
                                            &nbsp;&nbsp;
                                            <asp:Button ID="lnkbtnEnable" runat="server" Text="Enable" OnClick="lnkbtnEnable_Click" />
                                        </td>
                                    </tr>
                                </table>
                                <%--                                                </ContentTemplate> 
                                        </asp:UpdatePanel>--%>
                            </td>
                            <td style="width: 200px" />
                        </tr>
                        <tr>
                            <td style="padding: 5px" colspan="3">
                                <p>
                                    <FTB:FreeTextBox ID="ftbOfferText" runat="Server" EnableToolbars="true" EnableHtmlMode="true"
                                        Height="300px" Width="100%" SupportFolder="aspnet_client/FreeTextBox/" ButtonSet="Office2003"
                                        BackColor="158, 190, 245" GutterBackColor="169, 226, 226">
                                    </FTB:FreeTextBox>
                                </p>
                                <div style="display: none;">
                                    <asp:TextBox ID="txtEnglish" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    <asp:TextBox ID="txtSpanish" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    <asp:TextBox ID="txtLang" runat="server"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="Left" colspan="3">
                                <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Text="Update Changes" OnClick="btnUpdate_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblEng" runat="server" Font-Size="11px" Text="<b>Maximum text length:</b> 1000 characters">
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                    <%--                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="lnkEnglish" />
                            <asp:PostBackTrigger ControlID="lnkSpanish" />
                            <asp:PostBackTrigger ControlID="lnkbtnEnable" />
                            <asp:PostBackTrigger ControlID="btnUpdate" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
