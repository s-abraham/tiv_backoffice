﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class HNIFeeds : System.Web.UI.Page
    {
        protected DataTable _dealerList = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Request.Form["type"] == null)
            {

                Label MasterLabel = (Label)Form.Parent.FindControl("lblTitle");
                MasterLabel.Text = "Dealer Administration - HNI Feeds";
                _dealerList = DealerBLL.GetHNIFeedDealerList();
            }
            else
            {
                string json = "";
                switch (this.Request.Form["type"].Trim().ToLower())
                {
                    case "dlist":
                        DataTable dlist = DealerBLL.GetDealerList();
                        if (dlist != null)
                        {
                            json = Newtonsoft.Json.JsonConvert.SerializeObject(dlist);
                        }
                        break;

                    case "upd":
                        string hnidealerid = this.Request.Form["hnidealerid"];
                        string dealerkey = this.Request.Form["dealerkey"];
                        DealerBLL.AssignPrimaryDealerToHNIFeed(hnidealerid, dealerkey);
                        break;

                    default:
                        break;
                }

                Response.ContentType = "application/json";
                Response.BinaryWrite(System.Text.Encoding.UTF8.GetBytes(json));
                Response.End();

            }

        }


    }
}