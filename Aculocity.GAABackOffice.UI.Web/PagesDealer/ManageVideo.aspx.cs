﻿using System;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class AddVideo : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack) ReFillDealerLists();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' Controls's Events '
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DealerBLL.AllowVideo(lstDealers.SelectedValue);
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
            ReFillDealerLists();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DealerBLL.DenyVideo(lstSelectedDealers.SelectedValue);
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
            ReFillDealerLists();
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~//PagesDealer//ManageDealer.aspx");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region ' Methods '
        private void FillNonVideoDealerList()
        {
            try
            {
                var dealers = new Dealers();
                dealers.GetNonVideoDealers();

                lstDealers.DataTextField = "DealerName";
                lstDealers.DataValueField = "DealerKey";
                lstDealers.DataSource = dealers;
                lstDealers.DataBind();
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }
        
        private void FillAllowedVideoDealerList()
        {
            try
            {
                var dealers = new Dealers();
                dealers.GetAllowedVideoDealers();

                lstSelectedDealers.DataTextField = "DealerName";
                lstSelectedDealers.DataValueField = "DealerKey";
                lstSelectedDealers.DataSource = dealers;
                lstSelectedDealers.DataBind();
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        private void ReFillDealerLists()
        {
            try
            {
                FillNonVideoDealerList();
                FillAllowedVideoDealerList();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion
    }
}