﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aculocity.GAABackOffice.UI.Web.PagesDealer
{
    public partial class LeadSummaryReport : UserControl {

        public string DealerKey {
            get;
            set;
        }
        
        public DateTime PeriodFrom {
            get;
            set;
        }

        public DateTime PeriodTill {
            get;
            set;
        }

        public string FirstName {
            get;
            set;
        }

        public string LastName {
            get;
            set;
        }

        public string UserEmail {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public string LeadsPerPage
        {
            get;
            set;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);

            if (Page.IsPostBack)
                LoadDataFromViewState();

        }
        
        protected override void OnPreRender(EventArgs e) {
            base.OnPreRender(e);

            if (!Page.IsPostBack) 
                SaveDataToViewState();   
            

        }

        public void GenerateReport() {
            lrcCharts.Visible = true;
            lrcCharts.DealerKey = DealerKey;
            lrcCharts.GenerateCharts("2");

            lrdmgGrid.DealerKey = DealerKey;
            lrdmgGrid.PeriodFrom = PeriodFrom;
            lrdmgGrid.PeriodTill = PeriodTill;
            lrdmgGrid.FirstName = FirstName;
            lrdmgGrid.LastName = LastName;
            lrdmgGrid.UserMail = UserEmail;
            lrdmgGrid.Phone = Phone;
            

            Session["SummaryDealerKey"] = DealerKey;
            Session["SummaryPeriodFrom"] = PeriodFrom;
            Session["SummaryPeriodTill"] = PeriodTill;
            Session["SummaryFirstName"] = FirstName;
            Session["SummaryLastName"] = LastName;
            Session["SummaryUserMail"] = UserEmail;
            Session["SummaryPhone"] = Phone;
            Session["SummaryLeadsPerPage"] = LeadsPerPage;

            lrdmgGrid.GenerateReport();

            SaveDataToViewState();
        }

        void SaveDataToViewState() {

            ViewState["DealerKey"] = DealerKey;
            ViewState["PeriodFrom"] = PeriodFrom;
            ViewState["PeriodTill"] = PeriodTill;
            ViewState["FirstName"] = FirstName;
            ViewState["LastName"] = LastName;
            ViewState["UserMail"] = UserEmail;
            ViewState["Phone"] = Phone;
            ViewState["LeadsPerPage"] = LeadsPerPage;
        
        }

        void LoadDataFromViewState() {
            DealerKey = (ViewState["DealerKey"] ?? string.Empty).ToString();
            PeriodFrom = (DateTime)(ViewState["PeriodFrom"] ?? DateTime.MinValue);
            PeriodTill = (DateTime)(ViewState["PeriodTill"] ?? DateTime.MinValue);
            FirstName = (ViewState["FirstName"] ?? string.Empty).ToString();
            LastName = (ViewState["LastName"] ?? string.Empty).ToString();
            UserEmail = (ViewState["UserMail"] ?? string.Empty).ToString();
            Phone = (ViewState["Phone"] ?? string.Empty).ToString();
            Phone = (ViewState["LeadsPerPage"] ?? string.Empty).ToString();
        }

    }
}