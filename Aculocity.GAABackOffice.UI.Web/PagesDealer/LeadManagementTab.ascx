﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="LeadManagementTab.ascx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesDealer.LeadManagementTab" %>

<table border="0" cellpadding="0" cellspacing="3">
    <tr ID="rowProviderAndProduct" runat="server">
        <td class="CellLabel">
            Provider And Product
        </td>
        <td>
            <asp:DropDownList ID="ddlProviderAndProduct" runat="server" Width="150px"></asp:DropDownList>
        </td>
        <td style="width:150px;"></td>
    </tr>
    <tr>
        <td class="CellLabel">
            Lead Email 1
            <%--<asp:RegularExpressionValidator ID="valRegLeadEmail1" runat="server" ControlToValidate="txtEmailLeads1"
                                                Display="Dynamic" ErrorMessage="Please enter a valid first Lead Email address."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="valReqLeadEmail1" runat="server" ControlToValidate="txtLeadEmail1" Display="Dynamic"
                                                    ErrorMessage="Please enter the first Lead Email address." ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtEmailLeads1" runat="server" Width="150px"></asp:TextBox>
        </td>
        <td class="CellLabel">
            Lead Email 2
<%--            <asp:RegularExpressionValidator ID="valRegLeadEmail2" runat="server" ControlToValidate="txtEmailLeads2"
                                                Display="Dynamic" ErrorMessage="Please enter a valid second Lead E-Mail address."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="valReqLeadEmail2" runat="server" ControlToValidate="txtLeadEmail2" Display="Dynamic"
                                                    ErrorMessage="Please enter the first Lead Email address." Enabled="False" ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtEmailLeads2" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="CellLabel">
            Format
        </td>
        <td>
            
            <GAABackOfficeControls:TwoValuesControl ID="tvcFormatOfLead" runat="server" FirstCaption="XML" SecondCaption="Text"  FirstElemntCompared="true" />
        </td>
        <td class="CellLabel">
            Format
        </td>
        <td>
           <GAABackOfficeControls:TwoValuesControl ID="tvcFormatOfLead2" runat="server" FirstCaption="XML" SecondCaption="Text"  FirstElemntCompared="true" />
        </td>
    </tr>
    <tr>
        <td class="CellLabel">
            Lead Email 3
            <%--<asp:RegularExpressionValidator ID="valRegLeadEmail3" runat="server" ControlToValidate="txtEmailLeads3"
                                                Display="Dynamic" ErrorMessage="Please enter a valid third Lead Email address."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtEmailLeads3" runat="server" Width="150px"></asp:TextBox>
            
        </td>
        <td class="CellLabel">
            Lead Email 4
<%--            <asp:RegularExpressionValidator ID="valRegLeadEmail4" runat="server" ControlToValidate="txtEmailLeads4"
                                                Display="Dynamic" ErrorMessage="Please enter a valid forth Lead E-Mail address."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtEmailLeads4" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="CellLabel">
            Format
        </td>
        <td>
            <GAABackOfficeControls:TwoValuesControl ID="tvcFormatOfLead3" runat="server" FirstCaption="XML" SecondCaption="Text"  FirstElemntCompared="true" />  
        </td>
        <td class="CellLabel">
            Format
        </td>
        <td>
            <GAABackOfficeControls:TwoValuesControl ID="tvcFormatOfLead4" runat="server" FirstCaption="XML" SecondCaption="Text"  FirstElemntCompared="true" />  
        </td>
    </tr>
    <tr ID="rowDealershipKey" runat="server">
        <td class="CellLabel">
            Dealership Key
        </td>
        <td colspan="3" style="padding-right:5px;">
            <asp:TextBox ID="txtDealershipKey" runat="server" Width="100%" Enabled="false"></asp:TextBox>
        </td>
    </tr>
    <tr ID="rowContactNameContactEmail" runat="server">
        <td class="CellLabel">
            Contact Name
        </td>
        <td>
            <asp:TextBox ID="txtContactName" runat="server" Width="150px"></asp:TextBox>
        </td>
        <td class="CellLabel">
            Contact Email
    <%--        <asp:RegularExpressionValidator ID="valRegLeadContactEmail" runat="server" ControlToValidate="txtContactEmail"
                                                Display="Dynamic" ErrorMessage="Please enter a valid Lead Contact E-mail address."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator
                                                    ID="valReqLeadContactEmail" runat="server" ControlToValidate="txtLeadContactEmail"
                                                    Display="Dynamic" ErrorMessage="Please enter a Lead Contact E-mail address."
                                                    Enabled="False" ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtContactEmail" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
    <tr ID="rowContactPhoneContactFax" runat="server">
        <td class="CellLabel">
            Contact Phone
<%--            <asp:RegularExpressionValidator ID="valRegLeadContactPhone" runat="server" ControlToValidate="txtContactPhone"
                                                Display="Dynamic" ErrorMessage="Please enter a Lead Contact Phone number. e.g. XXX-XXX-XXXX"
                                                ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$"
                                                ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="valReqLeadContactPhone"
                                                    runat="server" ControlToValidate="txtLeadContactPhone" Display="Dynamic" ErrorMessage="Please enter a Lead Contact Phone number."
                                                    Enabled="False" ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtContactPhone" runat="server" Width="150px"></asp:TextBox>
        </td>
        <td class="CellLabel">
            Contact Fax
<%--            <asp:RegularExpressionValidator ID="valRegLeadContactFax" runat="server" ControlToValidate="txtContactFax"
                                                Display="Dynamic" ErrorMessage="Please enter a Lead Contact Fax number. e.g. XXX-XXX-XXXX"
                                                ValidationExpression="^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$"
                                                ForeColor="White">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="valReqLeadContactFax"
                                                    runat="server" ControlToValidate="txtContactFax" Display="Dynamic" ErrorMessage="Please enter a Lead Contact Fax number."
                                                    Enabled="False" ForeColor="White">*</asp:RequiredFieldValidator>--%>
        </td>
        <td>
            <asp:TextBox ID="txtContactFax" runat="server" Width="150px"></asp:TextBox>
        </td>
    </tr>
   
</table>
