﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="ExceptionLog.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Pages.ExceptionLog" Theme="BackOfficeMainTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">

    <acw:SimpleGrid ID="sqMain" runat="server">
        <Grid>
            <Columns>
                <asp:BoundField DataField="ExceptionDate" HeaderText="Date" /> 
                <asp:BoundField DataField="Message" HeaderText="Message" />
                <asp:BoundField DataField="StackTrace" HeaderText="Stack Trace" ItemStyle-Width="200px" />
                <asp:BoundField DataField="Source" HeaderText="Source" />
            </Columns>
        </Grid>
    </acw:SimpleGrid>

</asp:Content>
