﻿using System;
using System.Web;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;
using GAABackOffice.BLL;

namespace Aculocity.GAABackOffice.UI.Web.Pages
{
    public partial class GetEmail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["AprId"] != string.Empty && Request.QueryString["type"] != string.Empty)
            {
                try
                {
                    tblError.Visible = false;

                    switch(Request.QueryString["type"])
                    {
                        case "lead":
                            string content = Email.GetBody(Convert.ToInt32(Request.QueryString["AprId"]), Request.QueryString["num"]);
                            if (!string.IsNullOrEmpty(content))
                            {
                                if (content.IndexOf("<?xml version=\"1.0\"?>") != -1)
                                {
                                    HttpContext.Current.Response.Clear();
                                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + Request.QueryString["AprId"] + ".xml");
                                    HttpContext.Current.Response.AddHeader("Content-Length", content.Length.ToString());
                                    HttpContext.Current.Response.ContentType = "application/octet-stream";
                                    HttpContext.Current.Response.Write(content);
                                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                                }
                                else
                                {
                                    Response.Clear();
                                    Response.AddHeader("Content-Disposition", "attachment; filename=" + Request.QueryString["AprId"] + ".txt");
                                    Response.AddHeader("Content-Length", content.Length.ToString());
                                    Response.ContentType = "application/octet-stream";
                                    Response.Write(content);
                                    Response.End();
                                }
                            }
                            else
                            {
                                string xmlContent = AppraiseXML.GetXML(Convert.ToInt32(Request.QueryString["AprId"]), Request.QueryString["DealerKey"]);
                                Response.Clear();
                                Response.AddHeader("Content-Disposition", "attachment; filename=" + Request.QueryString["AprId"] + ".xml");
                                Response.AddHeader("Content-Length", xmlContent.Length.ToString());
                                Response.ContentType = "application/octet-stream";
                                Response.Write(xmlContent);
                                Response.End();
                            }
                            break;
                        case "summary":
                            content = Email.GetBody(Convert.ToInt32(Request.QueryString["AprId"]), "summary");
                            if (!string.IsNullOrEmpty(content))
                            {
                                Response.Clear();
                                Response.Write(content);
                                Response.End();
                            }
                            else
                            {
                                tblError.Visible = true;
                            }
                            break;
                    }
                    
                    
                }
                catch (Exception ex)
                {
                    ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());
                    tblError.Visible = true;
                }
            }
        }
    }
}