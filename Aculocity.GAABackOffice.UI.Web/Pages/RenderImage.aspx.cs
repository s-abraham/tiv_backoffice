﻿using System;
using System.Drawing;
using System.IO;
using System.Web.UI;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.Pages
{
    public partial class RenderImage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            {
                if (!IsPostBack)
                {
                    DrawImage(300, 120);
                }
            }
        }
        #region ' Methods '
        private void DrawImage(int intDesiredWidth, int intDesiredHeight)
        {
            try
            {
                var stream = new MemoryStream();
                var image = DealerBLL.GetImageFile(Request.QueryString["DealerKey"], Convert.ToInt32(Request.QueryString["FileName"]));

                stream.Write(image, 0, image.Length);
                var bm = new Bitmap(stream);

                double newWidth = 0; double newHeight = 0;
                try
                {
                    if ((Request.QueryString["width"] != null)) intDesiredWidth = Convert.ToInt32(Request.QueryString["width"]);
                    if ((Request.QueryString["height"] != null)) intDesiredHeight = Convert.ToInt32(Request.QueryString["height"]);
                }
                catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }

                var targetRatio = intDesiredWidth / intDesiredHeight;
                var currentRatio = bm.PhysicalDimension.Width / bm.PhysicalDimension.Height;
                if (currentRatio > targetRatio)
                {
                    newWidth = intDesiredWidth;
                    newHeight = (newWidth / bm.Width) * bm.Height;
                }
                else
                {
                    newHeight = intDesiredHeight;
                    newWidth = (newHeight / bm.Height) * bm.Width;
                }

                var resized = new Bitmap((int)newWidth, (int)newHeight);
                Graphics g = Graphics.FromImage(resized);

                g.DrawImage(bm, new Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
                g.Dispose();

                Response.ContentType = "image/jpeg";
                resized.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
        #endregion
    }
}
