﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Pages.ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="headErrorPage" runat="server">
    <title>Error Page</title>
    <link href="~/CSS/Common.css" rel="stylesheet" type="text/css" />
</head>
<body class="errorBody">
    <form id="formErrorPage" runat="server">
    <div class="errorMessage">
    <table class="errorTable">
        <tr>
            <td>
                <img src="~/Images/Common/Error.png" alt=""/>
            </td>
            <td align="left">
                Sorry, it seems as though an error has occurred.<br />
                Please click on the Back button in your browser to return to the previous page.
            </td>
        </tr>
        <tr></tr>
    </table>
    </div>
    </form>
</body>
</html>
