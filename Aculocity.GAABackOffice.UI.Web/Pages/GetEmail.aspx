﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetEmail.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Pages.GetEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Page Unavailable</title>
</head>
<body>
    <table width="100%" cellspacing="0" cellpadding="15" border="1" bordercolor="gray" id="tblError" runat="server">
        <tr>
            <td width="20%">
                <p>
                    <img id="imgOops" runat="server" alt="Oops!" src="../Images/Oops/oops.jpg" /></p>
            </td>
            <td bgcolor="gray">
                <p>
                    <span style="font-size: 10pt; color: White; font-weight: bold">
                    The following error occurred:</span>
                </p>
                <ul>
                    <li><span style="font-size: 10pt; color: White; font-weight: bold">The lead file requested is not available.</span></li>
                </ul>
                <p>
                    <span style="font-size: 10pt; color: White; font-weight: bold">So what do I do now??</span>
                </p>
                <ul>
                    <li><span style="font-size: 10pt; color: White; font-weight: bold">Try opening the BackOffice login page and log in again.</span></li>
                    <li><span style="font-size: 10pt; color: White; font-weight: bold">If all else fails
                        please contact your dealership.</span></li>
                </ul>
            </td>
        </tr>
    </table>
</body>
</html>
