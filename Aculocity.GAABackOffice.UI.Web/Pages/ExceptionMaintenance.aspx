﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExceptionMaintenance.aspx.cs"
    Inherits="Aculocity.GAABackOffice.UI.Web.Pages.ExceptionMaintenance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GetAutoAppraise Exception Maintenance</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager" EnablePartialRendering="true" />
    <div>
        <table>
            <tr>
                <td>
                    <p>
                        <b>
                            <asp:Label ID="lblTitle" runat="server" Text="GetAutoAppraise Exception Maintenance"
                                Font-Size="Larger"></asp:Label></b></p>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                Exception Date Search:
                            </td>
                            <td>
                                <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="calFromDate" TargetControlID="txtStartDate" />
                            </td>
                            <td>
                                To:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="calToDate" TargetControlID="txtEndDate" />
                            </td>
                            <td>
                                <asp:Button ID="btnGetExceptions" runat="server" Text="Search" OnClick="btnGetExceptions_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan>
                    <asp:GridView ID="grdExceptions" runat="server">
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
