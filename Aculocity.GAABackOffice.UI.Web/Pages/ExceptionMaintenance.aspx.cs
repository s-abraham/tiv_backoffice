﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.Pages
{
    public partial class ExceptionMaintenance : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGetExceptions_Click(object sender, EventArgs e)
        {
            var dt = new DataTable();
            try
            {
                using (var conn = new SqlConnection(SettingManager.ConnectionStringToGalvesDb))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.Text;
                    if (!String.IsNullOrEmpty(txtStartDate.Text) && !String.IsNullOrEmpty(txtEndDate.Text))
                    {
                        cmd.CommandText = "Select * from ExceptionLog where ExceptionDate >= '" + txtStartDate.Text + "' AND ExceptionDate <= DATEADD(day, 1, '" + txtEndDate.Text + "') ORDER BY ExceptionDate DESC";
                    }
                    else
                    {
                        cmd.CommandText = "Select top 500 * from ExceptionLog ORDER BY ExceptionDate DESC";
                    }
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr != null) dt.Load(dr);
                    }
                    conn.Close();
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, 0); }

            grdExceptions.DataSource = dt;
            grdExceptions.DataBind();
        }
    }
}
