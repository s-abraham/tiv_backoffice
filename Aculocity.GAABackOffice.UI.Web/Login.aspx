﻿<%@ Page Title="" Language="C#" AutoEventWireup="True" CodeBehind="Login.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.Login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head" runat="server">
    <link rel="SHORTCUT ICON" href="http://www.getautoappraise.com/favicon.ico"/>
    <link href="CSS/Content.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Master.css" rel="stylesheet" type="text/css" />
    <title>TradeInVelocity Dealer Administration</title>
    <style type="text/css">
        .banner {
            border: 1px silver solid;
        }

        .loginForm {
            border: 1px #F5831F solid;
            width: 230px;
            height: 270px;
            font-size: 12px;
            font-family: Arial;
        }

        .loginForm table {
            margin: 10px;
            background: transparent;
        }

        .loginForm table .label {
            width:85px;
            text-align: left;
        }

        .loginButton {
            font-size: 12px;
            line-height: 17px;
            padding: 6px 0px 7px;
            width: 75px;
            border: 0;
            font-weight: bold;
            color: #666;
            background: url(Images/login-button.png) no-repeat;
        }

        .footerSection {
            height: 73px;
            width: 285px;
        }

        .footerSection h3 {
            margin-bottom: 4px;
            font-family: Arial;
            font-size: 16px;
            font-weight: bold;
            color: #666;
            text-decoration: none;
        }

        .footerSection img {
            float: left;
            width: 70px;
            height: 70px;
            margin-right: 7px;
            margin-bottom: 6px;
            border: 0;
        }
        .footerSection p {
            font-weight: lighter;
            margin-top: 4px;
            text-decoration: none;
        }

    </style>
</head>
<body>
    <form id="mainForm" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager" EnablePartialRendering="true" />
        <div id="container">
            <div id="header">
                <div class="Logo"><img src="Images/tivlogo.png" style="border-width:0px;"></div>
                <div id="ctl00_divLogin">
                    <div id="nav">
                        <div id="nav-menu">
                            <ul>
                                <li class="active"><a href="/Login.aspx">Dashboard</a></li>
                                <li><a href="http://www.tradeinvelocity.com/our-solutions/">Our Solutions</a></li>
                                <li><a href="http://www.tradeinvelocity.com/dealer-features/">Dealer Features</a></li>
                                <li><a href="http://www.tradeinvelocity.com/dealer-features/platform/">Tool Customization</a></li>
                                <li><a href="http://www.tradeinvelocity.com/contact-us/">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <asp:UpdatePanel ID="upLoginError" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlLoginError" runat="server" Visible="false" CssClass="pnlLoginError">
                            <asp:Label ID="lblLoginError" runat="server" />
                        </asp:Panel>                
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnLogin" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <table>
                <tr>
                    <td align="left">
                        <div class="banner"><img src="Images/banner.jpg" /></div>
                    </td>
                    <td align="left">
                        <div class="loginForm">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="2">
                                        <h1>Login</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        <strong>Username:&nbsp;</strong></td>
                                    <td>
                                        <asp:TextBox ID="txtUsr" runat="server" Width="125px" MaxLength="50"></asp:TextBox></td>
                                </tr>
                                <tr><td style="height:15px; line-height: 1px; font-size: 1px;" colspan="2"></td></tr>
                                <tr>
                                    <td class="label">
                                        <strong><span>Password:&nbsp;</span></strong>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" Width="125px" MaxLength="50"></asp:TextBox>&nbsp;
                                    </td>
                                </tr>
                                <tr><td style="height:15px; line-height: 1px; font-size: 1px;" colspan="2"></td></tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        <asp:CheckBox ID="chbSaveCredentials" runat="server" Text="Save Credentials"/>
                                    </td>
                                </tr>
                                <tr><td style="height:15px; line-height: 1px; font-size: 1px;" colspan="2"></td></tr>
                                <tr>
                                    <td colspan="2" align="right">
                                        <asp:Button ID="btnLogin" runat="server" OnClick="LoginControl_Authenticate" class="loginButton" Text="Login"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <span style="color: gray;" id="message" runat="server">&nbsp;</span></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="upContent" runat="server">
                        <ContentTemplate>
                            <asp:UpdateProgress ID="UPrgMain" runat="server">
                                <ProgressTemplate>
                                    <div class="div_transparentBackground"></div>
                                    <div align="center" class="div_updateProgress">
                                        <asp:Image ID="imgProgressImage" runat="server" ImageUrl="~/Images/progress.gif" />
                                        &nbsp;<b>Loading...</b>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnLogin" />
                        </Triggers>
                    </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0">
                <tbody>
                    <tr>
                        <td>
                            <div class="footerSection">
                                <a href="http://www.tradeinvelocity.com/our-solutions/howitworks/">
                                <img src="Images/login-page-icons/howitworks.png" />
                                <h3>How it Works</h3>
                                <p>Discover how our solution merges seamlessly with your internet marketing campaign.</p>
                                 </a>
                            </div>
                        </td>
                        <td>
                            <div class="footerSection">
                                <a href="http://www.tradeinvelocity.com/category/news/">
                                <img src="Images/login-page-icons/whatsnew.png" />
                                <h3>What's New</h3>
                                <p>Catch up on what's trending in the world of Trade-In Appraisal.</p>
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="footerSection">
                                <a href="http://www.tradeinvelocity.com/dealer-features/social-plus/">
                                <img src="Images/login-page-icons/social-plus.png" />
                                <h3>Social Plus</h3>
                                <p>Facebook Integration and Message Syndication.</p>
                                </a>
                            </div>

                        </td>
                    </tr>
                </tbody>

            </table>
        </div>
    </form>
</body>
</html>
    
