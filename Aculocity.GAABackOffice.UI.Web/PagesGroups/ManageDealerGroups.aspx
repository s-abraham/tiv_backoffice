﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="ManageDealerGroups.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesGroups.ManageDealerGroups" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
<script language="JavaScript">
    changeclass("ctl00_liManageDealerGroups");
 
    </script>
    <asp:UpdatePanel ID="upContent" runat="server">
        <ContentTemplate>
            <table cellpadding="5">
                <tr>
                    <td colspan = "3" align="center" valign="middle">
                        <asp:Label ID="lblFeedback" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblGroups" runat="server" CssClass="text" Text="Group"></asp:Label>
                        <asp:DropDownList ID="ddlDealerGroups" runat="server" CssClass="text" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlDealerGroups_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td>
                        <h2>Free Dealers</h2>
                        <asp:ListBox ID="lstFreeDealers" runat="server" Height="400px" Width="400px" SelectionMode="Multiple"></asp:ListBox>
                    </td>
                    <td align="center" valign="middle">
                        <table>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="BtnAdd" runat="server" ImageUrl="~/Images/Buttons/right.png" OnClick="btnAdd_Click"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="BtnDelete" runat="server" ImageUrl="~/Images/Buttons/left.png" OnClick="btnDelete_Click" />
                                </td>
                            </tr>
                        </table>      
                    </td>
                    <td>
                       <h2>Selected Dealers</h2>
                        <asp:ListBox ID="lstSelectedDealers" runat="server" Height="400px" Width="400px" SelectionMode="Multiple"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td colspan = "3" align="center" valign="middle">
                    <asp:Button id="btnBack" runat="server" Text="Back" OnClick="btnBack_Click"></asp:Button>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
