﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesGroups
{
    public partial class ManageDealerGroups : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    FillDealerGroups();
                    ddlDealerGroups.SelectedIndex = 0;
                    RefreshControls();
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region ' Controls 's Events '
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~//PagesDealer//ManageDealer.aspx");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnAdd_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                foreach(ListItem li in lstFreeDealers.Items)
                {
                    if (li.Selected)
                    {
                        DealerBLL.UpdateDealerGroup(Convert.ToInt32(ddlDealerGroups.SelectedValue), li.Value.Trim());
                    }
                }
                RefreshControls();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                foreach (ListItem li in lstSelectedDealers.Items)
                {
                    if (li.Selected)
                    {
                        DealerBLL.UpdateDealerGroup(0, li.Value.Trim());
                    }
                }
                RefreshControls();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void ddlDealerGroups_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillLstSelectedDealers(Convert.ToInt32(ddlDealerGroups.SelectedValue));
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #endregion

        #region ' Methods '
        private void FillDealerGroups()
        {
            try
            {
                var dealerGroups = new DealerGroups();
                dealerGroups.GetDealerGroupsByDealerAdmin(Security.GetGroupLoggedUser().ToString());

                ddlDealerGroups.Items.Clear();
                ddlDealerGroups.DataTextField = "Description";
                ddlDealerGroups.DataValueField = "ID";
                ddlDealerGroups.DataSource = dealerGroups;
                ddlDealerGroups.DataBind();
                ddlDealerGroups.Items.Insert(0, new ListItem() { Text = "None", Value = "0" });
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void RefreshControls()
        {
            try
            {
                FillLstSelectedDealers(Convert.ToInt32(ddlDealerGroups.SelectedValue));
                FillLstFreeDealers();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void FillLstSelectedDealers(int dealerGroupID)
        {
            try
            {
                var dealers = new Dealers();
                dealers.GetAllSelectedDealers(dealerGroupID);

                lstSelectedDealers.Items.Clear();
                lstSelectedDealers.DataTextField = "DealerName";
                lstSelectedDealers.DataValueField = "DealerKey";
                lstSelectedDealers.DataSource = dealers;
                lstSelectedDealers.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        private void FillLstFreeDealers()
        {
            try
            {
                var dealers = new Dealers();
                var dealerTypeID = (int)Security.GetDealerTypeLoggedUser(Security.GetGroupLoggedUser());
                dealers.GetAllFreeDealers(dealerTypeID);

                lstFreeDealers.Items.Clear();
                lstFreeDealers.DataTextField = "DealerName";
                lstFreeDealers.DataValueField = "DealerKey";
                lstFreeDealers.DataSource = dealers;
                lstFreeDealers.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion
    }
}