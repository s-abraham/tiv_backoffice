﻿<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="ManageGroups.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.PagesGroups.ManageGroups" Theme="BackOfficeMainTheme"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        table.GridBackOffice,
        table.GridBackOffice td,
        table.GridBackOffice th {
            border-color: #666;

        }
        table.GridBackOffice th {
            text-align: left;
            background-color: #CCC;
            color: #000;
        }
        table.GridBackOffice th,
        table.GridBackOffice td {
            text-align: left;
            padding: 6px 6px 5px 7px;
        }

        table.GridBackOffice td {
            font-weight: lighter;
            color: #333;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
    <script type="text/javascript" language="JavaScript">
     changeclass("ctl00_liManageGroups");
 
    </script>
    <table width="100%" >
        <tr>
            <td>
                <table width="100%" >
                    <tr>
                        <td>
                            <asp:ValidationSummary ID="vsRow" runat="server" CssClass="errorText" ValidationGroup="GridRowVal" />
                            <asp:ValidationSummary ID="nsFooter" runat="server" CssClass="errorText" ValidationGroup="GridFooterVal" />
                            <asp:Label ID="lblError" runat="server" CssClass="errorText" EnableViewState="False" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lblInfo" runat="server" CssClass="infoText" EnableViewState="False" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvGroups" runat="server" AllowPaging="True" AllowSorting="True"
                                            ShowFooter="True" AutoGenerateColumns="False" Width="100%"  
                                            AlternatingRowStyle-BackColor="WhiteSmoke" 
                                            CssClass="GridBackOffice"
                                            DataKeyNames="ID" PageSize="30"
                                            OnRowDeleting="gvGroups_RowDeleting"
                                            OnRowEditing="gvGroups_RowEditing"
                                            OnRowCancelingEdit="gvGroups_RowCancelingEdit"
                                            OnRowCommand="gvGroups_RowCommand"
                                            OnSorting="gvGroups_Sorting"
                                            OnPageIndexChanging="gvGroups_PageIndexChanging">
                                <HeaderStyle BackColor="#CCC" ForeColor="#000" HorizontalAlign="Left" CssClass="HeaderClass" />
                                <FooterStyle BackColor="WhiteSmoke" />
                                <AlternatingRowStyle BackColor="WhiteSmoke" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Group Name" SortExpression="Description" HeaderStyle-ForeColor="#000000">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("Description") %>'/>
                                            <asp:TextBox ID="txtGroupE" Text='<%# Bind("Description") %>' Visible="false" 
                                                         runat="server" ValidationGroup="GridRowVal"/>
                                            
                                            <asp:RequiredFieldValidator ID="reqGroupAdd" runat="server" ControlToValidate="txtGroupE"
                                                ErrorMessage="Please enter a group name" ValidationGroup="GridRowVal">*</asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtGroup" runat="server" ValidationGroup="GridFooterVal"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqGroupEdit" runat="server" ControlToValidate="txtGroup" 
                                                ErrorMessage="Please enter a group name" ValidationGroup="GridFooterVal">*</asp:RequiredFieldValidator>
                                        </FooterTemplate>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group Admin" SortExpression="DealerTypeName" HeaderStyle-ForeColor="#000000">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGroupAdminType" runat="server" Text = '<%# Bind("DealerTypeName")%>'/>
                                            <asp:Label ID="lblDealerTypeID" runat="server" Text = '<%# Bind("DealerTypeID")%>' Visible="false"/>
                                            <asp:DropDownList ID="ddlGroupAdminE" runat="server" Visible="false">
                                                <asp:ListItem Text="HOMENET" Value='4'></asp:ListItem>
                                                <asp:ListItem Text="GALVES" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="BSB" Value="6"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlGroupAdmin" runat="server">
                                                <asp:ListItem Text="Select Group Admin" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="HOMENET" Value='4'></asp:ListItem>
                                                <asp:ListItem Text="GALVES" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="BSB" Value="6"></asp:ListItem>
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tools">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit"  runat="server" CommandName="Edit" CommandArgument='<%# Bind("ID") %>'
                                                Text="Edit" CausesValidation="true" />&nbsp;
                                            <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                CommandArgument='<%# Bind("ID") %>' OnClientClick='return confirm("Are you sure you want to delete this group?");'
                                                Text="Delete" />
                                            <asp:LinkButton ID="lnlUpdateEdit" runat="server" CommandName="Edit_Update" Text="Update" Visible="false"
                                                CausesValidation="True" ValidationGroup="GridRowVal" />&nbsp;
                                            <asp:LinkButton ID="lnkCancelEdit" runat="server" CommandName="Cancel" Text="Cancel" Visible="false"
                                                CausesValidation="false" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:LinkButton ID="lnlUpdateAdd" runat="server" CommandName="Add_Update" Text="Insert"
                                                CausesValidation="true" ValidationGroup="GridFooterVal" />
                                        </FooterTemplate>
                                        <FooterStyle VerticalAlign="Top" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                                
                            </asp:GridView>
                               
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnBack" runat="server" Text="Back" OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
