﻿using System;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.BLL.Collections;
using Aculocity.GAABackOffice.BLL.Comparers;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web.PagesGroups
{
    public partial class ManageGroups : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Boolean isAdmin = Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.AllDealerAdmin;
                isAdmin = isAdmin || Security.GetGroupLoggedUser() == Enumerations.AuthUserGroup.BSBConsultingFullAdmin; 
                if (!IsPostBack) FillDealerGroupGrid();
                gvGroups.Columns[1].Visible = isAdmin;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        #region Properties
        public string SortDirection
        {
            get { return ViewState["SortDirection"] as string; }
            set { ViewState["SortDirection"] = value; }
        }
        public string SortColumn
        {
            get { return ViewState["SortColumn"] != null ? ViewState["SortColumn"].ToString() : "Description"; }
            set { ViewState["SortColumn"] = value; }
        }
        #endregion

        #region ' Grid Events '
        protected void gvGroups_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if(Page.IsValid)
                {
                    var selectedRow = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    var dealerGroup = new DealerGroupBLL();
                    switch (e.CommandName)
                    {
                        case "Edit_Update":
                            int id = Convert.ToInt32(gvGroups.DataKeys[selectedRow.RowIndex].Value);
                            string groupNameEdit = ((TextBox) gvGroups.Rows[selectedRow.RowIndex].Cells[0].FindControl("txtGroupE")).Text;
                            int dealerTypeIDEdit = 0;

                            switch(Security.GetGroupLoggedUser())
                            {
                                case Enumerations.AuthUserGroup.AllDealerAdmin:
                                    dealerTypeIDEdit = Convert.ToInt32(((DropDownList)gvGroups.Rows[selectedRow.RowIndex].Cells[0].FindControl("ddlGroupAdminE")).SelectedValue);
                                    break;
                                case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                                    dealerTypeIDEdit = (int)Enumerations.GroupDealerAdmin.BSB;
                                    break;
                                case Enumerations.AuthUserGroup.GalvesFullAdmin:
                                    dealerTypeIDEdit = (int)Enumerations.GroupDealerAdmin.Galves;
                                    break;
                                case Enumerations.AuthUserGroup.HomeNetSuperUser:
                                    dealerTypeIDEdit = (int)Enumerations.GroupDealerAdmin.HomeNet;
                                    break;
                            }

                            dealerGroup = new DealerGroupBLL { ID = id, Description = groupNameEdit, DealerTypeID = dealerTypeIDEdit };

                            if (dealerGroup.Update() < 0) lblError.Text = "<li>The group entered already exists.";
                            else
                            {
                                FillDealerGroupGrid();
                                lblInfo.Text = "<li>(Group updated successfully)";
                            }

                            break;
                        case "Add_Update":
                            string groupNameInsert = ((TextBox) gvGroups.FooterRow.FindControl("txtGroup")).Text;
                            int dealerTypeIDInsert = 0;

                            switch (Security.GetGroupLoggedUser())
                            {
                                case Enumerations.AuthUserGroup.AllDealerAdmin:
                                    dealerTypeIDInsert = Convert.ToInt32(((DropDownList)gvGroups.FooterRow.FindControl("ddlGroupAdmin")).SelectedValue);
                                    break;
                                case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                                    dealerTypeIDInsert = (int)Enumerations.GroupDealerAdmin.BSB;
                                    break;
                                case Enumerations.AuthUserGroup.GalvesFullAdmin:
                                    dealerTypeIDInsert = (int)Enumerations.GroupDealerAdmin.Galves;
                                    break;
                                case Enumerations.AuthUserGroup.HomeNetSuperUser:
                                    dealerTypeIDInsert = (int)Enumerations.GroupDealerAdmin.HomeNet;
                                    break;
                            }

                            if (groupNameInsert.Trim() == string.Empty) lblError.Text = "<li>Please enter a group name.";
                            else if (dealerTypeIDInsert == 0) lblError.Text = "<li>Please select a group admin.";
                            else
                            {
                                dealerGroup = new DealerGroupBLL { Description = groupNameInsert, DealerTypeID = dealerTypeIDInsert };
                                if (dealerGroup.Insert() < 0) lblError.Text = "<li>The group entered already exists.";
                                else
                                {
                                    FillDealerGroupGrid();
                                    lblInfo.Text = "<li>(Group inserted successfully)";
                                }
                            }
                            break;
                    }
                }
            }
            catch (Exception ex){ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID());}
        }

        protected void gvGroups_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                string groupName = ((Label)gvGroups.Rows[e.NewEditIndex].Cells[0].FindControl("lblGroup")).Text;
                ((TextBox)gvGroups.Rows[e.NewEditIndex].Cells[0].FindControl("txtGroupE")).Text = groupName;

                //refresh all controls
                foreach (GridViewRow row in gvGroups.Rows) { ShowHideControls(row.RowIndex, false); }
                //visible controls for updated row only
                bool isVisible = (gvGroups.Rows[e.NewEditIndex].Cells[2].FindControl("lnkEdit")).Visible;
                ShowHideControls(e.NewEditIndex, isVisible);

                //set selected value for dealer type
                string groupID = ((Label)gvGroups.Rows[e.NewEditIndex].Cells[1].FindControl("lblDealerTypeID")).Text;
                var ddlGroupAdminE = ((DropDownList)gvGroups.Rows[e.NewEditIndex].Cells[1].FindControl("ddlGroupAdminE"));
                if (!groupID.Trim().Equals("0")) ddlGroupAdminE.SelectedValue = groupID;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvGroups_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                bool isVisible = (gvGroups.Rows[e.RowIndex].Cells[2].FindControl("lnkEdit")).Visible;
                ShowHideControls(e.RowIndex, isVisible);
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvGroups_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(gvGroups.DataKeys[e.RowIndex].Value);
                var dealerGroup = new DealerGroupBLL() { ID = id };
                if (dealerGroup.Delete() > 0) lblInfo.Text = "<li>(Group deleted successfully)";
                //refill Grid
                FillDealerGroupGrid();
            }
            catch (Exception ex) {ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        protected void gvGroups_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                if (SortColumn.ToUpper() == e.SortExpression.ToUpper() && SortDirection == "ASC")
                { SortDirection = "DESC"; }
                else { SortDirection = "ASC"; }
                //set sort column
                SortColumn = e.SortExpression;

                var delerGroups = GetDealerGroups(new DealerGroups());
                delerGroups = SortDealerGroups(delerGroups);
                gvGroups.DataSource = delerGroups;
                gvGroups.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected void gvGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                var delerGroups = GetDealerGroups(new DealerGroups());
                delerGroups = SortDealerGroups(delerGroups);
                gvGroups.DataSource = delerGroups;
                gvGroups.PageIndex = e.NewPageIndex;
                gvGroups.DataBind();
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
        #endregion

        #region ' Methods '
        private void FillDealerGroupGrid()
        {
            try
            {
                var delerGroups = GetDealerGroups(new DealerGroups());
                if (delerGroups.Count > 0)
                {
                    delerGroups = SortDealerGroups(delerGroups);
                    gvGroups.DataSource = delerGroups;
                    gvGroups.DataBind();

                    foreach (GridViewRow row in gvGroups.Rows)
                    {
                        ShowHideControls(row.RowIndex, false);
                    }
                }
                else
                {
                    delerGroups.Add(new DealerGroupBLL());
                    gvGroups.DataSource = delerGroups;
                    gvGroups.DataBind();

                    int colCount = gvGroups.Rows[0].Cells.Count;
                    gvGroups.Rows[0].Cells.Clear();
                    gvGroups.Rows[0].Cells.Add(new TableCell());
                    gvGroups.Rows[0].Cells[0].ColumnSpan = colCount;
                    gvGroups.Rows[0].Cells[0].Text = "No data found.";
                }

                
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        protected DealerGroups GetDealerGroups(DealerGroups delerGroups)
        {
            try
            {
                delerGroups.GetDealerGroupsByDealerAdmin(Security.GetCurrentUserGroup().ToString());
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return delerGroups;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/PagesDealer/ManageDealer.aspx");
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }
    
        private void ShowHideControls(int rowIndex, bool isVisible)
        {
            try
            {
                //cell[0]
                (gvGroups.Rows[rowIndex].Cells[0].FindControl("lblGroup")).Visible = !isVisible;
                (gvGroups.Rows[rowIndex].Cells[0].FindControl("txtGroupE")).Visible = isVisible;
                //cell[1]
                (gvGroups.Rows[rowIndex].Cells[1].FindControl("lblGroupAdminType")).Visible = !isVisible;
                (gvGroups.Rows[rowIndex].Cells[1].FindControl("ddlGroupAdminE")).Visible = isVisible;
                //cell[2]
                (gvGroups.Rows[rowIndex].Cells[2].FindControl("lnkEdit")).Visible = !isVisible;
                (gvGroups.Rows[rowIndex].Cells[2].FindControl("lnkDelete")).Visible = !isVisible;
                (gvGroups.Rows[rowIndex].Cells[2].FindControl("lnlUpdateEdit")).Visible = isVisible;
                (gvGroups.Rows[rowIndex].Cells[2].FindControl("lnkCancelEdit")).Visible = isVisible;
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

        private DealerGroups SortDealerGroups(DealerGroups usersObj)
        {
            try
            {
                var type = (Enumerations.DealerGroupComparisonType)Enum.Parse(typeof(Enumerations.DealerGroupComparisonType), SortColumn);
                if (SortDirection == "DESC") usersObj.Sort(new DealerGroupComparerReverse(type));
                else usersObj.Sort(new DealerGroupComparer(type));

            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
            return usersObj;
        }
        #endregion
    }
}