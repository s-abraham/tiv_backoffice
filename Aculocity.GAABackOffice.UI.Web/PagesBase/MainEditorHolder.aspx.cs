﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web {

    public partial class MainEditorHolder : MainEditorHolderPageBase {

        bool _simpleUser;
        int _currentUserId;
        Enumerations.AuthUserGroup _currentGroup;

        public Button UpdateButton {
            get {
                return btnUpdate;
            }
        }

        public override bool RedirectAfterSaving {
            get {
                return false;
            }
        }

        protected override void OnInit(EventArgs e) {
            base.OnInit(e);

            _currentUserId = Security.GetLoggedUserID();
            _currentGroup = Security.GetCurrentUserGroup();

            btnBackA.Click += new EventHandler(btnBackA_Click);
            btnBackB.Click += new EventHandler(btnBackA_Click);

            btnUpdate.Click += new EventHandler(btnUpdate_Click);
            btnDeactivate.Click += new EventHandler(btnDeactivate_Click);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ApplySecurity();
        }

        public override Control EditorHolderControl {
            get {
                return divMainContent;
            }
        }

        public override void SetMessage(string message, bool error) {

            spanStatusInfoA.InnerText = message;
            spanStatusInfoB.InnerText = message;

            string color = error ? "Red" : "Green";

            spanStatusInfoA.Style["color"] = color;
            spanStatusInfoB.Style["color"] = color;

            

        }

        void btnBackA_Click(object sender, EventArgs e) 
        {
            //RedirectToParent();
            if (_currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin)
            {
                Response.Redirect("~/PagesDealer/ManageDealerForSpecificDealer.aspx", false);
            }
            else
            {
                Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
            }
        }

        void btnDeactivate_Click(object sender, EventArgs e) 
        {
                        
        }

        void btnUpdate_Click(object sender, EventArgs e) 
        {
            Save();
        }

        void ApplySecurity()
        {

            _simpleUser = _currentGroup == Enumerations.AuthUserGroup.GalvesDealerAdmin         ||
                          _currentGroup == Enumerations.AuthUserGroup.SpecificDealerAdmin       || 
                          _currentGroup == Enumerations.AuthUserGroup.HomeNetDealerAdmin        ||
                          _currentGroup == Enumerations.AuthUserGroup.ResellerAdmin ||
                          _currentGroup == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin;
            
            //btnUpdate.Visible = !_simpleUser;

            if(_simpleUser)
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "changeclass", "changeclass('ctl00_liEditDealer')", true);
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "changeclass", "changeclass('ctl00_liAddDealear')", true);
            }
        }

    }
}
