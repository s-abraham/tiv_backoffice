﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Aculocity.Controls.Web;

namespace Aculocity.GAABackOffice.UI.Web {
    
    public abstract class MainEditorHolderPageBase : EditorHolderPageBase {

        public override string DataManagerName {
            get {
                return "MainDataManager";
            }
        }

    }

}
