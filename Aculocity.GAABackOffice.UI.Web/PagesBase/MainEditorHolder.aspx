<%@ Page Title="" Language="C#" MasterPageFile="~/GlobalMaster.Master" AutoEventWireup="true" CodeBehind="MainEditorHolder.aspx.cs" Inherits="Aculocity.GAABackOffice.UI.Web.MainEditorHolder" Theme="BackOfficeMainTheme"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContent" runat="server">
<%--<script id="jsScript" runat="server" language="JavaScript">
    changeclass("ctl00_liAddDealear");
</script>--%>

<table border="0" cellpadding="0" cellspacing="5" >
    <tr>
        <td align="right">
            <asp:Button ID="btnBackA" runat="server" Text="Back" />
        </td>
    </tr>
    <tr>
        <td align="center">
            <span ID="spanStatusInfoA" runat="server" ></span>
        </td>
    </tr>
    <tr>
        <td>
            <asp:PlaceHolder ID="divMainContent" runat="server">
            </asp:PlaceHolder>
            <asp:Label ID="lblStatus" runat="server" Text="" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center">
            <span ID="spanStatusInfoB" runat="server" ></span>
        </td>
    </tr>
    <tr style="float:right;">
        <td align="right">
            <table border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tr>
                    <td ID="rowAdditionalHolder" runat="server" Visible="false">
                        <asp:Button ID="btnDeactivate" runat="server" Text="Deactivate" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnUpdate" runat="server" Text="Save Changes"/>        
                    </td>
                    <td align="right" style="width:25px">
                        <asp:Button ID="btnBackB" runat="server" align="right" Text="Back"/>
                    </td>        
                </tr>
            </table> 
        </td>
    </tr>
</table>

</asp:Content>
