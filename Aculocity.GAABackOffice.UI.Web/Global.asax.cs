﻿using System;
//using Aculocity.GAABackOffice.DAL.Commands;
//using Aculocity.GAABackOffice.DAL.Engine;
using Aculocity.GAABackOffice.Global;
using Aculocity.SimpleData;
using Aculocity.Controls.Web;

using Aculocity.GAABackOffice.DAL;

namespace Aculocity.GAABackOffice.UI.Web
{
    public class Global : System.Web.HttpApplication {

        void Application_Start(object sender, EventArgs e) {

            DataManagerService.RegisterDataManagerService("MainDataManager", System.Configuration.ConfigurationManager.ConnectionStrings["GAABackOffice.GalvesDB"].ConnectionString);
            DALMainEngine.Init();

            EntityEditorBinding.Init();
        }

        void Application_Error(object sender, EventArgs e) {
            Exception exception = Server.GetLastError();
            SystemCommands.HN_LogException(DataManagerService.GetDataManager("MainDataManager"), exception.Message, exception.StackTrace, exception.Source);
        }
               
        protected void Session_Start(object sender, EventArgs e) {
            Session.Timeout = Convert.ToInt32(SettingManager.LoginTimeout);
        }
    }
}