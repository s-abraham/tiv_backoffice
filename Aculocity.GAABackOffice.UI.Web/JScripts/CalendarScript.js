// JScript File

var ns6	= document.getElementById&&!document.all

function showCalendar(imageId, calID)
{
    image = document.getElementById(imageId);
    cal = document.getElementById(calID);
    
    cal.style.top = GetTagPixels(image, "TOP") + "px";
    cal.style.left = GetTagPixels(image, "LEFT") + "px";
    cal.style.visible = "visible";
    cal.style.position = "absolute";
    cal.style.display = "block";
}

function showCalendar2(imageId, calID, Row, txtID)
{
    image = document.getElementById(imageId);
    cal = document.getElementById(calID);
    
    cal.style.top = GetTagPixels(image, "TOP") + "px";
    cal.style.left = GetTagPixels(image, "LEFT") + "px";
    cal.style.visible = "visible";
    cal.style.display = "block";
    
    txt = document.getElementById(txtID);
    txt.value = Row;
}

function hideCalendar(calID, e)
{
    var obj 
    var className = "";
    
    try
    {
        obj = ns6 ? e.target : event.srcElement;
    }
    catch(err)
    {
        cal = document.getElementById(calID);
        cal.style.visible = "hidden";
        cal.style.display = "none";
    }
    
    try
    {
        className = obj.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.className;
    }
    catch(err)
    {
        
    }
    
    try
    {
        if (obj == null) {
            cal = document.getElementById(calID);
            cal.style.visible = "hidden";
            cal.style.display = "none";
        }
        else if (!((obj.parentNode.className) == "MonthSelect") && !(className == "calendar"))
        {
            cal = document.getElementById(calID);
            cal.style.visible = "hidden";
            cal.style.display = "none";
        } 
        
        if (className == "calendar")
        {
            setTimeout("cal = document.getElementById('" + calID +"'); cal.style.visible = 'hidden'; cal.style.display = 'none';", 200);
        }
    }
    catch(err){}
}

//get pixels from TOP or LEFT
function GetTagPixels(StartTag, Direction) {
   var PixelAmt = (Direction == 'LEFT') ? StartTag.offsetLeft : StartTag.offsetTop;
   while ((StartTag.tagName != 'BODY') && (StartTag.tagName != 'HTML')) {
      StartTag = StartTag.offsetParent;
      PixelAmt += (Direction == 'LEFT') ? StartTag.offsetLeft : StartTag.offsetTop;
   }
   return PixelAmt;
}

//function addClickEvent(func) {
//    $addHandler(document, "mousedown", func);
//}

function addClickEvent(func) {
  var oldonclick = document.onmouseup;
  if (typeof document.onmouseup != 'function') {
    document.onmouseup = func;
  } else {
    document.onmouseup = function() {
      if (oldonclick) {
        oldonclick();
      }
      func();
    }
  }
}
