﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web
{
    public partial class GetAutoAppraise : MasterPage, ICallbackEventHandler
    {
        string _dealerKey;
        protected void Page_Init(object sender, EventArgs e)
        {
            var link = new HtmlLink { Href = "~/CSS/Master.css" };
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Head.Controls.Add(link);

            link = new HtmlLink { Href = "~/CSS/rblTabs.css" };
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Head.Controls.Add(link);

            link = new HtmlLink { Href = "http://www.getautoappraise.com/favicon.ico" };
            link.Attributes.Add("rel", "SHORTCUT ICON");
            Head.Controls.Add(link);

            link = new HtmlLink { Href = "~/CSS/uniform/themes/tiv/uniform.tiv.css" };
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Head.Controls.Add(link);


            link = new HtmlLink { Href = "~/CSS/selectbox/tiv/jquery.selectBox.css" };
            link.Attributes.Add("rel", "stylesheet");
            link.Attributes.Add("type", "text/css");
            Head.Controls.Add(link);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
            SetUserDeatails();
            CreateCallServerFunction();

            //Set Dealer Key
            var user = new UserBLL();
            user.GetUserByUserName(Security.GetLoggedInUser().Trim());
            _dealerKey = user.DealerKey;
        }

        #region ' Main Menu's Methods'

        protected void lnkDashboard_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesDealer/Dashboard.aspx", false);
        }

        //Manual links for the edit dealer tabs
        protected void lnkDA_DealerInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx?DealerKey={0}&acttab=0", _dealerKey), false);
        }

        protected void lnkDA_ValueRange_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx?DealerKey={0}&acttab=1", _dealerKey), false);
        }

        protected void lnkDA_ToolCustomization_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx?DealerKey={0}&acttab=2", _dealerKey), false);
        }

        protected void lnkDA_FacebookApp_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx?DealerKey={0}&acttab=5", _dealerKey), false);
        }
                            
        protected void lnkManageDealers_Click(object sender, EventArgs e) {
            Response.Redirect("~/PagesDealer/ManageDealer.aspx", false);
        }

        protected void lnkBlackListedEmails_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesDealer/BlackLIstedEmails.aspx", false);
        }

        protected void lnkDraftDealers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesDealer/DraftDealers.aspx", false);
        }

        protected void lnkAddDealear_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx"), false);
            //Response.Redirect(string.Format("~/PagesBase/MainEditorHolder.aspx?BindingName=DealerTabHolder&ParentPath={0}",Server.UrlEncode("~//PagesDealer//ManageDealer.aspx"), false));
        }
        protected void lnkLeadsReports_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesReport/LeadReport.aspx", false);
        }
        protected void BillableDealers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesReport/BillableDealersReport.aspx", false);
        }
        protected void lnkManageUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesUsers/ManageUsers.aspx", false);
        }
        protected void lnkManageGroups_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesGroups/ManageGroups.aspx", false);
        }
        protected void lnkManageDealerGroups_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesGroups/ManageDealerGroups.aspx", false);
        }
        protected void lnkAddVideo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/PagesDealer/ManageVideo.aspx", false);
        }
        protected void lnkExceptionLog_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/ExceptionLog.aspx", false);
        }
        protected void lnkEditDealer_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/DealerAdmin.aspx?DealerKey={0}", _dealerKey), false);
            //Response.Redirect(string.Format("~/PagesBase/MainEditorHolder.aspx?BindingName=DealerTabHolder&DealerKey={0}&ParentPath=~//PagesDealer//ManageDealer.aspx", _dealerKey), false);
        }
        protected void lnkManageImages_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/UploadImages.aspx?DealerKey={0}", _dealerKey), false);
        }
        protected void lnkSpecialOfferText_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/SpecialOfferText.aspx?DealerKey={0}", _dealerKey), false);
        }
        protected void lnkHeaderText_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/PagesDealer/HeaderText.aspx?DealerKey={0}", _dealerKey), false);
        }
        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Security.PerformLogout();
            Response.Redirect("~/Login.aspx", false);
        }

        #endregion

        #region ' ICallbackEventHandler's Methods '
        public void RaiseCallbackEvent(string eventArgument)
        {
            string[] jsError = eventArgument.Split(new[] { "@@@***@@@" }, StringSplitOptions.None);
            var exJS = new ExceptionJS(jsError[0], jsError[1], Convert.ToInt32(jsError[2]));
            ExceptionHandler.HandleException(ref exJS, Security.GetLoggedUserID());
        }

        public string GetCallbackResult()
        {
            return string.Empty;
        }
        #endregion

        #region ' Functions '
        private void SetUserDeatails()
        {
            string loggedInUser = Security.GetLoggedInUser();

            lblUsername.Text = string.Empty;
            if (loggedInUser == "Guest")
            {
                if (Request.FilePath.IndexOf("Login.aspx") < 0)
                {
                    Session.Abandon();
                    Security.PerformLogout();
                    Response.Redirect("~/Login.aspx");
                    return;
                }
            }
            else
            {
                lblUsername.Text = string.Format("[{0}]", loggedInUser);
                DefineMainMenu(Security.GetGroupLoggedUser());
            }


            //liExceptionLog

        }

        private void CreateCallServerFunction()
        {
            ClientScriptManager cm = Page.ClientScript;
            string cbReference = cm.GetCallbackEventReference(this, "arg", "ReceiveServerData", "");
            string callbackScript = string.Empty;
            callbackScript += string.Format("function CallServer(arg, context){{{0};}}", cbReference);
            cm.RegisterClientScriptBlock(GetType(), "CallServer", callbackScript, true);
        }
        #endregion

        #region ' Methods '
        private void DefineMainMenu(Enumerations.AuthUserGroup group)
        {
            switch (group)
            {
                case Enumerations.AuthUserGroup.AllDealerAdmin:
                    liManageDealers.Visible = true;
                    liAddDealear.Visible = true;
                    liLeadsReports.Visible = true;
                    liBillableDealers.Visible = true;
                    liManageUsers.Visible = true;
                    liManageGroups.Visible = true;
                    liManageDealerGroups.Visible = true;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = true;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = true;

                    liDA_DealerInfo.Visible = false;
                    liDA_ValueRange.Visible = false;
                    liDA_ToolCustomization.Visible = false;
                    liDA_FacebookApp.Visible = false;
                    liEditDealer.Visible = false;

                    liManageImages.Visible = false;
                    liSpecialOfferText.Visible = false;
                    liHeaderText.Visible = false;
                    break;

                case Enumerations.AuthUserGroup.HomeNetSuperUser:
                    liManageDealers.Visible = true;
                    liAddDealear.Visible = true;
                    liLeadsReports.Visible = true;
                    liBillableDealers.Visible = true;
                    liManageUsers.Visible = true;
                    liManageGroups.Visible = true;
                    liManageDealerGroups.Visible = true;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = false;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = false;

                    liDA_DealerInfo.Visible = false;
                    liDA_ValueRange.Visible = false;
                    liDA_ToolCustomization.Visible = false;
                    liDA_FacebookApp.Visible = false;
                    liEditDealer.Visible = false;

                    liManageImages.Visible = false;
                    liSpecialOfferText.Visible = false;
                    liHeaderText.Visible = false;
                    break;

                case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                    liManageDealers.Visible = true;
                    liAddDealear.Visible = true;
                    liLeadsReports.Visible = true;
                    liBillableDealers.Visible = true;
                    liManageUsers.Visible = true;
                    liManageGroups.Visible = true;
                    liManageDealerGroups.Visible = true;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = false;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = true;

                    liDA_DealerInfo.Visible = false;
                    liDA_ValueRange.Visible = false;
                    liDA_ToolCustomization.Visible = false;
                    liDA_FacebookApp.Visible = false;
                    liEditDealer.Visible = false;

                    liManageImages.Visible = false;
                    liSpecialOfferText.Visible = false;
                    liHeaderText.Visible = false;
                    break;

                case Enumerations.AuthUserGroup.GalvesFullAdmin:
                    liManageDealers.Visible = true;
                    liAddDealear.Visible = true;
                    liLeadsReports.Visible = true;
                    liBillableDealers.Visible = true;
                    liManageUsers.Visible = true;
                    liManageGroups.Visible = true;
                    liManageDealerGroups.Visible = true;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = false;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = false;

                    liDA_DealerInfo.Visible = false;
                    liDA_ValueRange.Visible = false;
                    liDA_ToolCustomization.Visible = false;
                    liDA_FacebookApp.Visible = false;
                    liEditDealer.Visible = false;

                    liManageImages.Visible = false;
                    liSpecialOfferText.Visible = false;
                    liHeaderText.Visible = false;
                    break;

                case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                case Enumerations.AuthUserGroup.ResellerAdmin:
                    liManageDealers.Visible = false;
                    liAddDealear.Visible = false;

                    if (group == Enumerations.AuthUserGroup.BSBConsultingDealerAdmin)
                        liLeadsReports.Visible = false;
                    else
                        liLeadsReports.Visible = true;

                    liBillableDealers.Visible = false;
                    liManageUsers.Visible = false;
                    liManageGroups.Visible = false;
                    liManageDealerGroups.Visible = false;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = false;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = false;

                    liDA_DealerInfo.Visible = true;
                    liDA_ValueRange.Visible = true;
                    liDA_ToolCustomization.Visible = true;
                    liDA_FacebookApp.Visible = true;
                    //Old Edit Dealer
                    liEditDealer.Visible = false;

                    liManageImages.Visible = true;
                    liSpecialOfferText.Visible = true;
                    liHeaderText.Visible = false;
                    break;

                case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                    liManageDealers.Visible = false;
                    liAddDealear.Visible = false;
                    liLeadsReports.Visible = false;
                    liBillableDealers.Visible = false;
                    liManageUsers.Visible = false;
                    liManageGroups.Visible = false;
                    liManageDealerGroups.Visible = false;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = false;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = false;

                    liDA_DealerInfo.Visible = true;
                    liDA_ValueRange.Visible = true;
                    liDA_ToolCustomization.Visible = true;
                    liDA_FacebookApp.Visible = true;
                    //Old Edit Dealer
                    liEditDealer.Visible = false;

                    liManageImages.Visible = true;
                    liSpecialOfferText.Visible = true;
                    liHeaderText.Visible = false;
                    break;

                case Enumerations.AuthUserGroup.HomeNetDealerAdmin:
                    liManageDealers.Visible = false;
                    liAddDealear.Visible = false;
                    liLeadsReports.Visible = false;
                    liBillableDealers.Visible = false;
                    liManageUsers.Visible = false;
                    liManageGroups.Visible = false;
                    liManageDealerGroups.Visible = false;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = false;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = false;

                    liDA_DealerInfo.Visible = true;
                    liDA_ValueRange.Visible = true;
                    liDA_ToolCustomization.Visible = true;
                    liDA_FacebookApp.Visible = true;
                    //Old Edit Dealer
                    liEditDealer.Visible = false;

                    liManageImages.Visible = true;
                    liSpecialOfferText.Visible = true;
                    liHeaderText.Visible = false;
                    break;

                case Enumerations.AuthUserGroup.GroupDealerAdmin:
                    liManageDealers.Visible = false;
                    liAddDealear.Visible = false;
                    liLeadsReports.Visible = true;
                    liBillableDealers.Visible = false;
                    liManageUsers.Visible = false;
                    liManageGroups.Visible = false;
                    liManageDealerGroups.Visible = false;
                    liAddVideo.Visible = false;
                    liExceptionLog.Visible = false;
                    liDraftDealers.Visible = false;
                    liBlacklistedEmails.Visible = false;

                    liDA_DealerInfo.Visible = false;
                    liDA_ValueRange.Visible = false;
                    liDA_ToolCustomization.Visible = false;
                    liDA_FacebookApp.Visible = false;
                    liEditDealer.Visible = false;

                    liManageImages.Visible = false;
                    liSpecialOfferText.Visible = false;
                    liHeaderText.Visible = false;
                    break;
            }

            liHeaderText.Visible = DealerHeaderTextBLL.IsAllowedCompanyDealer(Security.GetLoggedUserID());
        }
        #endregion
    }
}
