﻿using System;
using System.Web.UI;
using Aculocity.GAABackOffice.BLL;
using Aculocity.GAABackOffice.Global;

namespace Aculocity.GAABackOffice.UI.Web
{
    public class BasePage : Page
    {
        protected Enumerations.AuthUserGroup _currentGroup;

        protected override void OnPreInit(EventArgs e)
        {
            try
            {
                _currentGroup = Security.GetCurrentUserGroup();

                switch (_currentGroup)
                {
                    case Enumerations.AuthUserGroup.AllDealerAdmin:
                        MasterPageFile = "~/GlobalMaster.Master";
                        break;
                    case Enumerations.AuthUserGroup.BSBConsultingDealerAdmin:
                    case Enumerations.AuthUserGroup.ResellerAdmin:
                        MasterPageFile = "~/GetAutoAppraise.master";
                        break;
                    case Enumerations.AuthUserGroup.BSBConsultingFullAdmin:
                        MasterPageFile = "~/GetAutoAppraise.master";
                        break;
                    case Enumerations.AuthUserGroup.GalvesDealerAdmin:
                        MasterPageFile = "~/Galves.Master";
                        break;
                    case Enumerations.AuthUserGroup.GalvesFullAdmin:
                        MasterPageFile = "~/Galves.Master";
                        break;
                    case Enumerations.AuthUserGroup.HomeNetDealerAdmin:
                        MasterPageFile = "~/HomeNet.Master";
                        break;
                    case Enumerations.AuthUserGroup.HomeNetSuperUser:
                        MasterPageFile = "~/HomeNet.Master";
                        break;
                    case Enumerations.AuthUserGroup.GroupDealerAdmin:
                        if (Convert.ToBoolean(Session["HomeNet"].ToString()) == true)
                        {
                            MasterPageFile = "~/HomeNet.Master"; 
                        }
                        else
                        {
                            if (Convert.ToBoolean(Session["Galves"].ToString()) == true)
                            {
                                MasterPageFile = "~/Galves.Master";
                            }
                            else if (Convert.ToBoolean(Session["BSB"].ToString()) == true)
                            {
                                MasterPageFile = "~/GetAutoAppraise.Master";
                            }
                            else
                            {
                                MasterPageFile = "~/GlobalMaster.Master";
                            }
                        }
                        break;
                }
            }
            catch (Exception ex) { ExceptionHandler.HandleException(ref ex, Security.GetLoggedUserID()); }
        }

    }
}
