﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using Aculocity.SimpleData;

using Aculocity.GAABackOffice.DAL;
using Aculocity.GAABackOffice.BLL;


namespace Aculocity.GAABackOffice.UI.Web {
    public class UtilityFunctions {

        public static bool CheckIsAllowedCompanyDealer(DataManager dataManager) {

            int userId = Security.GetLoggedUserID();

            DataTable table = DealerCommands.GetAllowedCompanyDealers(dataManager).ExecuteDataTable();
            table.PrimaryKey = new DataColumn[] { table.Columns[0] };

            return table.Rows.Contains(userId);

        }

    }
}
